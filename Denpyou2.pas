unit Denpyou2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Denpyou, Grids, ComCtrls, StdCtrls, Buttons, ExtCtrls, Db, DBTables;

type
  TfrmDenpyou2 = class(TfrmDenpyou)
    lDate: TLabel;
    lTokuisakiName: TLabel;
    lNyuuryokusha: TLabel;
    dabDenpyou: TDatabase;
    lGatsubun: TLabel;
    BitBtn8: TBitBtn;
    edbDenpyouBanngou: TEdit;
    lblTeisei: TLabel;
    edbBikou: TEdit;
    lblBIkou: TLabel;
    Query1: TQuery;
    Label21: TLabel;
    lblTorikeshi: TLabel;
    Label23: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure SG1DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure BitBtn8Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private 宣言 }
		procedure MakeSG;
    Function InsertDenpyou       : Boolean;
    Function InsertDenpyouDetail : Boolean;
    Function GetDenpyouBanngou   : Boolean;
    Function UpdateItem2         : Boolean;
    Function UpdateDenpyou       : Boolean;
		Function CheckUriageKingaku(sDenpyouNo:String):Integer;
  public
    { Public 宣言 }
  end;

  const
	//グリッドの列の定義

  CintSeq           = 0;
  CintNum           = 1;
  CintItemCode1     = 2;
  CintItemCode2     = 3;
  CintItemName      = 4;
  CintItemTax       = 5;
  CintItemYomi      = 6;
  CintItemKikaku    = 7;
  CintItemIrisuu    = 8;
  CintItemCount     = 9;
  CintItemTanni     = 10;
  CintItemTanka     = 11;
  CintItemShoukei   = 12;
  CintItemHindo     = 13;
  CintItemSaishuu   = 14;
  CintEnd           = 15;   //ターミネーター

var
  frmDenpyou2 : TfrmDenpyou2;
  curSum      : Currency;
  curTax      : Currency;
  gDenpyouNo  : String;    // フォーム内で使い回す伝票番号の変数

implementation
uses
 Inter, DenpyouPrint, HKLib, Variants;

{$R *.DFM}

procedure TfrmDenpyou2.FormActivate(Sender: TObject);
var
	i : integer;

begin
  edbDenpyouBanngou.Text  := gDenpyouNo;
	lDate.Caption           := GDate;
  lNyuuryokusha.Caption   := GNyuuryokusha;
  lTokuisakiName.caption  := GTokuisakiName2;
  //2006.05.15
  //lGatsubun.Caption       := GGatsubun;
  EditGatsubun.Text       := GGatsubun;
  MemoBikou.Text          := GMemo;
  edbBikou.Text           := GBikou;

  //2004.09.18
  lbAriaCode.Caption      := GAriaCode;

  // GTeisei=1であれば，訂正伝票ラベルを Visible=Trueにする．
  if GTeisei = 1 then lblTeisei.Visible := True;

  //add 2015.03.26
  // GTeisei=1であれば，取り消し伝票ラベルを Visible=Trueにする．
  if GTeisei = 2 then lblTorikeshi.Visible := True;

  // StringGridを作る．
	MakeSG;

  // MakeSGでセットしたStringGridをソートする．
  SG1 := HKLib.SGSort(SG1, CintNum);

  // ソート後のStringGridに連番をつける．
 	For i := 0 to SG1.RowCount - 2 do begin
  	SG1.Cells[CintSeq, i+1] := IntToStr(i+1);
  end;


 // if GInfoDenpyouno <> '' then begin
 //      Label18.Caption := GInfoDenpyouno;
      //edbDenpyouBanngou.Text := '';
     // BitBtn1.Enabled := True;
     // gDenpyouNo      := GInfoDenpyouno;
//  end;



end;

procedure TfrmDenpyou2.MakeSG;
var
	i      : integer;
  sTax   : String;

begin
	with SG1 do begin
    DefaultDrawing   := True;
    RowCount         := GRowCount;
    ColCount         := CintEnd;
    Align            := alBottom;
    FixedCols        := 0;
    FixedColor       := clYellow;
    Canvas.Font.Size := 11;
    DefaultRowHeight := 20;
    ColWidths[0]     := 64;
    Font.Color       := clNavy;

    ColWidths[CintSeq]         := 20;         // 並び順に変更があった為，ここの順番は滅茶苦茶
    Cells[CintSeq, 0]          := 'No.';

    ColWidths[CintNum]         := 0;
    Cells[CintNum, 0]          := '';

    ColWidths[CintItemCode1]:= 0;
    Cells[CintItemCode1, 0] := '商品コード1';

    ColWidths[CintItemCode2]:= 0;
    Cells[CintItemCode2, 0] := '商品コード2';

    ColWidths[CintItemName]:= 100;
    Cells[CintItemName, 0] := '商品名';

    ColWidths[CintItemTax]:= 60;
    Cells[CintItemTax, 0] := '消費税';

    ColWidths[CintItemYomi]:= 0;
    Cells[CintItemYomi, 0] := '商品読み';

    ColWidths[CintItemKikaku]:= 0;
    Cells[CintItemKikaku, 0] := '規格';      // このデータは下記へ統合された．

    ColWidths[CintItemIrisuu]:= 60;
    Cells[CintItemIrisuu, 0] := '規格';      // 旧入り数が規格へ変更された．

    ColWidths[CintItemHindo]:= 60;
    Cells[CintItemHindo, 0] := '頻度';

    ColWidths[CintItemSaishuu]:= 80;
    Cells[CintItemSaishuu, 0] := '最終出荷日';

    ColWidths[CintItemCount]:= 120;
    Cells[CintItemCount, 0] := '数量';

    ColWidths[CintItemCount]:= 30;
    Cells[CintItemTanni, 0] := '単位';

    ColWidths[CintItemTanka]:= 60;
    Cells[CintItemTanka, 0] := '単価';

    ColWidths[CintItemShoukei]:= 60;
    Cells[CintItemShoukei, 0] := '小計';

    curSum := 0.0;
    curTax := 0.0;

    // もともと Rowcount - 1 が行数．タイトル分を考えて RowCount - 2迄となる
    	For i := 0 to RowCount - 2 do begin
    		with SG1 do begin
      		Cells[CintNum,        i+1] := GArray[i, 0];
    			Cells[CintItemCode1,  i+1] := GArray[i, 1];
  		  	Cells[CintItemCode2,  i+1] := GArray[i, 2];
  		  	Cells[CintItemName,   i+1] := GArray[i, 3];
  		  	Cells[CintItemTax,    i+1] := GArray[i, 13];
  		   	Cells[CintItemYomi,   i+1] := GArray[i, 4];
  			  Cells[CintItemKikaku, i+1] := GArray[i, 5];
  			  Cells[CintItemIrisuu, i+1] := GArray[i, 6];
  			  Cells[CintItemHindo,  i+1] := GArray[i, 7];
  			  Cells[CintItemSaishuu,i+1] := GArray[i, 8];
  			  Cells[CintItemCount,  i+1] := GArray[i, 9];
  			  Cells[CintItemTanni,  i+1] := GArray[i,10];
  			  Cells[CintItemTanka,  i+1] := GArray[i,11];
  			  Cells[CintItemShoukei,i+1] := GArray[i,12];
          curSum := curSum + StrToCurr(Cells[CintItemShoukei, i+1]);

          sTax := StringReplace(GArray[i, 13], '%', '',
                          [rfReplaceAll, rfIgnoreCase]);

          curTax := curTax + (StrToCurr(Cells[CintItemShoukei, i+1]) * (StrToCurr(sTax)/100));
      	end;
      end;

      lSum.Caption := '合計金額　' + CurrToStr(curSum) + '円';
  end;

end;


procedure TfrmDenpyou2.BitBtn2Click(Sender: TObject);
begin

  // 伝票番号をグローバル変数にセットして
  SetLength(GArrayOfDenpyouBanngou, 1);
	GArrayOfDenpyouBanngou[0] := edbDenpyouBanngou.Text;

  // 印刷フォームを生成する．
  TfrmDenpyouPrint.Create(Self);

  ShowMessage('印刷が完了しました');

end;

procedure TfrmDenpyou2.FormCreate(Sender: TObject);
var
  s : String;
begin
  inherited;
{
  //トランザクションの初期設定
  dabDenpyou.Connected := False;
  //showmessage(dabDenpyou.Params.Text);
  dabDenpyou.LoginPrompt := False;
  dabDenpyou.Params.Clear;
  //showmessage(dabDenpyou.Params.Text);
  s := 'user_name=' + GUser + ',';
  dabDenpyou.Params.add(s);
  s := 'password=' + GPass;
  dabDenpyou.Params.add(s);
  //showmessage(dabDenpyou.Params.Text);

  dabDenpyou.Connected := True;
}

  Width := 650;
  Height := 390;
  // 伝票番号をグローバル変数からローカル変数へ保存
  // 以後，フォーム内の作業は gDenpyouNoを使用する．
  // 複数者で作業している時，グローバル変数はいつ変わるか分からない．
  gDenpyouNo := GDenpyouBanngou;

  //検索されて表示したときの処理
  //2002.06.11 Hajime Kubota
  if GDenpyouBanngou<>'' then begin
    BitBtn2.Enabled := True; //印刷ボタンを有効にする
    BitBtn1.Enabled := False; //登録ボタンは無効にする
  end;


  //2003.09.21
  BitBtnExcel.Visible := False;

end;

//登録ボタンがクリックされた
procedure TfrmDenpyou2.BitBtn1Click(Sender: TObject);
var dlg: TForm;
    res: Word;
    s : String;
    i : Integer;
    FileHandle: Integer;

    sLockFileName : String;
    sCCode, sCCode2 : String;
begin
  //確認メッセージ
  dlg := CreateMessageDialog('登録しますか', mtInformation,
                             [mbYes, mbNo]);
  dlg.ActiveControl := TWinControl(dlg.FindComponent('No'));
  res := dlg.ShowModal;
  if res <> mrYes then begin
   exit;
  end;
  dlg.Free;

  //2004.09.18
  //ロックファイル名の生成
  sLockFileName :=  'Lock_' + lbAriaCode.Caption + '.txt';

  //2003.08.03
  //ロックファイルがあるかどうか
  //if FileExists(CFileName_Lock)=True then begin
  if FileExists(sLockFileName)=True then begin
	  ShowMessage('現在他のユーザーがエリアコード' + lbAriaCode.Caption + 'の伝票番号を取得中です。数秒お待ちください');
    exit;
  end else begin
	  //ここでロックファイルを作成する
    //FileHandle := FileCreate(CFileName_Lock);
    FileHandle := FileCreate(sLockFileName);
	  if FileHandle = -1 then begin
    	Showmessage('このコースには現在ロックがかかっています。再度時間をおいて登録してください。');
      exit;
    end else begin
	    FileClose(FileHandle);
    end;
  end;



  // トランザクション開始
  dabDenpyou.Open;
  dabDenpyou.StartTransaction;

  // tblTDenpyouDetail へデータを登録
  if InsertDenpyouDetail = false then
    begin
    	Showmessage('伝票（明細）の登録に失敗しました');
    end
  // tblTDenpyou       へデータを登録
  else if InsertDenpyou  = false then
    begin
    	Showmessage('伝票(合計)の登録に失敗しました');
    end
  // tblMItems2のデータを更新
  else if UpdateItem2 = false then
    begin
    	Showmessage('tblMItem2の更新に失敗しました');
    end
  else if UpdateDenpyou = false then
    begin
    	Showmessage('元伝票の更新に失敗しました');
    end
  else
    begin
      dabDenpyou.Commit;
		Beep();
      Showmessage('登録しました');
      // 登録後，初めて印刷できる様にする．
      BitBtn2.Enabled := True;
      // 登録後，は登録できない様にする．
      BitBtn1.Enabled := False;
      // 登録後，伝票番号をフォームに表示する
      edbDenpyouBanngou.Text := gDenpyouNo;
    end;

  dabDenpyou.Close;


  i := CheckUriageKingaku(gDenpyouNo);

  if i=0 then begin
		ShowMessage('伝票金額の整合性確認しました');
  end else begin
		ShowMessage('伝票金額の整合性が取れていません！至急管理者に連絡してください');
		ShowMessage('問題の発生した伝票番号は「'+gDenpyouNo+'」です。');
		ShowMessage('この問題を解決するには、「'+gDenpyouNo+'」の伝票を一度削除して再度登録してください。');
		Beep();
  end;

  //2006.05.17
  sCCode := lbAriaCode.Caption;
  sCCode2 := Copy(edbDenpyouBanngou.Text,3,2);
  if sCCode <> sCCode2 then begin
    ShowMessage('伝票番号とコースコードの整合性が違っています');
    ShowMessage('問題の発生した伝票番号は「'+gDenpyouNo+'」です。');
    ShowMessage('この問題を解決するには、「'+gDenpyouNo+'」の伝票を一度削除して再度登録してください。');
    Beep();
  end;


  //2003.3.10
  GDenpyouFlag := 1;

//2003.08.03
//ここでロックファイルを削除する
 //if DeleteFile(CFileName_Lock) = true then begin
 if DeleteFile(sLockFileName) = true then begin
   ;
 end else begin
 	ShowMessage('伝用番号のロックファイル' + sLockFileName + 'がありませんでした。伝票がきちんと登録されているかどうか確認してください');
 end;
 gDenpyouNo := '';
end;

Function TfrmDenpyou2.CheckUriageKingaku(sDenpyouNo:String):Integer;
var
	sSql:String;
  i, isum,igenkin : Integer;
  strTokuisakiCode1 : String;
begin
	sSql := 'Select UriageKingaku from ' + CtblTDenpyou;
  sSql := sSql + ' where DenpyouCode=''' +  sDenpyouNo + '''';
  with Query1 do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    open;
    i := FieldByName('UriageKingaku').asInteger;
    Close;
  end;

	sSql := 'Select Sum(Shoukei) AS iSum from ' + CtblTDenpyouDetail;
  sSql := sSql + ' where DenpyouCode=''' +  sDenpyouNo + '''';
  with Query1 do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    open;
    iSum := FieldByName('iSum').asInteger;
    Close;
  end;
  Result := i-iSum;

  //CtblTDenpyou,CtblTDenpyouDetailのチェックの時点で金額が違っていれば処理を抜ける
  if Result <> 0 then begin
       exit;
  end;

strTokuisakiCode1 := Copy(lTokuisakiName.caption, 1, (pos(',', lTokuisakiName.caption)-1));

 //
 // 得意先コード１が5000番代の場合，現金回収テーブルもチェック
 // 2007/8/8 追加
  if (StrToInt(strTokuisakiCode1) >= 50000) and (StrToInt(strTokuisakiCode1) < 60000) then begin
	  sSql := 'Select Shoukei from ' + CtblTGennkinnKaishuu;
    sSql := sSql + ' where DenpyouCode=''' +  sDenpyouNo + '''';
    with Query1 do begin
  	  Close;
      Sql.Clear;
      Sql.Add(sSql);
      open;
      igenkin := FieldByName('Shoukei').asInteger;
      Close;
    end;
 //CtblTDenpyou,CtblTGennkinnKaishuuの金額を比較　誤差　+- 1で計算
    if ((i-igenkin) > 1) Or ((igenkin-i) > 1) then begin
        Result := 1
    end else begin
        Result := 0
    end;
  end;
end;



procedure TfrmDenpyou2.SG1DrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
  type
  TVowels = set of char;

var
	DRect  : TRect;
  Mode   : Integer;
  Vowels : TVowels;
	C      : LongInt;
  myRect : TRect;
  iCol   : Integer;
begin
//  inherited;
   if not (gdFixed in state)  then begin

    Vowels := [char(CintItemTax)];
		SG1.Canvas.FillRect(Rect);
	  DRect.Top := Rect.Top + 2;
  	DRect.Left := Rect.Left + 2;
	  DRect.Right := Rect.Right - 2;
  	DRect.Bottom := Rect.Bottom - 2;

	  if  char(ACol) IN Vowels then begin       //右寄せ
    	Mode := DT_RIGHT;
	  end else begin
  		Mode := DT_LEFT;
    end;

    DrawText(SG1.Canvas.Handle, PChar(SG1.Cells[ACol,ARow]),
	           Length(SG1.Cells[ACol,ARow]), DRect, Mode);
  end;
end;

Function TfrmDenpyou2.InsertDenpyouDetail : Boolean;

var
 i                 : Integer;
 sSql              : String;
 strDenpyouBangou  : String;
 strTokuisakiCode1 : String;
 strNyuuryokusha   : String;
 strNo             : String;
 strCode1          : String;
 strCode2          : String;
 strSuuryou        : String;
 strTannka         : String;
 strShoukei        : String;
 strNouhinDate     : String;
 strNyuuryokuDate  : String;
 strDennpyouDate   : String;
 strUpdateDate     : String;
 strTax            : String;

begin

try
  // データ作成（共通部分）

  // Get DenpyouBangou ( Set GDenpyouBangou )
  if GetDenpyouBanngou = False then begin
    Result := False;
    Exit;
  end;

  strDenpyouBangou  := gDenpyouNo;
  strTokuisakiCode1 := Copy(lTokuisakiName.caption, 1, (pos(',', lTokuisakiName.caption)-1));
  strNyuuryokusha   := lNyuuryokusha.Caption;
  strNouhinDate     := lDate.Caption;
  strNyuuryokuDate  := DateTimeToStr(Date());

  //2001.12.26 Change by H.K.
  //strDennpyouDate   := DateTimeToStr(Date());
  strDennpyouDate   := lDate.Caption;

  // 更新の場合も，新規の場合もまず伝票番号のレコードをtblTDenpyouDetailから削除する．
  // その後，データを挿入する．
  sSql := 'DELETE FROM ' + CtblTDenpyouDetail;
  sSql := sSql + ' WHERE DenpyouCode = ' + '''' + gDenpyouNo + '''';
  with QueryDenpyou do begin
	 	Close;
    Sql.Clear;
  	Sql.Add(sSql);
	  ExecSql;
    Close;
  end;


  // tblTDenpyouDetailへデータを挿入

  with SG1 do begin

    i := 0;

  	while Cells[CintItemCode1,i+1]<>'' do begin

      // データ作成（個別部分)
      strNo         := Cells[CintSeq,        i+1];
      strCode1      := Cells[CintItemCode1,  i+1];
      strCode2      := Cells[CintItemCode2,  i+1];
      strSuuryou    := Cells[CintItemCount,  i+1];
      strTannka     := Cells[CintItemTanka,  i+1];
      strShoukei    := Cells[CintItemShoukei,i+1];
      strUpdateDate := DateTimeToStr(Now);
      strUpdateDate := '2019/03/13 14:07:56AM';
      strTax        := Cells[CintItemTax,i+1];
      strTax        := StringReplace(strTax, '%', '',
                          [rfReplaceAll, rfIgnoreCase]);

      // thuyptt 20190826
     if strNouhinDate < CsChangeTaxtDate10 then begin
      strTax :=  floattostr(CsTaxRate * 100);
     end else begin
      strTax :=  strTax;
     end;

      // SQL文作成
	    sSql := 'INSERT INTO ' + CtblTDenpyouDetail + ' (';
      sSql := sSql + ' DenpyouCode, TokuisakiCode1, Nyuuryokusha,';
      sSql := sSql + ' No, Code1, Code2, Suuryou, Tannka, Shoukei,';
      sSql := sSql + ' NouhinDate, NyuuryokuDate, DenpyouDate, tax,'; 
	  if Label18.Caption <> '' then begin
		sSql := sSql + ' InfoDenpyouNo, ';
	  end;
	  if Label21.Caption <> '' then begin
		sSql := sSql + ' InfoTorihikiId, ';
	  end;

      sSql := sSql + ' CreatedDate, UpdateDate) VALUES ( ';  //thuyptt@todo 20190313
      sSql := sSql + '''' + strDenpyouBangou   + ''', ' ;
      sSql := sSql +        strTokuisakiCode1  +   ', ' ;
      sSql := sSql + '''' + strNyuuryokusha    + ''', ' ;
      sSql := sSql + '' + strNo              + ', ' ;
      sSql := sSql + '''' + strCode1           + ''', ' ;
      sSql := sSql + '''' + strCode2           + ''', ' ;
      sSql := sSql + '' + strSuuryou         + ', ' ;
      sSql := sSql + '' + strTannka          + ', ' ;
      sSql := sSql + '' + strShoukei         + ', ' ;
      sSql := sSql + '#' + strNouhinDate      + '#, ' ;
      sSql := sSql + '#' + strNyuuryokuDate   + '#, ' ;
      sSql := sSql + '#' + strDennpyouDate    + '#, ' ;
      sSql := sSql + '' + strTax         + ', ' ;
	  if Label18.Caption  <> '' then begin
		sSql := sSql + '''' + Label18.Caption    + ''', ' ;
	  end;
	  if Label21.Caption  <> '' then begin
		sSql := sSql + '''' + Label21.Caption    + ''', ' ;
	  end;

    sSql := sSql + 'Now(), ' ;     //thuyptt@todo 20190313
    sSql := sSql + 'Now() )' ;     //thuyptt@todo 20190313

      // SQL文実行
      with QueryDenpyou do begin
  		 	Close;
	      Sql.Clear;
  	  	Sql.Add(sSql);
  		  ExecSql;
	      Close;
      end;

      i := i + 1;

    end;

  end;

    Result := true;

except
  on E: EDBEngineError do begin
    dabDenpyou.Rollback;
   	ShowMessage(E.Message);
    Result := False;
  end;
end;

end;


Function TfrmDenpyou2.GetDenpyouBanngou : Boolean;

var
 sSql              : String;
 wkGDenpyouBangou  : String;
 wkDate1           : String;
 wkDate2           : String;
 wksNumber         : String;
 sYy,sYy2,sYyNext  : String;

begin

try

  // もしGDenpyouBangouが既にセットされているなら，(Denpyou1より引き継いでいるなら）
  // それは既存伝票の修正なので，それをそのまま使う．よって，この関数を抜ける．
  // GDenpyouBangouがnullの場合，新規伝票なので伝票番号生成へ移行する．
  if gDenpyouNo <> '' then begin
    Result := True;
    Exit;
  end;


  // 伝票番号生成開始

   //2004.09.18
   // 伝票番号の上4桁（実日付 YYMM)を取得する．
   //DateTimeToString(wkDate1, 'yymm', Date());
   // 伝票番号の上2桁（実日付 YY)を取得する．
   DateTimeToString(wkDate1, 'YY', Date());

   //2005.01.12
   //sYYのとり方を変更
   //理由：昨年の伝票を打つときに伝票番号が正しくとれない為
   //伝票日付の年をとるようにする
   //DateTimeToString(sYy, 'YY', Date());
   sYy := Copy(GDate,3,2);

   wkDate1 := wkDate1 + lbAriaCode.Caption;
   //一応エラーチェック
   if Length(wkDate1) <> 4 then begin
    ShowMessage('伝票番号の上4桁が不正です->' + wkDate1);
    exit;
   end;

   // 伝票番号の最大値を取得し，その下5桁を取得する．
   //
   // 1.伝票自動採番ロジックの変更
   //
   //2004.09.18
   //04　11　　　　　　00001
   //年　コースコード　連番
   //
   //エリアコードも考慮してマックス値をとる
   //sSql := 'SELECT MAX(DenpyouCode) FROM ' + CtblTDenpyouDetail;
   //
   //2004.10.17
   //伝票番号作成時の年号も考慮しないといけない
   // sYy
   //
   //
   sSql := 'SELECT MAX(DenpyouCode) FROM ' + CtblTDenpyou;
   sSql := sSql + ' where ';
   //sSql := sSql + ' DDate >= ''20' + sYy + '/01/01''';   //mod  before utada 20190313
   sSql := sSql + ' DDate >= #20' + sYy + '/01/01#';       // mod  after utada 20190313
   sSql := sSql + ' and ';

   //2005.01.05
   //年度単位で伝票番号のMAXを求めるため
   sYyNext := IntToStr(StrToInt(sYy)+1);
   If Length(sYyNext)=1 then begin
    sYyNext := '0' + sYyNext;
  end;
   //sSql := sSql + ' DDate < ''20' + sYyNext + '/01/01''';  mod  before utada 20190313
   sSql := sSql + ' DDate < #20' + sYyNext + '/01/01#';  // mod  after  utada 20190313
   sSql := sSql + ' and ';


   sSql := sSql + ' TokuisakiCode2 = ' + lbAriaCode.Caption;

   with QueryDenpyou do begin
//      QueryDenpyou.
   	 Close;
     Sql.Clear;
     Sql.Add(sSql);
     Open;
     if EOF then begin
      wkGDenpyouBangou := '000000000';
     end else begin
      wkGDenpyouBangou := Fields.FieldByNumber(1).AsString;
     end;
     Close;
   end;

   //2005.12.29
   if wkGDenpyouBangou='' then begin //年初の場合
     gDenpyouNo := sYy + lbAriaCode.Caption + '00001';
   end else begin
    sYy2      := Copy(wkGDenpyouBangou, 1, 2);
    wkDate2   := Copy(wkGDenpyouBangou, 1, 4);
    wksNumber := Copy(wkGDenpyouBangou, 5, 9);

    // 伝票番号の下4桁の生成

    // もし月初の1番最初の伝票であるなら，伝票番号下5桁を00001にセットする．
    // この時のロジックは以下の通り．
    //　 実日付から生成した伝票番号の上4桁(wkDate1)と，
    //　 現在の最後の伝票番号の上4桁が異なる．
    //　 例えば，wkDate1='0112' wkDate2='0111'であれば月初の最初の伝票．
    //　 月初の2個目の伝票以降は，wkDate1='0112' wkDate2='0112'となる．
    //　 これは月初営業日が1日で無い場合にも対応する．
    //if wkDate1 <> wkDate2 then
    if sYy <> sYy2 then begin
      wksNumber := '00001';
    end else begin
      // 月初の一番最初の伝票で無い場合は，単純にプラス1する．
      wksNumber   := FormatFloat('00000', (StrToInt(wksNumber) + 1));

      //2004.09.18
      //一応エラーチェック
      if StrToint(wksNumber) > 90000 then begin
        Showmessage('警告！　エリアコード->' + wkDate1 + '連番が90000を超えました');
        Showmessage('処理は続行できますが一応管理者へ電話してください');
      end;
    end;

     // 伝票番号の生成
    //2005.01.12
    //wkDate1は現時点の日付を保持しているため
    //gDenpyouNo := wkDate1 + wksNumber;
    gDenpyouNo := wkDate2 + wksNumber;

   end; //ofif 2005.12.29

   Result := true;

except
  on E: EDBEngineError do begin
   	ShowMessage(E.Message);
    Result := False;
  end;
end;

end;


Function TfrmDenpyou2.InsertDenpyou : Boolean;

var
 sSql              : String;
 strTokuisakiCode1 : String;
 strTokuisakiCode2 : String;
 strInputDate      : String;
 strDDate          : String;
 strUriageKingaku  : String;
 strShouhizei      : String;
 strGoukei         : String;
 strGatsubun       : String;
 strDenpyouBanngou : String;
 strTeiseiflg      : String;
 curShouhizei      : Currency;
 curGoukei         : Currency;
 strMemo           : String; //Added by Hajime Kubota 2002.06.06
 strBIkou          : String;
// strInfoDenpyouNo  : String;
 douTax            : Double;   //2014.04.01 消費税対応

begin

try
  // データ作成
  strTokuisakiCode1 := Copy(lTokuisakiName.caption, 1, (pos(',', lTokuisakiName.caption)-1));
  strInputDate      := DateTimeToStr(Date());

  //Changed by H.K.
  //strDDate          := DateTimeToStr(Date());
  strDDate          := lDate.caption;

  //curShouhizei      := Trunc(curSum * CsTaxRate);
  //2004.06.09
  //2004.08.03
  //curShouhizei      := Round(curSum * CsTaxRate);
  //curShouhizei      := Round(curSum * CsTaxRate + 0.01);
  //20050703 HajimeKubota
  //返品伝票の場合の消費税の計算

 //2014.04.01 消費税対応 begin
 if strDDate < CsChangeTaxDate then begin
    douTax :=  CsOldTaxRate;
 end else begin
    douTax :=  CsTaxRate;
 end;
//2014.04.01 消費税対応 end

  if curSum > 0 then begin
     curShouhizei      := Round(curSum * douTax + 0.01); //2014.04.01 消費税対応
  end else begin
     curShouhizei      := Round(curSum * douTax - 0.01); //2014.04.01 消費税対応
  end;

  //thuyptt add tax
  if strDDate >= CsChangeTaxtDate10 then begin
     //curShouhizei      := Round(currTax);

    if curSum > 0 then begin
     curShouhizei      := Round(curTax + 0.01);
    end else begin
     curShouhizei      := Round(curTax - 0.01);
    end;

  end;

  curGoukei         := curSum + curShouhizei;

  strUriageKingaku  := CurrtoStr(curSum);
  strShouhizei      := CurrtoStr(curShouhizei);
  strGoukei         := CurrtoStr(curGoukei);
  //2006.05.15
  //strGatsubun       := lGatsubun.Caption + '/01';
  strGatsubun       := EditGatsubun.Text + '/01';
  strDenpyouBanngou := gDenpyouNo;

  strBikou          := edbBikou.Text;

  // 訂正ラベルが見えている＝訂正伝票，そうでない＝普通の掛伝票
  if lblTeisei.Visible = True then begin
     strTeiseiflg := '1';
     strMemo      := '返品';
  end else if lblTorikeshi.Visible = True then begin
     strTeiseiflg := '2';
     strMemo       := '取消';
  end else begin
     strTeiseiflg := '0';
     strMemo       := '';
  end;

  // TokuisakiCode2の取得
  sSql := 'SELECT TokuisakiCode2 FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE TokuisakiCode1=' + strTokuisakiCode1;
  with QueryDenpyou do begin
	 	Close;
    Sql.Clear;
  	Sql.Add(sSql);
    Open;
    strTokuisakiCode2 := FieldbyName('TokuisakiCode2').AsString;
    Close;
  end;

  // 更新の場合も，新規の場合もまず伝票番号のレコードをtblTDenpyouから削除する．
  // その後，データを挿入する．

  sSql := 'DELETE FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE DenpyouCode = ' + '''' + gDenpyouNo + '''';
  with QueryDenpyou do begin
	 	Close;
    Sql.Clear;
  	Sql.Add(sSql);
	  ExecSql;
    Close;
  end;

  // tblTDenpyouへデータを挿入

  // SQL文作成
  sSql := 'INSERT INTO ' + CtblTDenpyou;
  sSql := sSql + ' ( TokuisakiCode1, TokuisakiCode2, InputDate, DDate, ';
  sSql := sSql + 'UriageKingaku, Gatsubun, DenpyouCode, Memo, Teiseiflg, ';
  if Label18.Caption <> '' then begin
	sSql := sSql + ' InfoDenpyouNo,';
  end;
  if Label21.Caption <> '' then begin
	sSql := sSql + ' InfoTorihikiId,';
  end;
  sSql := sSql + ' CreatedDate, Bikou ) VALUES ( ';
  sSql := sSql + '''' + strTokuisakiCode1  + ''', ' ;
  sSql := sSql + '''' + strTokuisakiCode2  + ''', ' ;
  sSql := sSql + '''' + strInputDate       + ''', ' ;
  sSql := sSql + '''' + strDDate           + ''', ' ;
  sSql := sSql +        strUriageKingaku   + ', '   ;
  sSql := sSql + '''' + strGatsubun        + ''', ' ;
  sSql := sSql + '''' + strDenpyouBanngou  + ''', ' ;
  sSql := sSql + '''' + strMemo            + ''', ' ;
  sSql := sSql + '''' + strTeiseiflg       + ''', ' ;
  if Label18.Caption  <> '' then begin
	sSql := sSql + '''' + Label18.Caption    + ''', ' ;
  end;
  if Label21.Caption  <> '' then begin
	sSql := sSql + '''' + Label21.Caption    + ''', ' ;

  end;

  sSql := sSql + 'Now(), ' ; 

  sSql := sSql + '''' + strBikou           + ''' )' ;


  // SQL文実行
  with QueryDenpyou do begin
  	Close;
    Sql.Clear;
  	Sql.Add(sSql);
 	  ExecSql;
    Close;
  end;


  //
 // 得意先コード１が5000番代の場合，現金回収テーブルにもデータをInsert
 //

 if (StrToInt(strTokuisakiCode1) >= 50000) and (StrToInt(strTokuisakiCode1) < 60000) then begin

   // tblTGennkinnkaishuuへデータを挿入

   // SQL文作成
   sSql := 'INSERT INTO ' + CtblTGennkinnKaishuu;
   sSql := sSql + ' ( DenpyouCode, NouhinDate, TokuisakiCode1, Shoukei, Shouhizei, Goukei, CourseID, CreatedDate, UpdatedDate, Kaishuuflg )';
   sSql := sSql + ' VALUES ( ';
   sSql := sSql + '''' + strDenpyouBanngou  + ''', ' ;
   sSql := sSql + '''' + strDDate           + ''', ' ;
   sSql := sSql + '''' + strTokuisakiCode1  + ''', ' ;
   sSql := sSql +        strUriageKingaku   + ', '   ;
   sSql := sSql +        strShouhizei       + ', '   ;
   sSql := sSql +        strGoukei          + ', '   ;
   sSql := sSql + '''' + strTokuisakiCode2  + ''', ' ;
   sSql := sSql +  'Now(),' ; //thuyptt 20190515
   sSql := sSql +  'Now(),' ; //thuyptt 20190515
   sSql := sSql + '''' + '0'                + ''' )' ;

   // SQL文実行
   with QueryDenpyou do begin
  	 Close;
     Sql.Clear;
  	 Sql.Add(sSql);
 	   ExecSql;
     Close;
   end;

 end;

 Result := True;

except
  on E: EDBEngineError do begin
    dabDenpyou.Rollback;
   	ShowMessage(E.Message);
    Result := False;
  end;
end;

end;


Function TfrmDenpyou2.UpdateItem2 : Boolean;

var
 i                 : Integer;
 iHindo            : Integer;
 strTokuisaki      : String;
 strCode1          : String;
 strCode2          : String;
 strSaishuu        : String;
 sSql,strTokuisakiCode1 : String;
 strDDate          : String; //Add utada 2007.08.24

begin

//2002.07.01 Added by H.Kubota
if lblTeisei.Visible = True then begin
  Result := True;
  ShowMessage('返品伝票なので頻度は上げませんでした');
  exit;
end;

if lblTorikeshi.Visible = True then begin
  Result := True;
  ShowMessage('取消伝票なので頻度は上げませんでした');
  exit;
end;

  // Add utada 2007.08.24
  strDDate          := lDate.caption;


try

 for i := 1 to SG1.RowCount -1 do begin

   //2001.12.26 added by H.K.
   if SG1.Cells[CintItemCode1, i]='' then begin
     Break;
   end;

   // 頻度を取得
   strCode1      := SG1.Cells[CintItemCode1, i];
   strCode2      := SG1.Cells[CintItemCode2, i];

   sSql := 'SELECT Hindo FROM ' + CtblMItem2;
   sSql := sSql + ' WHERE Code1 = ' + '''' + strCode1 + '''';
   sSql := sSql + ' AND   Code2 = ' + '''' + strCode2 + '''';

   //2001.12.26 added by H.K.
   strTokuisakiCode1 := Copy(lTokuisakiName.caption, 1, (pos(',', lTokuisakiName.caption)-1));
   sSql := sSql + ' AND   TokuisakiCode = ' + '' + strTokuisakiCode1 + '';


   with QueryDenpyou do begin
	   Close;
     Sql.Clear;
  	 Sql.Add(sSql);
     Open;
     iHindo := FieldbyName('Hindo').AsInteger;
     Close;
   end;

   // 頻度，最終出荷日を更新
   iHindo       := iHindo + 1;
   strTokuisaki := Copy(lTokuisakiName.caption, 1, (pos(',', lTokuisakiName.caption)-1));
   //strSaishuu   := DateTimeToStr(Date());  //Comment OUt utada 2007.08.24
   strSaishuu := strDDate;  //Add utada 2007.08.24 最終出荷日は、納品日に合わせる
   sSql := 'UPDATE ' + CtblMItem2;
   sSql := sSql + ' SET Hindo = '           + InttoStr(iHindo)    + ', ';
   sSql := sSql + ' UpdatedDate = Now(), ';  //20190531  
   sSql := sSql + ' SaishuuShukkabi = '     + '''' + strSaishuu   + '''';
   sSql := sSql + ' WHERE TokuisakiCode = ' + '' + strTokuisaki + '';
   sSql := sSql + ' AND   Code1 = '         + '''' + strCode1     + '''';
   sSql := sSql + ' AND   Code2 = '         + '''' + strCode2     + '''';

   with QueryDenpyou do begin
     Close;
     Sql.Clear;
     Sql.Add(sSql);
 	   ExecSql;
     Close;
   end;

 end;

  Result := True;

except
  on E: EDBEngineError do begin
    dabDenpyou.Rollback;
   	ShowMessage(E.Message);
    Result := False;
  end;
end;

end;


Function TfrmDenpyou2.UpdateDenpyou : Boolean;

var
 sSql,strTeiseiflg,strInfoTorihikiId : String;
 strTokuisakiCode1:String;
 sOldDenpyouBangou : String;
begin

  strTokuisakiCode1 := Copy(lTokuisakiName.caption, 1, (pos(',', lTokuisakiName.caption)-1));

try

  if lblTorikeshi.Visible = True then begin
     strTeiseiflg := '2';
  end;

  if Label21.Caption  <> '' then begin

  strInfoTorihikiId :=  Label21.Caption

  end;

  sOldDenpyouBangou :=  oldDenpyouBangou.Caption;
  //add 20150116 infoマートの赤伝の場合元伝票を修正
  //if(strTeiseiflg = '2') and (strInfoTorihikiId  <> '') then begin
  if(strTeiseiflg = '2') and ( sOldDenpyouBangou <> '') then begin

 // SQL文作成
   sSql := 'UPDATE ' + CtblTDenpyou;
   sSql := sSql + ' SET Teiseiflg = 2,';
   sSql := sSql + ' UpdatedDate = Now(), ';  //20190531
   sSql := sSql + ' Memo = '     + '''' + '取消済み' + ''',';
   sSql := sSql + ' Bikou = Bikou + '     + '''' + ' 取消済み' + '''';
   sSql := sSql + ' WHERE DenpyouCode = ' + '''' + sOldDenpyouBangou + '''';
   sSql := sSql + ' AND   TokuisakiCode1 = ' + '''' + strTokuisakiCode1+ '''';
   sSql := sSql + ' AND   Teiseiflg = 0';

   // SQL文実行
   with QueryDenpyou do begin
  	 Close;
     Sql.Clear;
  	 Sql.Add(sSql);
 	   ExecSql;
     Close;
   end;

 end;

 Result := True;

except
  on E: EDBEngineError do begin
    dabDenpyou.Rollback;
   	ShowMessage(E.Message);
    Result := False;
  end;
end;

end;



procedure TfrmDenpyou2.BitBtn8Click(Sender: TObject);
var
 i                 : Integer;
 strTanka          : String;
 strTokuisaki      : String;
 strCode1          : String;
 strCode2          : String;
 sSql              : String;
 sDate             :String;
begin

try

 for i := 1 to SG1.RowCount -1 do begin

   //2001.12.26 Added by H.K.
   if SG1.Cells[CintItemTanka, i]='' then begin
     Break;
   end;

   // 単価を更新
   strTanka      := SG1.Cells[CintItemTanka, i];
   strTokuisaki  := Copy(lTokuisakiName.caption, 1, (pos(',', lTokuisakiName.caption)-1));
   strCode1      := SG1.Cells[CintItemCode1, i];
   strCode2      := SG1.Cells[CintItemCode2, i];

   sSql := 'UPDATE ' + CtblMItem2;
   sSql := sSql + ' SET Tanka = ' + '''' + strTanka   + '''';
   sSql := sSql + ' ,InfoSendDate = Null';
//   sSql := sSql + ' WHERE ID = 47208853';
   sSql := sSql + ' WHERE TokuisakiCode = ' + '' + strTokuisaki + '';
   sSql := sSql + ' AND   Code1 = '         + '''' + strCode1     + '''';
   sSql := sSql + ' AND   Code2 = '         + '''' + strCode2     + '''';

   with QueryDenpyou do begin
     Close;
     Sql.Clear;
     Sql.Add(sSql);
 	   ExecSql;
     Close;
   end;

    sDate := DateToStr(Date);

   //見積りデータの更新
   sSql := 'UPDATE tblMItem3';
   sSql := sSql + ' SET Tanka = ' + '''' + strTanka   + '''';
   sSql := sSql + ', Bikou = ''' + sDate + '価格変更, '' + Bikou';
   sSql := sSql + ' WHERE TokuisakiCode = ' + '' + strTokuisaki + '';
   sSql := sSql + ' AND   Code1 = '         + '''' + strCode1     + '''';
   sSql := sSql + ' AND   Code2 = '         + '''' + strCode2     + '''';

   with QueryDenpyou do begin
     Close;
     Sql.Clear;
     Sql.Add(sSql);
 	   ExecSql;
     Close;
   end;


 end;

 ShowMessage('フィードバック完了');

except
  on E: EDBEngineError do begin
   	ShowMessage(E.Message);
  end;
end;

end;

procedure TfrmDenpyou2.FormDestroy(Sender: TObject);
begin
  inherited;
  GDenpyouBanngou := '';
//  GInfoDenpyouno :='';
//  GDenpyouCode :='';


end;

procedure TfrmDenpyou2.Timer1Timer(Sender: TObject);
begin
  if Length(MemoBikou.Text) > 1 then begin
  	if Label14.Font.Color = clRed then begin
	  	Label14.Font.Color := clNavy;
    end else begin
		  Label14.Font.Color := clRed;
    end;
  end;

end;

end.
