unit ShiireDaichou;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, ComCtrls, StdCtrls, Buttons, ExtCtrls, Grids, DB,
  DBTables, ShellAPI;

type
  TfrmShiireDaichou = class(TfrmMaster)
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    Label6: TLabel;
    EditShiiresakiCode: TEdit;
    Label7: TLabel;
    CBShiiresakiName: TComboBox;
    DTPFrom: TDateTimePicker;
    Label8: TLabel;
    DTPTo: TDateTimePicker;
    Label9: TLabel;
    SG1: TStringGrid;
    QueryMShiiresaki: TQuery;
    QuerySG: TQuery;
    QueryZan: TQuery;
    Label3: TLabel;
    CBItemName: TComboBox;
    QueryItem: TQuery;
    lbSID: TLabel;
    Button1: TButton;
    edDenpyouCode: TEdit;
    QueryDel: TQuery;
    Button2: TButton;
    Query1: TQuery;
    Query2: TQuery;
    procedure FormCreate(Sender: TObject);
    procedure CBShiiresakiNameKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EditShiiresakiCodeExit(Sender: TObject);
    procedure CBShiiresakiNameExit(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure CBItemNameKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBItemNameExit(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SG1DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure SG1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SG1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Button1Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
    procedure MakeSG;
    procedure MakeCBShiiresakiName(sKey, sWord:String; iKubun:Integer);
    procedure PrintSG;
    procedure PrintSG2;
    procedure ClearSG;
    procedure MakeSG2(myQuery : TQuery);
    procedure MakeSG3(myQuery : TQuery);
   // function GetZandaka(sTokuisakiCode, FromDate, ToDate:string) : integer;
    function GetZandaka(sTokuisakiCode, FromDate, ToDate:string) : Double;
    function GetTotalTax(sTokuisakiCode, FromDate, ToDate:string) : Double;
    procedure MakeCBName(sKey, sWord:String; iKubun:Integer);
    Procedure AddTaxLine(sSDenpyouCode,sShiiresakiCode:String; cuSum:Currency);
  public
    { Public declarations }
  end;

  const
	//グリッドの列の定義
  CintDate            = 0; //日付
  CintItem            = 1; //品名
  CintSuuryou         = 2; //数量
  CintTanka           = 3; //単価
  CintShiireKingaku   = 4; //仕入金額
  CintShiharaiKingaku = 5; //支払金額
  CintZandaka         = 6; //残高
  CintDenpyouCode     = 7; //伝票番号
  CintBikou           = 8; //備考
  CintFlag            = 9; //Flag

  CintEnd             = 10; //ターミネーター


  GRowCount = 1000; //行数
  
var
  frmShiireDaichou: TfrmShiireDaichou;

implementation
uses
	DMMaster, Inter, HKLib, PasswordDlg;


{$R *.dfm}
var
	GiRow : Integer;
  GiTax: Currency;
  GiKungakuSum: Currency;
  GiShoukei: Currency;
  GdTotalTax : Currency;


procedure TfrmShiireDaichou.FormCreate(Sender: TObject);
begin

  //2006.05.17
  dlgPasswordDlg := TdlgPasswordDlg.Create(Self);
  if dlgPasswordDlg.ShowModal = mrOK then begin
    ;
  end else begin
    Exit;
  end;
  if GPassWord = CPassword1 then begin
    Beep;
  //追加2009.04.08 utada
  end else if GPassWord = CPassword2 then begin
    Beep;
	end else if GPassWord = CPassword3 then begin
    Beep;
  // ここまで
  end else begin
    //ShowMessage('PassWordが違います');
    close;
  end;
  GPassWord := '';

  Width := 920;
  Height := 540;

  //ストリンググリッドの作成
  MakeSG;

  DTPFrom.Date := Date() - 31;
  DTPTo.Date := Date();

end;

procedure TfrmShiireDaichou.MakeSG;
begin
  with SG1 do begin
    DefaultDrawing := True;
    RowCount := GRowCount;
    ColCount := CintEnd;
    Align := alBottom;
    FixedCols := 0;
    FixedColor := clYellow;
    Canvas.Font.Size := 11;
    DefaultRowHeight := 20;
    ColWidths[0]:= 64;
    Font.Color := clNavy;

    ColWidths[CintDate]:= 100;
    Cells[CintDate, 0] := '日付';

    ColWidths[CintItem]:= 165;
    Cells[CintItem, 0] := '品 名';

    ColWidths[CintSuuryou]:= 50;
    Cells[CintSuuryou, 0] := '数量';

    ColWidths[CintTanka]:= 60;
    Cells[CintTanka, 0] := '単価';

    ColWidths[CintShiireKingaku]:= 80;
    Cells[CintShiireKingaku, 0] := '仕入金額';

    ColWidths[ CintShiharaiKingaku]:= 80;
    Cells[ CintShiharaiKingaku, 0] := '支払金額';

    ColWidths[CintZandaka]:= 80;
    Cells[CintZandaka, 0] := '残高';

    ColWidths[CintDenpyouCode]:= 120;
    Cells[CintDenpyouCode, 0] := '伝番';

    ColWidths[CintBikou]:= 120;
    Cells[CintBikou, 0] := '備考';

    ColWidths[CintFlag]:= 15;
    Cells[CintFlag, 0] := 'F';

  end;//of with

end;


procedure TfrmShiireDaichou.CBShiiresakiNameKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
if (Key=VK_F1) then begin
  MakeCBShiiresakiName('Yomi', CBShiiresakiName.Text, 1);
end;
end;

{
	得意先名のコンボボックスを作成する
  引数：sKey   -> 検索列名
  			sWord  -> 検索ワード
  			iKubun -> 0=前方一致, 1=曖昧検索, 2=数字の完全一致
}
procedure TfrmShiireDaichou.MakeCBShiiresakiName(sKey, sWord:String; iKubun:Integer);
var
	sSql : String;
begin
	//2002.09.29
  if sWord='' then begin
  	Exit;
  end;

  CBShiiresakiName.Items.Clear;


	sSql := 'SELECT * FROM ' + CtblMShiiresaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + sKey;
  case iKubun of
  0: sSql := sSql + ' LIKE ''' + sWord + '%''';
  1: sSql := sSql + ' LIKE ''%' + sWord + '%''';
  2: sSql := sSql + ' = ' + sWord;
  end;

  //sSql := sSql + ' ORDER BY Code ';
  CBShiiresakiName.Clear;
	with QueryMShiiresaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	CBShiiresakiName.Items.Add(FieldByName('Code').AsString + ',' +
       FieldByName('Name').AsString);
    	Next;
    end;//of while
    Close;
  end;//of with

  CBShiiresakiName.DroppedDown := True;
end;

procedure TfrmShiireDaichou.EditShiiresakiCodeExit(Sender: TObject);
var
  sSql:String;
begin
  if EditShiiresakiCode.Text = '' then exit;
  if CBShiiresakiName.Text <> '' then exit;

  CBShiiresakiName.Clear;

  //仕入先名を検索する
  sSql := 'SELECT * from ' +CtblMShiiresaki;
  sSql := sSql + ' where ';
  sSql := sSql + ' Code= ' + EditShiiresakiCode.Text;
	with QueryMShiiresaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	CBShiiresakiName.Items.Add(FieldByName('Code').AsString + ',' +
       FieldByName('Name').AsString);
    	Next;
    end;//of while
    Close;
  end;//of with

  CBShiiresakiName.DroppedDown := True;
end;

procedure TfrmShiireDaichou.CBShiiresakiNameExit(Sender: TObject);
var
 sCode : String;
begin
  //仕入先コードの取得
  if Pos(',',CBShiiresakiName.Text)>1 then begin
    sCode := Copy(CBShiiresakiName.Text, 1, Pos(',',CBShiiresakiName.Text)-1);
    EditShiiresakiCode.Text := sCode;
    EditShiiresakiCode.SetFocus;
    //CBShiiresakiName.SetFocus;
  end;
end;

//表示ボタンがクリックされた
procedure TfrmShiireDaichou.BitBtn3Click(Sender: TObject);
begin
  GdTotalTax := 0;
  GShiiresakiCode := '';
  PrintSG;
end;

//要約表示ボタンがクリックされた
procedure TfrmShiireDaichou.BitBtn4Click(Sender: TObject);
begin
  PrintSG2;
end;

procedure TfrmShiireDaichou.PrintSG;
var
  sSql, sTokuisakiCode, sDateTO:String;
  sSDenpyouCode, sSDenpyouCode2, sDateFrom, sCode1, sCode2 : String;
  iZandaka ,sShiireKakaku: Integer;
  dZandaka : Double;
  wYyyy, wMm, wDd:Word;
const
  //2006.05.28
  CsDateFrom = '2006/03/01'; //残高計算対象期間
begin
  ClearSG;

  //表示期間の前日までの残高を求める
  if EditShiiresakiCode.Text = '' then begin
    ShowMessage('仕入先コードが空白です');
    exit;
  end;
  //iZandaka := GetZandaka(EditShiiresakiCode.Text, CsDateFrom, DateToStr(DTPFrom.Date - 1));
    dZandaka := GetZandaka(EditShiiresakiCode.Text, CsDateFrom, DateToStr(DTPFrom.Date - 1));


  with SG1 do begin
    GiRow := 1;
    Cells[CintDate, GiRow] := DateToStr(DTPFrom.Date - 1);
    //Cells[CintZandaka, GiRow] := IntoStr(iZandaka);
    Cells[CintZandaka, GiRow] := FloatToStr(dZandaka);
    Cells[CintItem, GiRow] := '繰越残高';
  end;
  //ItemCodeの取得
  if lbSID.Caption = '' then lbSID.Caption := '0';

  if lbSID.Caption <> '0' then begin
    sSql := 'SELECT Code1, Code2 ';
    sSql := sSql + ' FROM ';
    sSql := sSql + CtblMItem ;
    sSql := sSql + ' WHERE ';
    sSql := sSql + ' SID= ' +  lbSID.Caption;
    with QuerySG do begin
      Close;
      Sql.Clear;
      Sql.Add(sSql);
      Open;
      if not EOF then begin
        sCode1 := FieldByName('Code1').asString;
        sCode2 := FieldByName('Code2').asString;
      end else begin
        ShowMessage('商品コードが見つかりません');
        exit;
      end;
      Close;
    end;//of with
  end;//of if


  //表示情報の取得
  sSql := 'SELECT * ';
  sSql := sSql + ' FROM ';
  sSql := sSql + CtblTShiireDetail ;
  sSql := sSql + ' WHERE ';
  sSql := sSql + 'ShiiresakiCode = ' +  EditShiiresakiCode.Text;
  sSql := sSql + ' AND ';
  sSql := sSql + 'NouhinDate Between ';
  sSql := sSql + '''' + DateToStr(DTPFrom.Date) + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + '''' + DateToStr(DTPTo.Date) + '''';

  if sCode1<>'' then begin
    sSql := sSql + ' AND ';
    sSql := sSql + ' Code1=''' + sCode1 + '''';
    sSql := sSql + ' AND ';
    sSql := sSql + ' Code2=''' + sCode2 + '''';
    Showmessage('商品を限定表示しますので、残高表示は無効です');
  end;

  sSql := sSql + ' ORDER BY NouhinDate, ID';

  GiTax := 0;
  GiRow := GiRow + 1;
  GiKungakuSum := dZandaka;
  GiShoukei := 0;
  with QuerySG do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    sSDenpyouCode  := FieldByName('SDenpyouCode').asString;
    sSDenpyouCode2 := FieldByName('SDenpyouCode').asString;
    While not EOF do begin
      if sSDenpyouCode = sSDenpyouCode2 then begin
        MakeSG2(QuerySG);
        Next;
        sSDenpyouCode2 := FieldByName('SDenpyouCode').asString;
      end else begin //小計の行を追加
        if SG1.Cells[CintFlag, GiRow-1] = '0' then begin
          SG1.Cells[CintItem, GiRow]          := '##小計##';
          SG1.Cells[CintShiireKingaku, GiRow] := CurrToStr(GiShoukei);
          SG1.Cells[CintFlag, GiRow]          := '2';
          //SG1.Cells[CintZandaka, GiRow] := CurrToStr(GiKungakuSum); //comment out by utada
          SG1.Cells[CintZandaka, GiRow] := CurrToStr(GiKungakuSum - GdTotalTax);  // add utada

        end;//of if
        GiShoukei := 0;
        sSDenpyouCode := sSDenpyouCode2
      end;
      GiRow := GiRow + 1;
    end;
    Close;
  end;//of with

  //最後の行に小計の行をつける
  if SG1.Cells[CintFlag, GiRow-1] = '0' then begin
    SG1.Cells[CintItem, GiRow]          := '##小計##';
    SG1.Cells[CintShiireKingaku, GiRow] := CurrToStr(GiShoukei);
    SG1.Cells[CintFlag, GiRow]          := '2';
    //SG1.Cells[CintZandaka, GiRow] := CurrToStr(GiKungakuSum);
    SG1.Cells[CintZandaka, GiRow] := CurrToStr(GiKungakuSum- GdTotalTax);
  end;//of if

end;

//表示期間の前日までの残高を求める
function TfrmShiireDaichou.GetZandaka(sTokuisakiCode, FromDate, ToDate:string) : Double;
var
  sSql : String;
  i : double;
  t : double;
begin
  sSql := 'Select s=sum(Shoukei) from ' + CtblTShiireDetail;
  sSql := sSql + ' where ';
  sSql := sSql + ' ShiiresakiCode = ' + sTokuisakiCode;
  sSql := sSql + ' and ';
  sSql := sSql + ' NouhinDate between ''' + FromDate + ''' and ''' + ToDate + '''';

  with QueryZan do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    i := FieldByName('s').asFloat; //.asInteger;
    Close;
  end; //of with

  t := GetTotalTax(sTokuisakiCode, FromDate, ToDate);

  Result := i - t;

end;

function TfrmShiireDaichou.GetTotalTax(sTokuisakiCode, FromDate, ToDate:string) : Double;
var
  sSql : String;
  t : double;
begin
    sSql := 'Select Tax =sum(Shoukei) from ' + CtblTShiireDetail;
    sSql := sSql + ' where ';
    sSql := sSql + ' ShiiresakiCode = ' + sTokuisakiCode;
    sSql := sSql + ' and ';
    sSql := sSql + '  ShiireShiharaiFlg=3';
    sSql := sSql + ' and ';
    sSql := sSql + ' NouhinDate <= ''' + ToDate + '''';

  with QueryZan do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    t := FieldByName('Tax').asFloat;
    Close;
  end; //of with

  Result := t;

end;



//グリッドのコンテンツ作成
//詳細表示の場合
procedure TfrmShiireDaichou.MakeSG2(myQuery : TQuery);
var
 sNouhinDate,sItemName, sSuuryou, sShiireKingaku, sShoukei, sShiireKakaku : String;
 sFlag, sShiireShiharaiFlg, sCode1, sCode2, sWhere: String;
 sDenpyouCode, sBikou : String;
 iTotalTax :Integer;
begin
  with SG1 do begin
    GiKungakuSum := GiKungakuSum + myQuery.FieldByName('Shoukei').AsFloat;
    sNouhinDate := myQuery.FieldByName('NouhinDate').AsString;
    sCode1 := myQuery.FieldByName('Code1').AsString;
    sCode2 := myQuery.FieldByName('Code2').AsString;
    sShiireShiharaiFlg := myQuery.FieldByName('ShiireShiharaiFlg').AsString;

    //商品名をゲット
    sWhere := ' Code1 = ''' + sCode1 + '''';
    sWhere := sWhere + ' and Code2 = ''' + sCode2 + '''';
    sItemName := DMMaster.GetFieldData(CtblMItem, 'Name', sWhere);
    sItemName := sItemName + ' ' + DMMaster.GetFieldData(CtblMItem, 'Irisuu', sWhere);
    //Add utada 2007.07.25
    //sShiireKakaku := DMMaster.GetFieldData(CtblMItem, 'ShiireKakaku', sWhere);

    sSuuryou := myQuery.FieldByName('Suuryou').AsString;
    sShiireKingaku := myQuery.FieldByName('ShiireKingaku').AsString;   //mode before 2007.07.25 utada
     sShoukei := myQuery.FieldByName('Shoukei').AsString;
    sFlag := myQuery.FieldByName('ShiireShiharaiFlg').AsString;
    sDenpyouCode  := myQuery.FieldByName('SDenpyouCode').AsString;
    sBikou := trim(myQuery.FieldByName('Memo').AsString);

    //2006.05.04
    GiShoukei := GiShoukei + StrToCurr(sShoukei);




    Cells[CintDate, GiRow] := sNouhinDate;
    Cells[CintItem, GiRow] := sItemName;
    Cells[CintSuuryou, GiRow] := sSuuryou;
    Cells[CintTanka, GiRow] := sShiireKingaku;  //mode before 2007.07.25 utada
    //Cells[CintTanka, GiRow] := sShiireKakaku;     // mod After 2007.07.25 utada


    //2006.09.05
    Cells[CintBikou, GiRow] := sBikou;


    if sShiireShiharaiFlg = '0' then begin
      Cells[CintShiireKingaku, GiRow] := sShoukei;
      //2006.05.10
      //Cells[CintZandaka, GiRow] := CurrToStr(GiKungakuSum);
      Cells[CintFlag, GiRow] := sFlag;
      Cells[CintDenpyouCode, GiRow] := sDenpyouCode;
      Cells[CintBikou, GiRow] := sBikou;
    end else if sShiireShiharaiFlg = '1' then begin  //支払いの場合
      sShoukei := IntToStr(StrToInt(sShoukei)*-1); //マイナスの反転
      Cells[CintShiireKingaku+1, GiRow] := sShoukei;
      Cells[CintSuuryou, GiRow] := '';
      Cells[CintTanka, GiRow] := '';
      Cells[CintZandaka, GiRow] := CurrToStr(GiKungakuSum); //mode before 2007.08.25 utada
      //Cells[CintZandaka, GiRow] := CurrToStr(GiKungakuSum - GdTotalTax); // mod After 2007.08.25 utada
      Cells[CintFlag, GiRow] := sFlag;
      Cells[CintDenpyouCode, GiRow] := sDenpyouCode;
      Cells[CintBikou, GiRow] := sBikou;
      GiRow := GiRow -1;
    end else if sShiireShiharaiFlg = '9' then begin  //消費税の場合


      //2006.11.12
      //日付、品名、　　数量、単価、　　　仕入れ金額、支払い金額、残高
      //日付、#消費税#、NUL 、税抜き金額、消費税額、　合計金額、　残高

      Cells[CintShiireKingaku, GiRow] := sShoukei;   //消費税の額
      Cells[CintSuuryou, GiRow] := '';
      //Cells[CintTanka, GiRow] := '';
      Cells[CintTanka, GiRow] := sShiireKingaku;     //税抜き金額
      //追加
      Cells[CintShiharaiKingaku, GiRow] := sSuuryou; //税込み月合計


      //Cells[CintZandaka, GiRow] := CurrToStr(GiKungakuSum); //Comment out by utada.
      Cells[CintZandaka, GiRow] := CurrToStr(GiKungakuSum - GdTotalTax);  //Add utada
      Cells[CintFlag, GiRow] := sFlag;
      Cells[CintDenpyouCode, GiRow] := sDenpyouCode;
      Cells[CintBikou, GiRow] := sBikou;

      GiRow := GiRow -1;
    end else if sShiireShiharaiFlg = '3' then begin  //消費税調整の場合

      //add utada
      GdTotalTax := GetTotalTax(EditShiiresakiCode.Text, DateToStr(DTPFrom.Date), DateToStr(DTPTo.Date));


      Cells[CintShiireKingaku, GiRow] := sShoukei;
      Cells[CintSuuryou, GiRow] := '';
      Cells[CintTanka, GiRow] := '';
      //Cells[CintZandaka, GiRow] := CurrToStr(GiKungakuSum);  Comment out by utada.
      Cells[CintZandaka, GiRow] := CurrToStr(GiKungakuSum - GdTotalTax);  //Add utada
      Cells[CintFlag, GiRow] := sFlag;
      Cells[CintDenpyouCode, GiRow] := sDenpyouCode;
      Cells[CintBikou, GiRow] := sBikou;
      GiRow := GiRow -1;
    end;



  end; //of with
end;

procedure TfrmShiireDaichou.PrintSG2;
var
	sSql, sTokuisakiCode, sDateTO:String;
  sDateFrom : String;
  iZandaka : Integer;
  wYyyy, wMm, wDd:Word;
const
	CsDateFrom = '2006/04/01'; //残高計算対象期間
begin
{
  ClearSG;

	//表示情報の取得
	sSql := 'SELECT * ';
  sSql := sSql + ' FROM ';
  sSql := sSql + CtblTShiireDetail ;
  sSql := sSql + ' WHERE ';
  sSql := sSql + 'ShiiresakiCode = ' +  EditShiiresakiCode.Text;
  sSql := sSql + ' AND ';
  sSql := sSql + 'DDate Between ';
  sSql := sSql + '''' + DateToStr(DTPFrom.Date) + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + '''' + DateToStr(DTPTo.Date) + '''';
  sSql := sSql + ' ORDER BY DDate, ID';

  GiRow := 1;
  GiKungakuSum := 0;
  GiTax := 0;

  //表示期間の前日までの残高を求める
  //iZandaka := GetZandaka(EditTokuisakiCode.Text, CsDateFrom, DateToStr(DTPFrom.Date - 1));

  GiRow := GiRow + 1;
  GiKungakuSum := iZandaka;

  with QuerySG do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    While not EOF do begin
      MakeSG2(QuerySG);
      Next;
      GiRow := GiRow + 1;
    end;
    Close;
  end;//of with
}
end;
//グリッドのコンテンツ作成
//要約表示の場合
procedure TfrmShiireDaichou.MakeSG3(myQuery : TQuery);
var
	iDenpyouKingaku, iTax,iSum, iRuikei : Integer;
  wYyyy, wMm, wDd : Word;
  sMemo, sYyyyMmDdTo, sYyyyMmDdFrom, sDate, sGatsbun:String;
  iKaisuuHouhou : Integer;
begin
{
	//締め日の比較
  //DecodeDate(StrToDate(myQuery.FieldByName('DDate').AsString), wYyyy, wMm, wDd);

  //売上金額の取得
  iDenpyouKingaku := myQuery.FieldByName('DenpyouKingaku').AsInteger;
  iTax            := myQuery.FieldByName('Tax').AsInteger;

	with SG1 do begin
  	sDate := myQuery.FieldByName('DDate').AsString;
  	sMemo := myQuery.FieldByName('Memo').AsString;
    sMemo := Trim(sMemo);

  	Cells[CintDate, GiRow] := sDate;
  	Cells[CintMemo, GiRow] := sMemo;
  	Cells[CintDate, GiRow] := Copy(sDate, 3, 10);

    GiKungakuSum := GiKungakuSum + iKingaku;

    //1999/09/14追加
    sGatsbun := myQuery.FieldByName('Gatsubun').AsString;
	 	Cells[CintGatsubun, GiRow] := Copy(sGatsbun, 1, 7);

    //通常の売上伝票
    if (iKingaku > 0) AND (iFlag = 0) then begin
      //2005.10.11
      if iKaisuuHouhou > 3 then begin //返金伝票など
        Cells[CintUkeKingaku, GiRow] := FormatFloat('0,', iKingaku);//３桁ごとに区切って表示
        Cells[CintZandakan, GiRow] := FormatFloat('0,', GiKungakuSum);//３桁ごとに区切って表示
      end else begin //通常の伝票
	  	  Cells[CintUriKingaku, GiRow] := FormatFloat('0,', iKingaku);//３桁ごとに区切って表示
      end;
      //GiKungakuSum := GiKungakuSum + iKingaku;

      //1999/06/01　復活
      if ((Cells[CintUkeKingaku, GiRow] <> '') or
         (Cells[CintMemo, GiRow] = CsCTAX)) and
         (iKaisuuHouhou < 4) then begin
		  	Cells[CintTougetsu, GiRow] := FormatFloat('0,', GiKungakuSum);//３桁ごとに区切って表示
      end;

    end else if (iKingaku > 0) AND (iFlag = 1) then begin
	    if (Cells[CintMemo, GiRow] <> CsCTAX) then begin
      	if Copy(Cells[CintMemo, GiRow], 1, 4) = '返金' then begin //2000/03/03　Added
		 			Cells[CintUkeKingaku, GiRow] := FormatFloat('0,', iKingaku);
	  			Cells[CintTougetsu, GiRow] := FormatFloat('0,', GiKungakuSum);

        //2005.10.11
        end else if iKaisuuHouhou > 3 then begin //返金伝票など
          Cells[CintUkeKingaku, GiRow] := FormatFloat('0,', iKingaku);
          Cells[CintZandakan, GiRow] := FormatFloat('0,', GiKungakuSum);//３桁ごとに区切って表示

  	    end else begin
		    	GiRow := GiRow - 1;
  	      Exit;
        end;
      end;
    end else if (iKingaku <= 0)then begin
      //返品・値引の場合
      if (Copy(Cells[CintMemo, GiRow], 1, 4) = '返品') or
         (Copy(Cells[CintMemo, GiRow], 1, 4) = '値引') then begin

        Cells[CintUriKingaku, GiRow] := FormatFloat('0,', iKingaku);
	      //GiKungakuSum := GiKungakuSum + iKingaku;
      //2005.10.11
      end else if iKaisuuHouhou > 3 then begin //返金伝票など
        Cells[CintUriKingaku, GiRow] := FormatFloat('0,', iKingaku);
      end else begin
      	//2001/03/15
        if sMemo = '##消費税##' then begin
		  		Cells[CintUkeKingaku, GiRow] := '';
          GiKungakuSum := GiKungakuSum - iKingaku;
	  			Cells[CintTougetsu, GiRow] := FormatFloat('0,', GiKungakuSum);
        end else begin
		      //受け入れ金額の場合
	  			Cells[CintUkeKingaku, GiRow] := FormatFloat('0,', iKingaku);
		      //1999/06/01　復活
  		    //GiKungakuSum := GiKungakuSum + iKingaku;
	  			//Cells[CintTougetsu, GiRow] := FormatFloat('0,', GiKungakuSum);

          //2001/07/19
     	  	Cells[CintZandakan, GiRow] := FormatFloat('0,', GiKungakuSum);//３桁ごとに区切って表示

        end;
      end;//of if
    end;

    //消費税の行だったら当月の税抜き合計の残を計算して表示する
    //1999/06/01　累計を表示する
    if Cells[CintMemo, GiRow] = CsCTAX then begin
	    sYyyyMmDdTo := Cells[CintDate, GiRow];
      DecodeDate(StrToDate(sYyyyMmDdTo), wyyyy, Wmm, wDd);

      if wDd >= 28 then begin //月末ならば
      	wDd := 1;
      end else begin
	      wDd := wDd + 1;
	      wMm := wMm - 1;
  	    if wMm = 0 then begin
    	  	wMm := 12;
      	  wYyyy := wYyyy-1;
	      end;
      end;

      sYyyyMmDdFrom := DateToStr(EncodeDate(wYyyy, wMm, wDd));
	    iRuikei := TKaishuu.GetZandaka(EditTokuisakiCode.Text, sYyyyMmDdFrom, sYyyyMmDdTo);

      //2001/10/12 added
      if (iKingaku<0) then begin
  	  	Cells[CintUriKingaku, GiRow] := FormatFloat('0,', iRuikei);//３桁ごとに区切って表示
        GiKungakuSum := GiKungakuSum + iKingaku;
        iRuikei := iRuikei + iKingaku
      end else begin
  	  	Cells[CintUriKingaku, GiRow] := FormatFloat('0,', iRuikei-iKingaku);//３桁ごとに区切って表示
      end;
	  	Cells[CintSasihiki, GiRow] := FormatFloat('0,', iRuikei);
	  	Cells[CintTougetsu, GiRow] := FormatFloat('0,', GiKungakuSum);//３桁ごとに区切って表示

	  	Cells[CintUriKingaku2, GiRow] := FormatFloat('0,', iRuikei);//３桁ごとに区切って表示
	  	Cells[CintCTax, GiRow] := FormatFloat('0,', iKingaku);//３桁ごとに区切って表示

      //残高表示　2001/07/26追加
	  	//Cells[CintZandakan, GiRow] := FormatFloat('0,', iRuikei);//３桁ごとに区切って表示
	  	Cells[CintZandakan, GiRow] := FormatFloat('0,', GiKungakuSum);//３桁ごとに区切って表示
    end;
  end;
}
end;



//グリッドのクリア
procedure TfrmShiireDaichou.ClearSG;
var
  iCol, iRow : Integer;
begin
  with SG1 do begin
  	for iCol := 0 to ColCount do begin
    	for iRow := 1 to RowCount do begin
	      Cells[iCol, iRow] := '';
      end;
    end;

  end;
end;


procedure TfrmShiireDaichou.CBItemNameKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key=VK_F1) then begin
    MakeCBName('Yomi', CBItemName.Text, 1);
  end;
end;
{
	仕入先名のコンボボックスを作成する
  引数：sKey   -> 検索列名
  			sWord  -> 検索ワード
  			iKubun -> 0=前方一致, 1=曖昧検索, 2=数字の完全一致
}
procedure TfrmShiireDaichou.MakeCBName(sKey, sWord:String; iKubun:Integer);
var
	sSql : String;
begin

  CBItemName.Items.Clear;


	sSql := 'SELECT * FROM ' + CtblMItem;
  sSql := sSql + ' WHERE ';
  sSql := sSql + sKey;
  case iKubun of
  0: sSql := sSql + ' LIKE ''' + sWord + '%''';
  1: sSql := sSql + ' LIKE ''%' + sWord + '%''';
  2: sSql := sSql + ' = ' + sWord;
  end;

  sSql := sSql + ' ORDER BY Yomi ';

	with QueryItem do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	CBItemName.Items.Add(FieldByName('SID').AsString +
       ',' + FieldByName('Name').AsString +
       ',' + FieldByName('Irisuu').AsString +
       ',' + FieldByName('Kikaku').AsString);
    	Next;
    end;//of while
    Close;
  end;//of with

  CBItemName.DroppedDown := True;
end;


procedure TfrmShiireDaichou.CBItemNameExit(Sender: TObject);
var
 sid : string;
begin
//SIDを取得
  if (pos(',', CBItemName.Text)>0) then begin
    sid := Copy(CBItemName.Text, 1, (pos(',', CBItemName.Text)-1));
  end;
  lbSID.Caption := sid;
end;

procedure TfrmShiireDaichou.FormActivate(Sender: TObject);
begin
  EditShiiresakiCode.SetFocus;
  EditShiiresakiCode.Text := GShiiresakiCode;
  CBShiiresakiName.SetFocus;
end;

procedure TfrmShiireDaichou.SG1DrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);

type
  TVowels = set of char;
var
  DRect:  TRect;  Mode: Integer;
  Vowels: TVowels;
  C : LongInt;
  myRect : TRect;
  iCol : Integer;
  sTmp : String;
begin
  if not (gdFixed in state)  then begin

    Vowels := [char(CintTanka),
               char(CintShiireKingaku),
               char(CintShiharaiKingaku),
               char(CintZandaka)];
    SG1.Canvas.FillRect(Rect);
    DRect.Top := Rect.Top + 2;
    DRect.Left := Rect.Left + 2;
    DRect.Right := Rect.Right - 2;
    DRect.Bottom := Rect.Bottom - 2;

    if  char(ACol) IN Vowels then begin       //右寄せ
      Mode := DT_RIGHT;
    end else begin
      Mode := DT_LEFT;
    end;

    //else if Col = 2 then   //左寄せ
    //	Mode := DT_LEFT
    //else    Mode := DT_CENTER;   //中央

    if ACol = CintFlag then begin
      sTmp := Trim(SG1.Cells[CintFlag, ARow]);
    end;

    //消費税の行は色をつける
    if sTmp = '9' then begin
      IdentToColor('clRed', C);
      SG1.Canvas.Brush.Color := C;
      SG1.Canvas.Font.Color := $FFFFFF - C;

      for iCol := 0 to CintEnd-1 do begin
        myRect := SG1.CellRect(iCol, ARow);
        SG1.Canvas.FillRect(myRect);
        DrawText(SG1.Canvas.Handle,
                  PChar(SG1.Cells[iCol, ARow]),
                  Length(SG1.Cells[iCol, ARow]),
                  myRect,
                  DT_CENTER or DT_VCENTER or DT_SINGLELINE);
      end;//of for
      Exit;
    end;

    //if sTmp = CsCTAX then begin
    if sTmp = '2' then begin
      IdentToColor('clBlue', C);
      SG1.Canvas.Brush.Color := C;
      SG1.Canvas.Font.Color := $FFFFFF - C;

      for iCol := 0 to CintEnd-1 do begin
        myRect := SG1.CellRect(iCol, ARow);
        SG1.Canvas.FillRect(myRect);
        DrawText(SG1.Canvas.Handle,
                  PChar(SG1.Cells[iCol, ARow]),
                  Length(SG1.Cells[iCol, ARow]),
                  myRect,
                  DT_CENTER or DT_VCENTER or DT_SINGLELINE);
      end;//of for
      Exit;
    end;

    //回収の行も色をつける
    if sTmp = '1' then begin
      IdentToColor('clTeal', C);
      SG1.Canvas.Brush.Color := C;
      SG1.Canvas.Font.Color := $FFFFFF - C;

      for iCol := 0 to CintEnd - 1 do begin
        myRect := SG1.CellRect(iCol, ARow);
        SG1.Canvas.FillRect(myRect);
        DrawText(SG1.Canvas.Handle,
                  PChar(SG1.Cells[iCol, ARow]),
                  Length(SG1.Cells[iCol, ARow]),
                  myRect,
                  DT_CENTER or DT_VCENTER or DT_SINGLELINE);
      end;//of for
      Exit;
    end;

    //消費税調整
    if sTmp = '3' then begin
      IdentToColor('clGreen', C);
      SG1.Canvas.Brush.Color := C;
      SG1.Canvas.Font.Color := $FFFFFF - C;

      for iCol := 0 to CintEnd - 1 do begin
        myRect := SG1.CellRect(iCol, ARow);
        SG1.Canvas.FillRect(myRect);
        DrawText(SG1.Canvas.Handle,
                  PChar(SG1.Cells[iCol, ARow]),
                  Length(SG1.Cells[iCol, ARow]),
                  myRect,
                  DT_CENTER or DT_VCENTER or DT_SINGLELINE);
      end;//of for
      Exit;
    end;

    DrawText(SG1.Canvas.Handle, PChar(SG1.Cells[ACol,ARow]),
              Length(SG1.Cells[ACol,ARow]), DRect, Mode);


  end;//of if
end;

procedure TfrmShiireDaichou.SG1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  SG1.Repaint;

end;

procedure TfrmShiireDaichou.SG1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  SG1.Repaint;
end;
//伝票の削除
procedure TfrmShiireDaichou.Button1Click(Sender: TObject);
var
 sSql : String;
begin
  if edDenpyouCode.Text = '' then begin
   ShowMessage('伝票番号を入力してください。');
   exit;
  end;
  dlgPasswordDlg := TdlgPasswordDlg.Create(Self);
  if dlgPasswordDlg.ShowModal = mrOK then begin
  end else begin
    Exit;
  end;

  if GPassWord = CPassword1 then begin
    Beep;
  end else if GPassWord = CPassword2 then begin
    Beep;
  end else if GPassWord = CPassword3 then begin
    ShowMessage('PassWordが違います');
    exit;
  end else begin
    ShowMessage('PassWordが違います');
    exit;
  end;
  GPassWord := '';
  if MessageDlg('伝票削除しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;

  sSql := 'Delete from ' + CtblTShiireDetail;
  sSql := sSql + ' where SDenpyouCode=''' + edDenpyouCode.Text + '''';
  with QueryDel do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    ExecSql;
    Close;
  end;

  sSql := 'Delete from ' + CtblTShiire;
  sSql := sSql + ' where SDenpyouCode=''' + edDenpyouCode.Text + '''';
  with QueryDel do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    ExecSql;
    Close;
  end;
  ShowMessage('伝票を削除しました。');
  PrintSG;

end;

procedure TfrmShiireDaichou.FormResize(Sender: TObject);
begin
  inherited;
  SG1.Align := alClient;
end;

procedure TfrmShiireDaichou.Button2Click(Sender: TObject);
var
 sSql,SDenpyouCode : String;
 sSql2,SDenpyouCode2 : String;
 cuSum, cuTax : Currency;
 ShiiresakiCode, ShiireShiharaiFlg : String;
begin
  inherited;
//消費税の行を追加する。
  sSql := 'SELECT * FROM tblTShiireDetail ORDER BY SDenpyouCode, ShiireShiharaiFlg';
  with Query1 do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    SDenpyouCode  := FieldByName('SDenpyouCode').AsString;
    SDenpyouCode2 := FieldByName('SDenpyouCode').AsString;
    cuSum := 0;
    while not EOF do begin
      if SDenpyouCode = SDenpyouCode2 then begin
        cuSum := cuSum + FieldByName('Shoukei').AsCurrency;
        next;
        SDenpyouCode2 := FieldByName('SDenpyouCode').AsString;
      end else begin
        ShiireShiharaiFlg := FieldByName('ShiireShiharaiFlg').AsString;
        if ShiireShiharaiFlg = '0' then begin
          SDenpyouCode := FieldByName('SDenpyouCode').AsString;
          ShiiresakiCode := FieldByName('ShiiresakiCode').AsString;
          AddTaxLine(SDenpyouCode,ShiiresakiCode,cuSum);
        end;
        SDenpyouCode := FieldByName('SDenpyouCode').AsString;
      end;
    end;//of while
    Close;
  end;//of with
end;

Procedure TfrmShiireDaichou.AddTaxLine(sSDenpyouCode,sShiiresakiCode:String; cuSum:Currency);
var
  sSql : String;
begin
  sSql := 'Insert into tblTShiireDetail ';
  sSql := sSql + ' (';
  sSql := sSql + ' SDenpyouCode, ';
  sSql := sSql + ' ShiiresakiCode,';
  sSql := sSql + ' NouhinDate,';
  sSql := sSql + ' NyuuryokuDate,';
  sSql := sSql + ' Hyoujijun,';
  sSql := sSql + ' Code1,';
  sSql := sSql + ' Code2,';
  sSql := sSql + ' Suuryou,';
  sSql := sSql + ' ShiireKingaku,';
  sSql := sSql + ' Shoukei,';
  sSql := sSql + ' UpdateDate,';
  sSql := sSql + ' ShiireShiharaiFlg,';
  sSql := sSql + ' Gatsubun,';
  sSql := sSql + ' Memo,';
  sSql := sSql + ')';
  sSql := sSql + ' VALUES ';
  sSql := sSql + ' (';
  sSql := sSql + '''' + sSDenpyouCode   + ''',' ;
  sSql := sSql + '''' + sShiiresakiCode   + ''',' ;

  sSql := sSql + ' )';

  with Query2 do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    ExecSql;
    Close;
  end;
end;

//仕入れ台帳をエクセルにコンバートする
//2006.09.01 hkubota
procedure TfrmShiireDaichou.BitBtn2Click(Sender: TObject);
var
  CFileName_ShiDaichou : String;
  sLine : String;
  sDate, sItem, sSuuryou, sTanka, sShiireKingaku: String;
  sShiharaiKingaku, sZandaka : String;
  sDenpyouCode,  sBikou,  sFlag : String;

  iRow : Integer;
  F : TextFile;
begin
  //出力先TextFileの名前セット
  CFileName_ShiDaichou := 'ShiireDaichou.txt';

	//確認メッセージ
  if MessageDlg('印刷しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;

	//スタートトランザクション

	//出力するファイルを作成する、すでにあれば削除する
  //請求明細用
 	AssignFile(F, CFileName_ShiDaichou);
  Rewrite(F);
	CloseFile(F);

  //基本情報の取得
  Screen.Cursor := crHourGlass;

  sLine := CBShiiresakiName.Text;
  //sLine := sLine + ',(' + EditShiharaijouken.Text + ')';
  HMakeFile(CFileName_ShiDaichou, sLine);




  sLine := '日付,品名,数量,単価,仕入金額,支払金額,残高,伝票番号,備考,Flag';
  HMakeFile(CFileName_ShiDaichou, sLine);

  with SG1 do begin
  	for iRow := 1 to RowCount do begin
		  sDate        := Cells[CintDate, iRow];
		  sItem        := Cells[CintItem, iRow];
      if (sDate = '') AND (sItem = '') then begin
      	Break;
      end;
      sSuuryou  := Cells[CintSuuryou, iRow];
      sTanka    := Cells[CintTanka, iRow];
      sShiireKingaku := Cells[CintShiireKingaku, iRow];
      sShiharaiKingaku := Cells[CintShiharaiKingaku, iRow];
      sZandaka := Cells[CintZandaka, iRow];
      sDenpyouCode := Cells[CintDenpyouCode, iRow];
      if sDenpyouCode <> '' then begin
        sDenpyouCode := 'No.' + sDenpyouCode;
      end;
      sBikou := Cells[CintBikou, iRow];
      sFlag := Cells[CintFlag, iRow];

		  sLine := '"' + sDate + '","' + sItem + '","' + sSuuryou + '","' +
               sTanka + '","' +  sShiireKingaku + '","' + sShiharaiKingaku + '","' +
               sZandaka  + '","' +
               sDenpyouCode  + '","' +
               sBikou  + '","' +
               sFlag  + '"';
               
 		  HMakeFile(CFileName_ShiDaichou, sLine);
    end;//of for
  end;//of with
  Screen.Cursor := crDefault;

	//エンドトランザクション
  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '仕入台帳.xls', '', SW_SHOW);
end;

end.



