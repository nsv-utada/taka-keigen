unit PEstimate;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, StdCtrls, Buttons, ExtCtrls, Grids, ComCtrls, ShellAPI;

type
  TfrmPEstimate = class(TfrmMaster)
    BitBtn3: TBitBtn;
    SGEstimate: TStringGrid;
    Panel4: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    CBShiharaibiD: TComboBox;
    CBShiharaibiM: TComboBox;
    BitBtn5: TBitBtn;
    CBYyyy: TComboBox;
    Label6: TLabel;
    BitBtn4: TBitBtn;
    EditTokuisakiCode1: TEdit;
    Label5: TLabel;
    BitBtn6: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
  private
    { Private 宣言 }
		procedure MakeSG;
		//procedure PutUriageKingaku(sTokuisakiCode1, sMm : String);
		procedure PutUriageKingaku(sTokuisakiCode1: String; iYyyy, iMm : Integer);
		procedure PutUriageKingaku2(sTokuisakiCode1: String; iYyyy, iMm : Integer);
		procedure ClearGrid;
  public
    { Public 宣言 }
  end;

  const
	//グリッドの列の定義
  CintIndex         = 0;
  CintTokuisakiCode = 1;
  CintTokuisakiName = 2;
  CintMm            = 3;
  CintUriKingaku    = 4;
  CintMemo          = 5;	//領収書発行メモ

  //Ver2.0 追加
  CintYomi          = 6;
  CintKa            = 7;
  CintShimebi       = 8;
  CintEnd           = 9;   //ターミネーター

	GRowCount = 3000; //行数

var
  frmPEstimate: TfrmPEstimate;

  GiRow : Integer;

	//function GetShiharaibiM(iMNumber, iMm : Integer):String;
	function GetShiharaibiM(var iMNumber, iMm, iYyyy : Integer):String;

implementation


uses
	DMMaster, Inter, MTokuisaki, HKLib;

{$R *.DFM}

procedure TfrmPEstimate.FormCreate(Sender: TObject);
var
  wYyyy, wMm, wDd      : Word;
  sYear, sMonth, sDay  : String;
begin
  inherited;
	Width := 700;
  Height := 540;

   // 年・月を当日の年・月に設定

   DecodeDate(Date(), wYyyy, wMm, wDd);
   sYear  := IntToStr(wYyyy);
   sMonth := IntToStr(wMm);
   sDay   := IntToStr(wDd);

   CBYyyy.Text := sYear;
   CBShiharaibiM.Text   := sMonth;

  //ストリンググリッドの作成
  MakeSG;

end;

procedure TfrmPEstimate.MakeSG;
begin
  with SGEstimate do begin
    DefaultDrawing := True;
    RowCount := GRowCount;
    ColCount := CintEnd;
    Align := alBottom;
    FixedCols := 0;
    FixedColor := clYellow;
    Canvas.Font.Size := 11;
    DefaultRowHeight := 20;
    ColWidths[0]:= 64;
    Font.Color := clNavy;


    ColWidths[CintIndex]:= 50;
    Cells[CintIndex, 0] := '';

    ColWidths[CintTokuisakiCode]:= 60;
    Cells[CintTokuisakiCode, 0] := 'コード';

    ColWidths[CintTokuisakiName]:= 200;
    Cells[CintTokuisakiName, 0] := '得意先名';

    ColWidths[CintMm]:= 80;
    Cells[CintMm, 0] := '月分';

    ColWidths[CintUriKingaku]:= 80;
    Cells[CintUriKingaku, 0] := '売上金額';

    ColWidths[CintMemo]:= 200;
    Cells[CintMemo, 0] := '領主書発行メモ';

//Ver2.0 2000/03/01
    ColWidths[CintYomi]:= 50;
    Cells[CintYomi, 0] := 'よみ';
    ColWidths[CintKa]:= 50;
    Cells[CintKa, 0] := '課';
    ColWidths[CintShimebi]:= 50;
    Cells[CintShimebi, 0] := '締日';

		//フォームの幅をあわせる
{
		frmPEstimate.Width := ColWidths[CintIndex] + ColWidths[CintTokuisakiCode] +
  	                    ColWidths[CintTokuisakiName] +ColWidths[CintMm] +
    	                  ColWidths[CintUriKingaku] + ColWidths[CintMemo]

}
  end;//of with
end;


//グリッドのクリア
procedure TfrmPEstimate.ClearGrid;
var
	i : Integer;
begin
  with SGEstimate do begin
  	for i := 1 to RowCount do begin
	    Cells[CintIndex, i] := '';
	    Cells[CintTokuisakiCode, i] := '';
	    Cells[CintTokuisakiName, i] := '';
	    Cells[CintMm, i] := '';
	    Cells[CintUriKingaku, i] := '';
	    Cells[CintMemo, i] := '';
    end;
  end;
end;

//表示ボタンがクリックされた
procedure TfrmPEstimate.BitBtn5Click(Sender: TObject);
var
	sTokuisakiName, sSql, sShiharaibiD, sYomi1, sYomi2, sShiharaibiM, sTokuisakiCode1, sMm : String;
  sEstimateMemo, sTokuisakiCode2 : String;
  iMNumber, iMm, iYyyy, i: Integer;
begin

  //グリッドのクリア
	ClearGrid;

	sShiharaibiD := Trim(Copy(CBShiharaibiD.Text, 1, 2));

  sSql := 'SELECT TokuisakiCode1, TokuisakiName, TokuisakiNameYomi, ShiharaibiM, EstimateMemo ';
  sSql := sSql + ', TokuisakiNameYomi, Ka, SeikyuuSimebi';
  sSql := sSql + ' FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' ShiharaibiD = ' + sShiharaibiD;
  sSql := sSql + ' AND ';
  sSql := sSql + ' ShiharaiKubun in (10,11) ';//現金・小切手

  if Length(EditTokuisakiCode1.Text) > 0 then begin
	  sSql := sSql + ' AND TokuisakiCode1 = ' + EditTokuisakiCode1.Text;
  end;

  sSql := sSql + ' ORDER BY TokuisakiNameYomi';

  GiRow := 1;
  with frmDMMaster.QueryEstimate do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
   	sYomi1 := Copy(FieldByName('TokuisakiNameYomi').AsString, 1, 2);
    sYomi2 := '';
  	while not EOF do begin
    	if sYomi1 <> sYomi2 then begin
	      SGEstimate.Cells[CintIndex, GiRow]:= sYomi1;
		   	sYomi2 := Copy(FieldByName('TokuisakiNameYomi').AsString, 1, 2);
      end;
      sTokuisakiName := FieldByName('TokuisakiName').AsString;
      sTokuisakiCode1 := FieldByName('TokuisakiCode1').AsString;
      sTokuisakiCode2 := FieldByName('TokuisakiCode2').AsString;
      sEstimateMemo := FieldByName('EstimateMemo').AsString;

      SGEstimate.Cells[CintTokuisakiName, GiRow]:= sTokuisakiName;
      SGEstimate.Cells[CintTokuisakiCode, GiRow]:= sTokuisakiCode1 + '-' + sTokuisakiCode2;
      SGEstimate.Cells[CintMemo, GiRow]:= sEstimateMemo;

      iMNumber := FieldByName('ShiharaibiM').AsInteger;
      iMm := StrToInt(CBShiharaibiM.Text);//iMmは何月分の請求金額を求めるか？
      iYyyy := StrToInt(CBYyyy.Text);
      sMm := GetShiharaibiM(iMNumber, iMm, iYyyy);
      if sMm = '' then begin
      	ShowMessage(sTokuisakiName + 'の支払い月が不正です -> ' + IntToStr(iMNumber));
        Next;
		   	sYomi1 := Copy(FieldByName('TokuisakiNameYomi').AsString, 1, 2);
        Continue;
      end;

      SGEstimate.Cells[CintMm, GiRow]:= sMm + '月分';


      //領収金額の計算と書き込み
      PutUriageKingaku(FieldByName('TokuisakiCode1').AsString, iYyyy, StrToInt(sMm));

    	Next;
      GiRow := GiRow + 1;
	   	sYomi1 := Copy(FieldByName('TokuisakiNameYomi').AsString, 1, 2);
    end;
    Close;
  end;

end;

//領収金額の計算と書き込み
procedure TfrmPEstimate.PutUriageKingaku(sTokuisakiCode1: String; iYyyy, iMm : Integer);
var
	sSum, sSql, sFrom, sTo, sYyyy, sDd : String;
  sToYyyy, sToMm, sToDd, sFromYyyy, sFromMm, sFromDd : String;
begin

  //締め日を求める
  sDd := MTokuisaki.GetShimebi(sTokuisakiCode1);
  if sDd = '0' then begin
  	sFromDd := '1';
  	sToDd := IntToStr(HKLib.GetGetsumatsu(iYyyy, iMm));
    sFromMm := IntToStr(iMm);
    sToMm := IntToStr(iMm);
    sFromYyyy := IntToStr(iYyyy);
    sToYyyy := IntToStr(iYyyy);
  end else begin
    sFromDd := IntToStr(StrToInt(sDd) + 1);
    sToDd := sDd;
    sToMm := IntToStr(iMm);
    iMm := iMm - 1;
    if iMm = 0 then begin
    	iMm := 12;
	    sFromMm := IntToStr(iMm-1);
	    sToYyyy := IntToStr(iYyyy);
      iYyyy := iYyyy-1;
	    sFromYyyy := IntToStr(iYyyy);
    end else begin;
	    sFromMm := IntToStr(iMm);
	    sFromYyyy := IntToStr(iYyyy);
  	  sToYyyy := IntToStr(iYyyy);
    end;
  end;

	//計算対象期間を求める
  //何月分を求める
	sTo   := sToYyyy   + '/' + sToMm   + '/' + sToDd;
  sFrom := sFromYyyy + '/' + sFromMm + '/' + sFromDd;

	sSql := 'SELECT esum = SUM(UriageKingaku) ';
  sSql := sSql + ' FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' DDate BETWEEN ';
  sSql := sSql + '''' + sFrom + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + '''' + sTo + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + ' TokuisakiCode1 =  ' + sTokuisakiCode1;
  sSql := sSql + ' AND ';
  //sSql := sSql + ' UriageKingaku > 0 ';
 // sSql := sSql + ' ((UriageKingaku > 0) OR (SUBSTRING(Memo,1, 2) IN (''返品'', ''値引'')))' ;
  sSql := sSql + ' ((UriageKingaku > 0) OR (SUBSTRING(Memo,1, 2) IN (''返品'', ''値引'', ''取消'')))' ;

  //1999/08/09
  sSql := sSql + ' AND ';
  sSql := sSql + ' PATINDEX(''%返金%'', Memo) = 0 ';

  with frmDMMaster.QueryEstimateSum do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    sSum := FieldByName('esum').AsString;
    if sSum = '' then begin
    	sSum := '0';
    end;
    sSum := FormatFloat('0,', StrToInt(sSum));
    SGEstimate.Cells[CintUriKingaku, GiRow]:= sSum;
    Close;
	end;//of with
end;


{
0 当月
1 翌月
2 翌々月
3 翌翌々月
}
//支払い月を求める
function GetShiharaibiM(var iMNumber, iMm, iYyyy : Integer):String;
begin
	case iMNumber of
  	0 : Result := IntToStr(iMm);
  	1 : begin //翌月分
    			iMm := iMm-1;
          if iMm = 0 then begin
          	iMm := 12;
            iYyyy := iYyyy-1;
          end;
          Result := IntToStr(iMm);
        end;
  	2 : begin //翌々月分
    			iMm := iMm-2;
          if iMm = -1 then begin
          	iMm := 11;
            iYyyy := iYyyy-1;
          end else if iMm = 0 then begin
          	iMm := 12;
            iYyyy := iYyyy-1;
          end;
          Result := IntToStr(iMm);
        end;
  	3 : begin
    			iMm := iMm-3;
          if iMm = -2 then begin
          	iMm := 10;
            iYyyy := iYyyy-1;
          end else if iMm = -1 then begin
          	iMm := 11;
            iYyyy := iYyyy-1;
          end else if iMm = 0 then begin
          	iMm := 12;
            iYyyy := iYyyy-1;
          end;
          Result := IntToStr(iMm);
        end;
    else
        result := '';
  end;//of case

end;



//印刷開始ボタンがクリックされた
procedure TfrmPEstimate.BitBtn3Click(Sender: TObject);
var
  sLine, sIndex, sTokuisakiCode, sTokuisakiCode1, sTokuisakiCode2,sTokuisakiName, sMm, sUriKingaku, sMemo:String;
  iRow : Integer;
 	F : TextFile;
  sYomi, sKa, sSeikyuuShimebi:String;
begin
	//確認メッセージ
  if MessageDlg('印刷しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;

	//スタートトランザクション
  Screen.Cursor := crHourGlass;

	//出力するファイルを作成する、すでにあれば削除する
  //請求明細用
 	AssignFile(F, CFileName_Estimate);
  Rewrite(F);
	CloseFile(F);
  //支払日
  if Copy(CBShiharaibiD.Text, 1, 1) = '0' then begin
	  sLine := CBYyyy.Text + ' 年　' + CBShiharaibiM.Text + ' 月 末日 支払日';
  end else begin
	  sLine := CBYyyy.Text + ' 年　' + CBShiharaibiM.Text + ' 月　' + CBShiharaibiD.Text + ' 支払日';
  end;

  HMakeFile(CFileName_Estimate, sLine);
  sLine := '';

  //基本情報の取得
  with SGEstimate do begin
  	for iRow := 1 to RowCount do begin
		  sIndex         := Cells[CintIndex, iRow];
		  sTokuisakiCode := Cells[CintTokuisakiCode, iRow];

      //2002.08.30
		  //sTokuisakiName := Cells[CintTokuisakiName, iRow];
      sTokuisakiCode1  := Copy(sTokuisakiCode, 1, Pos('-', sTokuisakiCode)-1);

      //2002.09.29
      if sTokuisakiCode1='' then begin
      	sTokuisakiCode1 := '0';
      end;
      if StrToInt(sTokuisakiCode1)>50000 then begin
      	Continue;
      end;

      sTokuisakiCode2 := Copy(sTokuisakiCode, Pos('-', sTokuisakiCode)+1, Length(sTokuisakiCode));
	  	sTokuisakiName := MTokuisaki.GetTokuisakiName(sTokuisakiCode1);

      if sTokuisakiName = '' then begin
      	Break;
      end;
		  sMm            := Cells[CintMm, iRow];
		  sUriKingaku    := Cells[CintUriKingaku, iRow];

		  sMemo          := Cells[CintMemo, iRow];
      sMemo := Trim(sMemo);
      if sMemo = '' then
      	sMemo := '　';

      //Ver2.0 2000/03/01
      sYomi           := Cells[CintYomi, iRow];

      sKa             := Cells[CintKa, iRow];
      if sKa = '' then begin
      	sKa := '　';
      end;

      sSeikyuuShimebi := Cells[CintShimebi, iRow];
      if sSeikyuuShimebi = '' then begin
      	sSeikyuuShimebi := '　';
      end;

		  sLine := sIndex + ',' + sTokuisakiCode1 + ',' + sTokuisakiCode2 + ',' + sTokuisakiName + ',' +
               sMm + ',"' +  sUriKingaku + '","' + sMemo + '"';

      //Ver2.0
      sLine := sLine + ',' + Trim(sYomi) + ',' + Trim(sKa) + ',' + Trim(sSeikyuuShimebi);

      //2004.07.07
      //得意先コードが０のものは表示しない
      if sTokuisakiCode1 = '0' then begin
 		  end else begin
        HMakeFile(CFileName_Estimate, sLine);
      end;
    end;//of for
  end;//of with

	//エンドトランザクション
  Screen.Cursor := crDefault;

  //エクセルの起動
  //2006.11.07 hkubota
  //ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '領収書発行補助画面.xls', '', SW_SHOW);
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '領収書発行補助画面2.xls', '', SW_SHOW);

end;

//表示ボタン２がクリックされた
//1999/09/25
procedure TfrmPEstimate.BitBtn4Click(Sender: TObject);
var
	sTokuisakiName, sSql, sShiharaibiD, sYomi1, sYomi2, sShiharaibiM, sTokuisakiCode1, sMm : String;
  sEstimateMemo, sGatsubunMin : String;
  iMNumber, iMm, iYyyy, i: Integer;
begin

  //グリッドのクリア
	ClearGrid;

	sShiharaibiD := Trim(Copy(CBShiharaibiD.Text, 1, 2));

  sSql := 'SELECT TokuisakiCode1, TokuisakiName, TokuisakiNameYomi, ShiharaibiM, EstimateMemo ';
  sSql := sSql + ' FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' ShiharaibiD = ' + sShiharaibiD;
  sSql := sSql + ' AND ';
  sSql := sSql + ' ShiharaiKubun in (10,11) ';//現金・小切手

  if Length(EditTokuisakiCode1.Text) > 0 then begin
	  sSql := sSql + ' AND TokuisakiCode1 = ' + EditTokuisakiCode1.Text;
  end;

  sSql := sSql + ' ORDER BY TokuisakiNameYomi';

  GiRow := 1;
  with frmDMMaster.QueryEstimate do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
   	sYomi1 := Copy(FieldByName('TokuisakiNameYomi').AsString, 1, 2);
    sYomi2 := '';

    //得意先のある間ループ
  	while not EOF do begin
      sTokuisakiCode1 := FieldByName('TokuisakiCode1').AsString;
      //Gatsubunデータがすべて入力されているかチェック
      //1999/09/13
      sGatsubunMin := DMMaster.CheckGatsubun(sTokuisakiCode1, 'Min');
      if StrToDate(sGatsubunMin) = StrToDate(CsMinDate) then begin //一番はじめの日
      	Next;
      	Continue;
      end;

    	if sYomi1 <> sYomi2 then begin
	      SGEstimate.Cells[CintIndex, GiRow]:= sYomi1;
		   	sYomi2 := Copy(FieldByName('TokuisakiNameYomi').AsString, 1, 2);
      end;
      sTokuisakiName := FieldByName('TokuisakiName').AsString;
      sTokuisakiCode1 := FieldByName('TokuisakiCode1').AsString;
      sEstimateMemo := FieldByName('EstimateMemo').AsString;

      SGEstimate.Cells[CintTokuisakiName, GiRow]:= sTokuisakiName;
      SGEstimate.Cells[CintTokuisakiCode, GiRow]:= sTokuisakiCode1;
      SGEstimate.Cells[CintMemo, GiRow]:= sEstimateMemo;

      iMNumber := FieldByName('ShiharaibiM').AsInteger;
      iMm := StrToInt(CBShiharaibiM.Text);//iMmは何月分の請求金額を求めるか？
      iYyyy := StrToInt(CBYyyy.Text);
      sMm := GetShiharaibiM(iMNumber, iMm, iYyyy);
      if sMm = '' then begin
      	ShowMessage(sTokuisakiName + 'の支払い月が不正です -> ' + IntToStr(iMNumber));
        Next;
		   	sYomi1 := Copy(FieldByName('TokuisakiNameYomi').AsString, 1, 2);
        Continue;
      end;

      SGEstimate.Cells[CintMm, GiRow]:= sMm + '月分';

      //領収金額の計算と書き込み
      PutUriageKingaku2(FieldByName('TokuisakiCode1').AsString, iYyyy, StrToInt(sMm));

    	Next;
      GiRow := GiRow + 1;
	   	sYomi1 := Copy(FieldByName('TokuisakiNameYomi').AsString, 1, 2);
    end;
    Close;
  end;
end;

//領収金額の計算と書き込み２
//1999/09/25
procedure TfrmPEstimate.PutUriageKingaku2(sTokuisakiCode1: String; iYyyy, iMm : Integer);
var
	sSum, sSql, sFrom, sTo, sYyyy, sDd, sGatsubun : String;
  sToYyyy, sToMm, sToDd, sFromYyyy, sFromMm, sFromDd : String;
begin

  //月分を求める
  sGatsubun := IntToStr(iYyyy) + '/' + IntToStr(iMm) + '/' + '01';

  //締め日を求める
  sDd := MTokuisaki.GetShimebi(sTokuisakiCode1);
  if sDd = '0' then begin
  	sFromDd := '1';
  	sToDd := IntToStr(HKLib.GetGetsumatsu(iYyyy, iMm));
    sFromMm := IntToStr(iMm);
    sToMm := IntToStr(iMm);
    sFromYyyy := IntToStr(iYyyy);
    sToYyyy := IntToStr(iYyyy);
  end else begin
    sFromDd := IntToStr(StrToInt(sDd) + 1);
    sToDd := sDd;
    sToMm := IntToStr(iMm);
    iMm := iMm - 1;
    if iMm = 0 then begin
    	iMm := 12;
	    sFromMm := IntToStr(iMm-1);
	    sToYyyy := IntToStr(iYyyy);
      iYyyy := iYyyy-1;
	    sFromYyyy := IntToStr(iYyyy);
    end else begin;
	    sFromMm := IntToStr(iMm);
	    sFromYyyy := IntToStr(iYyyy);
  	  sToYyyy := IntToStr(iYyyy);
    end;
  end;

	//計算対象期間を求める
  //何月分を求める
	sTo   := sToYyyy   + '/' + sToMm   + '/' + sToDd;
  sFrom := sFromYyyy + '/' + sFromMm + '/' + sFromDd;

  //1999/09/28
  sToDd := Trim(Copy(CBShiharaibiD.Text, 1, 2));
  if sToDd = '0' then begin
  	sToDd := IntToStr(HKLib.GetGetsumatsu(StrToint(CBYyyy.Text), StrToint(CBShiharaibiM.Text)));
  end;
  sTo := CBYyyy.Text + '/' + CBShiharaibiM.Text + '/' + sToDd;
  SB1.SimpleText := SB1.SimpleText + '-sGatsubun=' +sGatsubun+ '-sTo=' + sTo;
  SB1.Update;

  sSql := 'SELECT esum = SUM(UriageKingaku) ';
  sSql := sSql + ' FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' TokuisakiCode1 =  ' + sTokuisakiCode1;
  sSql := sSql + ' AND ';
  sSql := sSql + ' Gatsubun = ''' + sGatsubun + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + ' DDate <= ''' + sTo + '''';

  with frmDMMaster.QueryEstimateSum do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    sSum := FieldByName('esum').AsString;
    if sSum = '' then begin
    	sSum := '0';
    end;
    sSum := FormatFloat('0,', StrToInt(sSum));
    SGEstimate.Cells[CintUriKingaku, GiRow]:= sSum;
    Close;
	end;//of with
end;


//表示ボタン３がクリックされた
//1999/09/27 月分未入力をはじかない
procedure TfrmPEstimate.BitBtn6Click(Sender: TObject);
var
  sTokuisakiName, sSql, sShiharaibiD, sYomi1, sYomi2, sShiharaibiM, sTokuisakiCode1, sMm : String;
  sEstimateMemo, sSeikyuuSimebi, sGatsubunMin, sTokuisakiCode2 : String;
  iMNumber, iMm, iYyyy, i: Integer;
begin

  //グリッドのクリア
  ClearGrid;

  sShiharaibiD := Trim(Copy(CBShiharaibiD.Text, 1, 2));

  sSql := 'SELECT TokuisakiCode1, TokuisakiCode2, TokuisakiCode2,TokuisakiName, TokuisakiNameYomi, ShiharaibiM, EstimateMemo ';
  sSql := sSql + ', TokuisakiNameYomi, Ka, SeikyuuSimebi';

  sSql := sSql + ' FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' ShiharaibiD = ' + sShiharaibiD;
  sSql := sSql + ' AND ';
  sSql := sSql + ' ShiharaiKubun in (10,11) ';//現金・小切手

  if Length(EditTokuisakiCode1.Text) > 0 then begin
    sSql := sSql + ' AND TokuisakiCode1 = ' + EditTokuisakiCode1.Text;
  end;

  sSql := sSql + ' ORDER BY TokuisakiNameYomi';

  GiRow := 1;
  with frmDMMaster.QueryEstimate do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    sYomi1 := Copy(FieldByName('TokuisakiNameYomi').AsString, 1, 2);
    sYomi2 := '';

    //得意先のある間ループ
    while not EOF do begin
      sTokuisakiCode1 := FieldByName('TokuisakiCode1').AsString;
      //Gatsubunデータがすべて入力されているかチェック
      //1999/09/13
      {
      sGatsubunMin := DMMaster.CheckGatsubun(sTokuisakiCode1, 'Min');
      if StrToDate(sGatsubunMin) = StrToDate(CsMinDate) then begin //一番はじめの日
      	Next;
      	Continue;
      end;
      }

      if sYomi1 <> sYomi2 then begin
        SGEstimate.Cells[CintIndex, GiRow]:= sYomi1;
        sYomi2 := Copy(FieldByName('TokuisakiNameYomi').AsString, 1, 2);
      end;
      sTokuisakiName  := FieldByName('TokuisakiName').AsString;
      sTokuisakiCode1 := FieldByName('TokuisakiCode1').AsString;
      sTokuisakiCode2 := FieldByName('TokuisakiCode2').AsString;
      sEstimateMemo   := FieldByName('EstimateMemo').AsString;

      SGEstimate.Cells[CintTokuisakiName, GiRow]:= sTokuisakiName;
      SGEstimate.Cells[CintTokuisakiCode, GiRow]:= sTokuisakiCode1 + '-' + sTokuisakiCode2;
      SGEstimate.Cells[CintMemo, GiRow]:= sEstimateMemo;

      iMNumber := FieldByName('ShiharaibiM').AsInteger;
      iMm := StrToInt(CBShiharaibiM.Text);//iMmは何月分の請求金額を求めるか？
      iYyyy := StrToInt(CBYyyy.Text);
      sMm := GetShiharaibiM(iMNumber, iMm, iYyyy);
      if sMm = '' then begin
        ShowMessage(sTokuisakiName + 'の支払い月が不正です -> ' + IntToStr(iMNumber));
        Next;
        sYomi1 := Copy(FieldByName('TokuisakiNameYomi').AsString, 1, 2);
        Continue;
      end;

      SGEstimate.Cells[CintMm, GiRow]:= sMm + '月分';

      //Ver2.0 2000/03/01
      with SGEstimate do begin
        Cells[CintYomi, GiRow]:= FieldByName('TokuisakiNameYomi').AsString;
        Cells[CintKa, GiRow]:= FieldByName('Ka').AsString;
        sSeikyuuSimebi := FieldByName('SeikyuuSimebi').AsString;
        if sSeikyuuSimebi = '0' then begin
          sSeikyuuSimebi := '月末';
        end else begin
          sSeikyuuSimebi := sSeikyuuSimebi + '日';
        end;
        Cells[CintShimebi, GiRow]:= sSeikyuuSimebi;
      end;//of with


      //領収金額の計算と書き込み
      SB1.SimpleText := FieldByName('TokuisakiCode1').AsString;
      SB1.Update;
      PutUriageKingaku2(FieldByName('TokuisakiCode1').AsString, iYyyy, StrToInt(sMm));
    	Next;
      GiRow := GiRow + 1;
	   	sYomi1 := Copy(FieldByName('TokuisakiNameYomi').AsString, 1, 2);
    end;
    Close;
  end;
end;
end.
