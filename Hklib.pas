{
	Hajime Kubota 's Libraly
 Since 1996/09/01
 Now 1998/05/05

 Append CSV File
 1998/12/01

}
unit Hklib;

interface
uses
	Dialogs, SysUtils,  Db, DBTables, Grids;

function Str2Real(strR: string): Real;
function Real2Str(N: real; Width, Places: integer): String;
function HFmtCashType(strS: String; intI: Integer; strP: string) : string;
function HGetTime: string;
function PutP(strD:String; intI : Integer):String;

function iskanji(c: Char): Boolean;
function RepraceCh(strStr:String; Oldch, Newch:Char):String;

function FixLength(strStr, strTmp:String; intL:Integer):String;
function HMakeFile(FileName, Contents:String):Boolean;

function PrintCSVFile(MyQuery:TDataSet; FileName:String):Boolean;
function PrintCSVFile2(MyQuery:TDataSet; FileName:String):Boolean;
function GetYoubiFromDate(sDate:String):String;
function CutStr(sBuf, sCut:String):String;

function GetGetsumatsu(iYyyy, iMm:Integer):Integer;

//function GetDateFromYoubi(YoubiNum):String;

function EnableCamma(sData:String) : String;
function SGSort(StringGrid1:TStringGrid; Key:Integer) : TStringGrid;

implementation


//2002/01/08
//ストリンググリッドのソート
// 変更�@　2002/03/26
//　・とりあえず，ソートするカラムを決め打ちでなく，Integerのカラムを自由に指定できる
//　　様に，引数に Keyを追加した．(そのカラムの値が文字などではエラーになる)
//　・カラムの値としてNULLがある場合はエラーになるので，その場合は0とみなす．


function SGSort(StringGrid1:TStringGrid; Key:Integer) : TStringGrid;
var
  c,i,j,k,x,y:Integer;
  s:string;

begin

  c:=Key; // ソートするカラム

  for i:=1 to StringGrid1.RowCount-1 do begin

    for j:=i+1 to StringGrid1.RowCount-1 do begin

      // null対策
      if StringGrid1.Cells[c, j ] = '' then x:=0
      else x := StrToInt(StringGrid1.Cells[c, j ]);
      if StringGrid1.Cells[c, i] = '' then y:=0
      else y:= StrToInt(StringGrid1.Cells[c,i]);


      if x < y then begin
        for k:=0 to StringGrid1.ColCount-1 do begin
          s:=StringGrid1.Cells[k,i];
          StringGrid1.Cells[k,i]:=StringGrid1.Cells[k,j];
          StringGrid1.Cells[k,j]:=s;
        end;
      end;

    end;

  end;

  result := StringGrid1;

end;

{
function SGSort(SG1:TStringGrid) : TStringGrid;
var
 i,j,Col,Row: integer;
 Buff, TmpBuff: array [0..127] of Char;
 sBuff, sTmpBuff : String;
begin
 Col := 0;
 Row := SG1.RowCount - 1;

 for i := Row downto 1 do begin
   //StrPCopy(Buff,SG1.Cells[Col,i]);
   sBuff := SG1.Cells[Col,i];
   for j := i downto 1 do begin
     //StrPCopy(TmpBuff,SG1.Cells[Col,j]);
     sTmpBuff := SG1.Cells[Col,j];
     //if StrComp(Buff,TmpBuff) > 0 then begin
     if StrToInt(sBuff) > StrToInt(sTmpBuff) then begin
      //SG1.Cells[Col,j] := StrPas(Buff);
      SG1.Cells[Col,j] := sBuff;
      //Buff := TmpBuff;
      sBuff := sTmpBuff;
     end;//of if
   //SG1.Cells[Col,i] := StrPas(Buff);
   SG1.Cells[Col,i] := sBuff;
   end;//of for
 end;//of for
 result := SG1;
end;
}


//1999/08/08
//sData中にカンマがあった場合にそれをエクセル上で有効にする
//エクセルにコンバートするときなどに有効
function EnableCamma(sData:String) : String;
begin
end;


//
//文字列から特定の文字以降の文字列を取り除く
//例:  CutStr('あああ＜いい＞', '＜') -> 'あああ'
//
//1999/1/17
//
function CutStr(sBuf, sCut:String):String;
var
	sStr : String;
  iPos : Integer;
begin
	iPos := Pos(sCut, sBuf);
  if iPos > 0 then begin
		sStr := Copy(sBuf, 1, iPos-1);
  end else begin
    sStr := sBuf;
  end;
  result := sStr;
end;



//
//曜日の番号から日付の文字列を返す
//1998/12/24 Hajime kubota
//
{
function GetDateFromYoubi(YoubiNum):String;
begin
	//基準日
  Date
end;
}

//日付から曜日を返す。
//月〜日
//1998/12/31 Hajime Kubota
function GetYoubiFromDate(sDate:String):String;
var
  ADate: TDateTime;
begin
  ADate := StrToDate(sDate);
  case DayOfWeek(ADate) of
  1:result := '日';
  2:result := '月';
  3:result := '火';
  4:result := '水';
  5:result := '木';
  6:result := '金';
  7:result := '土';
	end;
end;


//データセットをCSVファイルに書き出す
//1998/12/01 Hajime Kubota
function PrintCSVFile(MyQuery:TDataSet; FileName:String):Boolean;
var
	strLine : String;
  i : Integer;
 	F : TextFile;
begin
	//出力するファイルを作成する、すでにあれば削除する
  AssignFile(F, FileName);
  Rewrite(F);
	CloseFile(F);

	with MyQuery do begin
  	//受け取ったクエリーのカラム名を求める
    strLine := '';
  	for i := 0 to FieldCount - 1 do
      strLine := strLine + '"' + Fields[i].FieldName + '",';
    HMakeFile(FileName, Copy(strLine, 1, Length(strLine)-1));

    //実際のデータを出力する
    First;
    strLine := '';
		while not EOF do begin
	  	for i := 0 to FieldCount - 1 do
  	  	strLine :=  strLine + '"' + Fields[i].AsString + '_",' ;
    	HMakeFile(FileName, Copy(strLine, 1, Length(strLine)-1));
      strLine := '';
      Next;
    end;
  end;
end;

//データセットをCSVファイルに書き出す
//1998/12/01 Hajime Kubota
//1998/12/12
//材料コードの頭が０だと数字として認識してしまうので
//例外として直した
function PrintCSVFile2(MyQuery:TDataSet; FileName:String):Boolean;
var
	strLine : String;
  i : Integer;
 	F : TextFile;
begin
	//出力するファイルを作成する、すでにあれば削除する
  AssignFile(F, FileName);
  Rewrite(F);
	CloseFile(F);

	with MyQuery do begin
  	//受け取ったクエリーのカラム名を求める
    strLine := '';
  	for i := 0 to FieldCount - 1 do
      strLine := strLine + '"' + Fields[i].FieldName + '",';
    HMakeFile(FileName, Copy(strLine, 1, Length(strLine)-1));

    //実際のデータを出力する
    First;
    strLine := '';
		while not EOF do begin
	  	for i := 0 to FieldCount - 1 do begin
      	if Fields[i].FieldName = 'ZairyouCode' then begin //材料コードの場合のみ
	  	  	strLine :=  strLine + '"' + Fields[i].AsString + '_",' ;
    	  end else begin
        	strLine :=  strLine + '"' + Fields[i].AsString + '",' ;
        end;
      end;//of for
    	HMakeFile(FileName, Copy(strLine, 1, Length(strLine)-1));
      strLine := '';
      Next;
    end;
  end;
end;

//テキストファイルに１行書き込む
// 1997/09/29 Hajime Kubota
//
//引数：FileName -> 書き込むファイル名、なければ新規作成する
//      Contents -> 書き込む文字列
//戻値：TRUE -> 成功
//      FALSE -> 失敗
function HMakeFile(FileName, Contents:String):Boolean;
var
 	F : TextFile;
begin
	try
		AssignFile(F, FileName);
		Append(F);
		Writeln(F, Contents);
		CloseFile(F);
  except
  end;
end;


{現在の日付を返す}
function HGetTime: string;
begin
		Result := DateTimeToStr(Now);
end;

{漢字の１バイト目}
function iskanji(c: Char): Boolean;
begin
	Result := (($81 <= Ord(c)) and (Ord(c) <= $9F))
         or (($E0 <= Ord(c)) and (Ord(c) <= $FC));
end;




{
	Real型をStr型に変換して返す
 ex. Real2Str(12.34, 2, 4)
   12.34  -> 文字列に変換したい実数
   2      -> 文字列の長さ
   4      -> 小数点以下の桁数
}
function Real2Str(N: real; Width, Places: integer): String;
var
  strTmp: string;
begin
	Str(N:Width:Places, strTmp);
 Result := strTmp;
end;

{
	str型をReal型に変換して返す
}
function Str2Real(strR: string): Real;
var
 ErrCode: Integer;
 realTmp: Real;
begin
	If Length(strR) = 0 then Result := 0
 else begin
 	Val(strR, realTmp, ErrCode);
   if ErrCode = 0 then
   	Result := realTmp
   else
   	Result := 0;
 end;
end;


{
	与えられた文字列を３桁ごとに区切り
	固定長にして返す
 9桁まで対応
 引数 strS -> 対象文字列
      intI -> 何桁の固定長にするか 0の時は固定長にしない
      strP -> 固定長に満たないときに頭に埋める文字
 例
 10000000 -> 10,000,000
 1  -> 001

 小数点に対応 1996/10/02
}
function HFmtCashType(strS: String; intI: integer; strP: string) : string;
var
	strShousu, strRet, strTmp : string;
 intLength, I, intF : integer;
const
	Sep = ',';
begin
	if strS = '' then begin
 	Result := '';
   exit;
 end;
	intF := 0 ;
	if Pos('.', strS) > 0 then begin {小数点}
 	intF := 1;
 	strShousu := Copy(strS, Pos('.', strS)+1, Length(strS));{小数点以下の文字を取得}
   strS := Copy(StrS, 1, Pos('.', strS)-1);
 end;
	if intI > 9+2 then begin
 	ShowMessage('有効桁数は9桁です');
   exit;
 end;

 intLength := Length(strS);
 if intLength>9 then begin
 	ShowMessage('有効桁数は9桁です');
   exit;
 end;

 //20051007 hkubota
 if Pos('-', strS) > 0 then begin
	result := strS;
	Exit;
 end;

 case intLength of
 1..3:
 	strRet := strS;
 4..6:
 begin
	 strRet := Sep + Copy(strS, intLength-2, 3);
	 strRet := Copy(strS, 1, intLength-3) + strRet;
 end;
 7..9:
 begin
	 strRet := Sep + Copy(strS, intLength-2, 3);
	 strRet := Sep + Copy(strS, intLength-5, 3) + strRet;
	 strRet := Copy(strS, 1, intLength-6) + strRet;
 end;
 else
 end;

 {固定長に直す}
 if intI = 0 then begin
	result := strRet;
	Exit;
 end;

 strTmp := '';
 if intF = 0 then begin
		if Length(strRet) <= intI then begin
  		for I := Length(strRet) to (intI-1) do
  			strTmp := strTmp + strP;
   	strRet := (strTmp + strRet)
 	end
 	else begin
  		{strRet := strS;}
 	end;
 end
 else begin {少数ならば}
		if Length(strRet) <= intI then begin
  		for I := (Length(strRet)+Length(strshousu)+1) to (intI-1) do
  			strTmp := strTmp + strP;
   	strRet := (strTmp + strRet) + '.' + strShousu;
 	end
 	else begin
  		strRet := strS + '.' + strShousu;
 	end;
 end;



 result := strRet;
end;

//下？桁にピリオドを入れる
//ex PutP('1000', 2);
// 1000 -> 10.00
//ex PutP('12', 5);
// 12 -> 0.00012
//
function PutP(strD:String; intI : Integer):String;
var
	strS : string;
  intL, i : Integer;
begin
  intL := Length(strD);
  if (intL <= intI) then begin
  	strS := '0.';
    for i := 0 to (intI - intL - 1) do begin
	    strS := strS + '0';
    end;//of for
    strS := strS + strD;
  	result := strS;
  end else begin
    strS := Copy(strD, 0, (intL-intI));
    strS := strS + '.' + Copy(strD, (intL-intI+1), intI);
    result := strS;
  end;
end;

//strStrで与えられた文字列の中の１バイト(strMoto)をstrAtoに置き換える
//1998/04/26 Hajime Kubota
function RepraceCh(strStr:String; Oldch, Newch:Char):String;
var
   i, Len: Integer;
begin
  	Len := Length(strStr);

    for i := 1 to Len do begin
    	if strStr[i] = Oldch then
      	strStr[i] := Newch;
    end;//of for

    Result := strStr;
end;

//与えられた文字列を固定長にして返す
//例：FixLength('123', '0', 8)
//			結果：'00000123'
function FixLength(strStr, strTmp:String; intL:Integer):String;
var
	i, j : Integer;
  strRet : String;
begin
	i := Length(strStr);
  strRet := '';
  if i > intL then begin
  	ShowMessage('HKLibのFixLengthの使い方エラー');
  end else if i = intL then begin
  	Result := strStr;
  end else begin
  	for j := 1 to (intL-i) do begin
			strRet:= strRet + strTmp;
    end;//of for
    strRet := strRet + strStr;
  	Result := strRet;
  end;

end;

//引数で与えられた年月の月末を求める
//1999/05/04
function GetGetsumatsu(iYyyy, iMm:Integer):Integer;
var
	bUruu : Boolean;
begin
  bUruu := False;

	//閏年の判定
  if iYyyy mod 4 = 0 then begin
	  bUruu := True;
  end;

 	case iMm of
    1 : Result := 31;
    2 : begin
    			if bUruu = True then begin
			    	Result := 29;
          end else begin
			    	Result := 28;
          end;
      	end;
    3 : Result := 31;
    4 : Result := 30;
    5 : Result := 31;
    6 : Result := 30;
    7 : Result := 31;
    8 : Result := 31;
    9 : Result := 30;
    10: Result := 31;
    11: Result := 30;
    12: Result := 31;
  end;//of case

end;

end.
