unit TKaishuu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, StdCtrls, Buttons, ExtCtrls, ComCtrls, DBTables, ShellAPI, Mask;

type
  TfrmTKaishuu = class(TfrmMaster)
    Shape1: TShape;
    Label3: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label4: TLabel;
    CBTokuisakiName: TComboBox;
    CBFName: TComboBox;
    CBCName: TComboBox;
    Shape2: TShape;
    Label5: TLabel;
    Label9: TLabel;
    EditTokuisakiCode2: TEdit;
    Label10: TLabel;
    EditTokuisakiName: TEdit;
    Label11: TLabel;
    DTP1: TDateTimePicker;
    Label12: TLabel;
    Label13: TLabel;
    CBKaishuuHouhou: TComboBox;
    Label14: TLabel;
    Memo1: TMemo;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    EditTokuisakiCode1: TEdit;
    Label15: TLabel;
    MemoZandaka: TMemo;
    Memo2: TMemo;
    CBKingaku: TComboBox;
    Label16: TLabel;
    MemoTokuisaki: TMemo;
    MemoZan1: TMemo;
    labelZan1: TLabel;
    labelZan2: TLabel;
    MemoZan2: TMemo;
    labelZan3: TLabel;
    MemoZan3: TMemo;
    Label17: TLabel;
    MemoShimebi: TMemo;
    Label18: TLabel;
    LabelID: TLabel;
    BitBtn6: TBitBtn;
    Label19: TLabel;
    MemoShiharaibiM: TMemo;
    Label20: TLabel;
    EditGatsubun: TMaskEdit;
    Button1: TButton;
    labelZan4: TLabel;
    MemoZan4: TMemo;
    Label21: TLabel;
    MemoShiharaibiD: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure CBTokuisakiNameExit(Sender: TObject);
    procedure CBCNameExit(Sender: TObject);
    procedure CBFNameExit(Sender: TObject);
    procedure CBTokuisakiNameKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBCNameKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBFNameKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBTokuisakiNameChange(Sender: TObject);
    procedure CBCNameChange(Sender: TObject);
    procedure CBFNameChange(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure EditTokuisakiCode1Exit(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure MemoZandakaKeyPress(Sender: TObject; var Key: Char);
    procedure DTP1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBKingakuChange(Sender: TObject);
    procedure CBKingakuExit(Sender: TObject);
    procedure DTP1Change(Sender: TObject);
    procedure CBKaishuuHouhouChange(Sender: TObject);
    procedure DTP1Exit(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure EditGatsubunExit(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private 宣言 }
		function GetTokuisaki(sKeyWord : string; iKey:Integer):Boolean;
		function GetTokuisakiInfo(sKeyWord : string; iKey:Integer) : Boolean;
		//function GetZandaka(sTokuisakiCode, sDateFrom, sDateTo:String):Integer;
		procedure GetKingaku(sTokuisakiCode, sDate : String);
		//function Check2Juu(sTokuisakiCode1, sKDate, sKHouhou:String):Integer;
		procedure GetUriageMonth;
		Function InsertDenpyou : Boolean;
		Function DeleteDenpyou : Boolean;
		procedure MakeMemo;
		Procedure CheckKaishuu;
		procedure GetData;
  public
    { Public 宣言 }
  end;

var
  frmTKaishuu: TfrmTKaishuu;

	function GetZandaka(sTokuisakiCode, sDateFrom, sDateTo:String):Integer;
	function GetZandaka2(sTokuisakiCode, sGatsubun:String):Integer;

implementation


uses
	Inter, DMMaster, HKLib, PUrikakeDaichou2, MTokuisaki;
{$R *.DFM}

var
	GFlag  :Boolean; //F1キーで得意先検索したかどうかのフラグ True->検索した False -> 検索していない
	GFlag2 :Boolean; //F1キーで金額検索したかどうかのフラグ True->検索した False -> 検索していない
	GFlag3 :Boolean; //F1キーで振込人検索したかどうかのフラグ True->検索した False -> 検索していない
	GFlag4 :Boolean; //F1キーでチェーン名検索したかどうかのフラグ True->検索した False -> 検索していない

procedure TfrmTKaishuu.FormCreate(Sender: TObject);
begin
  inherited;
	Width := 550;
  Height := 570;
  DTP1.Date := Date();
  GFlag := False;
  GFlag2 := False;
  GFlag3 := False;
  GFlag4 := False;

  if (GTokuisakiCode='') then begin
  	GTokuisakiCode := '0';
  end;
  if StrToInt(GTokuisakiCode)>0 then begin
  	EditTokuisakiCode1.text := GTokuisakiCode;
  end;
end;

procedure TfrmTKaishuu.CBTokuisakiNameExit(Sender: TObject);
var
	sTokuisakiCode1 : String;
begin
	if GFlag = True then begin
		CBCName.Text := '';
		CBFName.Text := '';
   	sTokuisakiCode1 := Copy(CBTokuisakiName.Text, 1, Pos(',', CBTokuisakiName.Text)-1);
  	if GetTokuisakiInfo(sTokuisakiCode1, 3) = True then begin
		  //1999/08/08 追加
  		GetUriageMonth;
    end;
  end;
end;

procedure TfrmTKaishuu.CBCNameExit(Sender: TObject);
var
	sTokuisakiCode1 : String;
begin
	if GFlag4 = True then begin
		CBTokuisakiName.Text := '';
		CBFName.Text := '';
  	if CBCName.Text <> '' then begin
    	sTokuisakiCode1 := Copy(CBCName.Text, 1, Pos(',', CBCName.Text)-1);
		  if GetTokuisakiInfo(sTokuisakiCode1, 3) = True then begin
			  //1999/08/08 追加
  			GetUriageMonth;
      end;
  	end;

  end;

end;

procedure TfrmTKaishuu.CBFNameExit(Sender: TObject);
var
	sTokuisakiCode1 : String;
begin
	if GFlag3 = True then begin
		CBTokuisakiName.Text := '';
		CBCName.Text := '';

  	if CBFName.Text <> '' then begin
	    sTokuisakiCode1 := Copy(CBFName.Text, 1, Pos(',', CBFName.Text)-1);
	  	if GetTokuisakiInfo(sTokuisakiCode1, 3) = True then begin
		  	//1999/08/08 追加
			  GetUriageMonth;
      end;
	  end;

  end;

end;

procedure TfrmTKaishuu.CBTokuisakiNameKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
	 	if GetTokuisaki(CBTokuisakiName.Text, 1) = True then begin;
	    GFlag := True;
    end else begin
	    GFlag := False;
    end;
  end;
end;

//得意先を検索する
//1->TokuisakiName
//2->CName
//3->FName
//4->TokuisakiCode1
//Ver2.0
//見つかればTRUE見つからなければFalseを返すようにした
function TfrmTKaishuu.GetTokuisaki(sKeyWord : string; iKey:Integer):Boolean;
var
	sSql, sSql2, sCName : String;
begin
	sSql := 'SELECT DISTINCT TokuisakiCode1, TokuisakiCode2, TokuisakiName, TokuisakiNameUp,CName, FName';
  sSql := sSql + ' FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
  case iKey of
  1 : begin
  			sSql := sSql + 'TokuisakiName Like ''%' + sKeyWord + '%''';
        //1999/06/27 よみでも検索するように変更
  			sSql := sSql + ' OR ';
  			sSql := sSql + 'TokuisakiNameYomi Like ''%' + sKeyWord + '%''';

  		end;
  2 : sSql := sSql + 'CName Like ''%' + sKeyWord + '%''';
  3 : sSql := sSql + 'FName Like ''%' + sKeyWord + '%''';
  4 : sSql := sSql + 'TokuisakiCode1 = ' + sKeyWord;
  end;//of case

  //CBの初期化
  CBTokuisakiName.Clear;
  CBCName.Clear;
  CBFName.Clear;
  MemoShimebi.Text := '';

	with frmDMMaster.QueryDKingaku do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    if EOF then begin
    	Showmessage('該当するデータが見つかりませんでした');
    	Result := False;
      Exit;
    end else begin
    	Result := True;
    end;
    while not EOF do begin
      case iKey of
	      1 : begin
        			CBTokuisakiName.Items.Add(FieldByName('TokuisakiCode1').AsString +
			       ',' + FieldByName('TokuisakiNameUp').AsString + ' ' +
      			 FieldByName('TokuisakiName').AsString);
              end;
  	    2 : begin
        			sCName := FieldByName('CName').AsString;

        			CBCName.Items.Add(FieldByName('TokuisakiCode1').AsString + ',' + sCName);
        		end;
    	  3 : begin
        			CBFName.Items.Add(FieldByName('TokuisakiCode1').AsString + ',' +
                                FieldByName('FName').AsString);
        		end;
        4 : begin
        			GetTokuisakiInfo(FieldByName('TokuisakiCode1').AsString, 3);
{
		        	EditTokuisakiCode2.Text := FieldByName('TokuisakiCode2').AsString;
    		    	EditTokuisakiName.Text := FieldByName('TokuisakiName').AsString;
						  //1999/06/28 追加
						  MemoShimebi.Text := MTokuisaki.GetShimebi(EditTokuisakiCode1.Text);
						  if MemoShimebi.Text = '0' then begin
						  	MemoShimebi.Text := '月末';
						  end else begin
						  	MemoShimebi.Text := MemoShimebi.Text + ' 日';

						  end;
}
	        	end;
      end;//of case
      //MemoTokuisaki.Text := FieldByName('Memo').AsString;
    	Next;
    end;//of while
    Close;
  end;//of with
  case iKey of
	  1 : CBTokuisakiName.DroppedDown := True;
    2 : CBCName.DroppedDown := True;
    3 : CBFName.DroppedDown := True;
    4 : CBKingaku.SetFocus;
  end;//of case

end;

procedure TfrmTKaishuu.CBCNameKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
	 	if GetTokuisaki(CBCName.Text, 2) = True then begin
	    GFlag4 := True;
    end else begin
	    GFlag4 := False;
    end;
  end;
end;

procedure TfrmTKaishuu.CBFNameKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
	 	if GetTokuisaki(CBFName.Text, 3) = True then begin
	    GFlag3 := True;
    end else begin
	    GFlag3 := False;
    end;
  end;
end;

//得意先情報を検索して表示する
//1->TokuisakiName
//2->CName
//3->FName のときは複数レコードあるのでTokuisakiCode1で渡される
//
//1999/04/21
// 残高を算出する
//1999/07/25
//得意先メモを表示する
//2000/01/25
//ShiharaibiDの追加
Function TfrmTKaishuu.GetTokuisakiInfo(sKeyWord : string; iKey:Integer) : Boolean;
var
	sSql, sSql2 : String;
begin
	if sKeyWord = '' then begin
  	Exit;
  end;

	sSql := 'SELECT TokuisakiCode1, TokuisakiCode2,TokuisakiNameUp,TokuisakiName, ShiharaibiM,ShiharaibiD, Memo';
  sSql := sSql + ' FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
  case iKey of
  1 : sSql := sSql + 'TokuisakiName = ''' + sKeyWord + '''';
  2 : sSql := sSql + 'CName = ''' + sKeyWord + '''';
  3 : begin
  			sSql := sSql + 'TokuisakiCode1 = ' + sKeyWord;
  		end;
  end;//of case

	with frmDMMaster.QueryDKingaku do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    if not EOF then begin
    	EditTokuisakiCode1.Text := FieldByName('TokuisakiCode1').AsString;
    	EditTokuisakiCode2.Text := FieldByName('TokuisakiCode2').AsString;
    	EditTokuisakiName.Text := FieldByName('TokuisakiNameUp').AsString +
                                ' ' + FieldByName('TokuisakiName').AsString;

		  //1999/06/28 追加
		  MemoShimebi.Text := MTokuisaki.GetShimebi(EditTokuisakiCode1.Text);
		  if MemoShimebi.Text = '0' then begin
  			MemoShimebi.Text := '月末';
		  end else begin
  			MemoShimebi.Text := MemoShimebi.Text + ' 日';
		  end;

      //支払い月の表示
		  MemoShiharaibiM.Text := FieldByName('ShiharaibiM').AsString;
      if FieldByName('ShiharaibiM').AsInteger = 0 then begin
      	MemoShiharaibiM.Text := FieldByName('ShiharaibiM').AsString + ',当月';
      end else if FieldByName('ShiharaibiM').AsInteger = 1 then begin
      	MemoShiharaibiM.Text := FieldByName('ShiharaibiM').AsString + ',翌月';
      end else if FieldByName('ShiharaibiM').AsInteger = 2 then begin
      	MemoShiharaibiM.Text := FieldByName('ShiharaibiM').AsString + ',翌々月';
      end else if FieldByName('ShiharaibiM').AsInteger = 3 then begin
      	MemoShiharaibiM.Text := FieldByName('ShiharaibiM').AsString + ',翌翌々月';
			end;

      //支払い日の表示 2000/01/25
		  MemoShiharaibiD.Text := FieldByName('ShiharaibiD').AsString;
      if FieldByName('ShiharaibiD').AsInteger = 0 then begin
      	MemoShiharaibiD.Text := '月末';
      end else begin
      	MemoShiharaibiD.Text := FieldByName('ShiharaibiD').AsString;
      end;

      //得意先メモの表示 1999/07/25
      memotokuisaki.Text := FieldByName('Memo').AsString;

    	Result := True;

    end else begin
    	Result := False;
    	ShowMessage('得意先が見つかりません');
      EditTokuisakiCode1.Setfocus;
    end;//of if
    Close;
  end;//of with
end;


//残高を計算する
//期間で指定できるように変更
//1999/06/01
// sDateFrom = '0' ならばすべての期間計算
//function TfrmTKaishuu.GetZandaka(sTokuisakiCode, sDateFrom, sDateTo:String):Integer;
//1999/06/18
//返品に対応
function GetZandaka(sTokuisakiCode, sDateFrom, sDateTo:String):Integer;
var
	sSql2:String;
begin
  //残高の計算
  //if EditTokuisakiCode1.Text = '' then Exit;

  sSql2 := 'SELECT Zan = SUM(UriageKingaku) ';
  sSql2 := sSql2 + ' FROM ' + CtblTDenpyou;
  sSql2 := sSql2 + ' WHERE ';
  sSql2 := sSql2 + ' TokuisakiCode1 = ' + sTokuisakiCode;

  if sDateFrom <> '0' then begin
	  sSql2 := sSql2 + ' AND ';
	  sSql2 := sSql2 + ' DDate Between ';
	  sSql2 := sSql2 + ''''  + sDateFrom + '''';
	  sSql2 := sSql2 + ' AND ';
	  sSql2 := sSql2 + ''''  + sDateTo + '''';
	  sSql2 := sSql2 + ' AND ';
	  //sSql2 := sSql2 + ' UriageKingaku > 0 ';
    //以下追加
	  //sSql2 := sSql2 + ' ((UriageKingaku > 0) OR (Memo IN (''返品'', ''値引'')))' ;
	  //sSql2 := sSql2 + ' ((UriageKingaku > 0) OR (SUBSTRING(Memo, 1, 2) IN (''返品'', ''値引'')))' ;
	  sSql2 := sSql2 + ' ((UriageKingaku > 0) OR (SUBSTRING(Memo, 1, 2) IN (''返品'', ''値引'',''取消'')))' ;   // mod 2015.04.19

    //1999/08/09
	  sSql2 := sSql2 + ' AND ';
	  sSql2 := sSql2 + ' PATINDEX(''%返金%'', Memo) = 0 ';

  end;

	with frmDMMaster.QueryDKingaku do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql2);
    Open;
    if not EOF then begin
    	result := FieldByName('Zan').AsInteger;
    end;//of if
    Close;
  end;//of with
end;

//GetZandakaを変更して期間ではなく月分で集計できるように変更
//1999/10/23
function GetZandaka2(sTokuisakiCode, sGatsubun:String):Integer;
var
  sSql2, sSql3 :String;
  iSum, iTax : Integer;
begin
  //残高の計算
  //if EditTokuisakiCode1.Text = '' then Exit;

  sSql2 := 'SELECT SUM(UriageKingaku) AS Zan';
  sSql2 := sSql2 + ' FROM ' + CtblTDenpyou;
  sSql2 := sSql2 + ' WHERE ';
  sSql2 := sSql2 + ' TokuisakiCode1 = ' + sTokuisakiCode;
  sSql2 := sSql2 + ' AND ';
  sSql2 := sSql2 + ' Gatsubun = ';
  sSql2 := sSql2 + ''''  + sGatsubun + '''';
{
  sSql2 := sSql2 + ' AND ';
  sSql2 := sSql2 + ' ((UriageKingaku > 0) OR (SUBSTRING(Memo, 1, 2) IN (''返品'', ''値引'')))' ;
  sSql2 := sSql2 + ' AND ';
  sSql2 := sSql2 + ' PATINDEX(''%返金%'', Memo) = 0 ';
}
//2001/03/15 Added
  sSql2 := sSql2 + ' AND not ';
  //sSql2 := sSql2 + ' ((UriageKingaku < 0) AND (PATINDEX(''%##消費税##%'', Memo) <> 0))' ; thuyptt 2019/03/08
  sSql2 := sSql2 + ' ((UriageKingaku < 0) AND (InStr(Memo, ''##消費税##'') <> 0))' ;

  //2005/09/14 Changed by Hkubota
  //上記SQLでは、マイナスの消費税が加わらない
  sSql3 := 'SELECT SUM(UriageKingaku) AS Zan';
  sSql3 := sSql3 + ' FROM ' + CtblTDenpyou;
  sSql3 := sSql3 + ' WHERE ';
  sSql3 := sSql3 + ' TokuisakiCode1 = ' + sTokuisakiCode;
  sSql3 := sSql3 + ' AND ';
  sSql3 := sSql3 + ' Gatsubun = ';
  sSql3 := sSql3 + ''''  + sGatsubun + '''';
  sSql3 := sSql3 + ' AND ';
  sSql3 := sSql3 + ' ((UriageKingaku < 0) AND (Memo=''##消費税##''))' ;

  with frmDMMaster.QueryDKingaku do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql2);
    Open;
    if not EOF then begin
      //result := FieldByName('Zan').AsInteger;
      iSum := FieldByName('Zan').AsInteger;
    end;//of if
    Close;
  end;//of with
  with frmDMMaster.QueryDKingaku do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql3);
    Open;
    if not EOF then begin
      iTax := FieldByName('Zan').AsInteger;
    end;//of if
    Close;
  end;//of with

  result := iSum + ITax;
end;


procedure TfrmTKaishuu.CBTokuisakiNameChange(Sender: TObject);
begin
{
	if GFlag = True then begin
	  GetTokuisakiInfo(CBTokuisakiName.Text, 1);

	  //1999/08/08 追加
  	GetUriageMonth;

    GFlag := False;
  end;
}
end;



procedure TfrmTKaishuu.CBCNameChange(Sender: TObject);
begin
{
	if GFlag4 = True then begin
	  GetTokuisakiInfo(CBCName.Text, 2);

	  //1999/08/08 追加
  	GetUriageMonth;

    GFlag4 := False;
  end;
}
end;

procedure TfrmTKaishuu.CBFNameChange(Sender: TObject);
var
	sTokuisakiCode1 : String;
begin
{
	if GFlag3 = True then begin
	  sTokuisakiCode1 := Copy(CBFName.Text, 1, Pos(',', CBFName.Text)-1);
  	GetTokuisakiInfo(sTokuisakiCode1, 3);

	  //1999/08/08 追加
  	GetUriageMonth;

    GFlag3 := False;
  end;
}

end;

//登録ボタンがクリックされた
procedure TfrmTKaishuu.BitBtn3Click(Sender: TObject);
var
	sSql: String;
  bFlag : String;
begin
  //月２回締めの得意先の警告
  //TokuisakiCode1Exitイベントが発生しない場合があるため
  //1999/09/06
  if Inter.CheckTokuisakiCode1(EditTokuisakiCode1.Text) = True then begin
     ShowMessage('「' + EditTokuisakiName.Text + '」は月２回締めです。');
     EditTokuisakiCode1.SetFocus;
  	 Exit;
  end;

	//確認メッセージ
  CheckKaishuu;
  if MessageDlg('登録しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;
	//２重登録のチェック
  //得意先コード、回収日、回収方法でユニーク
  {
  bFlag := Check2Juu(sTokuisakiCode1, sKDate, KHouhou);
  if bFlag = True then begin
	  if MessageDlg('この伝票はすでに登録されています。' + #10#13 +
                  'ここで中止して削除してから再登録してください' +#10#13+
                  '中止しますか？',
  	  mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
    	Exit;
	  end;
  end;
  }

  //登録開始
  //新規登録の場合
{
	sSql := 'INSERT INTO ' + CtblTDenpyou;
  sSql := sSql + '(';
  sSql := sSql + 'TokuisakiCode1, ';
  sSql := sSql + 'TokuisakiCode2, ';
  sSql := sSql + 'DNumber       , ';
  sSql := sSql + 'InputDate     , ';
  sSql := sSql + 'DDate         , ';
  sSql := sSql + 'UriageKingaku ,';
  sSql := sSql + 'Memo          ,';
  sSql := sSql + 'KaishuuHouhou,  ';
  sSql := sSql + 'Gatsubun  ';
  sSql := sSql + ')';
  sSql := sSql + ' VALUES ';
  sSql := sSql + '(';
  sSql := sSql + EditTokuisakiCode1.Text + ',';
  sSql := sSql + EditTokuisakiCode2.Text + ',';
  sSql := sSql + '0,'; //伝票ナンバーは使わない
  sSql := sSql + '''' + DateToStr(Date()) + ''', ';
  sSql := sSql + '''' + DateToStr(DTP1.Date) + ''', ';
  sSql := sSql + IntToStr((-1*StrToInt(CBKingaku.Text)))+ ', ';
  sSql := sSql + '''' + Memo1.Text + ''', ';
  sSql := sSql + Copy(CBKaishuuHouhou.Text, 1, 1) + ',';
  sSql := sSql + '''' + EditGatsubun.Text + '/01'''; //1999/09/10 追加 月分
  sSql := sSql + ')';
  with frmDMMaster.QueryInsert do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    ExecSql;
    Close;
  end;
}
  if InsertDenpyou = True then begin;
	  ShowMessage('登録に成功しました');
    //19991026
  	//MemoZandaka.Text := IntToStr(GetZandaka(EditTokuisakiCode1.Text, '0', '0'));
  	//MemoZandaka.Text := IntToStr(GetZandaka2(EditTokuisakiCode1.Text, '0', '0'));

	  //MemoZandaka.Text := HKLib.HFmtCashType(MemoZandaka.Text, 0, '');

    //1999/10/28追加
    GetUriageMonth;
    
  end else begin
	  ShowMessage('登録に失敗しました');
  	Exit;
  end;


end;

//回収の２重登録チェック
//得意先コード、回収日、回収方法でユニーク
{
function TfrmTDenpyou.Check2Juu(sTokuisakiCode1, sKDate, sKHouhou:String):Integer;
var
	sSql : String;
begin
	sSql := 'SELECT TokuisakiCode1 ';
  sSql := sSql + ' FROM ';
  sSql := sSql + CtblTKaishuu;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' KDate =  ''' + sKDate + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + ' TokuisakiCode1 =  ' + sTokuisakiCode1;
  sSql := sSql + ' AND ';
  sSql := sSql + ' KHouhou =  ' + sUriageKingaku;
  with frmDMMaster.QueryDKingaku do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    if EOF then begin
    	Result := False;
    end else begin
    	Result := True;
    end;
    Close;
  end;
end;
}

//売り掛け台帳表示のボタンがクリックされた
procedure TfrmTKaishuu.BitBtn4Click(Sender: TObject);
begin
	//確認メッセージ
  if MessageDlg('売り掛け台帳を表示しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;
  if EditTokuisakiCode1.Text = '' then
  	EditTokuisakiCode1.Text := '0';

	GTokuisakiCode := EditTokuisakiCode1.Text;
  GTokuisakiName := EditTokuisakiName.Text;

  TfrmPUrikakeDaichou2.Create(Self);

end;


//訂正ボタンがクリックされた
//1999/06/20
procedure TfrmTKaishuu.BitBtn6Click(Sender: TObject);
var
	sSql,sTokuisakiCode1,sDDate,sKingaku : String;
  bFlag : Boolean;
begin

  if MessageDlg('訂正しますか？',mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
	 	Exit;
  end;

  //まず削除
  if DeleteDenpyou = True then begin
		if InsertDenpyou = True then begin
		 	ShowMessage('訂正に成功しました');
  	end else begin
	  	ShowMessage('訂正に失敗しました');
    end;
  end else begin
  	ShowMessage('訂正に失敗しました');
  end;

end;

//得意先コード１で検索する
procedure TfrmTKaishuu.EditTokuisakiCode1Exit(Sender: TObject);
begin
	GetData;
end;

procedure TfrmTKaishuu.GetData;
begin

	if EditTokuisakiCode1.Text = '' then
  	Exit;

	if GetTokuisakiInfo(EditTokuisakiCode1.Text, 3) = False then begin
  	Exit; //得意先が見つからなければ
  end;;

  //月２回締めの得意先の警告
  if Inter.CheckTokuisakiCode1(EditTokuisakiCode1.Text) = True then begin
     ShowMessage('「' + EditTokuisakiName.Text + '」は月２回締めです。');
     EditTokuisakiCode1.SetFocus;
  	 Exit;
  end;


  //1999/07/15 追加
  GetUriageMonth;

  CBKingaku.SetFocus;
end;


//削除ボタンがクリックされた
procedure TfrmTKaishuu.BitBtn5Click(Sender: TObject);
var
	sSql : String;
begin
	//確認メッセージ
  if MessageDlg('削除しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;
	sSql := 'DELETE FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' TokuisakiCode1 =  ' + EditTokuisakiCode1.Text;
  sSql := sSql + ' AND ';
  sSql := sSql + ' DDate =  ''' + DateToStr(DTP1.Date) + '''';
  sSql := sSql + ' AND ';
  //sSql := sSql + ' UriageKingaku =  -' + CBKingaku.Text; この方法だと画面上で-の指定ができない
  sSql := sSql + ' UriageKingaku = ' + IntToStr((-1*StrToInt(CBKingaku.Text)));
  try
	  with frmDMMaster.QueryDelete do begin
  	  Close;
			Sql.Clear;
	    Sql.Add(sSql);
			ExecSql;
	  	Close;
	  end;//of with
  	ShowMessage('削除に成功しました');
    //後処理
    CBKingaku.Text := '';
    Memo1.Text := '';
    //残高の再計算
    //19991026
    //MemoZandaka.Text := IntToStr(GetZandaka(EditTokuisakiCode1.Text, '0', '0'));
    //MemoZandaka.Text := IntToStr(GetZandaka2(EditTokuisakiCode1.Text, '0', '0'));

    MemoZandaka.Text := HKLib.HFmtCashType(MemoZandaka.Text, 0, '');

  //20051007 追加
  GetUriageMonth;
  CBKingaku.SetFocus;

  except
	  on E: EDBEngineError do begin
    	ShowMessage(E.Message);
		  ShowMessage('削除に失敗しました');
    end;
  end;


end;

procedure TfrmTKaishuu.MemoZandakaKeyPress(Sender: TObject; var Key: Char);
begin
  //inherited;
	if Key = #13 then Key := #0;
end;

procedure TfrmTKaishuu.DTP1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
//  inherited;
	//回収金額の検索
	if (Key=VK_F1) then begin
	 	GetKingaku(EditTokuisakiCode1.Text, DateToStr(DTP1.Date));
 	  LabelID.Caption := 'ID';
    GFlag2 := True;
  end else begin
    GFlag2 := False;
  end;
end;

//得意先コードと日付から回収金額を検索する
procedure TfrmTKaishuu.GetKingaku(sTokuisakiCode, sDate : String);
var
	sSql : string;
begin
	sSql := 'SELECT ID, UriageKingaku ';
  sSql := sSql + ' FROM ';
  sSql := sSql + CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  sSql := sSql + 'TokuisakiCode1 = ' + sTokuisakiCode;
  sSql := sSql + ' AND ';
  sSql := sSql + 'DDate = ''' + sDate + '''';
  sSql := sSql + ' AND ';

  //2005.10.07 Hajime Kubota
  //sSql := sSql + 'UriageKingaku < ' + '0';
  sSql := sSql + 'KaishuuHouhou > ' + '0';
  CBKingaku.Items.Clear;
  with frmDMMaster.QueryDelete do begin
   Close;
	 Sql.Clear;
	 Sql.Add(sSql);
	 Open;
   if EOF then begin
   	ShowMessage('このデータは見つかりません');
    Exit;
   end;
   While not EOF do begin
    GFlag2 := True;
	  CBKingaku.Items.Add(IntToStr(-1*FieldByName('UriageKingaku').AsInteger));
   	Next;
   end;
   Close;
	end;//of with
	CBKingaku.DroppedDown := True;
end;

procedure TfrmTKaishuu.CBKingakuChange(Sender: TObject);
var
	sSql, sMemo, sKaishuuhouhou : String;
begin
//  inherited;
//Memoを検索する
{Exitへ移す
	if GFlag2 = True then begin
		sSql := 'SELECT Memo,KaishuuHouhou, ID ';
  	sSql := sSql + ' FROM ';
	  sSql := sSql + CtblTDenpyou;
  	sSql := sSql + ' WHERE ';
	  sSql := sSql + 'TokuisakiCode1 = ' + EditTokuisakiCode1.Text;
  	sSql := sSql + ' AND ';
	  sSql := sSql + 'DDate = ''' + DateToStr(DTP1.Date) + '''';
  	sSql := sSql + ' AND ';
	  sSql := sSql + 'UriageKingaku = -' + CBKingaku.Text;
	  with frmDMMaster.QueryDelete do begin
  	  Close;
		  Sql.Clear;
		  Sql.Add(sSql);
		  Open;
  	  sMemo := FieldByName('Memo').AsString;
  	  Memo1.Text := FieldByName('Memo').AsString;

      if (LabelID.Caption = 'ID') or (LabelID.Caption = '0') then
	  	  LabelID.Caption := FieldByName('ID').AsString;

      sKaishuuhouhou := FieldByName('KaishuuHouhou').AsString;
      CBKaishuuHouhou.Text := FieldByName('KaishuuHouhou').AsString;
	   	Close;
  	end;
  	//GFlag2 := False;
  end;//of if
}
end;

procedure TfrmTKaishuu.CBKingakuExit(Sender: TObject);
var
	sGatsubun, sSql, sMemo, sKaishuuhouhou : String;
begin
//  inherited;
//Memoを検索する
	if GFlag2 = True then begin
		sSql := 'SELECT Memo,KaishuuHouhou, ID, Gatsubun ';
  	sSql := sSql + ' FROM ';
	  sSql := sSql + CtblTDenpyou;
  	sSql := sSql + ' WHERE ';
	  sSql := sSql + 'TokuisakiCode1 = ' + EditTokuisakiCode1.Text;
  	sSql := sSql + ' AND ';
	  sSql := sSql + 'DDate = ''' + DateToStr(DTP1.Date) + '''';
  	sSql := sSql + ' AND ';

    //20051007
    if pos('-', CBKingaku.Text) > 0 then begin
	   sSql := sSql + 'UriageKingaku = ' + Copy(CBKingaku.Text,1,length(CBKingaku.Text));
    end else begin
	   sSql := sSql + 'UriageKingaku = -' + CBKingaku.Text;
	  end;
    with frmDMMaster.QueryDelete do begin
  	  Close;
		  Sql.Clear;
		  Sql.Add(sSql);
		  Open;
  	  sMemo := FieldByName('Memo').AsString;
  	  sGatsubun := FieldByName('Gatsubun').AsString;
  	  Memo1.Text := FieldByName('Memo').AsString;

      if (LabelID.Caption = 'ID') or (LabelID.Caption = '0') then begin
	  	  LabelID.Caption := FieldByName('ID').AsString;
{
        if Copy(sGatsubun, 1, 2) = '99' then begin
	        sGatsubun := '19' + sGatsubun;
        end else begin //2000年
	        sGatsubun := '20' + sGatsubun;
        end;
}
	      EditGatsubun.Text := Copy(sGatsubun, 1, 7);
      end;

      sKaishuuhouhou := FieldByName('KaishuuHouhou').AsString;
      CBKaishuuHouhou.Text := FieldByName('KaishuuHouhou').AsString;
	   	Close;
  	end;
  	//GFlag2 := False;
  end;//of if
end;


//月別残高を計算する
procedure TfrmTKaishuu.DTP1Change(Sender: TObject);
begin
	GetUriageMonth;
end;

//月別残高を計算する
procedure TfrmTKaishuu.GetUriageMonth;
var
	wYyyy, wMm, wDd, wDd2 : Word;
  sDd, sYyyyMmDdTo, sYyyyMmDdFrom : String;
  sYyyyMmDd: String;
begin
{
	//計算期間を取得
  DecodeDate(DTP1.Date, wYyyy, wMm, wDd);

  //締め日を取得する
  if EditTokuisakiCode1.Text = '' then begin
  	Exit;
  end;
  wDd := StrToInt(MTokuisaki.GetShimebi(EditTokuisakiCode1.Text));
  if wDd = CSeikyuuSimebi_End then begin //月末ならば
  	wDd2 := HKLib.GetGetsumatsu(wYyyy, wMm);
  end else begin
  	wDd2 := wDd;
  end;

	//月別残高を計算する

  //回収月
  sYyyyMmDdTo   := IntToStr(wYyyy) + '/' + IntToStr(wMm) + '/' + IntToStr(wDd2);
  labelZan1.Caption := INtTOStr(wMm) + '月分売上';
  if wDd = CSeikyuuSimebi_End then begin //月末ならば
  	wDd2 := 1;
  end else begin
	  wMm := wMm - 1;
  	if wMm = 0 then begin
  		wMm := 12;
	    wYyyy := wYyyy - 1;
  	end;
	  wDd2 := wDd2 + 1;
  end;

  sYyyyMmDdFrom := IntToStr(wYyyy) + '/' + IntToStr(wMm) + '/' + IntToStr(wDd2);
  //19991026
  //MemoZan1.Text := IntToStr(GetZandaka(EditTokuisakiCode1.Text, sYyyyMmDdFrom, sYyyyMmDdTo));
  sYyyyMmDdFrom := sYyyyMmDdFrom
  MemoZan1.Text := IntToStr(GetZandaka2(EditTokuisakiCode1.Text, sYyyyMmDdFrom));

  MemoZan1.Text := HKLib.HFmtCashType(MemoZan1.Text, 0, '');

  //前月
	sYyyyMmDdTo := DateToStr(StrToDate(sYyyyMmDdFrom) - 1);
  if wDd = CSeikyuuSimebi_End then begin //月末ならば
	  wMm := wMm - 1;
  	if wMm = 0 then begin
  		wMm := 12;
	    wYyyy := wYyyy - 1;
  	end;
  	wDd2 := HKLib.GetGetsumatsu(wYyyy, wMm);
  end else begin
  	wDd2 := wDd;
  end;

  labelZan2.Caption := IntToStr(wMm) + '月分売上';
  if wDd = CSeikyuuSimebi_End then begin //月末ならば
  	wDd2 := 1;
  end else begin
	  wMm := wMm - 1;
  	if wMm = 0 then begin
  		wMm := 12;
	    wYyyy := wYyyy - 1;
  	end;
	  wDd2 := wDd2 + 1;
  end;

  sYyyyMmDdFrom := IntToStr(wYyyy) + '/' + IntToStr(wMm) + '/' + IntToStr(wDd2);
  //1999/10/26
  //MemoZan2.Text := IntToStr(GetZandaka(EditTokuisakiCode1.Text, sYyyyMmDdFrom, sYyyyMmDdTo));
  MemoZan2.Text := IntToStr(GetZandaka2(EditTokuisakiCode1.Text, sYyyyMmDdFrom, sYyyyMmDdTo));
  MemoZan2.Text := HKLib.HFmtCashType(MemoZan2.Text, 0, '');

  //前前月
	sYyyyMmDdTo := DateToStr(StrToDate(sYyyyMmDdFrom) - 1);
  if wDd = CSeikyuuSimebi_End then begin //月末ならば
	  wMm := wMm - 1;
  	if wMm = 0 then begin
  		wMm := 12;
	    wYyyy := wYyyy - 1;
  	end;
  	wDd2 := HKLib.GetGetsumatsu(wYyyy, wMm);
  end else begin
  	wDd2 := wDd;
  end;

  labelZan3.Caption := IntToStr(wMm) + '月分売上';
  if wDd = CSeikyuuSimebi_End then begin //月末ならば
  	wDd2 := 1;
  end else begin
	  wMm := wMm - 1;
  	if wMm = 0 then begin
  		wMm := 12;
	    wYyyy := wYyyy - 1;
  	end;
	  wDd2 := wDd2 + 1;
  end;

  sYyyyMmDdFrom := IntToStr(wYyyy) + '/' + IntToStr(wMm) + '/' + IntToStr(wDd2);
  //1999/10/26
  //MemoZan3.Text := IntToStr(GetZandaka(EditTokuisakiCode1.Text, sYyyyMmDdFrom, sYyyyMmDdTo));
  MemoZan3.Text := IntToStr(GetZandaka2(EditTokuisakiCode1.Text, sYyyyMmDdFrom, sYyyyMmDdTo));
  MemoZan3.Text := HKLib.HFmtCashType(MemoZan3.Text, 0, '');
}
  //エラーチェック
  if EditTokuisakiCode1.Text = '' then Exit;

	//計算期間を取得
  DecodeDate(DTP1.Date, wYyyy, wMm, wDd);

  //回収月
  sYyyyMmDd   := IntToStr(wYyyy) + '/' + IntToStr(wMm) + '/01';

  labelZan1.Caption := IntToStr(wMm) + '月分売上';
  MemoZan1.Text := IntToStr(GetZandaka2(EditTokuisakiCode1.Text, sYyyyMmDd));
  MemoZan1.Text := HKLib.HFmtCashType(MemoZan1.Text, 0, '');

  //前月
  wMm := wMm - 1;
  if wMm = 0 then begin
  	wMm := 12;
    wYyyy := wYyyy - 1;
  end;
  sYyyyMmDd   := IntToStr(wYyyy) + '/' + IntToStr(wMm) + '/01';


  labelZan2.Caption := IntToStr(wMm) + '月分売上';
  MemoZan2.Text := IntToStr(GetZandaka2(EditTokuisakiCode1.Text, sYyyyMmDd));
  MemoZan2.Text := HKLib.HFmtCashType(MemoZan2.Text, 0, '');


  //前前月
  wMm := wMm - 1;
  if wMm = 0 then begin
  	wMm := 12;
    wYyyy := wYyyy - 1;
  end;
  sYyyyMmDd   := IntToStr(wYyyy) + '/' + IntToStr(wMm) + '/01';


  labelZan3.Caption := IntToStr(wMm) + '月分売上';
  MemoZan3.Text := IntToStr(GetZandaka2(EditTokuisakiCode1.Text, sYyyyMmDd));
  MemoZan3.Text := HKLib.HFmtCashType(MemoZan3.Text, 0, '');

  //前前前月
  wMm := wMm - 1;
  if wMm = 0 then begin
  	wMm := 12;
    wYyyy := wYyyy - 1;
  end;
  sYyyyMmDd   := IntToStr(wYyyy) + '/' + IntToStr(wMm) + '/01';


  labelZan4.Caption := IntToStr(wMm) + '月分売上';
  MemoZan4.Text := IntToStr(GetZandaka2(EditTokuisakiCode1.Text, sYyyyMmDd));
  MemoZan4.Text := HKLib.HFmtCashType(MemoZan4.Text, 0, '');
end;

//適用（メモ）を自動作成する
//1999/06/05
procedure TfrmTKaishuu.MakeMemo;
var
	sMemo : String;
begin
	if Copy(CBKaishuuHouhou.Text, 1, 1) = '1' then begin
		sMemo := EditGatsubun.Text + '月分集金';
  end else if Copy(CBKaishuuHouhou.Text, 1, 1) = '2' then begin
		sMemo := EditGatsubun.Text + '月分振込み';
  end else if Copy(CBKaishuuHouhou.Text, 1, 1) = '3' then begin
		sMemo := EditGatsubun.Text + '月分小切手回収';
  //1999/09/18　追加
  end else if Copy(CBKaishuuHouhou.Text, 1, 1) = '4' then begin
		sMemo := '返品 ' + EditGatsubun.Text + '月分';
  end else if Copy(CBKaishuuHouhou.Text, 1, 1) = '5' then begin //返金の場合
		sMemo := '返金 ' + EditGatsubun.Text + '月分';
    //2006.06.18
    Showmessage('返金の場合は、金額はマイナスで入力してください。');
    CBKingaku.Text := '-';
  end else if Copy(CBKaishuuHouhou.Text, 1, 1) = '6' then begin
		sMemo := '値引 ' + EditGatsubun.Text + '月分';
  end else if Copy(CBKaishuuHouhou.Text, 1, 1) = '7' then begin
		sMemo := EditGatsubun.Text + '月分貸し倒れ';
  end else if Copy(CBKaishuuHouhou.Text, 1, 1) = '8' then begin
		sMemo := EditGatsubun.Text + '月分雑収入';
  end else begin
  	Exit;
  end;
	Memo1.Lines.Clear;
  Memo1.Lines.Add(sMemo);
end;

procedure TfrmTKaishuu.CBKaishuuHouhouChange(Sender: TObject);
begin
  MakeMemo;
end;

procedure TfrmTKaishuu.EditGatsubunExit(Sender: TObject);
begin
  MakeMemo;
end;



Function TfrmTKaishuu.InsertDenpyou : Boolean;
var
	sSql : String;
begin
	try
		{
    sSql := 'INSERT INTO ' + CtblTDenpyou;
		sSql := sSql + '(';
  	sSql := sSql + 'TokuisakiCode1, ';
		sSql := sSql + 'TokuisakiCode2, ';
		sSql := sSql + 'DNumber,        ';
	  sSql := sSql + 'InputDate,      ';
	  sSql := sSql + 'DDate,          ';
  	sSql := sSql + 'UriageKingaku,  ';
	  sSql := sSql + 'Memo            ';
		sSql := sSql + ') VALUES (';
		sSql := sSql + EditTokuisakiCode1.Text    + ',';
		sSql := sSql + EditTokuisakiCode2.Text    + ',';
		sSql := sSql + '0,'; //伝票ナンバーはとりあえず使わない
		sSql := sSql + '''' + DateToStr(Date) + ''',';
		sSql := sSql + '''' + DateToStr(DTP1.Date)            + ''',';
		sSql := sSql + '-' + CBKingaku.Text       + ',';
		sSql := sSql + '''' + Memo1.Text     + '''';
  	sSql := sSql + ')';
    }

  //新規登録の場合
	sSql := 'INSERT INTO ' + CtblTDenpyou;
  sSql := sSql + '(';
  sSql := sSql + 'TokuisakiCode1, ';
  sSql := sSql + 'TokuisakiCode2, ';
  sSql := sSql + 'DNumber       , ';
  sSql := sSql + 'InputDate     , ';
  sSql := sSql + 'DDate         , ';
  sSql := sSql + 'UriageKingaku ,';
  sSql := sSql + 'Memo          ,';
  sSql := sSql + 'KaishuuHouhou,  ';
  sSql := sSql + 'Gatsubun  ';
  sSql := sSql + ')';
  sSql := sSql + ' VALUES ';
  sSql := sSql + '(';
  sSql := sSql + EditTokuisakiCode1.Text + ',';
  sSql := sSql + EditTokuisakiCode2.Text + ',';
  sSql := sSql + '0,'; //伝票ナンバーは使わない
  sSql := sSql + '''' + DateToStr(Date()) + ''', ';
  sSql := sSql + '''' + DateToStr(DTP1.Date) + ''', ';
  sSql := sSql + IntToStr((-1*StrToInt(CBKingaku.Text)))+ ', ';
  sSql := sSql + '''' + Memo1.Text + ''', ';
  sSql := sSql + Copy(CBKaishuuHouhou.Text, 1, 1) + ',';
  sSql := sSql + '''' + EditGatsubun.Text + '/01'''; //1999/09/10 追加 月分
  sSql := sSql + ')';



	 	with frmDMMaster.QueryInsert do begin
		 	Close;
	    Sql.Clear;
  		Sql.Add(sSql);
  		ExecSql;
	    Close;
  	end;//of with
    Result := True;
  except
	  on E: EDBEngineError do begin
    	ShowMessage(E.Message);
	    Result := False;
    end;
  end;
end;

//現在の伝票を削除する
//1999/06/20
Function TfrmTKaishuu.DeleteDenpyou : Boolean;
var
	sSql : String;
  iID : Integer;
begin
	if (LabelID.Caption = 'ID') or (LabelID.Caption = '0') then begin
  	ShowMessage('伝票番号が特定できないため削除できません');
    Exit;
  end;

	sSql := 'DELETE FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' ID =  ' + LabelID.Caption;

  try
	  with frmDMMaster.QueryDelete do begin
  	  Close;
			Sql.Clear;
	    Sql.Add(sSql);
			ExecSql;
	  	Close;
	  end;//of with
    Result := True;
  except
	  on E: EDBEngineError do begin
    	ShowMessage(E.Message);
	    Result := False;
    end;
  end;
end;

procedure TfrmTKaishuu.DTP1Exit(Sender: TObject);
begin
  CBKingaku.Setfocus;
end;

//1999/09/06
//回収データをエクセルに出力する
//回収日固定で、得意先名、金額、回収方法をCSV形式で
//
procedure TfrmTKaishuu.BitBtn2Click(Sender: TObject);
var
	sSql, sLine, strMemo, strDate,sWhere : String;
 	F : TextFile;
begin
	//確認メッセージ
  if MessageDlg('回収一覧を出力しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;

  //回収日の取得
  strDate := DateToStr(DTP1.Date);

  //SQL文の作成
  sSql := 'SELECT DDate, TokuisakiCode1,KaishuuHouhou,UriageKingaku, Memo FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE DDate = ''' + strDate + '''';
  //sSql := sSql + ' AND UriageKingaku < 0';   // 2007.07.21 CommentOut by utada
  sSql := sSql + ' AND KaishuuHouhou > 0';
  sSql := sSql + ' OR (DDate = ''' + strDate + '''' + 'AND Memo = ''返品'')';
    sSql := sSql + ' ORDER BY TokuisakiCode1';

	//出力するファイルを作成する、すでにあれば削除する
  AssignFile(F, CFileName_Kaishuu);
	Rewrite(F);
	CloseFile(F);

  //ファイルへ書き込み
  with frmDMMaster.QueryKaishuu do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	sLine := FieldByName('TokuisakiCode1').AsString;

      //2006.06.18
      sWhere := 'TokuisakiCode1=' + FieldByName('TokuisakiCode1').AsString;
      sLine := sLine + ',' + DMMaster.GetFieldData(CtblMTokuisaki, 'TokuisakiCode2', sWhere);

    	sLine := sLine + ',' + GetTokuisakiName(FieldByName('TokuisakiCode1').AsString);
    	//sLine := sLine + ',' + FieldByName('KaishuuHouhou').AsString; // Comment Out by utada
      //mod after by utada 回収方法がNULlの場合は返品(4)として処理
      if  (FieldByName('KaishuuHouhou').AsString ='')  And (FieldByName('Memo').AsString='返品') then begin
         sLine := sLine + ',4';
      end else  begin
         sLine := sLine + ',' + FieldByName('KaishuuHouhou').AsString;
      end;
    	sLine := sLine + ',' + IntToStr(-1 * FieldByName('UriageKingaku').AsInteger);
    	sLine := sLine + ',' + FieldByName('DDate').AsString;
    	strMemo := FieldByName('Memo').AsString;
      strMemo := HKLib.RepraceCh(strMemo, #10, Char(' '));
      strMemo := HKLib.RepraceCh(strMemo, #13, Char(' '));
    	sLine := sLine + ',' + strMemo;


		  HMakeFile(CFileName_Kaishuu, sLine);
      Next;
    end;//of while
  end;//of with
  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '回収一覧.xls', '', SW_SHOW);
end;

//新しい残高計算のボタンがクリックされた
procedure TfrmTKaishuu.Button1Click(Sender: TObject);
var
	wYyyy, wMm, wDd, wDd2 : Word;
  sDd, sYyyyMmDd: String;
begin
	//計算期間を取得
  DecodeDate(DTP1.Date, wYyyy, wMm, wDd);

  //回収月
  sYyyyMmDd   := IntToStr(wYyyy) + '/' + IntToStr(wMm) + '/01';

  labelZan1.Caption := IntToStr(wMm) + '月分売上';
  MemoZan1.Text := IntToStr(GetZandaka2(EditTokuisakiCode1.Text, sYyyyMmDd));
  MemoZan1.Text := HKLib.HFmtCashType(MemoZan1.Text, 0, '');

  //前月
  wMm := wMm - 1;
  if wMm = 0 then begin
  	wMm := 12;
    wYyyy := wYyyy - 1;
  end;
  sYyyyMmDd   := IntToStr(wYyyy) + '/' + IntToStr(wMm) + '/01';


  labelZan2.Caption := IntToStr(wMm) + '月分売上';
  MemoZan2.Text := IntToStr(GetZandaka2(EditTokuisakiCode1.Text, sYyyyMmDd));
  MemoZan2.Text := HKLib.HFmtCashType(MemoZan2.Text, 0, '');


  //前前月
  wMm := wMm - 1;
  if wMm = 0 then begin
  	wMm := 12;
    wYyyy := wYyyy - 1;
  end;
  sYyyyMmDd   := IntToStr(wYyyy) + '/' + IntToStr(wMm) + '/01';


  labelZan3.Caption := IntToStr(wMm) + '月分売上';
  MemoZan3.Text := IntToStr(GetZandaka2(EditTokuisakiCode1.Text, sYyyyMmDd));
  MemoZan3.Text := HKLib.HFmtCashType(MemoZan3.Text, 0, '');

end;

//回収方法がマスターと違った場合のメッセージ
//2000/01/24
Procedure TfrmTKaishuu.CheckKaishuu;
var
	sKaishuu, sWhere, sHouhou : String;
begin
	//マスターの回収方法の取得
  sWhere := 'TokuisakiCode1 = ' + EditTokuisakiCode1.Text;
	sKaishuu := GetFieldData(CtblMTokuisaki, 'ShiharaiKubun', sWhere);
  if sKaishuu = '10' then begin //10 現金
  	sKaishuu := '1';
    sHouhou := '現金';
  end else if sKaishuu = '11' then begin //11 小切手
  	sKaishuu := '3';
    sHouhou := '小切手';
  end else if sKaishuu = '20' then begin //20 振り込み
  	sKaishuu := '2';
    sHouhou := '振り込み';
	end;//of if

  if Copy(CBKaishuuHouhou.Text, 1, 1) <> sKaishuu then begin
  	ShowMessage('回収方法のアラートです。得意先マスターでは"' + sHouhou + '"です。');
  end;
end;


procedure TfrmTKaishuu.FormActivate(Sender: TObject);
begin
  inherited;
    GetData;

end;

end.
