unit PMishuukin;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, StdCtrls, Buttons, ExtCtrls, ComCtrls, ShellAPI;

type
  TfrmPMishuukin = class(TfrmMaster)
    Label3: TLabel;
    DTP1: TDateTimePicker;
    BitBtn3: TBitBtn;
    EditTokuisakiCode1: TEdit;
    Label4: TLabel;
    CBGatsubun: TComboBox;
    Label5: TLabel;
    procedure BitBtn2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
  private
    { Private 宣言 }
		Function GetUriageSum(sTokuisakiCode, sShimebi:String) : Integer;
    Function GetKaishuuSum(sTokuisakiCode, sShiharaibi:String): Integer;
    
  public
    { Public 宣言 }
  end;

var
  frmPMishuukin: TfrmPMishuukin;

implementation

uses DMMaster, Inter, HKLib;

{$R *.DFM}

procedure TfrmPMishuukin.BitBtn2Click(Sender: TObject);
var
	sSql, sSql2, sLine, sShiharaiKubun : String;
 	F : TextFile;
  sTokuisakiCode, sTokuisakiname, sShimebi, sShiharaibi, sKikan, sShiharaituki:String;
  wYyyy, wMm, wDd : Word;
  DTShimebi, DTShiharaibi : TDateTime;
  iUriageSum, iKaishuuSum : Integer;
begin
	//確認メッセージ
  if MessageDlg('印刷しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;

	//出力するファイルを作成する、すでにあれば削除する
  //請求明細用
 	AssignFile(F, CFileName_Mishuukin);
  Rewrite(F);
	CloseFile(F);
  sLine := DateToStr(DTP1.Date) + '現在';
  HMakeFile(CFileName_Mishuukin, sLine);

	sSql := 'SELECT TokuisakiCode1, TokuisakiName, SeikyuuSimebi, ShiharaibiD, ShiharaibiM, ShiharaiKubun';
  sSql := sSql + ' FROM ' + CtblMTokuisaki;

  //テスト用
  //sSql := sSql + ' WHERE TokuisakiCode1 = 1389';

  sSql := sSql + ' ORDER BY TokuisakiCode1';


//未収金の計算
//得意先ごとにループ
	with frmDMMaster.QPMTokuisaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	sTokuisakiCode := FieldByName('TokuisakiCode1').AsString;
    	sTokuisakiName := FieldByName('TokuisakiName').AsString;
      if sTokuisakiName = '' then begin
      	Next;
        Continue;
      end;
    	sShimebi := FieldByName('SeikyuuSimebi').AsString;
      if sShimebi = '' then begin
      	Next;
        Continue;
      end;
    	sShiharaibi := FieldByName('ShiharaibiD').AsString;
      if sShiharaibi = '' then begin
      	Next;
        Continue;
      end;
    	sShiharaituki := FieldByName('ShiharaibiM').AsString;
      if sShiharaituki = '' then begin
      	Next;
        Continue;
      end;
    	sShiharaiKubun := FieldByName('ShiharaiKubun').AsString;

      //情報の画面表示
      SB1.SimpleText := '得意先コード=' + sTokuisakiCode;
      Update;

			//1999/07/25
		  //直前の支払日を取得
      //(sShiharaibi -> 5, 10, 15, 20, 25, 0=月末)
      //(sShiharaituki -> 0=当月, 1=翌月, 2=翌々月, 3=翌翌々月)
      DecodeDate(DTP1.Date, wYyyy, wMm, wDd);
      if sShiharaibi = '0' then begin
      	sShiharaibi := IntToStr(GetGetsumatsu(wYyyy, wMm));
      end;
      if wDd >= StrToInt(sShiharaibi) then begin
      	wDd := StrToInt(sShiharaibi);
        DTShiharaibi := EncodeDate(wYyyy, wMm, wDd);
      end else begin
      	wMm := wMm - 1;
      	wDd := StrToInt(sShiharaibi);
        if wMm = 0 then begin
        	wMm := 12;
          wYyyy := wYyyy - 1;
        end;
      end;

      if FieldByName('ShiharaibiD').AsString = '0' then begin //月末締めならば
	     	sShiharaibi := IntToStr(GetGetsumatsu(wYyyy, wMm));
  	   	wDd := StrToInt(sShiharaibi);
      end;

      DTShiharaibi := EncodeDate(wYyyy, wMm, wDd);

		  //直前の請求締め日を取得(sShimebi -> 5, 10, 15, 20, 25, 0=月末)
      wMm := wMm - StrToInt(sShiharaituki);

      if sShimebi = '0' then begin
      	sShimebi := IntToStr(GetGetsumatsu(wYyyy, wMm));
      end;
     	wDd := StrToInt(sShimebi);
      DTShimebi := EncodeDate(wYyyy, wMm, wDd);

      //情報の画面表示
      SB1.SimpleText := SB1.SimpleText + ' 締め日=' + DateToStr(DTShimebi);
      Update;

		  //直前の請求締め日までの売上金額の合計を算出
      iUriageSum := GetUriageSum(sTokuisakiCode, DateToStr(DTShimebi));
      if iUriageSum = 0 then begin
      	Next;
        Continue;
      end;

  		//直前の支払日までの回収金額の合計を算出
      //1999/07/25 集計日までの回収金額の合計を算出に変更
      //iKaishuuSum := -1 * GetKaishuuSum(sTokuisakiCode, DateToStr(DTShiharaibi));
      iKaishuuSum := -1 * GetKaishuuSum(sTokuisakiCode, DateToStr(DTP1.Date));

		  //差額を求める
      if (iUriageSum - iKaishuuSum) <> 0 then begin
			  //ゼロ以外ならばファイルに出力
			  //	得意先コード・得意先名・締め日・支払期間・支払い月・差額

	      //(sShiharaituki -> 0=当月, 1=翌月, 2=翌々月, 3=翌翌々月)
        if sShiharaituki = '0' then begin
        	sShiharaituki := '当月';
        end else if sShiharaituki = '1' then begin
        	sShiharaituki := '翌月';
        end else if sShiharaituki = '2' then begin
        	sShiharaituki := '翌々月';
        end else if sShiharaituki = '3' then begin
        	sShiharaituki := '翌翌々月';
        end;

        sLine := sTokuisakiCode + ',' + sTokuisakiName + ',';
        sLine := sLine + sShimebi + '日〆/' + sShiharaituki + sShiharaibi + '日払い,';
        sLine := sLine + IntToStr(iUriageSum - iKaishuuSum) + ',';

        //支払い区分
        if sShiharaiKubun = '10' then begin
        	sShiharaiKubun := '現金';
        end else if sShiharaiKubun = '11' then begin
        	sShiharaiKubun := '小切手';
        end else if sShiharaiKubun = '20' then begin
        	sShiharaiKubun := '振込み';
        end;
        sLine := sLine + sShiharaiKubun;

        //テスト用情報
        sLine := sLine + ',' + DateToStr(DTShimebi) + ',' + DateToStr(DTShiharaibi);

	 		  HMakeFile(CFileName_Mishuukin, sLine);
      end;
  		Next;
  	end;//of while
    Close;
  end;//of do

	//エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '未収金.xls', '', SW_SHOW);

end;

//直前の請求締め日までの売上金額の合計を算出
Function TfrmPMishuukin.GetUriageSum(sTokuisakiCode, sShimebi:String) : Integer;
var
	sSql : String;
  iSum : Integer;
begin
  sSql := 'SELECT UriageSum = SUM(UriageKingaku) ';
  sSql := sSql + ' FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';

 	sSql := sSql + 'TokuisakiCode1 = ' + sTokuisakiCode;
 	sSql := sSql + ' AND ';

	sSql := sSql + 'DDate Between ';
	//sSql := sSql + '''' + '1999/01/01' + '''' + ' AND ';

  //mod bodere 2014.07.17 utada
  //sSql := sSql + '''' + CsMinDate + '''' + ' AND ';
  //mod after  2014.07.17 utada
	sSql := sSql + '''' + '2009/08/01' + '''' + ' AND ';

	sSql := sSql + '''' + sShimebi + '''';
	sSql := sSql + ' AND ';

  //sSql := sSql + ' ((UriageKingaku > 0) OR (Memo IN (''返品'', ''値引'')))' ;
//  sSql := sSql + ' ((UriageKingaku > 0) OR (SUBSTRING(Memo, 1, 2) IN (''返品'', ''値引'')))' ;
  sSql := sSql + ' ((UriageKingaku > 0) OR (SUBSTRING(Memo, 1, 2) IN (''返品'', ''値引'', ''取消'')))' ;

	//sSql := sSql + ' AND ';
	//sSql := sSql + ' Memo <> ''' + CsCTAX + '''';//消費税ではない

  with frmDMMaster.QPMUriage do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    open;
    iSum := FieldByName('UriageSum').AsInteger;
    Close;
  end;
  Result := iSum;
end;

//直前の支払日までの回収金額の合計を算出
Function TfrmPMishuukin.GetKaishuuSum(sTokuisakiCode, sShiharaibi:String): Integer;
var
	sSql : String;
  iSum : Integer;
begin
  sSql := 'SELECT UriageSum = SUM(UriageKingaku) ';
  sSql := sSql + ' FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';

 	sSql := sSql + 'TokuisakiCode1 = ' + sTokuisakiCode;
 	sSql := sSql + ' AND ';

	sSql := sSql + 'DDate Between ';
	//sSql := sSql + '''' + '1999/01/01' + '''' + ' AND ';

  //mod bodere 2014.07.17 utada
//	sSql := sSql + '''' + CsMinDate + '''' + ' AND ';
  //mod after  2014.07.17 utada
	sSql := sSql + '''' + '2009/08/01' + '''' + ' AND ';



	sSql := sSql + '''' + sShiharaibi + '''';
	sSql := sSql + ' AND ';

  //sSql := sSql + ' ((UriageKingaku < 0) AND (Memo NOT IN(''返品'', ''値引'')))' ;
//  sSql := sSql + ' ((UriageKingaku < 0) AND (SUBSTRING(Memo, 1, 2) NOT IN(''返品'', ''値引'')))' ;
   sSql := sSql + ' ((UriageKingaku < 0) AND (SUBSTRING(Memo, 1, 2) NOT IN(''返品'', ''値引'', ''取消'')))' ;


  with frmDMMaster.QKaishuu do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    open;
    iSum := FieldByName('UriageSum').AsInteger;
    Close;
  end;
  Result := iSum;
end;

procedure TfrmPMishuukin.FormCreate(Sender: TObject);
var
  wYyyy, wMm, wDd      : Word;
  sYear, sMonth, sDay  : String;
begin
	Width := 540;
  Height := 200;
  DTP1.Date := Date();

   // 年・月を当日の年・月に設定
   DecodeDate(Date(), wYyyy, wMm, wDd);
   sYear  := IntToStr(wYyyy);
   sMonth := IntToStr(wMm);
   sDay   := IntToStr(wDd);
   CBGatsubun.Text := sYear + '/' + sMonth
end;

//1999/09/10
//Excelへ出力
//月ごとに計算する
//とりあえずtblTDenpyouの回収データにGatsubunがすべて入っているもの
//のみ計算対象とする。
procedure TfrmPMishuukin.BitBtn3Click(Sender: TObject);
var
	sSql, sSql2, sLine, sShiharaiKubun, sGatsubun : String;
 	F : TextFile;
  sTokuisakiCode,sTokuisakiCode2, sTokuisakiname, sShimebi, sShiharaibi, sKikan, sShiharaituki:String;
  wYyyy, wMm, wDd : Word;
  sYyyy, sMm, sDd : String;
  DTShimebi, DTShiharaibi : TDateTime;
  iUriageSum, iKaishuuSum : Integer;
  sGatsubunMin, sGatsubunMax : String;
begin

	//確認メッセージ
  if MessageDlg('回収データに？月分がすべて入力されているもののみ計算します。' + #10#13 + '印刷しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;

	//出力するファイルを作成する、すでにあれば削除する
  //請求明細用
 	AssignFile(F, CFileName_Mishuukin);
  Rewrite(F);
	CloseFile(F);
  sLine := DateToStr(DTP1.Date) + '現在';
  HMakeFile(CFileName_Mishuukin, sLine);

	sSql := 'SELECT TokuisakiCode1, TokuisakiCode2,TokuisakiName, SeikyuuSimebi, ShiharaibiD, ShiharaibiM, ShiharaiKubun';
  sSql := sSql + ' FROM ' + CtblMTokuisaki;

  //2002.08.11
  sSql := sSql + ' WHERE TokuisakiCode1<50000 ';
  //テスト用
  if Length(EditTokuisakiCode1.Text) > 0 then
	  sSql := sSql + ' AND TokuisakiCode1 = ' + EditTokuisakiCode1.Text;

  sSql := sSql + ' ORDER BY TokuisakiCode1';


//未収金の計算
//得意先ごとにループ
	with frmDMMaster.QPMTokuisaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    if EOF then begin
    	ShowMessage(EditTokuisakiCode1.Text + 'のデータが見つかりません');
      Exit;
    end;

    while not EOF do begin
    	sTokuisakiCode := FieldByName('TokuisakiCode1').AsString;
    	sTokuisakiCode2 := FieldByName('TokuisakiCode2').AsString;
    	sTokuisakiName := FieldByName('TokuisakiName').AsString;

      //情報の画面表示
      SB1.SimpleText := '得意先コード=' + sTokuisakiCode + sTokuisakiName;
      Update;

      //エラースキップ
      if sTokuisakiName = '' then begin
      	Next;
        Continue;
      end;
    	sShimebi := FieldByName('SeikyuuSimebi').AsString;
      if sShimebi = '' then begin
      	Next;
        Continue;
      end;
    	sShiharaibi := FieldByName('ShiharaibiD').AsString;
      if sShiharaibi = '' then begin
      	Next;
        Continue;
      end;
    	sShiharaituki := FieldByName('ShiharaibiM').AsString;
      if sShiharaituki = '' then begin
      	Next;
        Continue;
      end;

      //情報の画面表示
      SB1.SimpleText := '得意先コード=' + sTokuisakiCode;
      Update;

      //得意先に関する共通情報の取得
      //支払い区分
	   	sShiharaiKubun := FieldByName('ShiharaiKubun').AsString;
      if sShiharaiKubun = '10' then begin
      	sShiharaiKubun := '現金';
      end else if sShiharaiKubun = '11' then begin
      	sShiharaiKubun := '小切手';
      end else if sShiharaiKubun = '20' then begin
      	sShiharaiKubun := '振込み';
      end;

      //Gatsubunデータがすべて入力されているかチェック
      //1999/09/13

      // mod  bofre utada 2014.07.17
      //sGatsubunMin := DMMaster.CheckGatsubun(sTokuisakiCode, 'Min');
      // mod  after utada 2014.07.17   
      sGatsubunMin := '2009/08/01';


      if Length(sGatsubunMin) <>10 then begin
      	ShowMessage(sTokuisakiCode + 'の月分が不正です');
        next;
        continue;
      end;

      //1999/10/23 意味不明のためコメントアウト
{
      if StrToDate(sGatsubunMin) = StrToDate(CsMinDate) then begin //一番はじめの日
      	Next;
      	Continue;
      end;
}
      //各月でループ sGatsubunMin から sGatsubunMax まで
      sGatsubunMax := DMMaster.CheckGatsubun(sTokuisakiCode, 'Max');

      if Length(sGatsubunMax) <>10 then begin
      	ShowMessage(sTokuisakiCode + 'の月分が不正です');
        next;
        continue;
      end;

			//1999/09/27 追加 「？月分まで」の指定
      if StrToDate(sGatsubunMax) > StrToDate(CBGatsubun.Text + '/01') then begin
      	sGatsubunMax := CBGatsubun.Text + '/01';
	    end;

      DecodeDate(StrToDate(sGatsubunMin), wYyyy, wMm, wDd);
      sYyyy := IntToStr(wYyyy);
      sMm := IntToStr(wMm);

      sGatsubun := sYyyy + '/' + sMm + '/' + '01';
      while StrToDate(sGatsubun) <= StrToDate(sGatsubunMax) do begin
	      //情報の画面表示
  	    SB1.SimpleText := '得意先コード=' + sTokuisakiCode + sTokuisakiName + '-' + sGatsubun + '月分';
    	  SB1.Update;
        //SB1.Repaint;

      	iUriageSum := GetGatsubunSum(sTokuisakiCode, sGatsubun);

        //1999/12/13
        //未収金のマイナスも出るように変更
        //if iUriageSum > 0 then begin
        if iUriageSum <> 0 then begin
          //1999/09/14
		  		//直前の請求締め日を取得(sShimebi -> 5, 10, 15, 20, 25, 0=月末)
		      //wMm := wMm - StrToInt(sShiharaituki);
		      DecodeDate(StrToDate(sGatsubun), wYyyy, wMm, wDd);
		    	sShimebi := FieldByName('SeikyuuSimebi').AsString;
 		     	if sShimebi = '0' then begin
    	  		sShimebi := IntToStr(GetGetsumatsu(wYyyy, wMm));
	    	  end;
	  	   	wDd := StrToInt(sShimebi);
  	  	  DTShimebi := EncodeDate(wYyyy, wMm, wDd);

				  //直前の支払日を取得
		      //(sShiharaibi -> 5, 10, 15, 20, 25, 0=月末)
    		  //(sShiharaituki -> 0=当月, 1=翌月, 2=翌々月, 3=翌翌々月)
		    	sShiharaituki := FieldByName('ShiharaibiM').AsString;
		      DecodeDate(StrToDate(sGatsubun), wYyyy, wMm, wDd);
          wMm := wMm + StrToInt(sShiharaituki);
          if wMm >= 13 then begin
          	wMm := wMm-12;
            wYyyy := wYyyy + 1;
          end;
    		  if sShiharaibi = '0' then begin
      			sShiharaibi := IntToStr(GetGetsumatsu(wYyyy, wMm));
		      end;
    		  if wDd >= StrToInt(sShiharaibi) then begin
		      	wDd := StrToInt(sShiharaibi);
    		    DTShiharaibi := EncodeDate(wYyyy, wMm, wDd);
		      end else begin
    		  	//wMm := wMm - 1;
		      	wDd := StrToInt(sShiharaibi);
    		    if wMm = 0 then begin
        			wMm := 12;
		          wYyyy := wYyyy - 1;
    		    end;
		      end;
		      if FieldByName('ShiharaibiD').AsString = '0' then begin //月末締めならば
	  		   	sShiharaibi := IntToStr(GetGetsumatsu(wYyyy, wMm));
  	   			wDd := StrToInt(sShiharaibi);
		      end;
    		  DTShiharaibi := EncodeDate(wYyyy, wMm, wDd);


		    	//(sShiharaituki -> 0=当月, 1=翌月, 2=翌々月, 3=翌翌々月)
	    	  if sShiharaituki = '0' then begin
  	    		sShiharaituki := '当月';
	  	    end else if sShiharaituki = '1' then begin
  	  	  	sShiharaituki := '翌月';
    	  	end else if sShiharaituki = '2' then begin
	      		sShiharaituki := '翌々月';
		      end else if sShiharaituki = '3' then begin
  		    	sShiharaituki := '翌翌々月';
    		  end;

	        sLine := sTokuisakiCode + ',' +sTokuisakiCode2 + ',' + sTokuisakiName + ',';
  	      sLine := sLine + sShimebi + '日〆/' + sShiharaituki + sShiharaibi + '日払い,';
    	    sLine := sLine + IntToStr(iUriageSum) + ',';

        	sLine := sLine + sShiharaiKubun;

	        //テスト用情報
  	      sLine := sLine + ',' + DateToStr(DTShimebi) + ',' + DateToStr(DTShiharaibi);

	 			  HMakeFile(CFileName_Mishuukin, sLine);
        end;//of if

        //次の処理
        sMm := IntToStr(StrToInt(sMm) + 1);
        if sMm = '13' then begin
          sYyyy := IntToStr(StrToInt(sYyyy) + 1);
          sMm := '01';
        end;
        sGatsubun := sYyyy + '/' + sMm + '/' + '01';
      end; // of while
  		Next;
  	end;//of while
    Close;
  end;//of do

	//エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '未収金.xls', '', SW_SHOW);

end;


end.
