inherited frmMShiiresaki: TfrmMShiiresaki
  Left = 458
  Top = 44
  Width = 650
  Height = 646
  Caption = 'frmMShiiresaki'
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  inherited Panel1: TPanel
    Width = 642
    inherited Label1: TLabel
      Width = 114
      Caption = #20181#20837#20808#12487#12540#12479
    end
  end
  inherited Panel2: TPanel
    Top = 555
    Width = 642
    inherited BitBtn1: TBitBtn
      Left = 544
      Top = 10
    end
    inherited BitBtn2: TBitBtn
      Left = 6
      Top = 10
      OnClick = BitBtn2Click
    end
    object BitBtn3: TBitBtn
      Left = 452
      Top = 10
      Width = 91
      Height = 25
      Caption = #30331#12288#37682
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = BitBtn3Click
      Kind = bkOK
    end
    object BitBtn4: TBitBtn
      Left = 360
      Top = 10
      Width = 91
      Height = 25
      Caption = #20181#20837#12428#21488#24115
      Default = True
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ModalResult = 1
      ParentFont = False
      TabOrder = 3
      OnClick = BitBtn4Click
      NumGlyphs = 2
    end
    object BitBtn5: TBitBtn
      Left = 290
      Top = 10
      Width = 69
      Height = 25
      Caption = #12463#12522#12450
      Default = True
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ModalResult = 1
      ParentFont = False
      TabOrder = 4
      OnClick = BitBtn5Click
      NumGlyphs = 2
    end
    object BitBtn6: TBitBtn
      Left = 161
      Top = 10
      Width = 109
      Height = 25
      Caption = #23451#21517#12521#12505#12523
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clPurple
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = BitBtn6Click
    end
  end
  inherited Panel3: TPanel
    Width = 642
    Height = 344
    Align = alTop
    object Label3: TLabel
      Left = 30
      Top = 12
      Width = 81
      Height = 12
      Caption = #20181#20837#20808#12467#12540#12489#65297
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 30
      Top = 37
      Width = 24
      Height = 12
      Caption = #12424#12415
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 30
      Top = 62
      Width = 52
      Height = 12
      Caption = #20181#20837#20808#21517
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label37: TLabel
      Left = 30
      Top = 110
      Width = 52
      Height = 12
      Caption = #38651#35441#30058#21495
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label39: TLabel
      Left = 30
      Top = 136
      Width = 25
      Height = 12
      Caption = 'FAX'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label17: TLabel
      Left = 228
      Top = 159
      Width = 52
      Height = 12
      Caption = #37117#36947#24220#30476
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label16: TLabel
      Left = 30
      Top = 158
      Width = 52
      Height = 12
      Caption = #37109#20415#30058#21495
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label19: TLabel
      Left = 30
      Top = 183
      Width = 50
      Height = 12
      Caption = #30058#22320#12394#12393
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 30
      Top = 86
      Width = 26
      Height = 12
      Caption = #30053#31216
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 290
      Top = 87
      Width = 39
      Height = 12
      Caption = #25285#24403#32773
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 290
      Top = 109
      Width = 39
      Height = 12
      Caption = #20195#34920#32773
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 294
      Top = 129
      Width = 26
      Height = 12
      Caption = #20869#23481
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 46
      Top = 273
      Width = 22
      Height = 12
      Caption = #12513#12514
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 30
      Top = 207
      Width = 26
      Height = 12
      Caption = #12294#26085
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label12: TLabel
      Left = 30
      Top = 231
      Width = 39
      Height = 12
      Caption = #25903#25173#26085
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label13: TLabel
      Left = 284
      Top = 207
      Width = 52
      Height = 12
      Caption = #25903#25173#26041#27861
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label14: TLabel
      Left = 284
      Top = 228
      Width = 33
      Height = 12
      Caption = #12469#12452#12488
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 284
      Top = 250
      Width = 65
      Height = 12
      Caption = #28040#36027#31246#35336#31639
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object CBShiiresakiCode: TComboBox
      Left = 126
      Top = 8
      Width = 97
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imClose
      ItemHeight = 13
      ParentFont = False
      TabOrder = 0
      OnKeyDown = CBShiiresakiCodeKeyDown
    end
    object CBYomi: TComboBox
      Left = 190
      Top = -32
      Width = 250
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imOpen
      ItemHeight = 13
      ParentFont = False
      TabOrder = 1
    end
    object CBName: TComboBox
      Left = 126
      Top = 56
      Width = 250
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imOpen
      ItemHeight = 13
      ParentFont = False
      TabOrder = 2
      OnExit = CBNameExit
      OnKeyDown = CBNameKeyDown
    end
    object CBTel: TComboBox
      Left = 126
      Top = 104
      Width = 145
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imClose
      ItemHeight = 13
      ParentFont = False
      TabOrder = 4
    end
    object CBFax: TComboBox
      Left = 126
      Top = 128
      Width = 145
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imClose
      ItemHeight = 13
      ParentFont = False
      TabOrder = 5
    end
    object CBAdd1: TComboBox
      Left = 310
      Top = 153
      Width = 119
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imOpen
      ItemHeight = 13
      ParentFont = False
      TabOrder = 7
      Text = #26481#20140#37117
      Items.Strings = (
        #26481#20140#37117
        #31070#22856#24029#30476)
    end
    object CBZip: TComboBox
      Left = 126
      Top = 152
      Width = 85
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imClose
      ItemHeight = 13
      ParentFont = False
      TabOrder = 6
    end
    object CBAdd2: TComboBox
      Left = 126
      Top = 176
      Width = 375
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imOpen
      ItemHeight = 13
      ParentFont = False
      TabOrder = 8
    end
    object CBName2: TComboBox
      Left = 126
      Top = 80
      Width = 145
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imOpen
      ItemHeight = 13
      ParentFont = False
      TabOrder = 3
    end
    object EditTantou: TEdit
      Left = 336
      Top = 84
      Width = 185
      Height = 20
      ImeMode = imOpen
      TabOrder = 9
    end
    object EditDaihyousha: TEdit
      Left = 336
      Top = 106
      Width = 185
      Height = 20
      TabOrder = 10
    end
    object EditNaiyou: TEdit
      Left = 336
      Top = 126
      Width = 269
      Height = 20
      ImeMode = imOpen
      TabOrder = 11
    end
    object MemoMemo: TMemo
      Left = 126
      Top = 272
      Width = 377
      Height = 65
      ImeMode = imOpen
      TabOrder = 12
    end
    object CBShimebi: TComboBox
      Left = 126
      Top = 200
      Width = 83
      Height = 20
      ItemHeight = 12
      TabOrder = 13
      Text = '0,'#26376#26411
      Items.Strings = (
        '0,'#26376#26411
        '10,10'#26085
        '15,15'#26085
        '20,20'#26085
        '25,25'#26085)
    end
    object CBShiharaibi: TComboBox
      Left = 126
      Top = 224
      Width = 83
      Height = 20
      ItemHeight = 12
      TabOrder = 14
      Text = '0,'#26376#26411
      Items.Strings = (
        '0,'#26376#26411
        '5,5'#26085
        '10,10'#26085
        '15,15'#26085
        '20,20'#26085
        '25,25'#26085)
    end
    object CBShiharaiHouhou: TComboBox
      Left = 360
      Top = 202
      Width = 121
      Height = 20
      ItemHeight = 12
      TabOrder = 15
      Text = #23567#20999#25163
      Items.Strings = (
        #23567#20999#25163
        #25163#24418
        #25391#12426#36796#12415
        #29694#37329)
    end
    object CBSite: TComboBox
      Left = 360
      Top = 224
      Width = 81
      Height = 20
      ItemHeight = 12
      TabOrder = 16
      Text = '30'
      Items.Strings = (
        '20'
        '25'
        '30'
        '45'
        '50'
        '55'
        '60'
        '65'
        '70'
        '75'
        '80'
        '85'
        '90'
        '105'
        '115')
    end
    object CBTax: TComboBox
      Left = 360
      Top = 248
      Width = 81
      Height = 20
      ItemHeight = 12
      TabOrder = 17
      Text = #22235#25448#20116#20837
      Items.Strings = (
        #22235#25448#20116#20837
        #20999#12426#19978#12370
        #20999#12426#25448#12390)
    end
    object EditYomi: TEdit
      Left = 126
      Top = 33
      Width = 243
      Height = 20
      ImeMode = imOpen
      TabOrder = 18
      OnKeyDown = EditYomiKeyDown
    end
  end
  inherited SB1: TStatusBar
    Top = 596
    Width = 642
  end
  object Panel4: TPanel
    Left = 0
    Top = 385
    Width = 642
    Height = 170
    Align = alClient
    Caption = 'Panel4'
    TabOrder = 4
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 640
      Height = 168
      ActivePage = tsOffice
      Align = alClient
      TabOrder = 0
      object tsOffice: TTabSheet
        Caption = #24773#22577
        object Label28: TLabel
          Left = 22
          Top = 6
          Width = 52
          Height = 12
          Caption = #27880#25991#26041#27861
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label29: TLabel
          Left = 22
          Top = 31
          Width = 52
          Height = 12
          Caption = #32224#12417#26178#38291
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label30: TLabel
          Left = 22
          Top = 55
          Width = 52
          Height = 12
          Caption = #32013#20837#26085#25968
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label32: TLabel
          Left = 216
          Top = 7
          Width = 78
          Height = 12
          Caption = #26332#26085#25351#23450#32013#21697
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label21: TLabel
          Left = 216
          Top = 31
          Width = 82
          Height = 12
          Caption = #26368#20302#20986#33655#12525#12483#12488
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label22: TLabel
          Left = 216
          Top = 55
          Width = 52
          Height = 12
          Caption = #20986#33655#24418#24907
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label23: TLabel
          Left = 22
          Top = 79
          Width = 77
          Height = 12
          Caption = #20182#65288#26465#20214#12394#12393#65289
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object NounyuuNissuu: TComboBox
          Left = 104
          Top = 51
          Width = 97
          Height = 21
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = []
          ImeMode = imOpen
          ItemHeight = 13
          ParentFont = False
          TabOrder = 2
          Text = '0, '#24403#26085
          Items.Strings = (
            '0, '#24403#26085
            '1, '#65297#26085
            '2, '#65298#26085
            '3, '#65299#26085
            '4, '#65300#26085#20197#19978)
        end
        object Shimejikan: TComboBox
          Left = 104
          Top = 27
          Width = 97
          Height = 21
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = []
          ImeMode = imOpen
          ItemHeight = 13
          ParentFont = False
          TabOrder = 1
          Text = '10:00'
          Items.Strings = (
            '10:00'
            '10:30'
            '11:00'
            '14:00')
        end
        object ChuumonHouhou: TComboBox
          Left = 104
          Top = 2
          Width = 97
          Height = 21
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = []
          ImeMode = imClose
          ItemHeight = 13
          ParentFont = False
          TabOrder = 0
          Text = '1.'#38651#35441
          Items.Strings = (
            '1.'#38651#35441
            '2.'#12501#12449#12483#12463#12473)
        end
        object ShukkaKeitai: TComboBox
          Left = 314
          Top = 50
          Width = 119
          Height = 21
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = []
          ImeMode = imOpen
          ItemHeight = 13
          ParentFont = False
          TabOrder = 3
          Text = '1.'#33258#31038#20415
          Items.Strings = (
            '1.'#33258#31038#20415
            '2.'#28151#36617
            '3.'#12513#12540#12459#12540#30452#36865)
        end
        object SaiteiShukkaRotto: TEdit
          Left = 314
          Top = 27
          Width = 55
          Height = 20
          ImeMode = imClose
          TabOrder = 4
          Text = '0'
        end
        object TaJouken: TMemo
          Left = 104
          Top = 78
          Width = 457
          Height = 51
          ImeMode = imOpen
          TabOrder = 5
        end
        object CBnon: TCheckBox
          Left = 304
          Top = 5
          Width = 41
          Height = 17
          Caption = #12394#12375
          TabOrder = 6
        end
        object CBMon: TCheckBox
          Left = 344
          Top = 5
          Width = 41
          Height = 17
          Caption = #26376
          TabOrder = 7
        end
        object CBTue: TCheckBox
          Left = 384
          Top = 5
          Width = 41
          Height = 17
          Caption = #28779
          TabOrder = 8
        end
        object CBWed: TCheckBox
          Left = 424
          Top = 5
          Width = 41
          Height = 17
          Caption = #27700
          TabOrder = 9
        end
        object CBThu: TCheckBox
          Left = 464
          Top = 5
          Width = 41
          Height = 17
          Caption = #26408
          TabOrder = 10
        end
        object CBFri: TCheckBox
          Left = 504
          Top = 5
          Width = 41
          Height = 17
          Caption = #37329
          TabOrder = 11
        end
        object CBSat: TCheckBox
          Left = 544
          Top = 5
          Width = 41
          Height = 17
          Caption = #22303
          TabOrder = 12
        end
        object CBSan: TCheckBox
          Left = 584
          Top = 5
          Width = 41
          Height = 17
          Caption = #26085
          TabOrder = 13
        end
        object CBTT: TComboBox
          Left = 374
          Top = 26
          Width = 81
          Height = 21
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = []
          ImeMode = imClose
          ItemHeight = 13
          ParentFont = False
          TabOrder = 14
          Items.Strings = (
            #30002
            #32566
            #26522
            #26412
            #34955
            #65419#65438#65437
            #65418#65439#65391#65400
            #65401#65392#65405
            #65408#65394
            #65402)
        end
      end
      object tsHome: TTabSheet
        Caption = #21942#26989#25152
        object Label34: TLabel
          Left = 22
          Top = 6
          Width = 52
          Height = 12
          Caption = #37109#20415#30058#21495
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label35: TLabel
          Left = 22
          Top = 31
          Width = 52
          Height = 12
          Caption = #37117#36947#24220#30476
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label36: TLabel
          Left = 22
          Top = 55
          Width = 50
          Height = 12
          Caption = #30058#22320#12394#12393
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label18: TLabel
          Left = 6
          Top = 80
          Width = 26
          Height = 12
          Caption = #25152#23646
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label38: TLabel
          Left = 240
          Top = 30
          Width = 39
          Height = 12
          Caption = #24066#30010#26449
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label20: TLabel
          Left = 152
          Top = 80
          Width = 52
          Height = 12
          Caption = #25285#24403#32773#21517
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label43: TLabel
          Left = 240
          Top = 6
          Width = 52
          Height = 12
          Caption = #21942#26989#25152#21517
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label24: TLabel
          Left = 326
          Top = 80
          Width = 26
          Height = 12
          Caption = #20633#32771
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label25: TLabel
          Left = 6
          Top = 104
          Width = 26
          Height = 12
          Caption = #25152#23646
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label26: TLabel
          Left = 152
          Top = 104
          Width = 52
          Height = 12
          Caption = #25285#24403#32773#21517
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label27: TLabel
          Left = 326
          Top = 104
          Width = 26
          Height = 12
          Caption = #20633#32771
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label31: TLabel
          Left = 6
          Top = 128
          Width = 26
          Height = 12
          Caption = #25152#23646
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label33: TLabel
          Left = 152
          Top = 128
          Width = 52
          Height = 12
          Caption = #25285#24403#32773#21517
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label40: TLabel
          Left = 326
          Top = 128
          Width = 26
          Height = 12
          Caption = #20633#32771
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object eAdd3: TComboBox
          Left = 104
          Top = 51
          Width = 375
          Height = 21
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = []
          ImeMode = imOpen
          ItemHeight = 0
          ParentFont = False
          TabOrder = 4
        end
        object eAdd1: TComboBox
          Left = 104
          Top = 27
          Width = 119
          Height = 21
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = []
          ImeMode = imOpen
          ItemHeight = 13
          ParentFont = False
          TabOrder = 2
          Text = #26481#20140#37117
          Items.Strings = (
            #26481#20140#37117
            #31070#22856#24029#30476)
        end
        object eZip: TComboBox
          Left = 104
          Top = 2
          Width = 85
          Height = 21
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = []
          ImeMode = imClose
          ItemHeight = 0
          ParentFont = False
          TabOrder = 0
        end
        object eAdd2: TComboBox
          Left = 290
          Top = 26
          Width = 145
          Height = 21
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = []
          ImeMode = imOpen
          ItemHeight = 13
          ParentFont = False
          TabOrder = 3
          Items.Strings = (
            #21697#24029#21306
            #28207#21306
            #24029#23822#24066
            #27178#27996#24066)
        end
        object eName: TEdit
          Left = 298
          Top = 2
          Width = 301
          Height = 20
          ImeMode = imOpen
          TabOrder = 1
        end
        object eTantousha1: TEdit
          Left = 208
          Top = 76
          Width = 105
          Height = 20
          TabOrder = 5
        end
        object eShozoku1: TEdit
          Left = 40
          Top = 76
          Width = 97
          Height = 20
          TabOrder = 6
        end
        object eBikou1: TEdit
          Left = 360
          Top = 76
          Width = 225
          Height = 20
          TabOrder = 7
        end
        object eShozoku2: TEdit
          Left = 40
          Top = 100
          Width = 97
          Height = 20
          TabOrder = 8
        end
        object eTantousha2: TEdit
          Left = 208
          Top = 100
          Width = 105
          Height = 20
          TabOrder = 9
        end
        object eBikou2: TEdit
          Left = 360
          Top = 100
          Width = 225
          Height = 20
          TabOrder = 10
        end
        object eShozoku3: TEdit
          Left = 40
          Top = 124
          Width = 97
          Height = 20
          TabOrder = 11
        end
        object eTantousha3: TEdit
          Left = 208
          Top = 124
          Width = 105
          Height = 20
          TabOrder = 12
        end
        object eBikou3: TEdit
          Left = 360
          Top = 124
          Width = 225
          Height = 20
          TabOrder = 13
        end
      end
      object TabSheet2: TTabSheet
        Caption = #20104#20633'('#25991#23383')'
        ImageIndex = 4
        object Label53: TLabel
          Left = 18
          Top = 14
          Width = 60
          Height = 11
          Caption = #20104#20633'1(400)'
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label54: TLabel
          Left = 18
          Top = 54
          Width = 60
          Height = 11
          Caption = #20104#20633'3(400)'
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label55: TLabel
          Left = 18
          Top = 34
          Width = 60
          Height = 11
          Caption = #20104#20633'2(400)'
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label61: TLabel
          Left = 18
          Top = 74
          Width = 60
          Height = 11
          Caption = #20104#20633'4(400)'
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object YobiStr1: TComboBox
          Left = 100
          Top = 10
          Width = 500
          Height = 19
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ImeMode = imClose
          ItemHeight = 0
          ParentFont = False
          TabOrder = 0
        end
        object YobiStr3: TComboBox
          Left = 100
          Top = 50
          Width = 500
          Height = 19
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ImeMode = imClose
          ItemHeight = 0
          ParentFont = False
          TabOrder = 2
        end
        object YobiStr2: TComboBox
          Left = 100
          Top = 30
          Width = 500
          Height = 19
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ImeMode = imClose
          ItemHeight = 0
          ParentFont = False
          TabOrder = 1
        end
        object YobiStr4: TComboBox
          Left = 100
          Top = 70
          Width = 500
          Height = 19
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ImeMode = imClose
          ItemHeight = 0
          ParentFont = False
          TabOrder = 3
        end
      end
      object TabSheet3: TTabSheet
        Caption = #20104#20633'('#25968#23383')'
        ImageIndex = 5
        object Label57: TLabel
          Left = 20
          Top = 18
          Width = 31
          Height = 11
          Caption = #20104#20633'1'
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label58: TLabel
          Left = 20
          Top = 42
          Width = 31
          Height = 11
          Caption = #20104#20633'2'
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label59: TLabel
          Left = 182
          Top = 16
          Width = 31
          Height = 11
          Caption = #20104#20633'3'
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label60: TLabel
          Left = 182
          Top = 42
          Width = 31
          Height = 11
          Caption = #20104#20633'4'
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object YobiInt1: TEdit
          Left = 74
          Top = 12
          Width = 73
          Height = 20
          ImeMode = imClose
          TabOrder = 0
          Text = '0'
        end
        object YobiInt2: TEdit
          Left = 76
          Top = 36
          Width = 73
          Height = 20
          ImeMode = imClose
          TabOrder = 1
          Text = '0'
        end
        object YobiInt3: TEdit
          Left = 232
          Top = 12
          Width = 73
          Height = 20
          ImeMode = imClose
          TabOrder = 2
          Text = '0'
        end
        object YobiInt4: TEdit
          Left = 232
          Top = 36
          Width = 73
          Height = 20
          ImeMode = imClose
          TabOrder = 3
          Text = '0'
        end
      end
    end
  end
  object Query1: TQuery
    DatabaseName = 'taka'
    Left = 236
    Top = 19
  end
  object QueryLabel: TQuery
    DatabaseName = 'taka'
    Left = 447
    Top = 422
  end
end
