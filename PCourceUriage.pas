unit PCourceUriage;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, StdCtrls, Buttons, ExtCtrls, ComCtrls, ShellAPI;

type
  TfrmPCourceUriage = class(TfrmMaster)
    DTP1: TDateTimePicker;
    Label3: TLabel;
    procedure BitBtn2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private 宣言 }
  public
    { Public 宣言 }
  end;

var
  frmPCourceUriage: TfrmPCourceUriage;

implementation

uses DMMaster, Inter, HKLib;

{$R *.DFM}

//エクセルへ出力ボタンがクリックされた
procedure TfrmPCourceUriage.BitBtn2Click(Sender: TObject);
var
	sLine, sSql, sTokuisakiCode1, sTokuisakiCode2, sTokuisakiname, sUriageKingaku, sDateFrom, sDateTo : string;
 	F : TextFile;
  iSum, iSum2, iZanOnOff : integer;
  wYyyy, wMm, wDd : Word;
  sShuukinCode : String;
begin
	//確認メッセージ
  if MessageDlg('印刷しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;

	//スタートトランザクション
  sSql := 'SELECT D.TokuisakiCode1, D.TokuisakiCode2, D.UriageKingaku,';
  sSql := sSql + ' T.TokuisakiName ';
  sSql := sSql + ', T.ShuukinCode '; //2004.04.16 Added H.K.
  sSql := sSql + ' FROM ';
  sSql := sSql + CtblMTokuisaki + ' T,';
  sSql := sSql + CtblTDenpyou + ' D';
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' D.DDate =  ''' + DateToStr(DTP1.Date) + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + ' D.TokuisakiCode1 = T.TokuisakiCode1 ';

	//以下追加
  //1999/07/06
  sSql := sSql + ' AND ';
  //sSql := sSql + ' ((UriageKingaku > 0) OR (SUBSTRING(D.Memo,1, 2) IN (''返品'', ''値引'')))' ;
  sSql := sSql + ' ((UriageKingaku > 0) OR (SUBSTRING(D.Memo,1, 2) IN (''返品'', ''値引'', ''取消'')))' ;

  //1999/10/04 追加
  sSql := sSql + ' AND ';
  sSql := sSql + ' D.Memo <> ''' + CsCTAX + '''';

  //2007.01.11 以下追加
  sSql := sSql + ' AND ((D.KaishuuHouhou is NULL) or (D.KaishuuHouhou=2))';  //返金の行を除く


  sSql := sSql + ' ORDER BY ';
  sSql := sSql + ' D.TokuisakiCode2, D.TokuisakiCode1';

	//出力するファイルを作成する、すでにあれば削除する
  //請求明細用
 	AssignFile(F, CFileName_CourceUri);
  Rewrite(F);
	CloseFile(F);

  //2000/03/18
  //まず日付を出力する
  sLine := DateToStr(DTP1.Date);
 	HMakeFile(CFileName_CourceUri, sLine);
  //タイトルを出力する
  sLine := '得意先コード２';
  sLine := sline + ',' + '得意先コード１';
  sLine := sline + ',' + '集金コード';
  sLine := sline + ',' + '得意先名';
  sLine := sline + ',' + '売上金額';
 	HMakeFile(CFileName_CourceUri, sLine);
  with frmDMMaster.QueryMTokuisaki do begin
		Close;
    Sql.Clear;
   	Sql.Add(sSql);
	  open;
    while not EOF do begin
			SB1.SimpleText := '計算中 -> '+FieldByName('TokuisakiCode1').AsString;
      Update;
      sTokuisakiCode2 := FieldByName('TokuisakiCode2').AsString;
      sTokuisakiCode1 := FieldByName('TokuisakiCode1').AsString;
      sShuukinCode    := FieldByName('ShuukinCode').AsString;
      sTokuisakiName := FieldByName('TokuisakiName').AsString;
      sUriageKingaku := FieldByName('UriageKingaku').AsString;

      sLine := sTokuisakiCode2 + ', ' + sTokuisakiCode1 + ',' + sShuukinCode;
      sLine := sLine + ',' + sTokuisakiName + ', ' + sUriageKingaku + ',';
 		  HMakeFile(CFileName_CourceUri, sLine);
     	Next;
    end;//of while
    Close;
  end;//of with

	//エンドトランザクション
  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + 'コース別売上.xls', '', SW_SHOW);
end;

procedure TfrmPCourceUriage.FormCreate(Sender: TObject);
begin
  inherited;
	Height := 200;
  Width := 550;
  DTP1.Date := Date;
end;

end.
