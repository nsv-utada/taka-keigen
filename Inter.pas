unit Inter;

interface

var
	GTokuisakiCode         : String;
	GTokuisakiCode2        : String;  //MItemTで新たに使う
  GTokuisakiName         : String;
  GTokuisakiName2        : String;  //frmDenpyouで使う
  GDate                  : String;
  GDenpyouBanngou        : String;   // 2001.12.12　伝票番号もCopyする様に変更
  GNyuuryokusha          : String;
  GGatsubun              : String;   // 2001.12.26  月分番号もCopyする様に変更
  GMemo                  : String;   // 2002.07.19 追加
  GRowCount              : Integer;  // 2001.12.26  行数もCopyする様に変更
  GTeisei                : Integer;  // 0:掛伝票　1:訂正伝票
  GBikou                 : String;
	GArray                 : array of array of String; // 2001.12.12　配列の次元を100から1000に増加．
  GArrayOfDenpyouBanngou : array of String;          // DenpyouPrintへ渡す伝票番号用配列
	GTokuisakiCode1        : String;   //　請求明細書印刷用
	GStartDate             : String; //　請求明細書印刷用
	GEndDate               : String; //　請求明細書印刷用

	GUser                  : String; //　ログインユーザー
	GPass                  : String; //　ログインユーザーパス

  //2002.02.28
	GPassWord              : String; //　制限パスワード

  //2003.03.10
	GDenpyouFlag           : Integer; //　伝票入力でプレビューから戻ってきたときに１がセットされる

  //2004.09.18
  GAriaCode              : String;  //Ver4.2 で使用　伝票番号の生成に必要

  //2006.03.19
  GArray2                 : array of array of String; //入庫入力で使用
  GRowCount2              : Integer;
  GDate2                  : String;
  GGatsubun2              : String;
  GNyuuryokusha2          : String;
  GShiiresakiName         : String;
  GShiiresakiCode         : String;

  //2006.05.14
  GCode1,GCode2           : String; //商品マスターから仕入れ価格履歴を修正するときに使用

  //2012.11.24
  GDDate                   : String;
    
const

	//
	//テーブル名のエリアス
  //
	CtblMTokuisaki       = 'tblMTokuisaki';
	CtblTDenpyou         = 'tblTDenpyou';
  CtblTDenpyouDetail   = 'tblTDenpyouDetail'; // 2001.12.12 追加
	CtblTZandaka         = 'tblTZandaka';
	CtblTKaishuu         = 'tblTKaishuu';
  CtblMList            = 'tblMList';
  CtblMShiiresaki      = 'tblMShiiresaki';
  CtblMItem            = 'tblMItem';
  CtblMItem2           = 'tblMItem2';
  CtblMItem3           = 'tblMItem3';
  CtblMCourse          = 'tblMCourse';
  CtblTGennkinnKaishuu = 'tblTGennkinnKaishuu';
  CtblTShiireDetail    = 'tblTShiireDetail';
  CtblTShiire          = 'tblTShiire';
  CtblMShiireKakakuRireki = 'tblMShiireKakakuRireki';
  CtblTMitsumori       = 'tblTMitsumori';
  CtblTItemRireki       = 'tblTItemRireki';
  CtblMConfig      = 'tblMConfig';
  CtblTBill         = 'tblTBill';

  // ビュー名のエイリアス
  CviwMItems           = 'viwMItems';         // 2001.12.12 追加
  CviwMItems2           = 'viwMItems2';       // 2006.03.18 追加


  //出力ファイル名
  CFileName_Tokuisaki        = 'Tokuisaki.txt';
  CFileName_UriageDenpyou    = 'UriageDenpyou.txt';
  CFileName_Seikyuu          = 'Seikyuu.txt';
  CFileName_Estimate         = 'Estimate.txt';
  CFileName_UriDaichou       = 'UriDaichou.txt';
  CFileName_CourceUri        = 'CourceUri.txt';
  CFileName_Mishuukin        = 'Mishuukin.txt';
  CFileName_Kaishuu          = 'Kaishuu.txt';
  CFileName_Urikakezan       = 'Urikakezan.txt';
  CFileName_Uriage           = 'Uriage.txt';
  CFileName_Nengajou         = 'Nengajou.txt';
  CFileName_GennkinnZanndaka = 'GennkinnZanndaka.txt';
  CFileName_Shukkohyou       = 'Shukkohyou.txt';
  CFileName_Item             = 'Item.txt';

  CFileName_TokusakiBetauKakaku = 'TokuisakibetsuKakaku.txt';
  CFileName_JyuchuuHindo        = 'JyuchuuHindo.txt';

  CFileName_Shiiresaki       = 'Shiiresaki.txt';

  CFileName_KaChouhyou1      = 'KaChouhyou1.txt'; //〆月
  CFileName_KaChouhyou2      = 'KaChouhyou2.txt'; //支払月

  //2006.05.19
  CFileName_Jouken           = 'jouken.txt'; //商品マスターの条件CB

  //200902.10 見積りシステム対応にて追加
  CFileName_M_ShouhinIchiran = 'M_ShouhinIchiran.txt';
  CFileName_M_ArariKensaku = 'M_ArariKensaku.txt';
  CFileName_M_Order = 'M_Order.txt';
  CFileName_M_Estimate = 'M_Estimate.txt';

  //請求締め日のエリアス
  CSeikyuuSimebi_5   =  5;
  CSeikyuuSimebi_10  = 10;
  CSeikyuuSimebi_15  = 15;
  CSeikyuuSimebi_20  = 20;
  CSeikyuuSimebi_25  = 25;
  CSeikyuuSimebi_End =  0; //月末

  //消費税の伝票を示すMemoに入力する文字列
  CsCTAX = '##消費税##';
  CsShiharai = '##支払い##';

  //消費税の税率
  CsTaxRate = 0.08; //2014.02.19
  CsOldTaxRate = 0.05; //2014.04.01 add new
  CsNewTaxRate = 0.1;

 //消費税の切り替え日
  CsChangeTaxDate = '2014/04/01'; //2014.04.01 add new

  CsChangeTaxtDate10 = '2019/10/01'; //2019.09.12

  //一番小さい年月（このシステムの開始日付）
  CsMinDate = '1998/12/01';

  //制限パスワード
 //東京  Mod before 2007.10.22utada
  //CPassword1     = '0246%'; //権限１
  //CPassword2     = '2534#'; //権限２
  //CPassword3     = '4398d'; //権限３
  //CPassword4     = '3546a'; //権限４
  //CPasswordGenka = '5255!'; //商品原価がみれる

  //Mod After 2009.03.06 utada
  CPassword1     = '3409$'; //権限１
  CPassword2     = '9934%'; //権限２
  CPassword3     = '1093#'; //権限３
  CPassword4     = '6543&'; //権限４
  CPasswordGenka = '8092%'; //商品原価がみれる
  CPassword5     = 'd39j0'; //伝票削除用パスワード

  //2006.11.07 hkubota
  //CFileDir = '\\ytaka01\bin\';
  CFileDir = '.\\';
  CFileName_Lock = CFileDir + 'Lock.txt';

  //横浜
{
  CPassword1     = '0444'; //権限１
  CPassword2     = '2534#'; //権限２
  CPassword3     = '0466'; //権限３
  CPassword4     = '0466'; //権限４
  CPasswordGenka = '5255!'; //商品原価がみれる
}

  type TInRecArry = array[1..150, 1..3] of String;

function CheckTokuisakiCode1(sTokuisakiCode1:String):Boolean;

implementation


//得意先コードのうち月２回〆のものを調べる
//そうであればTRUEを返す
//1999/07/25
//2334追加
//2000/05/04
//2553追加
//2000/07/22 この関数を無効にした。
//高橋さんの要望により
function CheckTokuisakiCode1(sTokuisakiCode1:String):Boolean;
begin
{
  if (sTokuisakiCode1 = '158') or
     (sTokuisakiCode1 = '325') or
     (sTokuisakiCode1 = '363') or
     (sTokuisakiCode1 = '2334') or
     (sTokuisakiCode1 = '2553') or
     (sTokuisakiCode1 = '412') then begin
     Result := True;
	end else begin
     Result := False;
  end;
}
  Result := False;
end;

end.
