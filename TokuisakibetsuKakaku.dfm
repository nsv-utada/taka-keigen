inherited frmTokuisakibetsuKakaku: TfrmTokuisakibetsuKakaku
  Left = 1087
  Top = 175
  Width = 855
  Height = 515
  Caption = 'frmTokuisakibetsuKakaku'
  PixelsPerInch = 96
  TextHeight = 12
  object Label14: TLabel [0]
    Left = 170
    Top = 80
    Width = 52
    Height = 12
    Caption = #24471#24847#20808#21517
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = [fsBold]
    ParentFont = False
  end
  inherited Panel1: TPanel
    Width = 847
    inherited Label1: TLabel
      Width = 160
      Caption = #24471#24847#20808#21029#20385#26684#19968#35239
    end
    inherited Label2: TLabel
      Left = 450
      Top = 20
    end
  end
  inherited Panel2: TPanel
    Top = 428
    Width = 847
    inherited BitBtn1: TBitBtn
      Left = 584
      Top = 10
    end
    inherited BitBtn2: TBitBtn
      Left = 424
      Top = 10
    end
  end
  inherited Panel3: TPanel
    Width = 847
    Height = 387
    OnClick = Panel3Click
    inherited Label3: TLabel
      Left = 22
      Top = 20
      Width = 76
      Caption = #24471#24847#20808#12467#12540#12489
    end
    inherited Label4: TLabel
      Left = 160
      Top = 268
      Width = 166
      Caption = '0'#20214#12398#24471#24847#20808#12391#20966#29702#12375#12414#12377#12290
      Font.Color = clRed
    end
    inherited Label5: TLabel
      Left = 618
      Top = 210
      Width = 167
      Height = 12
      Caption = '0'#12450#12452#12486#12512#12398#21830#21697#12391#20986#21147#12375#12414#12377
      Font.Color = clRed
      Font.Height = -12
    end
    inherited Label7: TLabel
      Left = 178
      Top = 78
      Width = 62
      Caption = #37197#36865#12467#12540#12489
    end
    inherited Label8: TLabel
      Left = 460
      Top = 20
      Width = 70
      Caption = #21830#21697#12467#12540#12489'1'
    end
    inherited Label9: TLabel
      Left = 232
      Top = 178
      Width = 25
      Caption = #12363#12425
    end
    inherited Label10: TLabel
      Left = 610
      Top = 22
      Width = 42
      Caption = #21830#21697#21517
    end
    object Label6: TLabel [7]
      Left = 6
      Top = 6
      Width = 48
      Height = 13
      Caption = '[Step1]'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel [8]
      Left = 438
      Top = 6
      Width = 48
      Height = 13
      Caption = '[Step2]'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label12: TLabel [9]
      Left = 180
      Top = 136
      Width = 28
      Height = 13
      Caption = #26399#26085
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label13: TLabel [10]
      Left = 664
      Top = 24
      Width = 79
      Height = 12
      Caption = 'F1'#12461#12540#12391#26908#32034
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel [11]
      Left = 610
      Top = 76
      Width = 76
      Height = 12
      Caption = #21830#21697#12467#12540#12489'(1)'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label16: TLabel [12]
      Left = 176
      Top = 24
      Width = 56
      Height = 13
      Caption = #24471#24847#20808#21517
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label17: TLabel [13]
      Left = 242
      Top = 26
      Width = 79
      Height = 12
      Caption = 'F1'#12461#12540#12391#26908#32034
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label18: TLabel [14]
      Left = 40
      Top = 364
      Width = 397
      Height = 12
      Caption = '*'#24471#24847#20808#25968#12392#21830#21697#25968#12434#20055#12376#12383#25968#12364'10,000'#20214#20197#19979#12395#12394#12427#12424#12358#12395#12375#12390#12367#12384#12373#12356#12290
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited CBYyyyTo: TComboBox
      Left = 180
      Top = 192
      Width = 83
    end
    inherited CBMmTo: TComboBox
      Left = 268
      Top = 192
    end
    inherited CBYyyyFrom: TComboBox
      Left = 180
      Top = 154
      Width = 83
      Text = ''
    end
    inherited CBMmFrom: TComboBox
      Left = 268
      Top = 154
    end
    object EditChainCode: TEdit
      Left = 180
      Top = 96
      Width = 63
      Height = 20
      ImeMode = imClose
      TabOrder = 4
      OnKeyPress = EditChainCodeKeyPress
    end
    object MemoTokuisakiCode: TMemo
      Left = 22
      Top = 40
      Width = 129
      Height = 273
      ScrollBars = ssBoth
      TabOrder = 5
      OnChange = MemoTokuisakiCodeChange
    end
    object Button1: TButton
      Left = 160
      Top = 242
      Width = 133
      Height = 21
      Caption = #24471#24847#20808#12467#12540#12489#12398#30906#23450
      TabOrder = 6
      OnClick = Button1Click
    end
    object CBName: TComboBox
      Left = 612
      Top = 42
      Width = 222
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imHira
      ItemHeight = 13
      ParentFont = False
      TabOrder = 7
      OnChange = CBNameChange
      OnKeyDown = CBNameKeyDown
    end
    object CheckBox1: TCheckBox
      Left = 614
      Top = 124
      Width = 97
      Height = 17
      Alignment = taLeftJustify
      Caption = #12377#12409#12390
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = CheckBox1Click
    end
    object MemoItemCode: TMemo
      Left = 464
      Top = 40
      Width = 137
      Height = 273
      ScrollBars = ssBoth
      TabOrder = 9
      OnChange = MemoItemCodeChange
    end
    object Button2: TButton
      Left = 618
      Top = 184
      Width = 133
      Height = 21
      Caption = #21830#21697#12467#12540#12489#12398#30906#23450
      TabOrder = 10
      OnClick = Button2Click
    end
    object CBCode1: TComboBox
      Left = 614
      Top = 92
      Width = 163
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imClose
      ItemHeight = 13
      ParentFont = False
      TabOrder = 11
      OnChange = CBCode1Change
      Items.Strings = (
        '01,'#26989#21209#29992#39135#26448
        '02,'#35519#21619#26009#35519#21619#39135#21697
        '03,'#39321#36763#26009
        '04,'#12499#12531#12289#32566#35440#39006
        '05,'#12467#12540#12498#12540#12289#21931#33590#26448#26009
        '06,'#20083#35069#21697#65288#12481#12540#12474#39006#65289
        '07,'#39135#29992#27833
        '08,'#31881#35069#21697
        '09,'#12418#12385
        '10,'#12473#12497#40634
        '11,'#40634
        '12,'#31859
        '13,'#20013#33775#26448#26009
        '14,'#21508#31278#12472#12517#12540#12473
        '15,'#12362#33590
        '16,'#27703#12471#12525#12483#12503
        '17,'#28460#29289#24803#33756
        '18,'#12362#36890#12375#29289
        '19,'#39135#32905#21152#24037#21697
        '20,'#28023#29987#29645#21619
        '21,'#20919#20941#39135#21697#39770#20171#39006
        '22,'#20919#20941#39135#21697#36786#29987#29289
        '23,'#20919#20941#39135#21697#19968#27425#21152#24037#21697
        '24,'#20919#20941#39135#21697#28857#24515#39006
        '25,'#12486#12522#12540#12492#39006
        '26,'#12524#12488#12523#12488#39006#28271#29006
        '27,'#20919#20941#35201#20919#12289#39321#36763#26009
        '28,'#12362#12388#12414#12415#35910#39006
        '29,'#12362#12388#12414#12415#12362#12363#12365#39006
        '30,'#12362#12388#12414#12415#12481#12483#12503#39006
        '31,'#12481#12519#12467#12524#12540#12488#12289#12476#12522#12540#39006
        '32,'#28023#29987#29289
        '33,'#24178#12375#32905
        ''
        '')
    end
    object CBTokuisakiName: TComboBox
      Left = 180
      Top = 44
      Width = 222
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imHira
      ItemHeight = 13
      ParentFont = False
      TabOrder = 12
      OnChange = CBNameChange
      OnKeyDown = CBTokuisakiNameKeyDown
    end
  end
  inherited SB1: TStatusBar
    Top = 469
    Width = 847
  end
  inherited QueryTokuisaki: TQuery
    Left = 294
    Top = 65535
  end
  object Query1: TQuery
    DatabaseName = 'taka'
    Left = 232
    Top = 1
  end
  object QueryHindo: TQuery
    DatabaseName = 'taka'
    Left = 342
    Top = 2
  end
end
