unit ZaikoKanri;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, ComCtrls, StdCtrls, Buttons, ExtCtrls, Grids;

type
  TfrmZaikoKanri = class(TfrmMaster)
    SG1: TStringGrid;
    Panel4: TPanel;
    CBTokuisakiCode2: TComboBox;
    Label4: TLabel;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private 宣言 }
		procedure MakeSG;
  public
    { Public 宣言 }
  end;

const
	//グリッドの列の定義

  CintNum           = 0;
  CintItemCode      = 1;
  CintItemName      = 2;
  CintItemMaxCount  = 3;
  CintItemMinCount  = 4;
  CintItemCount     = 5;
  CintEnd           = 6;   //ターミネーター


	GRowCount = 700; //行数


var
  frmZaikoKanri: TfrmZaikoKanri;

implementation

{$R *.DFM}

procedure TfrmZaikoKanri.FormCreate(Sender: TObject);
var
	i : integer;
begin
  inherited;

	Width := 450;
  Height := 450;

  
  //ストリンググリッドの作成
  MakeSG;

  //サンプルデータ
  with SG1 do begin
  i := 1;
  Cells[CintNum, i]       := IntToStr(i);
  Cells[CintItemCode, i]  := 'A0001';
  Cells[CintItemName, i]  := 'ﾅﾌｷﾝ（平）';
  Cells[CintItemMaxCount, i] := '10000';
  Cells[CintItemMinCount, i] := '5000';
  Cells[CintItemCount, i] := '7000';

  i := 2;
  Cells[CintNum, i]       := IntToStr(i);
  Cells[CintItemCode, i]  := 'B0001';
  Cells[CintItemName, i]  := '風味出し(ﾔﾏｻ)';
  Cells[CintItemMaxCount, i] := '100';
  Cells[CintItemMinCount, i] := '20';
  Cells[CintItemCount, i] := '25';
  end;
end;

procedure TfrmZaikoKanri.MakeSG;
begin
	with SG1 do begin
    DefaultDrawing := True;
    RowCount := GRowCount;
    ColCount := CintEnd;
    Align := alBottom;
    FixedCols := 0;
    FixedColor := clYellow;
    Canvas.Font.Size := 11;
    DefaultRowHeight := 20;
    ColWidths[0]:= 64;
    Font.Color := clNavy;

    ColWidths[CintNum]:= 40;
    Cells[CintNum, 0] := 'No.';

    ColWidths[CintItemCode]:= 90;
    Cells[CintItemCode, 0] := '商品コード';

    ColWidths[CintItemName]:= 100;
    Cells[CintItemName, 0] := '商品名';

    ColWidths[CintItemMaxCount]:= 60;
    Cells[CintItemMaxCount, 0] := '最大数量';

    ColWidths[CintItemMinCount]:= 60;
    Cells[CintItemMinCount, 0] := '最小数量';

    ColWidths[CintItemCount]:= 60;
    Cells[CintItemCount, 0] := '現在数量';

  end;
end;


procedure TfrmZaikoKanri.FormResize(Sender: TObject);
begin
	SG1.Align := alClient;

end;

end.
