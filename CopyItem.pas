unit CopyItem;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, StdCtrls, ComCtrls, Buttons, ExtCtrls, Db, DBTables;

type
  TfrmCopyItem = class(TfrmMaster)
    Label3: TLabel;
    CBTokuisakiCode1: TComboBox;
    Label5: TLabel;
    CBTokuisakiNameYomi: TComboBox;
    Label6: TLabel;
    CBTokuisakiName: TComboBox;
    RB420: TRadioButton;
    RB450: TRadioButton;
    Label4: TLabel;
    Query1: TQuery;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    CBTokuisakiCode2: TComboBox;
    procedure BitBtn2Click(Sender: TObject);
    procedure CBTokuisakiNameYomiKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBTokuisakiNameExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CBTokuisakiCode1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private 宣言 }
		procedure MakeCBTokuisakiName(sKey, sWord:String; iKubun:Integer);
  public
    { Public 宣言 }
  end;

var
  frmCopyItem: TfrmCopyItem;

implementation
uses
 Inter, DM1, DMMaster, MItemT, PasswordDlg;

{$R *.DFM}
//コピー開始ボタン
procedure TfrmCopyItem.BitBtn2Click(Sender: TObject);
var
	myA : TInRecArry;
	i,j,iflag : Integer;
  sumItems2,sumItems : String;
  sHindo,sSql,sKakakuKubun, sMsg : String;
begin
//2002.12.28
	dlgPasswordDlg := TdlgPasswordDlg.Create(Self);
	if dlgPasswordDlg.ShowModal = mrOK then begin

	end else begin
		Exit;
	end;

	if GPassWord = CPassword1 then begin
    Beep;
  //コメントアウト 2009.04.08 utada
	//end else if GPassWord = CPassword2 then begin
    //Beep;
	//end else if GPassWord = CPassword3 then begin
    //Beep;
	end else begin
	//ShowMessage('PassWordが違います');
   exit;
	end;
	GPassWord := '';

	//確認
  if (CBTokuisakiCode1.Text = '') then begin
  	ShowMessage('得意先コードは省略できません');
    Exit;
  end;
  if CBTokuisakiCode2.Text = '' then begin
    CBTokuisakiCode2.Text := '0';
  end;
  if strToInt(CBTokuisakiCode2.Text) > 0 then begin
    iflag := 1;
    RB420.Checked := False;
    RB450.Checked := False;
    sMsg := CBTokuisakiCode2.Text;
    sMsg := sMsg + 'の得意先商品マスターから商品データをコピーしますか?';
	  if MessageDlg(sMsg,
      mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
	    Exit;
	  end;
  end else begin
    iflag := 0;
    sMsg := '商品総マスターから商品データをコピーしますか?';
	  if MessageDlg(sMsg,
      mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
	    Exit;
	  end;
  end;//of if

  //まず削除する
  sMsg := 'すでに得意先別商品台帳がある場合には、いったん削除されます。';
  if MessageDlg(sMsg,
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
     Exit;
  end;
  i := 1;
  myA[i][1] := 'TokuisakiCode';
  myA[i][2] := CBTokuisakiCode1.Text;
  myA[i][3] := '';
  i := i + 1;
  myA[i][1] := 'END';
  myA[i][2] := 'END';
  myA[i][3] := 'END';

  DM1.DeleteRecord(myA, CtblMItem2);

  //コピー開始
  //本当は好ましくないが、これ以下の処理を
  //得意先マスターの新規登録にコピーした。
  //以下のルーチンを変更する場合は注意すること
  if iflag = 1 then begin //得意先商品マスターから
    sSql := 'select * from ' + CtblMItem2;
    sSql := sSql + ' where TokuisakiCode=''' + CBTokuisakiCode2.Text + '''';
    sSql := sSql + ' order by ';
    sSql := sSql + ' Code1, Code2';
  end else begin //商品総マスターから
    //sSql := 'select * from ' + CtblMItem; //　*でまとめて指定すると失敗する為修正
    sSql := 'select Code1,Code2,Tanka420,Tanka450 from ' + CtblMItem;
    //sSql := sSql + ' where Code2 >=1 ';
    sSql := sSql + ' order by ';
    sSql := sSql + ' Code1, Code2';
  end;
  with Query1 do begin
  	close;
    sql.Clear;
    sql.Add(sSql);
    open;
    j := 1;
    while not EOF do begin
        SB1.SimpleText := IntToStr(j) + ' ** Code1->' + FieldByName('Code1').asString +
                          ' Code2->'  +FieldByName('Code2').asString;
        SB1.Update;
  	//  InRecArry : TInRecArry;//[フィールド名,フィールド内容, 区切り]
  	i := 1;
  	myA[i][1] := 'Code1';
  	myA[i][2] := FieldByName('Code1').asString;
  	myA[i][3] := '''';

    i := i + 1;
  	myA[i][1] := 'Code2';
  	myA[i][2] := FieldByName('Code2').asString;
  	myA[i][3] := '''';

    i := i + 1;
  	myA[i][1] := 'Tanka';

    if RB420.Checked then begin
  	   myA[i][2] := FieldByName('Tanka420').asString;
       sKakakuKubun := '420';
       sHindo := '0';
    end else if RB450.Checked then begin
  	   myA[i][2] := FieldByName('Tanka450').asString;
       sKakakuKubun := '450';
       sHindo := '0';
    end else begin //得意先商品マスターから
  	   myA[i][2]    := FieldByName('Tanka').asString;
       sKakakuKubun := FieldByName('KakakuKubun').asString;
       sHindo       := FieldByName('Hindo').asString;
       if sHindo='' then begin
        sHindo := '0';
       end;
    end;

    if myA[i][2] = '' then begin
       myA[i][2] := '0';
    end;
  	myA[i][3] := '';

    i := i + 1;
  	myA[i][1] := 'TokuisakiCode';
  	myA[i][2] := CBTokuisakiCode1.Text;
  	myA[i][3] := '';

    i := i + 1;
  	myA[i][1] := 'KakakuKubun';
  	myA[i][2] := sKakakuKubun;
  	myA[i][3] := '';

    //Adeed by H.K. 2002/03/01
    i := i + 1;
  	myA[i][1] := 'YN';
  	myA[i][2] := '0';
  	myA[i][3] := '';

    i := i + 1;
  	myA[i][1] := 'CYN';
  	myA[i][2] := '0';
  	myA[i][3] := '';

    //Adeed bu H.K. 2002/07/05
    i := i + 1;
  	myA[i][1] := 'Hindo';
  	myA[i][2] := sHindo;
  	myA[i][3] := '';

    i := i + 1;
  	myA[i][1] := 'END';
  	myA[i][2] := 'END';
  	myA[i][3] := 'END';

	  DM1.InsertRecord(myA, CtblMItem2);
    j := j + 1;
    next;
    end;//of while
  end;//of with
  //得意先別商品リストの表示 frmMItemT
  //GTokuisakiCode2 := CBTokuisakiCode1.Text;
  //TfrmMItemT.Create(Self);

//Add  Utadaコピー後のチェック
 if iflag = 1 then begin
     ShowMessage('商品データのコピーに成功しました');
 end else begin

    sSql := 'Select items2=Count(*) from tblMItem2';
    sSql := sSql + ' where ';
    sSql := sSql + ' tokuisakicode=' + CBTokuisakiCode1.Text;
    with Query1 do begin
      Close;
      Sql.Clear;
      Sql.Add(sSql);
      Open;
      sumItems2 := FieldbyName('items2').asString;
      Close;
    end;
    sSql := 'Select items2=Count(*) from tblMItem';
    with Query1 do begin
      Close;
      Sql.Clear;
      Sql.Add(sSql);
      Open;
      sumItems := FieldbyName('items2').asString;
      Close;
    end;

	  if (sumItems2=sumItems) then begin
      ShowMessage('商品データのコピーに成功しました');
    end else begin
	    ShowMessage('商品データのコピーに失敗しました。管理者に連絡して下さい。');
    end;
 end;

end;

procedure TfrmCopyItem.CBTokuisakiNameYomiKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
    CBTokuisakiName.SetFocus;
	 	MakeCBTokuisakiName('TokuisakiNameYomi', CBTokuisakiNameYomi.Text, 1);
  end;
  //2002.08.30
  CBTokuisakiCode2.Text := '';
  
end;

{
	得意先名のコンボボックスを作成する
  引数：sKey   -> 検索列名
  			sWord  -> 検索ワード
  			iKubun -> 0=前方一致, 1=曖昧検索, 2=数字の完全一致
}
procedure TfrmCopyItem.MakeCBTokuisakiName(sKey, sWord:String; iKubun:Integer);
var
	sSql : String;
begin

  CBTokuisakiName.Items.Clear;


	sSql := 'SELECT * FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + sKey;
  case iKubun of
  0: sSql := sSql + ' LIKE ''' + sWord + '%''';
  1: sSql := sSql + ' LIKE ''%' + sWord + '%''';
  2: sSql := sSql + ' = ' + sWord;
  end;

  sSql := sSql + ' ORDER BY TokuisakiCode1 ';
  CBTokuisakiName.Clear;
	with frmDMMaster.QueryMTokuisaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	CBTokuisakiName.Items.Add(FieldByName('TokuisakiCode1').AsString + ',' +
             FieldByName('TokuisakiNameUp').AsString + ' ' +
						 FieldByName('TokuisakiName').AsString   + ',' +
             FieldByName('Kubun420450').AsString);
    	Next;
    end;//of while
    Close;
  end;//of with

  CBTokuisakiName.DroppedDown := True;
end;


procedure TfrmCopyItem.CBTokuisakiNameExit(Sender: TObject);
var
   sTokuisakiCode1,sKubun420450 : String;
begin
	//得意先コード１を取得して得意先を検索する
  sTokuisakiCode1 := Copy(CBTokuisakiName.Text, 1, (pos(',', CBTokuisakiName.Text)-1));
  CBTokuisakiCode1.Text := sTokuisakiCode1;
  sKubun420450 := Copy(CBTokuisakiName.Text, pos(',', CBTokuisakiName.Text)+1 , Length(CBTokuisakiName.Text));
  sKubun420450 := Copy(sKubun420450, pos(',', sKubun420450)+1 , Length(sKubun420450));

  //2004.03.30 H.Kubota
  if sKubun420450 = '420' then begin
    RB420.Checked := True;
  end else begin
    RB450.Checked := True;
  end;

end;

procedure TfrmCopyItem.FormCreate(Sender: TObject);
begin
  inherited;
  width := 500;
  height := 340;
end;

procedure TfrmCopyItem.CBTokuisakiCode1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
	 	MakeCBTokuisakiName('TokuisakiCode1', CBTokuisakiCode1.Text, 2);
  end;
end;

end.
