unit main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, DB, DBTables, StdCtrls;

type
  TfrmMain = class(TForm)
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    Exit1: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N21: TMenuItem;
    Tool1: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    gyoushu1: TMenuItem;
    N22: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    N19: TMenuItem;
    N20: TMenuItem;
    N23: TMenuItem;
    N24: TMenuItem;
    N25: TMenuItem;
    N26: TMenuItem;
    N27: TMenuItem;
    N28: TMenuItem;
    N29: TMenuItem;
    N30: TMenuItem;
    N31: TMenuItem;
    Tool2: TMenuItem;
    N32: TMenuItem;
    N33: TMenuItem;
    N34: TMenuItem;
    N35: TMenuItem;
    N36: TMenuItem;
    N37: TMenuItem;
    MP1: TMenuItem;
    QuerySelect: TQuery;
    N38: TMenuItem;
    N39: TMenuItem;
    N40: TMenuItem;
    N41: TMenuItem;
    N42: TMenuItem;
    N43: TMenuItem;
    N44: TMenuItem;
    Info1: TMenuItem;
    InfoMartW1: TMenuItem;
    procedure N4Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure N10Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N21Click(Sender: TObject);
    procedure N12Click(Sender: TObject);
    procedure N13Click(Sender: TObject);
    procedure gyoushu1Click(Sender: TObject);
    procedure N22Click(Sender: TObject);
    procedure N23Click(Sender: TObject);
    procedure N16Click(Sender: TObject);
    procedure N18Click(Sender: TObject);
    procedure N19Click(Sender: TObject);
    procedure N20Click(Sender: TObject);
    procedure N25Click(Sender: TObject);
    procedure N26Click(Sender: TObject);
    procedure N27Click(Sender: TObject);
    procedure N28Click(Sender: TObject);
    procedure N29Click(Sender: TObject);
    procedure N30Click(Sender: TObject);
    procedure N31Click(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure Tool2Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure N32Click(Sender: TObject);
    procedure N33Click(Sender: TObject);
    procedure N35Click(Sender: TObject);
    procedure N36Click(Sender: TObject);
    procedure N37Click(Sender: TObject);
    procedure MP1Click(Sender: TObject);
    procedure N38Click(Sender: TObject);
    procedure N39Click(Sender: TObject);
    procedure N41Click(Sender: TObject);
    procedure N43Click(Sender: TObject);
    procedure N42Click(Sender: TObject);
    procedure N44Click(Sender: TObject);
    procedure Info1Click(Sender: TObject);
    procedure InfoMartW1Click(Sender: TObject);
    procedure N241Click(Sender: TObject);
    procedure N242Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private 宣言 }
  public
    { Public 宣言 }
  end;

var
  frmMain: TfrmMain;
  i : integer;
implementation  
uses
DM1,inter,MTokuisaki, TDenpyou, TKaishuu,Denpyou, Mitsumori, MitsumoriKizon, MitsumoriArari,   
PBill, PEstimate, PUrikakeDaichou, PUrikakeDaichou2,PCourceUriage, PMishuukin,  
MItemT,MItem,MShiiresaki,MTokuisaki2, UrikakeZan, UriageShuukei,MGyoushu,
Hacchuu,ZaikoKanri,Shukko,MItemC,CopyItem,UriageDenpyouIkkatuInnsatu, GennkinnKaishuu,
GennkinnZanndaka,CheckItem2,PasswordDlg,TokuisakibetsuKakaku,
KaChouhyou,Shime,JuchuuHindo2,IkkatsuHenkou4,Sync_Hindo,Nyuuko,ShiireDaichou,ShiharaiNyuuryoku,ChangeItem,InfoMart, ListInfoMart, InfoBuy, ListInfoBuy;

{$R *.DFM}

procedure TfrmMain.N4Click(Sender: TObject);
begin
  TfrmMTokuisaki.Create(Self);
end;

procedure TfrmMain.N5Click(Sender: TObject);
begin
  TfrmTDenpyou.Create(Self);
end;

procedure TfrmMain.N7Click(Sender: TObject);
begin
  TfrmTKaishuu.Create(Self);

end;

procedure TfrmMain.N6Click(Sender: TObject);
begin
  TfrmPBill.Create(Self);
end;

procedure TfrmMain.N9Click(Sender: TObject);
begin
  TfrmPEstimate.Create(Self);
end;

procedure TfrmMain.N8Click(Sender: TObject);
begin
  TfrmPUrikakeDaichou.Create(Self);

end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
	//最大表示にする
	width := Screen.Width;
	height := Screen.Height;

  //Y2K
	ShortDateFormat := 'yyyy/mm/dd';

end;

procedure TfrmMain.N10Click(Sender: TObject);
begin
	TfrmPCourceUriage.Create(Self);
end;

procedure TfrmMain.N11Click(Sender: TObject);
begin
	TfrmPMishuukin.Create(Self);
end;

procedure TfrmMain.N21Click(Sender: TObject);
begin
  TfrmMTokuisaki2.Create(Self);
end;

procedure TfrmMain.N12Click(Sender: TObject);
begin
  TfrmUrikakeZan.Create(Self);
end;

procedure TfrmMain.N13Click(Sender: TObject);
begin
  TfrmUriageShuukei.Create(Self);
end;

procedure TfrmMain.gyoushu1Click(Sender: TObject);
begin
  TfrmMGyoushu.Create(Self);
end;

procedure TfrmMain.N22Click(Sender: TObject);
begin
  TfrmPUrikakeDaichou2.Create(Self);

end;

procedure TfrmMain.N23Click(Sender: TObject);
begin
  TfrmDenpyou.Create(Self);
end;

procedure TfrmMain.N16Click(Sender: TObject);
begin
  TfrmMShiiresaki.Create(Self);

end;

procedure TfrmMain.N18Click(Sender: TObject);
begin
  TfrmMItem.Create(Self);

end;

procedure TfrmMain.N19Click(Sender: TObject);
begin
  TfrmMItemT.Create(Self);
end;

procedure TfrmMain.N20Click(Sender: TObject);
begin
  TfrmMItemC.Create(Self);
end;

procedure TfrmMain.N25Click(Sender: TObject);
begin
  TfrmShukko.Create(Self);
end;

procedure TfrmMain.N26Click(Sender: TObject);
begin
  TfrmNyuuko.Create(Self);
end;

procedure TfrmMain.N27Click(Sender: TObject);
begin
  TfrmShiireDaichou.Create(Self);

end;

procedure TfrmMain.N28Click(Sender: TObject);
begin
  TfrmCopyItem.Create(Self);

end;

procedure TfrmMain.N29Click(Sender: TObject);
begin
  TfrmUriageDenpyouIkkatuInnsatu.Create(Self);
end; 

procedure TfrmMain.N30Click(Sender: TObject);
begin
  TfrmGennkinnKaishuu.Create(Self);
end;

procedure TfrmMain.N31Click(Sender: TObject);
begin
  TfrmGennkinnZanndaka.Create(Self);
end;

procedure TfrmMain.Exit1Click(Sender: TObject);
begin
  frmMain.close;
end;

procedure TfrmMain.Tool2Click(Sender: TObject);
begin
  TfrmCheckItem2.Create(Self);     
end;

procedure TfrmMain.FormActivate(Sender: TObject);
begin
  GUser := DM1.GetUser;       //#thuyptt 20190313

  //thuyptt 2019/05/07
  //i := 2;

  if ParamStr(1) = '1' then begin
    TfrmPBill.Create(Self);
  end;

  if ParamStr(1) = '2' then begin
    TfrmDenpyou.Create(Self);
  end;

  if ParamStr(1) = '3' then begin
    TfrmUriageDenpyouIkkatuInnsatu.Create(Self);
  end;

  if ParamStr(1) = '4' then begin
    TfrmMitsumori.Create(Self);
  end;

  if ParamStr(1) = '5' then begin
    TfrmMitsumoriKizon.Create(Self);
  end;

  //[E]thuyptt 2019/05/07

  if Pos('ユーザー',frmMain.Caption) = 0 then begin
    frmMain.Caption := frmMain.Caption + '　ユーザー名:'+  GUser;
  end;
end;

procedure TfrmMain.N32Click(Sender: TObject);
begin
  TfrmTokuisakibetsuKakaku.Create(Self);
end;

procedure TfrmMain.N33Click(Sender: TObject);
begin
  TfrmJuchuuHindo2.Create(Self);
end;

procedure TfrmMain.N35Click(Sender: TObject);
begin
  TfrmIkkatsuHenkou4.Create(Self);
end;

procedure TfrmMain.N36Click(Sender: TObject);
begin
  TfrmSync_Hindo.Create(Self);
end;

procedure TfrmMain.N37Click(Sender: TObject);
begin
  TfrmShiharaiNyuuryoku.Create(Self);

end;

procedure TfrmMain.MP1Click(Sender: TObject);
var
 sCode1, sCode2, sSql, sTmp, sCode, sWhere : String;
 i,j : Integer;
 myA : TInRecArry;
begin
exit;
  sSql := 'select Code1, Code2, ShiiresakiCode from tblMItem';
  with QuerySelect do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    i := 0;
    while not EOF do begin
      i := i+1;
      sCode1 := FieldbyName('Code1').AsString;
      sCode2 := FieldbyName('Code2').AsString;
      sTmp := '';
      sCode := '';
      if FieldbyName('ShiiresakiCode').isNull then begin
        sCode := '9999';
      end else begin
        sTmp := FieldbyName('ShiiresakiCode').AsString;
        if pos(',', sTmp) > 0 then begin
          sCode := Copy(sTmp, 1, (pos(',', sTmp)-1));
        end else begin
          sCode := sTmp;
        end;
      end;
      if sCode = '' then begin
        sCode := '9999';
      end;
      sCode := IntToStr(StrToIntDef(sCode, 9999));



      //Update
      j := 2;
      myA[j][1] := 'ShiiresakiCode';
      myA[j][2] := sCode;
      myA[j][3] := '''';
      j := j + 1;
      myA[j][1] := 'END';
      myA[j][2] := 'END';
      myA[j][3] := 'END';

      sWhere := ' where Code1 = ''' + sCode1 + ''' and Code2= ''' + sCode2 + '''';
      DM1.UpdateRecord2(myA, 'tblMItem', sWhere);
      next
    end;//of while
    showmessage(intToStr(i));
    Close;
  end;//of with

end;

procedure TfrmMain.N38Click(Sender: TObject);
begin
  TfrmShime.Create(Self);
end;

procedure TfrmMain.N39Click(Sender: TObject);
begin
  TfrmKaChouhyou.Create(Self);
end;

procedure TfrmMain.N41Click(Sender: TObject);
begin
   TfrmMitsumori.Create(Self);
end;

procedure TfrmMain.N43Click(Sender: TObject);
begin
  TfrmMitsumoriArari.Create(Self);
end;

procedure TfrmMain.N42Click(Sender: TObject);
begin
   TfrmMitsumoriKizon.Create(Self);
end;

procedure TfrmMain.N44Click(Sender: TObject);
begin
    TfrmChangeItem.Create(Self);
end;

procedure TfrmMain.Info1Click(Sender: TObject);
begin
    TfrmListInfoMart.Create(Self);
end;

procedure TfrmMain.InfoMartW1Click(Sender: TObject);
begin
    TfrmInfoMart.Create(Self);
end;

procedure TfrmMain.N241Click(Sender: TObject);
begin
    TfrmListInfoBuy.Create(Self);
end;

procedure TfrmMain.N242Click(Sender: TObject);
begin
    TfrmInfoBuy.Create(Self);
end;

procedure TfrmMain.Button1Click(Sender: TObject);
begin
TfrmPBill.Create(Self);
end;

end.
