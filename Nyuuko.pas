unit Nyuuko;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, ComCtrls, StdCtrls, Buttons, ExtCtrls, Grids, DB,
  DBTables;

type
  TfrmNyuuko = class(TfrmMaster)
    Label7: TLabel;
    cmbDenpyouBanngou: TComboBox;
    cmdDenpyouBanngouListMake: TButton;
    cmdKennsaku: TButton;
    Label3: TLabel;
    DTP1: TDateTimePicker;
    Label6: TLabel;
    CBNyuuryokusha: TComboBox;
    Label10: TLabel;
    edbYomi: TEdit;
    Label4: TLabel;
    CBShiiresakiName: TComboBox;
    cmdGo: TButton;
    Label11: TLabel;
    SG1: TStringGrid;
    Label5: TLabel;
    MemoBikou: TMemo;
    QueryMShiiresaki: TQuery;
    Query1: TQuery;
    lSum: TLabel;
    lbiInputNo: TLabel;
    RadioGroup1: TRadioGroup;
    Label13: TLabel;
    EditGatsubun: TEdit;
    lbShiiresakiCode: TLabel;
    QueryDenpyou: TQuery;
    Label19: TLabel;
    Label18: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure CBShiiresakiNameKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBShiiresakiNameExit(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure cmdGoClick(Sender: TObject);
    procedure edbYomiChange(Sender: TObject);
    procedure SG1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn2Click(Sender: TObject);
    procedure DTP1Change(Sender: TObject);
    procedure cmdDenpyouBanngouListMakeClick(Sender: TObject);
    procedure cmdKennsakuClick(Sender: TObject);
  private
    { Private declarations }
    procedure MakeSG;
    procedure MakeSG2(sDenpyouCode:String);
    procedure MakeCBShiiresakiName(sKey, sWord:String; iKubun:Integer);
    procedure DenpyouBanngouListMake;
    procedure ClearForm;
    function ChkArray(aCode1Code2 : array of string; sCode1,sCode2 : String) : Boolean;

    { Public declarations }
  end;

  const
  //グリッドの列の定義
  CintNum           = 0;
  CintItemCode1     = 1;
  CintItemCode2     = 2;
  CintItemName      = 3;
  CintItemYomi      = 4;
  CintItemMemo      = 5;
  CintItemKikaku    = 6;
  CintItemIrisuu    = 7;
  CintItemCount     = 8;
  CintItemTanni     = 9;
  CintItemTanka     = 10;
  CintItemShoukei   = 11;
  //CintItemHindo     = 12;
  CintShouhinJouken = 12; //追加　商品条件
  CintItemSaishuu   = 13;
  CintBikou         = 14;
  CintEnd           = 15;   //ターミネーター

var
  frmNyuuko: TfrmNyuuko;
  iInputNo   : Integer;        // 入力順番用変数

implementation
uses
  HKLib, DMMaster,inter, DM1, Nyuuko2, PasswordDlg;
var
 //2006.04.23
 //gSum2 :integer;
 gSum2 :Currency;

 giHit : integer;

{$R *.dfm}

procedure TfrmNyuuko.FormCreate(Sender: TObject);
begin

  //2006.05.17
  {   2013.10.15 パスワード無効にした by utasda
  dlgPasswordDlg := TdlgPasswordDlg.Create(Self);
  if dlgPasswordDlg.ShowModal = mrOK then begin
    ;
  end else begin
    Exit;
  end;
  if GPassWord = CPassword1 then begin
    Beep;
  //ADD 2009.04.08 utada
	end else if GPassWord = CPassword2 then begin
    Beep;
  // Add end
  end else begin
    //ShowMessage('PassWordが違います');
    close;
    GPassWord := '';
    exit;
  end;
  GPassWord := '';
 }
 //inherited;
  top  := 5;
  Left := 5;
  Width := 700+50+60+60+60;
  Height := 700;
  DTP1.Date := Date();

  //2002.08.29
  //GTokuisakiCode := '';

  //ストリンググリッドの作成
  MakeSG;

  // よみ使用可/不使用の初期値を不使用にする
  memobikou.Text := '';
end;

procedure TfrmNyuuko.MakeSG;
begin
	with SG1 do begin
    DefaultDrawing     := True;
    RowCount           := 10;   // とりあえず最初は10にしておく．MakeSG2の中で動的に設定
    ColCount           := CintEnd;
    Align              := alBottom;
    FixedCols          := 0;
    FixedColor         := clYellow;
    Canvas.Font.Size   := 11;
    DefaultRowHeight   := 20;
    ColWidths[0]       := 64;
    Font.Color         := clNavy;


    //ColWidths[CintNum]:= 0; // 並び順に変更があった為，ここの順番は滅茶苦茶
    // Cells[CintNum, 0] := '分類名';

    ColWidths[CintNum]:= 10;
    Cells[CintNum, 0] := '';

    ColWidths[CintItemCode1]:= 0;
    Cells[CintItemCode1, 0] := '商品コード1';

    ColWidths[CintItemCode2]:= 0;
    Cells[CintItemCode2, 0] := '商品コード2';

    ColWidths[CintItemName]:= 100+50;
    Cells[CintItemName, 0] := '商品名';

    ColWidths[CintItemYomi]:= 0;
    Cells[CintItemYomi, 0] := '商品読み';

    ColWidths[CintItemMemo]:= 100;
    Cells[CintItemMemo, 0] := 'メモ';

    ColWidths[CintItemKikaku]:= 1;
    Cells[CintItemKikaku, 0] := '規格';        // このデータは下記へ統合された．

    ColWidths[CintItemIrisuu]:= 60;
    Cells[CintItemIrisuu, 0] := '*入数';        // 旧入り数が規格へ変更された．

    //ColWidths[CintItemHindo]:= 60;
    //Cells[CintItemHindo, 0] := '頻度';

    ColWidths[CintItemSaishuu]:= 80;
    Cells[CintItemSaishuu, 0] := '最終入荷日';

    ColWidths[CintItemTanni]:= 30;
    Cells[CintItemTanni, 0] := '単位';

    ColWidths[CintItemCount]:= 60;
    Cells[CintItemCount, 0] := '数量';

    ColWidths[CintItemTanka]:= 60;
    Cells[CintItemTanka, 0] := '単価';

    ColWidths[CintItemShoukei]:= 60;
    Cells[CintItemShoukei, 0] := '小計';

    ColWidths[CintShouhinJouken]:= 120;
    Cells[CintShouhinJouken, 0] := '*商品条件';

    ColWidths[CintBikou]:= 120;
    Cells[CintBikou, 0] := '備考';

  end;
end;

procedure TfrmNyuuko.CBShiiresakiNameKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_F1) then begin
    // Make TokuisakiName
    MakeCBShiiresakiName('Yomi', CBShiiresakiName.Text, 1);
  end; //of if
end;

{
	得意先名のコンボボックスを作成する
  引数：sKey   -> 検索列名
  			sWord  -> 検索ワード
  			iKubun -> 0=前方一致, 1=曖昧検索, 2=数字の完全一致
}
procedure TfrmNyuuko.MakeCBShiiresakiName(sKey, sWord:String; iKubun:Integer);
var
	sSql : String;
begin
	//2002.09.29
  if sWord='' then begin
  	Exit;
  end;

  CBShiiresakiName.Items.Clear;


	sSql := 'SELECT * FROM ' + CtblMShiiresaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + sKey;
  case iKubun of
  0: sSql := sSql + ' LIKE ''' + sWord + '%''';
  1: sSql := sSql + ' LIKE ''%' + sWord + '%''';
  2: sSql := sSql + ' = ' + sWord;
  end;

  sSql := sSql + ' ORDER BY Code ';
  CBShiiresakiName.Clear;
	with QueryMShiiresaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	CBShiiresakiName.Items.Add(FieldByName('Code').AsString + ',' +
       FieldByName('Name').AsString);
    	Next;
    end;//of while
    Close;
  end;//of with

  CBShiiresakiName.DroppedDown := True;
end;

procedure TfrmNyuuko.CBShiiresakiNameExit(Sender: TObject);
begin
  lbiInputNo.Caption := '0';
  if CBShiiresakiName.Text = '' then begin
    exit;
  end;
  // Make StringGrid Data
  MakeSg2('');
  edbYomi.SetFocus;
end;

//2007.04.08
//伝票番号で検索できるように拡張
//procedure TfrmNyuuko.MakeSG2;
procedure TfrmNyuuko.MakeSG2(sDenpyouCode:String);
var
  i,j               : Integer;
  sShiiresakiCode : String;
  sSql,sWhere     : String;
  gRowNum,iRecordCount         : Integer;
  sCode1, sCode2 : String;
  sCode1n, sCode2n : String;
  aCode1Code2 : Array [ 1..5000 ] of String; //
  iArrayIndex : Integer;


begin
  // 入力順番用変数クリア
  iInputNo := 0;
  MemoBikou.Text := '';

  // Get ShiiresakiCode
  sShiiresakiCode := Copy(CBShiiresakiName.Text, 1, (pos(',', CBShiiresakiName.Text)-1));

  GShiiresakiCode := sShiiresakiCode;
  lbShiiresakiCode.Caption := sShiiresakiCode;

  //仕入先の情報を取得する
  sWhere := ' where Code = ' + sShiiresakiCode;
  //lbTokuisakiCode2.Caption := GetFieldData(CtblMShiiresaki, 'TokuisakiCode2', sWhere);
  //lbChainCode.Caption      := GetFieldData(CtblMShiiresaki, 'ChainCode', sWhere);
  MemoBikou.Text    := GetFieldData(CtblMShiiresaki, 'Memo', sWhere);

  //iRecordCount := DM1.GetCount(CtblMItem, sWhere) + 1;
  //確認のため
  iRecordCount := 5000;


  // Make StringGrid
  if sShiiresakiCode <> '' then

    // Make SQL
    //2006.11.14
    //sSql := 'SELECT * FROM tblMItem I LEFT OUTER JOIN ';
    sSql := 'SELECT I.Code1,I.Code2,I.Name,I.Yomi,I.kikaku,I.Irisuu,S.NouhinDate,I.Tanni,I.ShiireKakaku,I.Jouken,S.ShiireKingaku,S.Suuryou,S.Shoukei,I.Memo FROM tblMItem I LEFT OUTER JOIN ';
    //sSql := 'SELECT * FROM tblMItem I INNER JOIN ';
    sSql := sSql + '  tblTShiireDetail S ON I.Code1 = S.Code1 AND I.Code2 = S.Code2';

  //2007.04.08
  //伝票番号が入力されている場合
  if sDenpyouCode<>'' then begin
    sSql := sSql + ' WHERE SDenpyouCode=''' + sDenpyouCode + '''';
  end else begin
    sSql := sSql + ' WHERE I.ShiiresakiCode=' + lbShiiresakiCode.Caption;
    sSql := sSql + ' order by S.NouhinDate desc';
  end;//of if

{
    sSql := 'SELECT * FROM tblMItem I, ';
    sSql := sSql + '  tblTShiireDetail S ';
    sSql := sSql + ' where ';
    sSql := sSql + 'I.Code1 = S.Code1 AND I.Code2 = S.Code2';
    sSql := sSql + ' and ';
    sSql := sSql + ' I.ShiiresakiCode=' + lbShiiresakiCode.Caption;
    sSql := sSql + ' order by S.NouhinDate desc';
}


    // Excecute SQL
    with Query1 do begin
      Close;
      Sql.Clear;
      Sql.Add(sSql);
      Open;

      // もしレコードが無かったら，メッセージボックスを表示して処理を抜ける．
      if iRecordCount =0 then
        begin
          ShowMessage('この仕入先に登録されている商品がありません');
          Close;
          Exit;
        end;

      // Set RowCount
      gRowNum := iRecordCount + 1;  // タイトル分1行足してる
      SG1.RowCount := gRowNum;

      //ストリンググリッド初期化
      with SG1 do begin
        for i := 0 to ColCount - 1 do begin
          for j := 1 to RowCount - 1 do begin
          Cells[i,j] := '';
          end;
        end;
      end;


      // Set Data to StringGrid
      i := 1;
      iArrayIndex := 1;
      while not Query1.EOF do begin
        SB1.SimpleText := IntToStr(i);

        with SG1 do begin
          sCode1 := FieldbyName('Code1').AsString;
          sCode2 := FieldbyName('Code2').AsString;

          //すでに配列にあるかどうか検索する。
          //あれば、なにもしない
          if ChkArray(aCode1Code2, sCode1,sCode2) then begin
            //なければ書き込む
            Cells[CintItemCode1,   i] := FieldbyName('Code1').AsString;
            Cells[CintItemCode2,   i] := FieldbyName('Code2').AsString;
            Cells[CintItemName,    i] := FieldbyName('Name').AsString;
            Cells[CintItemYomi,    i] := FieldbyName('Yomi').AsString;

            Cells[CintItemMemo,    i] := FieldbyName('Memo').AsString;
            Cells[CintItemKikaku,  i] := FieldbyName('Kikaku').AsString;
            Cells[CintItemIrisuu,  i] := FieldbyName('Irisuu').AsString;
            //Cells[CintItemHindo,   i] := FieldbyName('Hindo').AsString;
            Cells[CintItemSaishuu, i] := FieldbyName('NouhinDate').AsString;
            Cells[CintItemCount,   i] := '';
            Cells[CintItemTanni,   i] := FieldbyName('Tanni').AsString;
            //Cells[CintItemTanka,   i] := FieldbyName('Genka').AsString;
            Cells[CintItemTanka,   i] := FieldbyName('ShiireKakaku').AsString;
            Cells[CintItemShoukei, i] := '';
            //Added h.Kubota 2004.03.30
            Cells[CintNum, i] := '';
            //2006.04.18
            Cells[CintShouhinJouken, i] := FieldbyName('Jouken').AsString;

            //2007.04.08
            if sDenpyouCode <> '' then begin
              Cells[CintItemTanka,   i] := FieldbyName('ShiireKingaku').AsString;  //add utada 2007.08.08
              Cells[CintItemCount, i] := FieldbyName('Suuryou').AsString;
              Cells[CintItemShoukei, i] := FieldbyName('Shoukei').AsString;
            end;


           //配列にsCode1+sCode2 を書き込む
            aCode1Code2[iArrayIndex] := sCode1 + sCode2;
            iArrayIndex := iArrayIndex + 1;
            i := i+1;
          end;//of if
          next;
        end; //of with
      end; //of while
      Close;
    end;
    sb1.SimpleText := IntToStr(i+1);
end;

//すでに配列に書き込まれていたらFalseを返す
//書き込まれていなければ、Trueを返す
function TfrmNyuuko.ChkArray(aCode1Code2 : array of String; sCode1,sCode2 : String) : Boolean;
var
 i : Integer;
begin
  for i:=0 to 5000 do begin
    if aCode1Code2[i] = sCode1 + sCode2 then begin
      Result := false;
      exit;
    end;
  end; //of while
  Result := True;
end;



procedure TfrmNyuuko.FormActivate(Sender: TObject);
begin
  inherited;

  //入力者をテキストファイルから読む
  CBNyuuryokusha.Items.LoadFromFile('.\\nyuuryokusha.txt');

  //月分の表示
  EditGatsubun.Text := DateTostr(DTP1.date);
  EditGatsubun.Text := Copy(EditGatsubun.Text,0,7);
  GGatsubun2 := EditGatsubun.Text;

  //CBNyuuryokusha.SetFocus;
  edbYomi.SetFocus;

end;

procedure TfrmNyuuko.cmdGoClick(Sender: TObject);
var
 i     : Integer;
 iHit  : Integer;
 sKey  : String;
 sYomi : String;

begin
 //added Hajime Kubota 2001/12/26
 //edbYomi.ImeMode := imClose;


 sKey := edbYomi.Text;
 iHit := 0;

 // 現在のフォーカス位置より下で，最初によみが商品よみに含まれる行 iHitを探す．

 // 1行目対策
 if (SG1.Row =1) AND (giHit=0) then begin
    sYomi := SG1.Cells[CintItemYomi, 1];
    if AnsiPos(sKey, sYomi) <> 0 then
      begin
        iHit := 1;
      end;
 end;
{ // 1行目以外
 else
   begin
}
 if iHit=0 then begin
     for i := (SG1.Row+1) to SG1.RowCount - 1 do begin
       sYomi := SG1.Cells[CintItemYomi, i];
       if AnsiPos(sKey, sYomi) <> 0 then
         begin
           iHit := i;
           Break;
         end;
     end;//of for
  end;//of if

//   end;

 // 見つからなかったら（iHitが初期値の0のままなら)，メッセージボックスを表示して抜ける．
 if iHit = 0 then
   begin
     ShowMessage('該当する商品はみつかりません');
   end
 else
   begin
    // 見つかったら，iHit行まで移動する．
    SG1.Col       := CintItemCount;
    SG1.Row       := iHit;
    SG1.SetFocus;
    giHit := iHit; //Added H.K.
   end;


end;

procedure TfrmNyuuko.edbYomiChange(Sender: TObject);
begin
  // よみが新しくなる度に検索初期値を1行にする．
  SG1.Row := 1;
  giHit:=0;
end;

procedure TfrmNyuuko.SG1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);

var
	curCount,curTanka,curSum : Currency;

begin

  // 現在のカラムが数量もしくは単価で，数値を入力し終わり，Enterを押した時に小計を計算する．
  if (Key=VK_RETURN) and ( (SG1.Col=CintItemCount) or (SG1.Col=CintItemTanka) ) then begin

    try

	    with SG1 do begin
        // 小計を計算する．この際，数量に小数点を許す．
        // 　入力を取り消す場合，CountにNULLが入れられる事になる．
        //   この場合は，CountとSumにNullをセットする．
        if Cells[CintItemCount, Row] = '' then
          begin
            Cells[CintItemCount,   Row] := '';
            Cells[CintItemShoukei, Row] := '';
            lSum.Caption;
          end
        else
          begin
            // マイナスの数量を入力していたならメッセージを表示して Exit
            {
            if StrToInt(Cells[CintItemCount, Row]) < 0 then begin
              ShowMessage('マイナスは入力できません');
              Cells[CintItemCount, Row] := '';
              Exit;
            end;
  		      }
            //上記では小数点に対応しないので以下のように修正した
            // 2002.06.06 Hajime Kubota

{  //2006.04.18 マイナスも可にした
            if HKLib.Str2Real(Cells[CintItemCount, Row]) < 0 then begin
              ShowMessage('マイナスは入力できません');
              Cells[CintItemCount, Row] := '';
              Exit;
            end;
}
            curCount  := StrToCurr(Cells[CintItemCount, Row]);
  		      curTanka  := StrToCurr(Cells[CintItemTanka, Row]);
            //2006.04.23 小数点を許す
  		      //curSum    := Trunc(curCount * curTanka);
  		      curSum    := curCount * curTanka;
    	      Cells[CintItemShoukei, Row] := CurrToStr(curSum);
            lSum.Caption;
          end;

        // 入力順をセットする．

        //Added by H.K. 2004.02.01
{
        if Cells[CintNum, SG1.Row]='' then begin
          iInputNo := iInputNo + 1;
          Cells[CintNum, SG1.Row] := IntToStr(iInputNo);
        end else
}
        //Added by H.K. 2004.03.30
        if (Cells[CintNum, SG1.Row]='') and
           (Cells[CintItemCount, SG1.Row]<>'') then begin
          iInputNo :=  StrToInt(lbiInputNo.Caption);
          iInputNo := iInputNo + 1;
          Cells[CintNum, SG1.Row] := IntToStr(iInputNo);
          lbiInputNo.Caption :=  IntToStr(iInputNo);
        end; //of if


        // KeyDownで小計を計算後，よみ使用可が選択されている場合のみ，
        // よみエディットボックスにフォーカスを移す

        if RadioGroup1.ItemIndex = 0 then
          begin
            edbYomi.SetFocus;
          end;

	    end;

    except
    	if SG1.Col=CintItemCount then begin
      	//if not Key=VK_BS then begin
    	  //	Showmessage('正しい数値を入力してください');
        //end;
      end;
    end;

  end;


end;

//入庫入力のプレビューボタンがクリックされた
procedure TfrmNyuuko.BitBtn2Click(Sender: TObject);
var
  i,j   : Integer;
  sSql  : String;
begin

// 前回分の入力の影響を消す為，まずGlobal変数を初期化する．
//  GDenpyouBanngou       := '';
  GDate2                 := '';
  GNyuuryokusha2         := '';
  GShiiresakiName        := '';
  GGatsubun2             := EditGatsubun.Text;
  //GMemo                 := '';

  //2004.09.18
  //GAriaCode             := '';

  GRowCount2            := 0;
  SetLength(GArray2,0,0);

  // frmDenpyou2へのデータを保存する（StringGrid以外)

  //GDenpyouBanngou := cmbDenpyouBanngou.Text;
	GDate2           := DateToStr(DTP1.Date);
  GNyuuryokusha2   := CBNyuuryokusha.Text;
  GShiiresakiName  := CBShiiresakiName.text;
  //GGatsubun       := Panel4.Caption;
  //GMemo           := MemoBikou.Text;

  //2004.09.18
  //GAriaCode       := lbAriaCode.Caption;

{
  if chbTeisei.Checked = True then GTeisei := 1 else GTeisei := 0;
  GBikou          := edbBikou1.Text;
}
  // Key Null Check!
  if (GDate2 ='') or (GNyuuryokusha2 ='') or (GShiiresakiName ='') then
    begin
      ShowMessage('納品日，得意先コード，入力者は省略できません');
      Exit;
    end;

  // 伝票番号の存在をチェックする．(伝票番号が nullでない場合，チェックをかける)
{
   if GDenpyouBanngou <> '' then begin

    sSql := 'SELECT * FROM ' + CtblTDenpyouDetail;
    sSql := sSql + ' WHERE DenpyouCode = ' + '''' + GDenpyouBanngou + '''';

      with QueryDenpyou do begin
   	    Close;
        Sql.Clear;
        Sql.Add(sSql);
        Open;
        if RecordCount = 0 then
          begin
            ShowMessage('この伝票番号の伝票は存在しません');
            Exit;
          end;
        Close;
      end;
   end;
}
  // GArray2の要素数をセット
  SetLength(GArray2, SG1.RowCount-1, SG1.ColCount);

  // frmDenpyou2へのデータを保存する（StringGrid分)
  with SG1 do begin
    // J=0はタイトル行なのでJ=1から始める
    i := 0;
    GSum2 := 0;
  	for j:=1 to RowCount - 1 do begin
    	if Cells[CintItemCount, j]<>'' then begin
        // ｊは入力のあったレコード数となる．
  			GArray2[i, CintNum]       := Cells[CintNum,          j];
  			GArray2[i, CintItemCode1] := Cells[CintItemCode1,    j];
  			GArray2[i, CintItemCode2] := Cells[CintItemCode2,    j];
  			GArray2[i, CintItemName] := Cells[CintItemName,      j];
  			GArray2[i, CintItemYomi] := Cells[CintItemYomi,      j];
  			GArray2[i, CintItemKikaku] := Cells[CintItemKikaku,  j];
  			GArray2[i, CintItemIrisuu] := Cells[CintItemIrisuu,  j];
  			GArray2[i, CintItemSaishuu] := Cells[CintItemSaishuu,j];
        // 訂正伝票の場合，数量，小計をマイナスに変換する．
{        if chbTeisei.Checked = True then begin
          GArray2[j, 9] := Real2Str(-1 * Str2Real(Cells[CintItemCount, i]),2,2);
   			  GArray2[j,12] := Real2str(-1 * Str2Real(Cells[CintItemShoukei,i]),2,0);
        end else begin
          GArray2[j, 9] := Cells[CintItemCount,  i];
   			  GArray2[j,12] := Cells[CintItemShoukei,i];
        end;
 }
        GArray2[i, CintItemCount]  := Cells[CintItemCount,    j];
        GArray2[i,CintItemShoukei] := Cells[CintItemShoukei,  j];
        GArray2[i,CintItemTanni]   := Cells[CintItemTanni,    j];
        GArray2[i,CintItemTanka]   := Cells[CintItemTanka,    j];

        //2006.04.23
        GArray2[i,CintBikou]   := Cells[CintBikou,    j];

        GSum2 := GSum2 + StrToCurr(GArray2[i,CintItemShoukei]);
        i := i + 1;
      end; //of if
    end;// of for
  end;//of with

  // GRowCountのセット
  GRowCount2 := i + 1;

  // Denpyou2のModal表示
  frmNyuuko2 := TfrmNyuuko1.Create(Self);
  frmNyuuko2.Label18.Caption := Label18.Caption;
{ 何故かModal表示の定石の下記コマンドがエラーになる．
  try
    frmDenpyou2.ShowModal;
  finally
    frmDenpyou2.Release;
  end;
}

end;

procedure TfrmNyuuko.DTP1Change(Sender: TObject);
begin
  EditGatsubun.Text := DateTostr(DTP1.date);
  EditGatsubun.Text := Copy(EditGatsubun.Text,0,7);
  GGatsubun2 := EditGatsubun.Text;
end;

procedure TfrmNyuuko.cmdDenpyouBanngouListMakeClick(Sender: TObject);
begin
   DenpyouBanngouListMake;
end;

procedure TfrmNyuuko.DenpyouBanngouListMake;
var
 sNouhinbi       : String;
 sShiiresakiCode : String;
 sSql            : String;

begin

 // 納品日 or 得意先コードがNullの場合，Messegeを出力して抜ける．
 if (DateTimeToStr(DTP1.Date) = '') or (CBShiiresakiName.Text = '') then
  begin
    ShowMessage('納品日と仕入先コードを指定して下さい．該当する伝票番号リストを作成します．');
    Exit;
  end;

 // パラメータ作成
 DateTimeToString(sNouhinbi, 'yyyy/mm/dd', DTP1.Date);
 sShiiresakiCode := Copy(CBShiiresakiName.Text, 1, (pos(',', CBShiiresakiName.Text)-1));

 // コンボボックスクリア
 cmbDenpyouBanngou.Items.Clear;

 // SQL作成
 sSql := 'SELECT DISTINCT SDenpyouCode FROM ' + CtblTShiireDetail;
 sSql := sSql + ' WHERE NouhinDate = ' + '''' + sNouhinbi + '''';
 sSql := sSql + ' AND ShiiresakiCode = ' + sShiiresakiCode;
 sSql := sSql + ' ORDER BY SDenpyouCode';

 // tblTDenpyouDetailより取得できるデータを取得
   with QueryDenpyou do begin
     Close;
     Sql.Clear;
     Sql.Add(sSql);
     Open;

   // レコードがなければメッセージを出力して抜ける
     if RecordCount = 0 then
       begin
         ShowMessage('該当する伝票番号はありません');
         Exit;
       end;

   // レコードが尽きる迄，ComboBoxのリストにItemを追加
      while not EOF do begin
      	cmbDenpyouBanngou.Items.Add(FieldByName('SDenpyouCode').AsString);
    	  Next;
      end;
     Close;
   end;

 // コンボボックスをドロップダウンさせる
 cmbDenpyouBanngou.DroppedDown := True;

end;


//検索ボタンがクリックされた
procedure TfrmNyuuko.cmdKennsakuClick(Sender: TObject);
var
  strDenpyouBanngou: String;
  strTokuisakiCode1: String;
  strTeiseiflg     : String;
  strBikou, sShiiresaki       : String;
  sSql             : String;
  i                : Integer;
  gRowNum          : Integer;
begin
  // 伝票番号セット
  strDenpyouBanngou := cmbDenpyouBanngou.Text;
  if strDenpyouBanngou = '' then begin
    ShowMessage('伝票番号が入力されていません');
    exit;
  end;
  // セットする前に一旦表示をクリアする．
  sShiiresaki := CBShiiresakiName.Text;
  ClearForm;
  MakeSG;

  //2007.04.07
  //基本情報の取得
  sSql := 'SELECT * FROM ' + CtblTShiire;
  sSql := sSql + ' WHERE SDenpyouCode = ' + '''' + strDenpyouBanngou + '''';
  with QueryDenpyou do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    // レコードがなければメッセージを出力して抜ける
    if RecordCount = 0 then begin
       ShowMessage('該当する伝票はありません');
       Close;
       Exit;
    end;
    // 入力者セット
    CBNyuuryokusha.Text   := FieldbyName('Nyuuryokusha').AsString;
    //仕入先復活
    CBShiiresakiName.Text := sShiiresaki;

	//
    Label18.Caption       := FieldbyName('InfoDenpyouNo').AsString;
	  
    Close;
  end;//of with
  MakeSG2(strDenpyouBanngou);
end;

//クリア
procedure TfrmNyuuko.ClearForm;
var
	i,j : Integer;
begin

  // 表示をクリアする．

  cmbDenpyouBanngou.Clear;
  CBShiiresakiName.Clear;
  CBNyuuryokusha.Clear;
  MemoBikou.Text := '';

  with SG1 do begin
    for i := 0 to ColCount - 1 do begin
      for j := 1 to RowCount - 1 do begin
       Cells[i,j] := '';
      end;
    end;
  end;

end;


end.
