object frmIkkatsuHenkou2: TfrmIkkatsuHenkou2
  Left = 504
  Top = 226
  Width = 627
  Height = 346
  Caption = 'IkkatsuHenkou'
  Color = clBtnFace
  Font.Charset = SHIFTJIS_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 12
  object Label3: TLabel
    Left = 12
    Top = 8
    Width = 84
    Height = 13
    Caption = #20385#26684#19968#25324#22793#26356
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 54
    Top = 28
    Width = 76
    Height = 12
    Caption = #21830#21697#12467#12540#12489'(1)'
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 54
    Top = 52
    Width = 39
    Height = 12
    Caption = #21830#21697#21517
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 56
    Top = 79
    Width = 24
    Height = 12
    Caption = #12424#12415
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = [fsBold]
    ParentFont = False
  end
  object CBCode1: TComboBox
    Left = 138
    Top = 22
    Width = 163
    Height = 21
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = []
    ImeMode = imClose
    ItemHeight = 13
    ParentFont = False
    TabOrder = 0
    Items.Strings = (
      '01,'#26989#21209#29992#39135#26448
      '02,'#35519#21619#26009#35519#21619#39135#21697
      '03,'#39321#36763#26009
      '04,'#12499#12531#12289#32566#35440#39006
      '05,'#12467#12540#12498#12540#12289#21931#33590#26448#26009
      '06,'#20083#35069#21697#65288#12481#12540#12474#39006#65289
      '07,'#39135#29992#27833
      '08,'#31881#35069#21697
      '09,'#12418#12385
      '10,'#12473#12497#40634
      '11,'#40634
      '12,'#31859
      '13,'#20013#33775#26448#26009
      '14,'#21508#31278#12472#12517#12540#12473
      '15,'#12362#33590
      '16,'#27703#12471#12525#12483#12503
      '17,'#28460#29289#24803#33756
      '18,'#12362#36890#12375#29289
      '19,'#39135#32905#21152#24037#21697
      '20,'#28023#29987#29645#21619
      '21,'#20919#20941#39135#21697#39770#20171#39006
      '22,'#20919#20941#39135#21697#36786#29987#29289
      '23,'#20919#20941#39135#21697#19968#27425#21152#24037#21697
      '24,'#20919#20941#39135#21697#28857#24515#39006
      '25,'#12486#12522#12540#12492#39006
      '26,'#12524#12488#12523#12488#39006#28271#29006
      '27,'#20919#20941#35201#20919#12289#39321#36763#26009
      '28,'#12362#12388#12414#12415#35910#39006
      '29,'#12362#12388#12414#12415#12362#12363#12365#39006
      '30,'#12362#12388#12414#12415#12481#12483#12503#39006
      '31,'#12481#12519#12467#12524#12540#12488#12289#12476#12522#12540#39006
      '32,'#28023#29987#29289
      '33,'#24178#12375#32905
      ''
      '')
  end
  object CBName: TComboBox
    Left = 138
    Top = 47
    Width = 250
    Height = 21
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = []
    ImeMode = imOpen
    ItemHeight = 13
    ParentFont = False
    TabOrder = 1
  end
  object CBYomi: TComboBox
    Left = 138
    Top = 74
    Width = 250
    Height = 21
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = []
    ImeMode = imHira
    ItemHeight = 13
    ParentFont = False
    TabOrder = 2
  end
end
