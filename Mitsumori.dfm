object frmMitsumori: TfrmMitsumori
  Left = 303
  Top = 64
  Width = 899
  Height = 604
  Caption = 'frmMitsumori'
  Color = clBtnFace
  Font.Charset = SHIFTJIS_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 12
  object Label9: TLabel
    Left = 680
    Top = 200
    Width = 178
    Height = 12
    Caption = #20966#29702#20013#12391#12377#12290#12375#12400#12425#12367#12362#24453#12385#19979#12373#12356'....'
  end
  object pnlHeader: TPanel
    Left = 0
    Top = 0
    Width = 891
    Height = 57
    Align = alTop
    Color = clTeal
    TabOrder = 0
    object lblFormName: TLabel
      Left = 16
      Top = 4
      Width = 68
      Height = 16
      Alignment = taCenter
      Caption = #35211#31309#20316#25104
      Color = clTeal
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object lblVersion: TLabel
      Left = 28
      Top = 28
      Width = 251
      Height = 13
      AutoSize = False
      Caption = #40441#26494#23627#22770#25499#31649#29702#12471#12473#12486#12512' Ver5.00'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clSilver
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object dumyDate: TEdit
      Left = 408
      Top = 16
      Width = 129
      Height = 20
      TabOrder = 0
      Visible = False
    end
  end
  object pnlFooter: TPanel
    Left = 0
    Top = 469
    Width = 891
    Height = 89
    Align = alBottom
    Color = clTeal
    TabOrder = 1
    object lbiInputNo: TLabel
      Left = 666
      Top = 50
      Width = 4
      Height = 12
    end
    object cmdExcel: TBitBtn
      Left = 149
      Top = 24
      Width = 108
      Height = 31
      Caption = #12456#12463#12475#12523#12408
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ModalResult = 4
      ParentFont = False
      TabOrder = 0
      OnClick = cmdExcelClick
      NumGlyphs = 2
    end
    object cmdMitsumoriIkkatsuPrint: TBitBtn
      Left = 274
      Top = 15
      Width = 155
      Height = 25
      Caption = #35211#31309#12426#19968#25324#21360#21047
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = cmdMitsumoriIkkatsuPrintClick
    end
    object cmdMitsumoriExcel: TBitBtn
      Left = 274
      Top = 48
      Width = 155
      Height = 25
      Caption = #35211#31309#12426#12456#12463#12475#12523#12408
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = cmdMitsumoriExcelClick
    end
    object cmdHacchushoExcel: TBitBtn
      Left = 444
      Top = 48
      Width = 155
      Height = 25
      Caption = #30330#27880#26360#12456#12463#12475#12523#12408
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = cmdHacchushoExcelClick
    end
    object cmdHacchushoIkkatsuPrint: TBitBtn
      Left = 444
      Top = 15
      Width = 155
      Height = 25
      Caption = #30330#27880#26360#19968#25324#21360#21047
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = cmdHacchushoIkkatsuPrintClick
    end
    object RadioGroup1: TRadioGroup
      Left = 616
      Top = 8
      Width = 139
      Height = 34
      Caption = #30330#27880#26360#12497#12479#12540#12531
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        '1'#34892
        '2'#34892
        '3'#34892)
      TabOrder = 5
    end
    object RadioGroup2: TRadioGroup
      Left = 617
      Top = 47
      Width = 139
      Height = 34
      Caption = #38971#24230#12539#22238#25968
      Columns = 4
      ItemIndex = 0
      Items.Strings = (
        '1'
        '2'
        '3'
        '4')
      TabOrder = 6
    end
    object RadioGroup3: TRadioGroup
      Left = 770
      Top = 13
      Width = 99
      Height = 60
      Caption = #26399#38291
      ItemIndex = 0
      Items.Strings = (
        #21322#24180#20197#20869
        '1'#24180#20197#20869)
      TabOrder = 7
    end
  end
  object pnlInput: TPanel
    Left = 0
    Top = 57
    Width = 891
    Height = 176
    Align = alTop
    TabOrder = 2
    OnClick = pnlInputClick
    object Label3: TLabel
      Left = 16
      Top = 8
      Width = 48
      Height = 15
      Caption = #20206#30058#21495
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 16
      Top = 37
      Width = 48
      Height = 15
      Caption = #25285#24403#32773
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 316
      Top = 135
      Width = 26
      Height = 13
      Caption = #12424#12415
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 341
      Top = 157
      Width = 365
      Height = 13
      Caption = #21830#21697#26908#32034#12399'"'#12424#12415'"'#12398#19968#37096#12434#20837#21147#12375#12390'Go'#12508#12479#12531#12434#12463#12522#12483#12463#12375#12390#19979#12373#12356#12290
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 236
      Top = 9
      Width = 52
      Height = 15
      Caption = #26082#23384'No'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -15
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 421
      Top = 12
      Width = 76
      Height = 13
      Caption = #24471#24847#20808#12525#12483#12463
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 270
      Top = 50
      Width = 92
      Height = 13
      Caption = #22320#22495#12539#24471#24847#20808#21517
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 244
      Top = 77
      Width = 118
      Height = 13
      Caption = #22320#22495#12539#24471#24847#20808#21517#12424#12415
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label14: TLabel
      Left = 719
      Top = 100
      Width = 33
      Height = 12
      Caption = #35211#31309#12426
    end
    object Label16: TLabel
      Left = 718
      Top = 116
      Width = 48
      Height = 12
      Caption = #24179#22343#31895#21033
    end
    object Label17: TLabel
      Left = 794
      Top = 100
      Width = 54
      Height = 12
      Caption = #38971#24230#12539#35330#27491
    end
    object Label18: TLabel
      Left = 794
      Top = 116
      Width = 48
      Height = 12
      Caption = #24179#22343#31895#21033
    end
    object Label1: TLabel
      Left = 303
      Top = 101
      Width = 56
      Height = 13
      Caption = #20385#26684#21306#20998
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 7
      Top = 157
      Width = 333
      Height = 13
      Caption = #9632#12510#12540#12463#12398#38917#30446#12399#12289#12479#12452#12488#12523#12434#12463#12522#12483#12463#12377#12427#20107#12391#12477#12540#12488#12391#12365#12414#12377#12290
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ParentFont = False
    end
    object GroupBox2: TGroupBox
      Left = 8
      Top = 122
      Width = 249
      Height = 33
      TabOrder = 21
    end
    object CBTantosha: TComboBox
      Left = 70
      Top = 35
      Width = 131
      Height = 20
      Style = csDropDownList
      DropDownCount = 20
      ImeMode = imClose
      ItemHeight = 12
      TabOrder = 3
      Items.Strings = (
        '')
    end
    object edbYomi: TEdit
      Left = 348
      Top = 131
      Width = 121
      Height = 20
      ImeMode = imOpen
      TabOrder = 2
      OnChange = edbYomiChange
    end
    object cmdGo: TButton
      Left = 474
      Top = 133
      Width = 41
      Height = 17
      Caption = 'Go'
      Default = True
      TabOrder = 4
      OnClick = cmdGoClick
    end
    object EditKaribango: TEdit
      Left = 70
      Top = 8
      Width = 65
      Height = 20
      Color = cl3DLight
      ReadOnly = True
      TabOrder = 0
    end
    object cmdKabangoToroku: TButton
      Left = 138
      Top = 8
      Width = 83
      Height = 20
      Caption = #20206#30058#21495#30331#37682
      TabOrder = 1
      OnClick = cmdKabangoTorokuClick
    end
    object GroupBox1: TGroupBox
      Left = 16
      Top = 64
      Width = 185
      Height = 47
      Caption = #21336#20385#36984#25246
      TabOrder = 5
      object cmdTanka420: TButton
        Left = 6
        Top = 17
        Width = 83
        Height = 20
        Caption = '420'#26032#35215
        TabOrder = 0
        OnClick = cmdTanka420Click
      end
      object cmdTanka450: TButton
        Left = 95
        Top = 17
        Width = 83
        Height = 20
        Caption = '450'#26032#35215
        TabOrder = 1
        OnClick = cmdTanka450Click
      end
    end
    object CBKizonNo: TComboBox
      Left = 290
      Top = 7
      Width = 119
      Height = 20
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imClose
      ItemHeight = 12
      ParentFont = False
      TabOrder = 6
      OnExit = CBKizonNoExit
      OnKeyDown = CBKizonNoKeyDown
      OnSelect = CBKizonNoSelect
      Items.Strings = (
        '')
    end
    object EditTokuisakiLock: TEdit
      Left = 504
      Top = 8
      Width = 147
      Height = 20
      ImeMode = imOpen
      ReadOnly = True
      TabOrder = 7
    end
    object cmdHontorokue: TButton
      Left = 672
      Top = 8
      Width = 65
      Height = 25
      Caption = #26412#30331#37682#12408
      TabOrder = 8
      OnClick = cmdHontorokueClick
    end
    object cmdHontoroku: TButton
      Left = 743
      Top = 8
      Width = 65
      Height = 25
      Caption = #26412#30331#37682
      TabOrder = 9
      OnClick = cmdHontorokuClick
    end
    object cmdHozon: TButton
      Left = 672
      Top = 54
      Width = 65
      Height = 20
      Caption = #20445#23384
      TabOrder = 10
      OnClick = cmdHozonClick
    end
    object CBShohinBumon: TComboBox
      Left = 116
      Top = 130
      Width = 131
      Height = 20
      ImeMode = imClose
      ItemHeight = 12
      TabOrder = 11
      OnExit = CBShohinBumonExit
      Items.Strings = (
        '00,'#12377#12409#12390#34920#31034
        '01,'#26989#21209#29992#39135#26448
        '02,'#35519#21619#26009#35519#21619#39135#21697
        '03,'#39321#36763#26009
        '04,'#12499#12531#12289#32566#35440#39006
        '05,'#12467#12540#12498#12540#12289#21931#33590#26448#26009
        '06,'#20083#35069#21697#65288#12481#12540#12474#39006#65289
        '07,'#39135#29992#27833
        '08,'#31881#35069#21697
        '09,'#12418#12385
        '10,'#12473#12497#40634
        '11,'#40634
        '12,'#31859
        '13,'#20013#33775#26448#26009
        '14,'#21508#31278#12472#12517#12540#12473
        '15,'#12362#33590
        '16,'#27703#12471#12525#12483#12503
        '17,'#28460#29289#24803#33756
        '18,'#12362#36890#12375#29289
        '19,'#39135#32905#21152#24037#21697
        '20,'#28023#29987#29645#21619
        '21,'#20919#20941#39135#21697#39770#20171#39006
        '22,'#20919#20941#39135#21697#36786#29987#29289
        '23,'#20919#20941#39135#21697#19968#27425#21152#24037#21697
        '24,'#20919#20941#39135#21697#28857#24515#39006
        '25,'#12486#12522#12540#12492#39006
        '26,'#12524#12488#12523#12488#39006#28271#29006
        '27,'#20919#20941#35201#20919#12289#39321#36763#26009
        '28,'#12362#12388#12414#12415#35910#39006
        '29,'#12362#12388#12414#12415#12362#12363#12365#39006
        '30,'#12362#12388#12414#12415#12481#12483#12503#39006
        '31,'#12481#12519#12467#12524#12540#12488#12289#12476#12522#12540#39006
        '32,'#28023#29987#29289
        '33,'#24178#12375#32905
        '90,'#29305#27530#12467#12540#12489' ')
    end
    object EditMitsumoriArari: TEdit
      Left = 715
      Top = 131
      Width = 65
      Height = 20
      Color = cl3DLight
      ReadOnly = True
      TabOrder = 12
    end
    object EditHindoTeiseiArari: TEdit
      Left = 791
      Top = 131
      Width = 65
      Height = 20
      Color = cl3DLight
      ReadOnly = True
      TabOrder = 13
    end
    object CBTokuisakiName: TComboBox
      Left = 375
      Top = 46
      Width = 218
      Height = 20
      ImeMode = imOpen
      ItemHeight = 12
      TabOrder = 14
      OnExit = CBTokuisakiNameExit
      OnKeyDown = CBTokuisakiNameKeyDown
    end
    object CBTokuisakiNameYomi: TComboBox
      Left = 375
      Top = 74
      Width = 218
      Height = 20
      ImeMode = imOpen
      ItemHeight = 12
      TabOrder = 15
      OnKeyDown = CBTokuisakiNameYomiKeyDown
    end
    object Button1: TButton
      Left = 764
      Top = 51
      Width = 106
      Height = 25
      Caption = #39015#23458#12510#12473#12479#30011#38754#12408
      TabOrder = 16
      OnClick = Button1Click
    end
    object CmbdumyMno: TComboBox
      Left = 368
      Top = 24
      Width = 145
      Height = 20
      ItemHeight = 12
      TabOrder = 17
      Visible = False
    end
    object EditKakakuKubun: TEdit
      Left = 375
      Top = 98
      Width = 65
      Height = 20
      Color = cl3DLight
      ReadOnly = True
      TabOrder = 18
    end
    object btnReset: TButton
      Left = 819
      Top = 8
      Width = 65
      Height = 25
      Caption = #12522#12475#12483#12488
      TabOrder = 19
      OnClick = btnResetClick
    end
    object EditTokuisakiCode: TEdit
      Left = 456
      Top = 96
      Width = 65
      Height = 20
      TabOrder = 20
      Visible = False
    end
    object StaticText1: TStaticText
      Left = 42
      Top = 132
      Width = 68
      Height = 19
      Caption = #21830#21697#37096#38272
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 22
    end
    object CheckBox1: TCheckBox
      Left = 18
      Top = 132
      Width = 17
      Height = 17
      BiDiMode = bdRightToLeft
      ParentBiDiMode = False
      TabOrder = 23
      OnClick = CheckBox1Click
    end
  end
  object SG1: TStringGrid
    Left = 24
    Top = 240
    Width = 689
    Height = 217
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
    TabOrder = 3
    OnClick = SG1Click
    OnDblClick = SG1DblClick
    OnDrawCell = SG1DrawCell
    OnExit = SG1Exit
    OnKeyDown = SG1KeyDown
    OnKeyPress = SG1KeyPress
    OnMouseDown = SG1MouseDown
    OnSelectCell = SG1SelectCell
    OnSetEditText = SG1SetEditText
    ColWidths = (
      64
      64
      64
      64
      64)
    RowHeights = (
      24
      24)
  end
  object SB2: TStatusBar
    Left = 0
    Top = 558
    Width = 891
    Height = 19
    Panels = <>
  end
  object Query1: TQuery
    DatabaseName = 'dabMitsumori'
    Left = 608
    Top = 105
  end
  object dabMitsumori: TDatabase
    AliasName = 'taka-access'
    DatabaseName = 'dabMitsumori'
    LoginPrompt = False
    Params.Strings = (
      'user_name=sa'
      'password=netsurf'
      'USER NAME=sa')
    SessionName = 'Default'
    Left = 296
    Top = 40
  end
end
