unit InfoBuy;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Master,
  Dialogs, DB, DBTables, StdCtrls, Buttons, ExtCtrls, ComCtrls, FMTBcd,
  SqlExpr;

type
  TfrmInfoBuy = class(TfrmMaster)
    Label3: TLabel;
    Label4: TLabel;
    btnRegister: TBitBtn;
    CBNyuuryokusha: TComboBox;
    Query2: TQuery;
    btnChooseFile: TBitBtn;
    Label5: TLabel;
    ListBox1: TListBox;
    Label6: TLabel;
    Edit1: TEdit;
    txtFilePath: TEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    dlgOpenFile: TOpenDialog;
    QueryDenpyou: TQuery;
    dabDenpyou: TDatabase;
    Label10: TLabel;
    DTP1: TDateTimePicker;
    
	  procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure btnChooseFileClick(Sender: TObject);
    procedure btnRegisterClick(Sender: TObject);
	  procedure Split(const Delimiter: Char;Input: string;const Strings: TStrings) ;
    procedure BitBtn3Click(Sender: TObject);

  private
    { Private declarations }
    Function InserttblTShiire(DenpyouDetail:TStringList; strSDenpyouCode:String): Boolean;
    Function InserttblTShiireDetail(DenpyouDetail:TStringList; strSDenpyouCode:String) : Boolean;	
	Function MakeSDenpyouCode(iShiiresakiCode:integer; sNyuukabi:String; strInfoDenpyouNo:String): string;
    Function Replace (Str:string; Before:string; After: string): String;
	Function UpdateTax (curSum:Currency; strSDenpyouCode:string): Boolean;
  public
    { Public declarations }
  end;

var
  frmInfoBuy: TfrmInfoBuy;
  //gDenpyouNo  : String;
  curSum      : Currency; 

implementation
uses
Inter, DenpyouPrint, HKLib, ListInfoBuy;

{$R *.dfm}

procedure TfrmInfoBuy.FormCreate(Sender: TObject);
begin
  inherited;
  Top  := 5;
  Left := 5;
	Width := 650;
  Height := 480;
  DTP1.Date := Date();  
end;

procedure TfrmInfoBuy.FormActivate(Sender: TObject);
begin
  inherited;
  if CBNyuuryokusha.Text ='' then begin
	  CBNyuuryokusha.SetFocus;
    CBNyuuryokusha.Items.LoadFromFile('nyuuryokusha.txt');
  end;
end;

procedure TfrmInfoBuy.btnChooseFileClick(Sender: TObject);
begin
    dlgOpenFile := TOpenDialog.Create(Self);
    dlgOpenFile.Filter := 'Only Csv files (*.csv)|*.csv';
    if dlgOpenFile.Execute then begin
        txtFilePath.Text:=dlgOpenFile.FileName;
    end;
    dlgOpenFile.Free;
    ChDir(ExtractFilePath(Application.ExeName));
    
end;

procedure TfrmInfoBuy.btnRegisterClick(Sender: TObject);
  var dlg           : TForm;
      res           : Word;
      i             : Integer;

      sFileName     : String;
      slCsv, slRow  : TStringList;
      sCurrentNo    : String;
      sNewNo        : String;
      iCommit       : Boolean;
      sInfoDenpyouNo: String;
      iRollback     : Boolean;
      strSDenpyouCode :String;
      strShiiresakiCode :String;
      strNouhinDate :String;
      strCode           : TStringList;
      strCode1          : String;
      strCode2          : String;
      sLockFileName     : String;
      strAriaCode       : String;
      FileHandle: Integer;
begin
    ListBox1.Items.Clear;

    if Trim(CBNyuuryokusha.Text) = '' then
    begin
      ShowMessage('入力者を選択して下さい。');
      Exit;
    end;


    sFileName:=txtFilePath.Text;

    if Trim(sFileName) = '' then
    begin
      ShowMessage('データファイルを選択して下さい。');
      Exit;
    end;

     //確認メッセージ
    dlg := CreateMessageDialog('登録しますか', mtInformation,
                             [mbYes, mbNo]);
    dlg.ActiveControl := TWinControl(dlg.FindComponent('No'));
    res := dlg.ShowModal;
    if res <> mrYes then begin
      exit;
    end;
    dlg.Free;



    //ALL CSV FILES
    slCsv :=  TStringList.Create;
    slCsv.LoadFromFile(sFileName);

    //ONE ROWS
    slRow := TStringList.Create;

 try

    // DATA START FROM ROW 2 IN CSV FILES
    dabDenpyou.Open;
    if not dabDenpyou.InTransaction then begin
       dabDenpyou.StartTransaction;
    end;
    iCommit   := True;
    iRollback := False;


    sInfoDenpyouNo := '';
    Split(',', slCsv[2], slRow);

    sCurrentNo:=slRow[2];
    GDDate    :=DatetoStr(DTP1.Date);

    Split(',', slCsv[2], slRow);
    sCurrentNo:=slRow[2];
    for i:=2 to slCsv.Count-1 do begin
         Split(',', slCsv[i], slRow);
         if (slRow[0] = 'D') then begin
            sNewNo:=slRow[2];
            //if 自社管理商品コード OR  取引先コード will rollback all and show message in textarea
            if(Trim(slRow[7]) = '')  then begin
                ListBox1.Items.Add(IntToStr(i + 1) + '行目：［取引先コード］が空の為登録できません。［伝票No］' + sNewNo);
                ListBox1.Items.Add('すべての登録処理をキャンセルします。インフォマートデータファイルを確認して下さい。');
                ListBox1.Update;
                iRollback := True;
                dabDenpyou.Rollback;
                Exit;
            end;

            strCode := TStringList.Create;
            strCode1 :='';
            strCode2 :='';
            if (Trim(slRow[16]) <> '' )then begin
                Split('-', slRow[16], strCode); //自社管理商品コード
                strCode1 := strCode[0];
                if(strCode.Count > 1) then begin
                    strCode2          := strCode[1];
                end;
            end;


            if(Trim(strCode1) <> '') OR (Trim(strCode2) <> '' )  then begin
                 if(Length(strCode1) <> 2 ) OR (Length(strCode2) <> 4 )  then begin
                    ListBox1.Items.Add(IntToStr(i + 1) + '行目：［自社管理商品コード］が不正の為登録できません。［伝票No］' + sNewNo);
                    ListBox1.Items.Add('すべての登録処理をキャンセルします。インフォマートデータファイルを確認して下さい。');
                    ListBox1.Update;
                    iRollback := True;
                    dabDenpyou.Rollback;
                    Exit;
                end;
            end;

            //if(Trim(slRow[7]) <> '') AND (Trim(slRow[16]) <> '') then begin
            if(Trim(slRow[7]) <> '') then begin

              //START INSERT TO DATABASE
              if(i = 2) OR (sCurrentNo<>sNewNo) then begin
				if (sCurrentNo<>sNewNo) then begin
				  UpdateTax(curSum, strSDenpyouCode);	
				end;  
                  ListBox1.Items.Add('伝票No :[ ' + sNewNo + ' ] 登録');
                  ListBox1.Update;

                  strShiiresakiCode := slRow[7];

                  //strAriaCode:=GetAriaCode(strShiiresakiCode);
                  strAriaCode:=strShiiresakiCode;

                  //ロックファイル名の生成
                  sLockFileName :=  'SLock_' + strAriaCode + '.txt';

                  //2003.08.03
                 //ロックファイルがあるかどうか
                  if FileExists(sLockFileName)=True then begin
	                    ShowMessage('現在他のユーザーがエリアコード' + strAriaCode + 'の伝票番号を取得中です。数秒お待ちください');
                          ListBox1.Items.Add('すべての登録処理をキャンセルします。');
                          ListBox1.Update;
                          iRollback := True;
                          dabDenpyou.Rollback;
                          dabDenpyou.Close;
                          exit;
                  end else begin
	                    //ここでロックファイルを作成する
                      FileHandle := FileCreate(sLockFileName);
	                    if FileHandle = -1 then begin
    	                    Showmessage('このコースには現在ロックがかかっています。再度時間をおいて登録してください。');
                          ListBox1.Items.Add('すべての登録処理をキャンセルします。');
                          ListBox1.Update;
                          iRollback := True;
                          dabDenpyou.Rollback;
                          dabDenpyou.Close;
                          Exit;
                      end else begin
	                        FileClose(FileHandle);
                     end;
                  end;

                //GET trDenpyouBangou

                 // strNouhinDate := slRow[37];   // mod before 20130227 utada
                  strNouhinDate :=DatetoStr(DTP1.Date);  //納品日 mod after 20130227 utada
                  strSDenpyouCode := MakeSDenpyouCode(StrToInt(strShiiresakiCode),strNouhinDate, sNewNo);
                  if strSDenpyouCode = '' then begin
                     ListBox1.Items.Add('伝票番号が取得出来ませんでした。');
                     ListBox1.Items.Add('すべての登録処理をキャンセルします。管理者へ連絡して下さい。');
                     ListBox1.Update;
                     iRollback := True;
                     dabDenpyou.Rollback;
                     dabDenpyou.Close;
                     DeleteFile(sLockFileName);

                     Exit;
                  end;

                  sCurrentNo:=sNewNo;
				  curSum 	:= 0.0;
                  if(InserttblTShiire(slRow, strSDenpyouCode) = False) OR (strSDenpyouCode = '') then begin
                     ListBox1.Items.Add('伝票登録に失敗しました。');
                     ListBox1.Items.Add('すべての登録処理をキャンセルします。管理者へ連絡して下さい。');
                     ListBox1.Update;
                     iRollback := True;
                     dabDenpyou.Rollback;
                     dabDenpyou.Close;
                     DeleteFile(sLockFileName);
                     Exit;
                  end;

                  if DeleteFile(sLockFileName) = true then begin
                  ;
                  end else begin
 	                  ShowMessage('伝用番号のロックファイル' + sLockFileName + 'がありませんでした。伝票がきちんと登録されているかどうか確認してください');
                  end;


              end;
			  
              if(InserttblTShiireDetail(slRow, strSDenpyouCode) = False) then begin
                  ListBox1.Items.Add('伝票明細の登録に失敗しました。');
                  ListBox1.Items.Add('すべての登録処理をキャンセルします。管理者へ連絡して下さい。');
                  ListBox1.Update;
                  iRollback := True;
                  dabDenpyou.Rollback;
                  dabDenpyou.Close;
                  Exit;
              end;

           end;

         end;
			UpdateTax(curSum, strSDenpyouCode);	
    end;

    if(iRollback = True) then
     begin
        dabDenpyou.Rollback;
     end
    else if(iCommit = True) then
     begin
        dabDenpyou.Commit;
        ShowMessage('登録が完了しました。');
     end;
    		Beep();
        dabDenpyou.Close;


 except
    on E: EDBEngineError do begin
      dabDenpyou.Rollback;
   	  ShowMessage(E.Message);
      dabDenpyou.Close;
      Beep();
    end;
  end;

end;



procedure TfrmInfoBuy.BitBtn3Click(Sender: TObject);
begin
  inherited;
  TfrmListInfoBuy.Create(Self);
end;

procedure TfrmInfoBuy.Split(const Delimiter: Char;Input: string;const Strings: TStrings) ;
begin
	Assert(Assigned(Strings)) ;
	Strings.Clear;
	Strings.Delimiter := Delimiter;
	Strings.DelimitedText := Replace(Input, ' ', '') ;
end;

//登録
//tblTShiire
Function TfrmInfoBuy.InserttblTShiire(DenpyouDetail:TStringList; strSDenpyouCode:String): Boolean;
  var
  sSql : String;
  
  Ddate	: String;	
  strShiiresakiCode : String;
  sDenpyouCode : String;
  strGatsubun : String;
  strInputDate : String;
  strDdate : String;
  strDenpyouKingaku : String;
  strTax : String;
  strNyuuryokusha : String;
  strBikou : String;
  strInfoDenpyouNo : String;
begin	

	//Ddate				:= DenpyouDetail[37];  //納品日
  Ddate				:= DatetoStr(DTP1.Date);  //納品日はフォームの日付を優先する

	strShiiresakiCode 	:= DenpyouDetail[7];  //取引先コード
	sDenpyouCode 		:= strSDenpyouCode;
	strDdate          		:= Ddate;
	//DateTimeToString(strGatsubun, 'yyyy/mm/01', StrToDateTime(strDdate));
  strGatsubun := Ddate; //納品日を同じ
	DateTimeToString(strInputDate, 'yyyy/mm/dd', now);

	strDenpyouKingaku   	:= DenpyouDetail[34];  //総合計
	strTax					:= CurrToStr(curSum * CsTaxRate);
	strNyuuryokusha 		:= CBNyuuryokusha.text;
	strBikou          		:= Edit1.text;
	strInfoDenpyouNo 		:= trim(DenpyouDetail[2]); //伝票No
try
  // 更新の場合も，新規の場合もまず伝票番号のレコードをtblTDenpyouDetailから削除する．
  // その後，データを挿入する．
  sSql := 'DELETE FROM ' + CtblTShiire;
  //sSql := sSql + ' WHERE SDenpyouCode = ' + '''' + strSDenpyouCode + '''';
  sSql := sSql + ' WHERE InfoDenpyouNo = ' + '''' + strInfoDenpyouNo + '''';
  sSql := sSql + ' AND ShiiresakiCode = ' + '''' + strShiiresakiCode + '''';

  with QueryDenpyou do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    ExecSql;
    Close;
  end;
  
  // 更新の場合も，新規の場合もまず伝票番号のレコードをtblTDenpyouDetailから削除する．
	// その後，データを挿入する．
	  sSql := 'DELETE FROM ' + CtblTShiireDetail;
	  //sSql := sSql + ' WHERE SDenpyouCode = ' + '''' + sDenpyouCode + '''';
    sSql := sSql + ' WHERE InfoDenpyouNo = ' + '''' + strInfoDenpyouNo + '''';
    sSql := sSql + ' AND ShiiresakiCode = ' + '''' + strShiiresakiCode + '''';
	  with QueryDenpyou do begin
		Close;
		Sql.Clear;
		Sql.Add(sSql);
		ExecSql;
		Close;
	  end;

  //登録
  sSql := 'Insert into ' + CtblTShiire;
  sSql := sSql + '(';
  sSql := sSql + 'ShiiresakiCode,';
  sSql := sSql + 'SDenpyouCode,';
  sSql := sSql + 'Gatsubun,';
  sSql := sSql + 'InputDate,';
  sSql := sSql + 'DDate,';
  sSql := sSql + 'DenpyouKingaku,';
  sSql := sSql + 'TAX,';
  sSql := sSql + 'Memo,';
  sSql := sSql + 'Flag,';
  sSql := sSql + 'NyukinStatus,';
  sSql := sSql + 'Nyuuryokusha,';
  sSql := sSql + 'Bikou,';
  sSql := sSql + 'InfoDenpyouNo';
  sSql := sSql + ') Values (';
  sSql := sSql + strShiiresakiCode  + ',';
  sSql := sSql + '''' + sDenpyouCode + '''' + ',';
  sSql := sSql + '''' + strGatsubun + '''' + ','; //月分
  sSql := sSql + '''' + strInputDate + ''''  + ','; //入力日
  sSql := sSql + '''' + strDdate + '''' + ','; //納品日
  sSql := sSql + strDenpyouKingaku + ',';
  sSql := sSql + strTax + ',';
  sSql := sSql + ''''  + strBikou + ''',';
  //フラグ　　仕入れ=0 支払い=1
  sSql := sSql + '0' + ',';
  sSql := sSql + '0' + ',';
  sSql := sSql + ''''  + strNyuuryokusha + ''',';  //入力者
  sSql := sSql + '''''' + ',';
  sSql := sSql + '''' + strInfoDenpyouNo + '''';  //備考
  sSql := sSql + ')';

  with QueryDenpyou do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    ExecSql;
    Close;
  end;
  Result := true;

except
  on E: EDBEngineError do begin
    //dabDenpyou.Rollback;
    ShowMessage(E.Message);
    Result := False;
  end;
  end;
end;

Function TfrmInfoBuy.InserttblTShiireDetail(DenpyouDetail:TStringList; strSDenpyouCode:String) : Boolean;
  var sSql          : String;
  strCode           : TStringList;

  sDenpyouCode      : String;
  strShiiresakiCode : String;
  strNouhinDate     : String;
  strHyoujijun      : String;
  strCode1          : String;
  strCode2          : String;
  strSuuryou, strTannka, strShoukei,strUpdateDate, strBikou, strInfoDenpyouNo: String;

begin			  
	sDenpyouCode := strSDenpyouCode;
	strShiiresakiCode 	:= DenpyouDetail[7];  //取引先コード
	//strNouhinDate     := DenpyouDetail[37]; // 納品日
  strNouhinDate				:= DatetoStr(DTP1.Date);  //納品日はフォームの日付を優先する

    strHyoujijun	   :=  DenpyouDetail[14]; // 伝票明細番号
	
	strCode := TStringList.Create;
	strCode1 :='';
    strCOde2 :='';
    if DenpyouDetail[16] <> '' then begin
        Split('-', DenpyouDetail[16], strCode);    //自社管理商品コード
        strCode1          := strCode[0];
        if(strCode.Count > 1) then begin
          strCode2          := strCode[1];
        end;
    end;
	
	strSuuryou        := DenpyouDetail[22];    //数量
    strTannka         := DenpyouDetail[21];    //単価
    strShoukei        := DenpyouDetail[24];    //金額
	
    DateTimeToString(strUpdateDate, 'yyyy/mm/dd', now);
	strBikou 		  := '';	
    strInfoDenpyouNo  := trim(DenpyouDetail[2]);//伝票No
    curSum := curSum + StrToCurr(strShoukei);
  try
	// SQL文作成
	  sSql := 'INSERT INTO ' + CtblTShiireDetail;
      sSql := sSql + ' (';
      sSql := sSql + 'SDenpyouCode,';
      sSql := sSql + 'ShiiresakiCode,';
      sSql := sSql + 'NouhinDate,';
      sSql := sSql + 'NyuuryokuDate,';
      sSql := sSql + 'Hyoujijun,';
      sSql := sSql + 'Code1,';
      sSql := sSql + 'Code2,';
      sSql := sSql + 'Suuryou,';
      sSql := sSql + 'ShiireKingaku,';
      sSql := sSql + 'Shoukei,';
      sSql := sSql + 'UpdateDate,';
      sSql := sSql + 'Memo,';
      sSql := sSql + 'ShiireShiharaiFlg,';
      sSql := sSql + 'InfoDenpyouNo';
      sSql := sSql + ') VALUES ( ';
      sSql := sSql + '''' + sDenpyouCode + ''', ' ;
      sSql := sSql +        strShiiresakiCode  +   ', ' ;
      sSql := sSql + '''' + strNouhinDate + '''' + ','; //納品日
      sSql := sSql + '''' + DateToStr(Date()) + ''''  + ','; //入力日
      sSql := sSql +        strHyoujijun             + ', ' ; //表示順
      sSql := sSql + '''' + strCode1           + ''', ' ;
      sSql := sSql + '''' + strCode2           + ''', ' ;
      sSql := sSql +        strSuuryou         + ', ' ;
      sSql := sSql +        strTannka          + ', ' ;
      sSql := sSql +        strShoukei         + ', ' ;
      sSql := sSql + '''' + strUpdateDate      + ''', ' ;
      sSql := sSql + '''' + strBikou           + ''', ' ;
      sSql := sSql +        '0'                + ', ' ;
      sSql := sSql +        strInfoDenpyouNo   + ' )' ;
	  
    with QueryDenpyou do begin
  		 	  Close;
	        Sql.Clear;
  	  	  Sql.Add(sSql);
  		    ExecSql;
	        Close;
    end;
    Result := True;    
  except
    on E: EDBEngineError do begin
      dabDenpyou.Rollback;
   	  ShowMessage(E.Message);
      Result := False;
    end;
  end;
end;

Function TfrmInfoBuy.UpdateTax (curSum:Currency; strSDenpyouCode:string): Boolean;
var  
i                 : Integer;
sSql              : String;
begin
try
  if strSDenpyouCode <> '' then begin

      sSql := 'UPDATE ' + CtblTShiire;
      sSql := sSql + ' SET Tax = ' + CurrToStr(curSum * CsTaxRate);;
      sSql := sSql + ' WHERE SDenpyouCode = ' + '''' + strSDenpyouCode + '''';

      with QueryDenpyou do begin
        Close;
        Sql.Clear;
        Sql.Add(sSql);
 	      ExecSql;
        Close;
      end;

   end;
   
   Result := True;

except
  on E: EDBEngineError do begin
    dabDenpyou.Rollback;
   	ShowMessage(E.Message);
    Result := False;
  end;
end;
end;

function TfrmInfoBuy.Replace(Str, Before, After: string): string;
var
  Len, N: Integer;
begin
  Len := Length(Before) - 1;
  N := AnsiPos(Before, Str);
  while N > 0 do begin
    Result := Result + Copy(Str, 1, N - 1) + After;
    Delete(Str, 1, N + Len);
    N := AnsiPos(Before, Str)
  end;
  Result := Result + Str
end;

//仕入れ伝票番号の作成
//2006.03.19
//仕入先コード＋入荷日＋000
//001         + 060301+001 = 001060301001

function TfrmInfoBuy.MakeSDenpyouCode(iShiiresakiCode:integer; sNyuukabi:String; strInfoDenpyouNo:String):String;
var
  sYyyy, sMm, sDd, sDenpyouCode : String;
  sNum, sYyMmDd, sYyMmDd2, sShiiresakiCode, sSql, sTmp:String;
  i, inum : Integer;
  strNyuukabi: String;
begin
  sYyyy := Copy(sNyuukabi,0,4);
  sMm := Copy(sNyuukabi,6,2);
  sDd := Copy(sNyuukabi,9,2);
  strNyuukabi := sNyuukabi;
  sNyuukabi := sYyyy + sMm + sDd;



  sShiiresakiCode := IntToStr(iShiiresakiCode);
  if length(sShiiresakiCode)=1 then begin
    sShiiresakiCode := '00' + sShiiresakiCode;
  end else if length(sShiiresakiCode)=2 then begin
    sShiiresakiCode := '0' + sShiiresakiCode;
  end;

  sSql := 'Select m=MAX(SDenpyouCode) from ' + CtblTShiire;
  sSql := sSql + ' where ShiiresakiCode=' +  sShiiresakiCode;

  sSql := sSql + ' and DDate=''' +  strNyuukabi + '''';
  //sSql := sSql + ' and InfoDenpyouNo=''' +  strInfoDenpyouNo + '''';
  with QueryDenpyou do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    open;
    sTmp := FieldByName('m').asString;
    Close;
  end;
  if sTmp='' then begin
    sDenpyouCode := sShiiresakiCode + sNyuukabi + '001';
  end else begin
    sYyMmDd := Copy(sTmp, 4, 6);
    sNum    := Copy(sTmp, 12, 3);
    iNum := StrToInt(sNum) + 1;
    if iNum > 999 then begin
      ShowMessage('伝票番号がオーバーしました。管理者に連絡してください');
      exit;
    end;
    sNum := IntToStr(iNum);
    if length(sNum) = 1 then begin
      sNum := '00' + sNum;
    end else if length(sNum) = 2 then begin
      sNum := '0' + sNum;
    end;
    sDenpyouCode := sShiiresakiCode + sNyuukabi + sNum;
  end;//of if
    Result := sDenpyouCode;
end;	

end.
