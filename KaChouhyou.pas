unit KaChouhyou;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, ComCtrls, StdCtrls, Buttons, ExtCtrls, DB, DBTables,ShellAPI;

type
  TfrmKaChouhyou = class(TfrmMaster)
    Label3: TLabel;
    CBYyyy: TComboBox;
    Label4: TLabel;
    CBMm: TComboBox;
    Label5: TLabel;
    BitBtn3: TBitBtn;
    Query1: TQuery;
    QuerySum: TQuery;
    BitBtn4: TBitBtn;
    procedure BitBtn3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
  private
    { Private declarations }
    function GetGatsubunSum(sCode, sYyyy, sMm, sDd : String) : currency;
    //function GetSum(sCode, sFrom, sTo : String) : Currency;
    function GetSum(sCode, sFrom, sTo : String) : Double;
    function GetShuukeiKikan(sFrom, sTo, sCode: string) : Currency;

  public
    { Public declarations }
  end;

var
  frmKaChouhyou: TfrmKaChouhyou;

implementation

uses PasswordDlg, Inter, HKLib;

{$R *.dfm}

//当月支払い明細がクリックされた
procedure TfrmKaChouhyou.BitBtn3Click(Sender: TObject);
var
  sLine, sSql : String;
  F : TextFile;
  i : integer;
  //currSum,currSum2 : Currency;
  currSum,currSum2 : Double;

  sCode, sYyyy, sMm, sDd : String;
begin

  dlgPasswordDlg := TdlgPasswordDlg.Create(Self);
  if dlgPasswordDlg.ShowModal = mrOK then begin

  end else begin
    Exit;
  end;

  if GPassWord = CPassword1 then begin
    Beep;
  //コメントアウト 2009.04.08 utada
  //end else if GPassWord = CPassword2 then begin
    //Beep;
  //ここまで
  end else begin
   //ShowMessage('PassWordが違います');
   exit;
  end;
  GPassWord := '';

  //確認メッセージ
  if MessageDlg('月別支払一覧を出力しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;
  if MessageDlg('〆処理は行いましたか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;

  //出力するファイルを作成する、すでにあれば削除する
  AssignFile(F, CFileName_KaChouhyou1);
  Rewrite(F);
  CloseFile(F);

  //作成日
  sLine := DateToStr(Date);
  HMakeFile(CFileName_KaChouhyou1, sLine);

  //タイトルを出力する
  sLine := '仕入先コード';
  sLine := sLine + ',' + '仕入先名';
  sLine := sLine + ',' + '仕入れ金額';
  sLine := sLine + ',' + '支払い方法';
  sLine := sLine + ',' + 'サイト';
  sLine := sLine + ',' + '支払日';
  sLine := sLine + ',' + '〆日';
  //2006.09.05
  sLine := sLine + ',' + '処理月';

  HMakeFile(CFileName_KaChouhyou1, sLine);

  //すべての仕入先でループ
  sSql := 'select Name, Code, Shimebi, Shiharaibi, site, ShiharaiHouhou from ' +
           CtblMShiiresaki + ' order by Code';

  //2006.11.12
  //Yomiでソート
  sSql := 'select Name, Code, Shimebi, Shiharaibi, site, ShiharaiHouhou from ' +
           CtblMShiiresaki + ' order by Yomi';

	with Query1 do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    i := 0;
    currSum2 := 0; //総合計
    while not EOF do begin
      sYyyy := CBYyyy.text;
      sMm   := CBMm.Text;
      sDd := FieldByName('Shimebi').AsString;
      sCode := FieldByName('Code').AsString;

      if sDd = '' then begin
        //Showmessage('仕入先コード='+ sCode + 'は締め日が入力されていませんので処理を停止します');
        //exit;
      end else begin
        currSum := GetGatsubunSum(sCode, sYyyy, sMm, sDd);
        currSum2 := currSum2 + currSum;
        sLine := sCode + ',';
        sLine := sLine + FieldByName('Name').AsString + ',';
        //sLine := sLine + CurrtoStr(currSum) + ',';
        sLine := sLine + FloatToStr(currSum) + ',';
        sLine := sLine + FieldByName('ShiharaiHouHou').AsString + ',';
        sLine := sLine + FieldByName('Site').AsString + ',';
        sLine := sLine + FieldByName('Shiharaibi').AsString + ',';
        sLine := sLine + sDd;

        //2006.09.05
        sLine := sLine + ',' + sMm + '月';
        //2006.11.12
        sLine := sLine + ',' + sYyyy;

        HMakeFile(CFileName_KaChouhyou1, sLine);

        i := i + 1;
        SB1.SimpleText := IntToStr(i) + '件目処理中 ' + sLine;
      end;
      Next;
    end;//of while
    Close;
  end;//of with
  //最終行に合計を出す
  sLine := ''    + ',';
  sLine := sLine + '合計' + ',';
  sLine := sLine + CurrtoStr(currSum2) + ',';
  //sLine := sLine + FloatToStr(currSum2) + ',';
  sLine := sLine + '' + ',';
  sLine := sLine + '' + ',';
  sLine := sLine + '' + ',';
  sLine := sLine + '　';
  HMakeFile(CFileName_KaChouhyou1, sLine);

  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '月別支払一覧.xls', '', SW_SHOW);

end;

//月分の合計を出す
//sCode : 仕入先コード
// sYyyy, sMm　集計月
// sDd は、〆日
//この関数は、〆処理の後に実行される
function TfrmKaChouhyou.GetGatsubunSum(sCode, sYyyy, sMm, sDd : String) : currency;
var
 sFrom, sTo, sSql, sGetsumatsu : String;
 iYyyy, iMm, iDd : Integer;
 iYyyy2, iMm2, iDd2 : Integer;
 sYyyy2, sMm2, sDd2 : String;
 currSum : Currency;
begin
  //集計期間を求める
  iYyyy := StrToInt(sYyyy);
  iMm   := StrToInt(sMm);
  iDd   := StrToInt(sDd);

  if sDd = '0' then begin
    sGetsumatsu := IntToStr(HKLib.GetGetsumatsu(StrToInt(sYyyy), StrToInt(sMm)));
    sFrom := sYyyy + '/' + sMm + '/01';
    sTo   := sYyyy + '/' + sMm + '/' + sGetsumatsu;
    currSum := GetSum(sCode, sFrom, sTo);
  end else begin
    iDd2 := iDd + 1;
    iMm2 := iMm - 1;
    if iMm2 = 0 then begin
      iMm2 := 12;
      iYyyy2 := iYyyy-1;
    end else begin
      iYyyy2 := iYyyy;
    end;
    sFrom := IntToStr(iYyyy2) + '/' + IntToStr(iMm2) + '/' + IntToStr(iDd2);
    sTo := IntToStr(iYyyy) + '/' + IntToStr(iMm) + '/' + IntToStr(iDd);
    currSum := GetSum(sCode, sFrom, sTo);
  end;
  result := currSum;
end;

function  TfrmKaChouhyou.GetSum(sCode, sFrom, sTo : String) : Double;
var
  sSql : String;
  currSum : Currency;
  // iSum : integer;
  iSum : Double;
begin
  sSql := 'Select s=sum(Shoukei) from ' + CtblTShiireDetail;
  sSql := sSql + ' where ';
  sSql := sSql + ' ShiiresakiCode=''' + sCode +  '''';
  sSql := sSql + ' and ';
  sSql := sSql + ' NouhinDate between ''' + sFrom + ''' and ''' + sTo + '''';
  sSql := sSql + ' and ';
  //2006.11.13
  // 消費税の調整も合計に加える
  //sSql := sSql + ' (ShiireShiharaiFlg = 0 or ShiireShiharaiFlg = 9)';
  sSql := sSql + ' (ShiireShiharaiFlg = 0 or ShiireShiharaiFlg = 9 or ShiireShiharaiFlg = 3)';

  //2007.01.31
  //大幅に変更
  sSql := 'Select Suuryou from ' + CtblTShiireDetail;
  sSql := sSql + ' where ';
  sSql := sSql + ' ShiiresakiCode=''' + sCode +  '''';
  sSql := sSql + ' and ';
  sSql := sSql + ' NouhinDate between ''' + sFrom + ''' and ''' + sTo + '''';
  sSql := sSql + ' and ';
  sSql := sSql + ' ShiireShiharaiFlg = 9 ';

  with QuerySum do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    //iSum := FieldByName('Suuryou').asInteger;
    iSum := FieldByName('Suuryou').asFloat;
    Close;
  end;
  //result := currSum;
  result := iSum;
end;

procedure TfrmKaChouhyou.FormCreate(Sender: TObject);
begin
  Top := 20;

  width  := 500;
  height := 230;
end;

{

  サイトで並び替える
　締め日で並び替える


　　　　　　支払日,サイト,仕入れ先名,月分,金額

  手形は、すべて月末締めという前提

}
procedure TfrmKaChouhyou.BitBtn4Click(Sender: TObject);
var
  sSql, sLine, sCode, sSite, sShimebi, sShiharaibi, sName : String;
  sShiharaibi2 : String;
  i, iGetsumatsu, iMm : Integer;
  sYyyy, sMm, sDd : String;
  sFrom, sTo, sGatsubun, sKingaku : String;
  dFrom, dTo, dShiharaibi : TDate;
  F : TextFile;
  currSum, currSum2 : Currency;
  wYyyy, wMm, wDd      : Word;
  sHeisei, sYear, sMonth, sDay  : String;
  sShimebiGatsubun : String; // Add utada
begin

  //出力するファイルを作成する、すでにあれば削除する
  AssignFile(F, CFileName_KaChouhyou2);
  Rewrite(F);
  CloseFile(F);

  //2006.11.25
  //sHeisei := IntToStr(StrToInt(CBYyyy.Text) - 1989);
  //2007.02.13
  sHeisei := IntToStr(StrToInt(CBYyyy.Text) - 1988);

  //2007.04.07
  //sLine := '平成' + sHeisei + '年' + CBMm.Text + '月手形支払い明細';
  sLine := '平成' + sHeisei + '年' + CBMm.Text + '月手形サイトカレンダー';

  HMakeFile(CFileName_KaChouhyou2, sLine);

  sLine := '　' + ',';
  HMakeFile(CFileName_KaChouhyou2, sLine);

  sLine := '〆日' + ',' + 'サイト' + ',' + '仕入先名' + ',' + '月分' + ',' +'金額';
  sLine := '支払日' + ',' + 'サイト' + ',' + '仕入先名' + ',' + '月分' + ',' +'金額'+ ',' +'〆日'+ ',' +'期間';
  HMakeFile(CFileName_KaChouhyou2, sLine);

  //支払い方法が手形のもののみ対象とする
  //Key1 支払日
  //Key2 サイト
  //Key3 仕入先コード
  sSql := 'select Name, Code, Shimebi, Shiharaibi, site, ShiharaiHouhou from ' +
           CtblMShiiresaki +
           ' where ShiharaiHouhou=''手形'' ' +
           //2006.12.26
           //' order by Shiharaibi, Site, Code';
           ' order by Shiharaibi, Site, Yomi';
  with Query1 do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    i := 0;
    currSum  := 0; //小計
    currSum2 := 0; //合計
    sShiharaibi2 := '0';
    while not EOF do begin
      sYyyy := CBYyyy.Text;
      sMm   := CBMm.Text;
      sShimebi := FieldByName('shimebi').asString;
      sShiharaibi := FieldByName('Shiharaibi').asString;
      if sShiharaibi <> sShiharaibi2 then begin
        sShiharaibi2 := sShiharaibi;
        sLine := '小計' + ',,,,' + CurrToStr(currSum);
        HMakeFile(CFileName_KaChouhyou2, sLine);
        sLine := '-----,-----,-----,-----,-----';
        HMakeFile(CFileName_KaChouhyou2, sLine);
        currSum  := 0; //小計
      end;
      sName    := FieldByName('Name').asString;
      sSite    := FieldByName('site').asString;
      sCode    := FieldByName('Code').asString;
      if sSite = '' then begin
        ShowMessage(sName + 'のサイトが入力されていません');
        next;
        Continue;
      end;


      if sShiharaibi = '0' then begin //月末
        iGetsumatsu := GetGetsumatsu(StrToInt(sYyyy), StrToInt(sMm));
        //sDay := IntToStr(iGetsumatsu); //支払日
        sDd  := IntToStr(iGetsumatsu); //支払日
      end else begin
        //sDay := sShiharaibi;
         sDd := sShiharaibi;
{
        // test
        if sShiharaibi = '25' then begin
            sDd := sShiharaibi;
        end else begin
          sDd  := IntToStr(iGetsumatsu); //支払日
        end;
}
      end;

      if sShimebi = '0' then begin //月末
        iGetsumatsu := GetGetsumatsu(StrToInt(sYyyy), StrToInt(sMm));
        sShimebi := IntToStr(iGetsumatsu); //締め日
        sShimebiGatsubun := '0';
      end else begin
        sShimebiGatsubun := sShimebi;
      end;

      //月分を計算する
      //その月の支払日からサイトを引く
      //2006.12.26
      //支払日からサイトを引く
      sDay := sYyyy + '/' + sMm + '/' + sDd;
      {
      DecodeDate(StrToDate(sShiharaibi), wYyyy, wMm, wDd);
      sYear  := IntToStr(wYyyy);
      sMonth := IntToStr(wMm);
      sDay   := IntToStr(wDd);
      }
      dShiharaibi := StrToDate(sDay);

      //2006.12.26
      //　1月の場合は、31日が12月と1月と二つ続くので
      if (sMm = '1' ) or (sMm = '8') then begin
        dTo := dShiharaibi - StrToint(sSite) -2;
      end else begin
        //サイトが90日の場合は、31日の月が2回入る
        if StrToint(sSite) > 60 then begin
          dTo := dShiharaibi - StrToint(sSite) -2;
        end else begin
          dTo := dShiharaibi - StrToint(sSite) -1;
        end;
      end;

      sTo := DateToStr(dTo);
      sGatsubun := Copy(sTo, 6,2);

      DecodeDate(StrToDate(sTo), wYyyy, wMm, wDd);
      sYyyy := IntToStr(wYyyy);
      sMm   := IntToStr(wMm);
      iGetsumatsu := GetGetsumatsu(StrToInt(sYyyy), StrToInt(sMm));

      //期間
      if StrtoInt(sShimebi)  >= 28 then begin //月末
        sTo := sYyyy + '/' + sMm + '/' + IntToStr(iGetsumatsu);
        sFrom := sYyyy + '/' + sMm + '/' + '01';
      end else begin
        sTo := sYyyy + '/' + sMm + '/' + sShimebi;
        //2007.02.12
        iMm := StrToInt(sMm)-1;
        if iMm = 0 then begin
         sYyyy := IntToStr(StrToInt(sYyyy)-1);
         iMm := 12;
        end;
        sFrom := sYyyy + '/' + IntToStr(iMm) + '/' + IntToStr(StrToInt(sShimebi)+1);
      end;

      //請求金額を求める
      sKingaku := CurrToStr(GetShuukeiKikan(sFrom, sTo, sCode));
      currSum  := currSum  + StrToCurr(sKingaku);
      currSum2 := currSum2 + StrToCurr(sKingaku);


      // Add utada しめ日は月分で出す
      if sShimebiGatsubun = '0' then begin
        sShimebiGatsubun := IntToStr(GetGetsumatsu(StrToInt(sYyyy), StrToInt(sGatsubun)));
      end;

      //支払日,サイト,仕入れ先名,月分,金額
      if sShiharaibi = '0' then begin
        //sLine := '月末' + ',' + sSite + ',' + sName + ',' + sGatsubun + ',' +sKingaku + ',' + sShimebi+ ',' + sFrom + '-' + sTo;
        sLine := '月末' + ',' + sSite + ',' + sName + ',' + sGatsubun + ',' +sKingaku + ',' + sShimebiGatsubun + ',' + sFrom + '-' + sTo;
      end else begin
        //sLine := sShiharaibi + '日' + ',' + sSite + ',' + sName + ',' + sGatsubun + ',' +sKingaku + ',' + sShimebi + ',' +sFrom + '-' + sTo;
        sLine := sShiharaibi + '日' + ',' + sSite + ',' + sName + ',' + sGatsubun + ',' +sKingaku + ',' + sShimebiGatsubun + ',' +sFrom + '-' + sTo;
      end;

      HMakeFile(CFileName_KaChouhyou2, sLine);

      i := i + 1;
      SB1.SimpleText := IntToStr(i) + '件目処理中 ' + sLine;
      Next;
    end;//of while
    Close;
  end;//of with

  sLine := '小計' + ',,,,' + CurrToStr(currSum);
  HMakeFile(CFileName_KaChouhyou2, sLine);
  sLine := '-----,-----,-----,-----,-----';
  HMakeFile(CFileName_KaChouhyou2, sLine);
  sLine := '合計' + ',,,,' + CurrToStr(currSum2);
  HMakeFile(CFileName_KaChouhyou2, sLine);

  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '手形サイトカレンダー.xls', '', SW_SHOW);


end;

//集計
//2007.02.15
//ロジックを変更する
Function TfrmKaChouhyou.GetShuukeiKikan(sFrom, sTo, sCode: string):Currency;
var
 sSql : String;
 sSql2 : String;
 currSum : Currency;
 currSum2 : Currency;
begin
  sSql := 'Select s=Sum(Shoukei) From ' + CtblTShiireDetail;
  sSql := sSql + ' where ';
  sSql := sSql + ' ShiiresakiCode = ' + sCode ;
  sSql := sSql + ' and ';
  sSql := sSql + ' NouhinDate between ''' + sFrom + '''' ;
  sSql := sSql + ' and ';
  sSql := sSql + '''' + sTo + '''';
  //2006.09.16
  sSql := sSql + ' and ';
  sSql := sSql + ' ShiireShiharaiFlg<>1'; //支払い以外

  with QuerySum do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    currSum := FieldByName('s').asCurrency;
    Close;
  end;
  //result := currSum;

  sSql2 := 'SELECT * FROM ' + CtblTShiireDetail;
  sSql2 := sSql2 + ' WHERE ';
  sSql2 := sSql2 + ' (ShiiresakiCode = ' + sCode + ')';
  sSql2 := sSql2 + ' AND ';
  sSql2 := sSql2 + ' (ShiireShiharaiFlg = 9) ';
  sSql2 := sSql2 + ' AND ';
  sSql2 := sSql2 + ' (NouhinDate = ''' + sTo + ''')';

  with QuerySum do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql2);
    Open;
    currSum2 := FieldByName('suuryou').asCurrency;
      Close;
  end;
  if currSum = currSum2 then begin
    result := currSum;
  end else begin
    result := currSum2;
  end;
end;

end.
