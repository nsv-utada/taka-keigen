unit ShiireKakakuRireki;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, Grids, DBGrids, ComCtrls, StdCtrls, Buttons, ExtCtrls,
  DB, DBTables, DBCtrls;

type
  TfrmShiireKakakuRireki = class(TfrmMaster)
    DBGrid1: TDBGrid;
    Query1: TQuery;
    DS1: TDataSource;
    DBNavigator1: TDBNavigator;
    Query1ID: TIntegerField;
    Query1Code1: TStringField;
    Query1Code2: TStringField;
    Query1Setteibi: TDateTimeField;
    Query1kakaku: TFloatField;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmShiireKakakuRireki: TfrmShiireKakakuRireki;

implementation
uses
 Inter;
{$R *.dfm}

procedure TfrmShiireKakakuRireki.FormActivate(Sender: TObject);
var
  sSql : String;
begin
  sSql := 'Select * from ' + CtblMShiireKakakuRireki;
  sSql := sSql + ' where ';
  sSql := sSql + ' Code1= ''' + GCode1 + '''';
  sSql := sSql + ' and ';
  sSql := sSql + ' Code2= ''' + GCode2 + '''';
  with query1 do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
  end;

end;

procedure TfrmShiireKakakuRireki.FormCreate(Sender: TObject);
begin
  query1.Close;
  Width := 380;
  Height := 370;
end;

procedure TfrmShiireKakakuRireki.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Query1.Close;
end;

end.
