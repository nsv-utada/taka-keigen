inherited frmMItem: TfrmMItem
  Left = 628
  Top = 46
  Width = 649
  Height = 690
  Caption = 'frmMItem'
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  object Label7: TLabel [0]
    Left = 30
    Top = 37
    Width = 24
    Height = 12
    Caption = #12424#12415
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label9: TLabel [1]
    Left = 34
    Top = 191
    Width = 26
    Height = 12
    Caption = #35215#26684
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label36: TLabel [2]
    Left = 224
    Top = 31
    Width = 82
    Height = 12
    Caption = #26368#20302#20986#33655#12525#12483#12488
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = [fsBold]
    ParentFont = False
  end
  inherited Panel1: TPanel
    Width = 641
    inherited Label1: TLabel
      Width = 131
      Caption = #21830#21697#32207#12510#12473#12479#12540
    end
  end
  inherited Panel2: TPanel
    Top = 596
    Width = 641
    inherited BitBtn1: TBitBtn
      Left = 474
    end
    inherited BitBtn2: TBitBtn
      Left = 10
      Top = 10
      OnClick = BitBtn2Click
    end
    object BitBtn3: TBitBtn
      Left = 374
      Top = 8
      Width = 91
      Height = 25
      Caption = #30331#12288#37682
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = BitBtn3Click
      Kind = bkOK
    end
    object BitBtn4: TBitBtn
      Left = 278
      Top = 8
      Width = 91
      Height = 25
      Hint = #26032#35215#30331#37682#12398#22580#21512#12399#12289#20808#12395#12371#12398#12508#12479#12531#12434#12463#12522#12483#12463#12375#12390#30011#38754#12434#21021#26399#21270#12375#12390#12367#12384#12373#12356#12290
      Caption = #30011#38754#12463#12522#12450
      Default = True
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ModalResult = 1
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = BitBtn4Click
      NumGlyphs = 2
    end
    object BitBtn5: TBitBtn
      Left = 198
      Top = 8
      Width = 75
      Height = 25
      Caption = #21066#12288#38500
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 4
      OnClick = BitBtn5Click
    end
  end
  inherited Panel3: TPanel
    Width = 641
    Height = 400
    Align = alTop
    Font.Color = clNavy
    Font.Style = [fsBold]
    ParentFont = False
    object Label3: TLabel
      Left = 30
      Top = 16
      Width = 76
      Height = 12
      Caption = #21830#21697#12467#12540#12489'(1)'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 436
      Top = 16
      Width = 26
      Height = 12
      Caption = #21336#20301
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 30
      Top = 40
      Width = 63
      Height = 12
      Caption = #21830#21697#21517'(26)'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label12: TLabel
      Left = 36
      Top = 93
      Width = 43
      Height = 12
      Caption = #35215#26684'(8)'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 32
      Top = 67
      Width = 55
      Height = 12
      Caption = #12424#12415'(200)'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 36
      Top = 117
      Width = 37
      Height = 12
      Caption = #12465#12540#12473
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 36
      Top = 140
      Width = 41
      Height = 13
      Caption = #26842#24403#12390
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 36
      Top = 188
      Width = 47
      Height = 12
      Caption = #21336#20385'420'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label13: TLabel
      Left = 36
      Top = 235
      Width = 26
      Height = 12
      Caption = #21407#20385
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = Label13Click
    end
    object Label14: TLabel
      Left = 242
      Top = 97
      Width = 47
      Height = 12
      Caption = #12513#12540#12459#12540
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 242
      Top = 123
      Width = 39
      Height = 12
      Caption = #20181#20837#20808
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label16: TLabel
      Left = 242
      Top = 148
      Width = 43
      Height = 12
      Caption = #35215#26684'(8)'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label17: TLabel
      Left = 36
      Top = 211
      Width = 47
      Height = 12
      Caption = #21336#20385'450'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label19: TLabel
      Left = 36
      Top = 164
      Width = 28
      Height = 13
      Caption = #22580#25152
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label18: TLabel
      Left = 288
      Top = 16
      Width = 76
      Height = 12
      Caption = #21830#21697#12467#12540#12489'(2)'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clNavy
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label20: TLabel
      Left = 242
      Top = 172
      Width = 57
      Height = 12
      Caption = #20633#32771'(250)'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label22: TLabel
      Left = 208
      Top = 317
      Width = 74
      Height = 12
      Caption = '('#12481#12455#12540#12531#30058#21495')'
      Enabled = False
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ParentFont = False
    end
    object Label23: TLabel
      Left = 210
      Top = 345
      Width = 68
      Height = 12
      Caption = '('#24471#24847#20808#30058#21495')'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ParentFont = False
    end
    object LabelSID: TLabel
      Left = 370
      Top = 40
      Width = 5
      Height = 12
    end
    object Label24: TLabel
      Left = 36
      Top = 263
      Width = 48
      Height = 12
      Caption = #21830#21697#12513#12514
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label33: TLabel
      Left = 324
      Top = 220
      Width = 56
      Height = 13
      Caption = #20181#20837#20385#26684
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label37: TLabel
      Left = 324
      Top = 244
      Width = 98
      Height = 13
      Caption = #20181#20837#20385#26684#35373#23450#26085
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label39: TLabel
      Left = 324
      Top = 268
      Width = 84
      Height = 13
      Caption = #20181#20837#20385#26684#23653#27508
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label45: TLabel
      Left = 36
      Top = 287
      Width = 26
      Height = 12
      Caption = #26465#20214
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object CBCode1: TComboBox
      Left = 114
      Top = 10
      Width = 163
      Height = 21
      DropDownCount = 20
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imClose
      ItemHeight = 13
      ParentFont = False
      TabOrder = 0
      OnExit = CBCode1Exit
      OnKeyDown = CBCode1KeyDown
      Items.Strings = (
        '01,'#26989#21209#29992#39135#26448
        '02,'#35519#21619#26009#35519#21619#39135#21697
        '03,'#39321#36763#26009
        '04,'#12499#12531#12289#32566#35440#39006
        '05,'#12467#12540#12498#12540#12289#21931#33590#26448#26009
        '06,'#20083#35069#21697#65288#12481#12540#12474#39006#65289
        '07,'#39135#29992#27833
        '08,'#31881#35069#21697
        '09,'#12418#12385
        '10,'#12473#12497#40634
        '11,'#40634
        '12,'#31859
        '13,'#20013#33775#26448#26009
        '14,'#21508#31278#12472#12517#12540#12473
        '15,'#12362#33590
        '16,'#27703#12471#12525#12483#12503
        '17,'#28460#29289#24803#33756
        '18,'#12362#36890#12375#29289
        '19,'#39135#32905#21152#24037#21697
        '20,'#28023#29987#29645#21619
        '21,'#20919#20941#39135#21697#39770#20171#39006
        '22,'#20919#20941#39135#21697#36786#29987#29289
        '23,'#20919#20941#39135#21697#19968#27425#21152#24037#21697
        '24,'#20919#20941#39135#21697#28857#24515#39006
        '25,'#12486#12522#12540#12492#39006
        '26,'#12524#12488#12523#12488#39006#28271#29006
        '27,'#20919#20941#35201#20919#12289#39321#36763#26009
        '28,'#12362#12388#12414#12415#35910#39006
        '29,'#12362#12388#12414#12415#12362#12363#12365#39006
        '30,'#12362#12388#12414#12415#12481#12483#12503#39006
        '31,'#12481#12519#12467#12524#12540#12488#12289#12476#12522#12540#39006
        '32,'#28023#29987#29289
        '33,'#24178#12375#32905
        '90,'#29305#27530#12467#12540#12489#13)
    end
    object CBTanni: TComboBox
      Left = 470
      Top = 10
      Width = 81
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imClose
      ItemHeight = 13
      ParentFont = False
      TabOrder = 1
      Items.Strings = (
        #30002
        #32566
        #26522
        #26412
        #34955
        #65419#65438#65437
        #65418#65439#65391#65400
        #65401#65392#65405
        #65408#65394
        #65402)
    end
    object CBName: TComboBox
      Left = 114
      Top = 35
      Width = 250
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imOpen
      ItemHeight = 13
      ParentFont = False
      TabOrder = 2
      OnExit = CBNameExit
      OnKeyDown = CBNameKeyDown
    end
    object CBIrisuu: TComboBox
      Left = 112
      Top = 89
      Width = 119
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 4
    end
    object CBYomi: TComboBox
      Left = 114
      Top = 62
      Width = 250
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imHira
      ItemHeight = 13
      ParentFont = False
      TabOrder = 3
      OnKeyDown = CBYomiKeyDown
    end
    object CBF1: TComboBox
      Left = 112
      Top = 138
      Width = 77
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 5
      Items.Strings = (
        '1F'
        '2F'
        'S1'
        'S2'
        'S3'
        'S4'
        'RZ'
        'R1'
        'R2')
    end
    object EditTanka420: TEdit
      Left = 112
      Top = 184
      Width = 73
      Height = 20
      ImeMode = imClose
      TabOrder = 6
      Text = '0'
      OnEnter = EditTanka420Enter
    end
    object EditGenka: TEdit
      Left = 112
      Top = 232
      Width = 73
      Height = 20
      ImeMode = imClose
      TabOrder = 8
      Text = '0'
      Visible = False
    end
    object CBMaker: TComboBox
      Left = 310
      Top = 91
      Width = 179
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 9
    end
    object CBShiiresaki: TComboBox
      Left = 310
      Top = 116
      Width = 229
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 10
      Items.Strings = (
        '5  '#26085
        '10 '#26085
        '15 '#26085
        '20 '#26085
        '25 '#26085
        '0  '#26376#26411
        '')
    end
    object CBKikaku: TComboBox
      Left = 310
      Top = 142
      Width = 77
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 11
      Visible = False
      Items.Strings = (
        #24179
        #20845#12388#25240
        #26354#12427
        #34180#21475
        #12502#12523#12540)
    end
    object EditTanka450: TEdit
      Left = 112
      Top = 205
      Width = 73
      Height = 20
      ImeMode = imClose
      TabOrder = 7
      Text = '0'
      OnEnter = EditTanka450Enter
    end
    object CBF2: TComboBox
      Left = 112
      Top = 161
      Width = 77
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 12
      Items.Strings = (
        '01'
        '02'
        '03'
        '04'
        '05'
        '06'
        '07'
        '08'
        '09'
        '10'
        '11'
        '12'
        '13'
        '14'
        '15'
        '16'
        '17')
    end
    object CBCode2: TComboBox
      Left = 370
      Top = 10
      Width = 61
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imClose
      ItemHeight = 13
      ParentFont = False
      TabOrder = 13
      OnKeyDown = CBCode2KeyDown
      OnKeyPress = CBCode2KeyPress
    end
    object RBSubete: TRadioButton
      Left = 34
      Top = 372
      Width = 89
      Height = 17
      Alignment = taLeftJustify
      BiDiMode = bdRightToLeft
      Caption = #12377#12409#12390
      Enabled = False
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clOlive
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentBiDiMode = False
      ParentFont = False
      TabOrder = 14
    end
    object RBChain: TRadioButton
      Left = 34
      Top = 314
      Width = 87
      Height = 17
      Alignment = taLeftJustify
      BiDiMode = bdRightToLeft
      Caption = #12481#12455#12540#12531
      Enabled = False
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clPurple
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentBiDiMode = False
      ParentFont = False
      TabOrder = 15
    end
    object RB420: TRadioButton
      Left = 132
      Top = 372
      Width = 61
      Height = 17
      Alignment = taLeftJustify
      BiDiMode = bdRightToLeft
      Caption = 'S(420)'
      Enabled = False
      ParentBiDiMode = False
      TabOrder = 16
    end
    object RB450: TRadioButton
      Left = 206
      Top = 372
      Width = 67
      Height = 17
      Alignment = taLeftJustify
      BiDiMode = bdRightToLeft
      Caption = 'L(450)'
      Enabled = False
      ParentBiDiMode = False
      TabOrder = 17
    end
    object RBKoko: TRadioButton
      Left = 34
      Top = 344
      Width = 87
      Height = 17
      Alignment = taLeftJustify
      BiDiMode = bdRightToLeft
      Caption = #20491#12293
      Enabled = False
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clTeal
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentBiDiMode = False
      ParentFont = False
      TabOrder = 18
    end
    object EditChainCode: TEdit
      Left = 130
      Top = 312
      Width = 73
      Height = 20
      Enabled = False
      ImeMode = imClose
      TabOrder = 19
    end
    object Edit2: TEdit
      Left = 130
      Top = 342
      Width = 73
      Height = 20
      TabOrder = 20
    end
    object Button1: TButton
      Left = 416
      Top = 40
      Width = 65
      Height = 17
      Caption = 'ID Reset'
      TabOrder = 21
      OnClick = Button1Click
    end
    object EditMemo: TComboBox
      Left = 112
      Top = 257
      Width = 169
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 22
      Items.Strings = (
        #29305#27880
        #20837#33655#27425#31532
        #22312#24235#30906#35469
        #29305#27880'A'
        #29305#27880'B')
    end
    object EShiireKakaku: TEdit
      Left = 432
      Top = 216
      Width = 73
      Height = 20
      ImeMode = imClose
      TabOrder = 23
      Text = '0'
      Visible = False
    end
    object DTPSetteibi: TDateTimePicker
      Left = 432
      Top = 240
      Width = 105
      Height = 20
      Date = 38746.764935254630000000
      Time = 38746.764935254630000000
      TabOrder = 24
      Visible = False
    end
    object MShiireKakakuRireki: TMemo
      Left = 432
      Top = 266
      Width = 169
      Height = 79
      TabOrder = 25
      Visible = False
      OnDblClick = MShiireKakakuRirekiDblClick
    end
    object CBBunrui: TMemo
      Left = 310
      Top = 165
      Width = 297
      Height = 44
      TabOrder = 26
    end
    object CBJouken: TComboBox
      Left = 112
      Top = 282
      Width = 127
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imOpen
      ItemHeight = 13
      ParentFont = False
      TabOrder = 27
      Items.Strings = (
        '20'#23550'1'
        '10'#23550'1'
        '5'#23550'1'
        #12465#12540#12473#20516#24341#12365#26377#12426
        #21336#20385#20516#24341#12365#26377#12426
        #38928#12363#12426#20998#12424#12426
        #12465#12540#12473#21336#20385
        #12496#12521#21336#20385
        #36865#26009#30906#35469
        #36865#26009#26377#12426)
    end
    object DelFlag: TCheckBox
      Left = 324
      Top = 371
      Width = 79
      Height = 17
      Alignment = taLeftJustify
      Caption = #21066#38500#12501#12521#12464
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 28
    end
  end
  inherited SB1: TStatusBar
    Top = 637
    Width = 641
    SimplePanel = True
  end
  object CBCase1: TComboBox
    Left = 112
    Top = 153
    Width = 77
    Height = 21
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = []
    ItemHeight = 13
    ParentFont = False
    TabOrder = 4
    Items.Strings = (
      '')
  end
  object Panel4: TPanel
    Left = 0
    Top = 441
    Width = 641
    Height = 155
    Align = alClient
    Caption = 'Panel4'
    TabOrder = 5
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 639
      Height = 153
      ActivePage = TabSheet3
      Align = alClient
      TabOrder = 0
      object tsOffice: TTabSheet
        Caption = #24773#22577
        object Label28: TLabel
          Left = 22
          Top = 6
          Width = 52
          Height = 12
          Caption = #36062#21619#26399#38480
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label29: TLabel
          Left = 22
          Top = 31
          Width = 37
          Height = 12
          Caption = #12469#12452#12474
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label30: TLabel
          Left = 22
          Top = 55
          Width = 52
          Height = 12
          Caption = #32013#20837#26085#25968
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label32: TLabel
          Left = 224
          Top = 7
          Width = 26
          Height = 12
          Caption = #36865#26009
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label21: TLabel
          Left = 224
          Top = 31
          Width = 82
          Height = 12
          Caption = #26368#20302#20986#33655#12525#12483#12488
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label25: TLabel
          Left = 224
          Top = 55
          Width = 52
          Height = 12
          Caption = #20986#33655#24418#24907
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label26: TLabel
          Left = 22
          Top = 79
          Width = 77
          Height = 12
          Caption = #20182#65288#26465#20214#12394#12393#65289
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object NounyuuHiduke: TComboBox
          Left = 104
          Top = 51
          Width = 97
          Height = 21
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = []
          ImeMode = imOpen
          ItemHeight = 13
          ParentFont = False
          TabOrder = 3
          Text = #24403#26085
          Items.Strings = (
            #24403#26085
            #65297#26085
            #65298#26085
            #65299#26085
            #65300#26085#20197#19978)
        end
        object Size: TComboBox
          Left = 104
          Top = 27
          Width = 97
          Height = 21
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = []
          ImeMode = imOpen
          ItemHeight = 0
          ParentFont = False
          TabOrder = 1
        end
        object ShoumiKigen: TComboBox
          Left = 104
          Top = 2
          Width = 97
          Height = 21
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = []
          ImeMode = imClose
          ItemHeight = 0
          ParentFont = False
          TabOrder = 0
        end
        object Souryou: TComboBox
          Left = 314
          Top = 2
          Width = 145
          Height = 21
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = []
          ImeMode = imOpen
          ItemHeight = 13
          ParentFont = False
          TabOrder = 2
          Text = #12394#12375
          Items.Strings = (
            #12394#12375
            #12354#12426#65288#26376#65289
            #12354#12426#65288#28779#65289
            #12354#12426#65288#27700#65289
            #12354#12426#65288#26408#65289
            #12354#12426#65288#37329#65289)
        end
        object ShukkaKeitai: TComboBox
          Left = 314
          Top = 50
          Width = 119
          Height = 21
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = []
          ImeMode = imOpen
          ItemHeight = 13
          ParentFont = False
          TabOrder = 4
          Text = '1.'#33258#31038#20415
          Items.Strings = (
            '1.'#33258#31038#20415
            '2.'#28151#36617
            '3.'#12513#12540#12459#12540#30452#36865)
        end
        object SaiteiShukkaRotto: TEdit
          Left = 314
          Top = 27
          Width = 55
          Height = 20
          TabOrder = 5
          Text = '0'
        end
        object SaiteiShukkaTani: TComboBox
          Left = 374
          Top = 26
          Width = 81
          Height = 21
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = []
          ImeMode = imClose
          ItemHeight = 13
          ParentFont = False
          TabOrder = 6
          Items.Strings = (
            #30002
            #32566
            #26522
            #26412
            #34955
            #65419#65438#65437
            #65418#65439#65391#65400
            #65401#65392#65405
            #65408#65394
            #65402)
        end
        object TaJouken: TMemo
          Left = 104
          Top = 77
          Width = 457
          Height = 51
          TabOrder = 7
        end
      end
      object tsHome: TTabSheet
        Caption = #29305#27880#24773#22577
        object Label34: TLabel
          Left = 22
          Top = 6
          Width = 33
          Height = 12
          Caption = #12467#12540#12489
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label35: TLabel
          Left = 22
          Top = 31
          Width = 26
          Height = 12
          Caption = #26085#20184
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label38: TLabel
          Left = 240
          Top = 30
          Width = 39
          Height = 12
          Caption = #25285#24403#32773
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label43: TLabel
          Left = 240
          Top = 6
          Width = 26
          Height = 12
          Caption = #24215#21517
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label27: TLabel
          Left = 22
          Top = 55
          Width = 26
          Height = 12
          Caption = #20633#32771
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object TCode: TComboBox
          Left = 104
          Top = 2
          Width = 85
          Height = 21
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = []
          ImeMode = imClose
          ItemHeight = 0
          ParentFont = False
          TabOrder = 0
        end
        object TTantousha: TComboBox
          Left = 298
          Top = 26
          Width = 145
          Height = 21
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = []
          ImeMode = imOpen
          ItemHeight = 13
          ParentFont = False
          TabOrder = 2
          Items.Strings = (
            #21697#24029#21306
            #28207#21306
            #24029#23822#24066
            #27178#27996#24066)
        end
        object TTenmei: TEdit
          Left = 298
          Top = 2
          Width = 301
          Height = 20
          ImeMode = imOpen
          TabOrder = 1
        end
        object TDate: TDateTimePicker
          Left = 104
          Top = 28
          Width = 105
          Height = 20
          Date = 38746.764935254630000000
          Time = 38746.764935254630000000
          TabOrder = 3
        end
        object TBikou: TMemo
          Left = 104
          Top = 56
          Width = 505
          Height = 73
          Lines.Strings = (
            ''
            '')
          TabOrder = 4
        end
      end
      object TabSheet1: TTabSheet
        Caption = #20181#20837#12428#12510#12473#12479#12540#12363#12425#12398#12522#12531#12463
        ImageIndex = 3
        object Label31: TLabel
          Left = 24
          Top = 23
          Width = 52
          Height = 12
          Caption = #32224#12417#26178#38291
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label40: TLabel
          Left = 24
          Top = 47
          Width = 52
          Height = 12
          Caption = #32013#20837#26085#25968
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label41: TLabel
          Left = 24
          Top = 71
          Width = 78
          Height = 12
          Caption = #26332#26085#25351#23450#32013#21697
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label42: TLabel
          Left = 192
          Top = 23
          Width = 82
          Height = 12
          Caption = #26368#20302#20986#33655#12525#12483#12488
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label44: TLabel
          Left = 192
          Top = 47
          Width = 52
          Height = 12
          Caption = #20986#33655#24418#24907
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object SShimejikan: TLabel
          Left = 115
          Top = 24
          Width = 36
          Height = 13
          Caption = '10:00'
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object SNounyuuNissuu: TLabel
          Left = 115
          Top = 48
          Width = 28
          Height = 13
          Caption = #24403#26085
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object sYoubiShitei: TLabel
          Left = 115
          Top = 70
          Width = 48
          Height = 13
          Caption = #12354#12426'('#26376')'
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object SSaiteiShukkarotto: TLabel
          Left = 288
          Top = 24
          Width = 35
          Height = 13
          Caption = '12 '#32566
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object SShukkakeitai: TLabel
          Left = 288
          Top = 48
          Width = 75
          Height = 13
          Caption = #12513#12540#12459#12540#30452#36865
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object TabSheet2: TTabSheet
        Caption = #20104#20633'('#25991#23383')'
        ImageIndex = 4
        object Label53: TLabel
          Left = 18
          Top = 14
          Width = 62
          Height = 11
          Caption = #20104#20633#65298'(400)'
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label54: TLabel
          Left = 18
          Top = 54
          Width = 62
          Height = 11
          Caption = #20104#20633#65300'(400)'
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label55: TLabel
          Left = 18
          Top = 34
          Width = 62
          Height = 11
          Caption = #20104#20633#65299'(400)'
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label61: TLabel
          Left = 18
          Top = 74
          Width = 62
          Height = 11
          Caption = #20104#20633#65301'(400)'
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object YobiStr1: TComboBox
          Left = 100
          Top = 10
          Width = 577
          Height = 19
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ImeMode = imClose
          ItemHeight = 0
          ParentFont = False
          TabOrder = 0
        end
        object YobiStr3: TComboBox
          Left = 100
          Top = 50
          Width = 577
          Height = 19
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ImeMode = imClose
          ItemHeight = 0
          ParentFont = False
          TabOrder = 2
        end
        object YobiStr2: TComboBox
          Left = 100
          Top = 30
          Width = 577
          Height = 19
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ImeMode = imClose
          ItemHeight = 0
          ParentFont = False
          TabOrder = 1
        end
        object YobiStr4: TComboBox
          Left = 100
          Top = 70
          Width = 577
          Height = 19
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ImeMode = imClose
          ItemHeight = 0
          ParentFont = False
          TabOrder = 3
        end
      end
      object TabSheet3: TTabSheet
        Caption = #20104#20633'('#25968#23383')'
        ImageIndex = 5
        object Label57: TLabel
          Left = 20
          Top = 18
          Width = 33
          Height = 11
          Caption = #20104#20633#65298
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label58: TLabel
          Left = 20
          Top = 42
          Width = 33
          Height = 11
          Caption = #20104#20633#65299
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label59: TLabel
          Left = 182
          Top = 16
          Width = 33
          Height = 11
          Caption = #20104#20633#65300
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label60: TLabel
          Left = 182
          Top = 42
          Width = 33
          Height = 11
          Caption = #20104#20633#65301
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object YobiInt1: TEdit
          Left = 74
          Top = 12
          Width = 73
          Height = 20
          TabOrder = 0
          Text = '0'
        end
        object YobiInt2: TEdit
          Left = 76
          Top = 36
          Width = 73
          Height = 20
          TabOrder = 1
          Text = '0'
        end
        object YobiInt3: TEdit
          Left = 232
          Top = 12
          Width = 73
          Height = 20
          TabOrder = 2
          Text = '0'
        end
        object YobiInt4: TEdit
          Left = 232
          Top = 36
          Width = 73
          Height = 20
          TabOrder = 3
          Text = '0'
        end
      end
    end
  end
  object Query1: TQuery
    DatabaseName = 'taka'
    Left = 236
    Top = 19
  end
  object Query2: TQuery
    DatabaseName = 'taka'
    Left = 270
    Top = 19
  end
  object Query3: TQuery
    DatabaseName = 'taka'
    Left = 374
    Top = 17
  end
  object Query4: TQuery
    DatabaseName = 'taka'
    Left = 344
    Top = 19
  end
  object QueryExcel: TQuery
    DatabaseName = 'taka'
    Left = 408
    Top = 20
  end
  object Query5: TQuery
    DatabaseName = 'taka'
    Left = 458
    Top = 20
  end
  object Query6: TQuery
    DatabaseName = 'taka'
    Left = 488
    Top = 22
  end
  object QueryShiireKakaku: TQuery
    DatabaseName = 'taka'
    Left = 508
    Top = 251
  end
  object QueryRireki: TQuery
    DatabaseName = 'taka'
    Left = 540
    Top = 251
  end
  object QueryShiireJouhou: TQuery
    DatabaseName = 'taka'
    Left = 236
    Top = 19
  end
  object QueryShiireKakakuRireki: TQuery
    DatabaseName = 'taka'
    Left = 488
    Top = 22
  end
  object Query7: TQuery
    DatabaseName = 'taka'
    Left = 482
    Top = 76
  end
end
