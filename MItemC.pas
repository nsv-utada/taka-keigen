unit MItemC;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  MItemT, ComCtrls, StdCtrls, Grids, Buttons, ExtCtrls, Db, DBTables;

type
  TfrmMItemC = class(TfrmMItemT)
    procedure FormCreate(Sender: TObject);
  private
    { Private �錾 }
  public
    { Public �錾 }
  end;

var
  frmMItemC: TfrmMItemC;

implementation

{$R *.DFM}

procedure TfrmMItemC.FormCreate(Sender: TObject);
var
	i : integer;
begin
  inherited;
  	with SG1 do begin
    ColWidths[CintSync]:= 100;
    Cells[CintSync, 0] := '';


    i := 1;
    Cells[CintNum, i]       := IntToStr(i);
    Cells[CintItemCode, i]  := 'A001';
    Cells[CintItemName, i]  := '�̷�(��)';
    Cells[CintItemPrice, i] := '420';
    Cells[CintYN, i]        := 'Y';
    Cells[CintSync, i]      := '';

    i := 2;
    Cells[CintNum, i]       := IntToStr(i);
    Cells[CintItemCode, i]  := 'A002';
    Cells[CintItemName, i]  := '�̷݁i�Z�b�܁j';
    Cells[CintItemPrice, i] := '630';
    Cells[CintYN, i]        := 'N';
    Cells[CintSync, i]      := '';
    end;
end;

end.
