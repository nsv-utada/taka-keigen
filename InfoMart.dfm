inherited frmInfoMart: TfrmInfoMart
  Left = 405
  Top = 151
  Width = 618
  Height = 541
  Caption = #20253#31080#19968#25324#30331#37682
  OldCreateOrder = False
  Position = poMainFormCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  object Label3: TLabel [0]
    Left = 56
    Top = 64
    Width = 48
    Height = 15
    Caption = #20837#21147#32773
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clRed
    Font.Height = -15
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel [1]
    Left = 56
    Top = 104
    Width = 189
    Height = 15
    Caption = #12452#12531#12501#12457#12510#12540#12488#12487#12540#12479#12501#12449#12452#12523
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clRed
    Font.Height = -15
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel [2]
    Left = 56
    Top = 160
    Width = 93
    Height = 15
    Caption = #23455#34892#32080#26524#12525#12464
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clRed
    Font.Height = -15
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = [fsBold]
    ParentFont = False
  end
  inherited Panel1: TPanel
    Width = 610
    inherited Label1: TLabel
      Width = 120
      Caption = #20253#31080#19968#25324#30331#37682
    end
    inherited Label2: TLabel
      Left = 362
    end
  end
  inherited Panel2: TPanel
    Top = 446
    Width = 610
    Height = 46
    object Label6: TLabel [0]
      Left = 8
      Top = 8
      Width = 36
      Height = 15
      Caption = #20633#32771':'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -15
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited BitBtn1: TBitBtn
      Left = 288
      Width = 97
      Caption = #19968#35239#30011#38754
      Font.Color = clWindowText
      TabOrder = 2
    end
    inherited BitBtn2: TBitBtn
      Left = -8
      Width = 0
      ParentBiDiMode = False
      TabOrder = 3
      Visible = False
    end
    object btnRegister: TBitBtn
      Left = 400
      Top = 8
      Width = 97
      Height = 25
      Caption = #30331#37682
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = btnRegisterClick
      NumGlyphs = 2
    end
    object Edit1: TEdit
      Left = 64
      Top = 8
      Width = 201
      Height = 20
      TabOrder = 1
    end
    object BitBtn3: TBitBtn
      Left = 280
      Top = 8
      Width = 105
      Height = 25
      Caption = #19968#35239#30011#38754#12408' '
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = BitBtn3Click
      NumGlyphs = 2
    end
    object BitBtn4: TBitBtn
      Left = 510
      Top = 8
      Width = 89
      Height = 25
      Caption = #38281#12376#12427'(&Z)'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = BitBtn1Click
      Kind = bkIgnore
    end
  end
  inherited Panel3: TPanel
    Width = 610
    Height = 405
    TabOrder = 6
    object Label7: TLabel
      Left = 54
      Top = 56
      Width = 42
      Height = 13
      Caption = #20837#21147#32773
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 54
      Top = 88
      Width = 168
      Height = 13
      Caption = #12452#12531#12501#12457#12510#12540#12488#12487#12540#12479#12501#12449#12452#12523' '
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 54
      Top = 120
      Width = 86
      Height = 13
      Caption = #23455#34892#32080#26524#12525#12464' '
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 54
      Top = 24
      Width = 42
      Height = 13
      Caption = #32013#21697#26085
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DTP1: TDateTimePicker
      Left = 110
      Top = 22
      Width = 121
      Height = 23
      Date = 37111.466265312500000000
      Time = 37111.466265312500000000
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      TabStop = False
    end
  end
  inherited SB1: TStatusBar
    Top = 492
    Width = 610
    Height = 22
  end
  object CBNyuuryokusha: TComboBox
    Left = 110
    Top = 96
    Width = 121
    Height = 20
    ImeMode = imOpen
    ItemHeight = 12
    TabOrder = 2
  end
  object btnChooseFile: TBitBtn
    Left = 496
    Top = 128
    Width = 65
    Height = 25
    Caption = #21442#29031
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = [fsBold]
    ModalResult = 5
    ParentFont = False
    TabOrder = 3
    OnClick = btnChooseFileClick
    NumGlyphs = 2
  end
  object ListBox1: TListBox
    Left = 48
    Top = 184
    Width = 513
    Height = 185
    ItemHeight = 12
    TabOrder = 4
  end
  object txtFilePath: TEdit
    Left = 224
    Top = 128
    Width = 265
    Height = 20
    TabOrder = 5
  end
  object Query2: TQuery
    DatabaseName = 'taka'
    Left = 16
    Top = 56
  end
  object dlgOpenFile: TOpenDialog
    Left = 16
    Top = 136
  end
  object QueryDenpyou: TQuery
    DatabaseName = 'dabDenpyou'
    Left = 288
    Top = 9
  end
  object dabDenpyou: TDatabase
    AliasName = 'taka'
    DatabaseName = 'dabDenpyou'
    LoginPrompt = False
    Params.Strings = (
      'user_name=sa'
      'password=netsurf'
      'USER NAME=sa')
    SessionName = 'Default'
    TransIsolation = tiDirtyRead
    Left = 336
    Top = 9
  end
end
