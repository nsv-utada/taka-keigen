unit Hacchuu2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, Grids, ComCtrls, StdCtrls, Buttons, ExtCtrls;

type
  TfrmHacchuu2 = class(TfrmMaster)
    Label3: TLabel;
    SG1: TStringGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private 宣言 }
		procedure MakeSG;
  public
    { Public 宣言 }
  end;

 const
	//グリッドの列の定義

  CintNum           = 0;
  CintItemCode      = 1;
  CintItemName      = 2;
  CintItemCount     = 3;  //現在数量
  CintItemTanni     = 4;  //単位
  CintItemHacchuu   = 5;  //発注数量
  CintItemTanka     = 6;  //単価
  CintItemShoukei   = 7;  //発注金額
  CintEnd           = 8;   //ターミネーター


	GRowCount = 700; //行数

var
  frmHacchuu2: TfrmHacchuu2;

implementation

{$R *.DFM}

procedure TfrmHacchuu2.FormCreate(Sender: TObject);
var
	i : integer;
begin
  inherited;
  //
  width := 570;
  height := 450;

  //ストリンググリッドの作成
  MakeSG;

  //サンプルデータ
  i := 1;
  with SG1 do begin
  Cells[CintNum, i]       := IntToStr(i);
  Cells[CintItemCode, i]  := 'A0001';
  Cells[CintItemName, i]  := 'ﾅﾌｷﾝ（平）';
  Cells[CintItemCount, i] := '2000';
  Cells[CintItemTanni, i] := '枚';
  Cells[CintItemTanni, i] := '枚';
  Cells[CintItemTanni, i] := '枚';

  i := 2;
  Cells[CintNum, i]       := IntToStr(i);
  Cells[CintItemCode, i]  := 'B0001';
  Cells[CintItemName, i]  := '風味出し(ﾔﾏｻ)';
  Cells[CintItemCount, i] := '2';
  Cells[CintItemTanni, i] := 'Kg';

    ColWidths[CintItemTanka]:= 60;
    Cells[CintItemTanka, 0] := '単価';

    ColWidths[CintItemShoukei]:= 60;
    Cells[CintItemShoukei, 0] := '発注金額';
  end;
end;

procedure TfrmHacchuu2.MakeSG;
begin
	with SG1 do begin
    DefaultDrawing := True;
    RowCount := GRowCount;
    ColCount := CintEnd;
    Align := alBottom;
    FixedCols := 0;
    FixedColor := clYellow;
    Canvas.Font.Size := 11;
    DefaultRowHeight := 20;
    ColWidths[0]:= 64;
    Font.Color := clNavy;

    ColWidths[CintNum]:= 40;
    Cells[CintNum, 0] := 'No.';

    ColWidths[CintItemCode]:= 80;
    Cells[CintItemCode, 0] := '商品コード';

    ColWidths[CintItemName]:= 100;
    Cells[CintItemName, 0] := '商品名';

    ColWidths[CintItemCount]:= 60;
    Cells[CintItemCount, 0] := '現在数量';

    ColWidths[CintItemTanni]:= 70;
    Cells[CintItemTanni, 0] := '発注単位';

    ColWidths[CintItemTanka]:= 60;
    Cells[CintItemTanka, 0] := '単価';

    ColWidths[CintItemHacchuu]:= 60;
    Cells[CintItemHacchuu, 0] := '発注数量';


    ColWidths[CintItemShoukei]:= 60;
    Cells[CintItemShoukei, 0] := '発注金額';

  end;
end;
end.
