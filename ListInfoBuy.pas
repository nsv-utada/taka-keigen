unit ListInfoBuy;


interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, Grids, StdCtrls, Buttons, ComCtrls, ExtCtrls, DB,
  DBTables;

type
  TfrmListInfoBuy = class(TfrmMaster)
    DTP1: TDateTimePicker;
    SG1: TStringGrid;
    cmdKennsaku: TButton;
    QueryDenpyou: TQuery;
    BitBtn7: TBitBtn;

    procedure FormCreate(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure cmdKennsakuClick(Sender: TObject);
    procedure SG1DblClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SG1DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);

  private
    { Private declarations }
  	 procedure MakeSG;
  public
    { Public declarations }
  end;
  const
  
  //グリッドの列の定義
  
  CintNum           = 0;
  CintItemCode      = 1;
  CintItemInfoCode  = 2;
  CintItemName      = 3;
  CintItemCount     = 4;
  CintEnd           = 5;

var
  frmListInfoBuy: TfrmListInfoBuy;

implementation
uses
Inter, DenpyouPrint, Denpyou, Nyuuko;

{$R *.dfm}



procedure TfrmListInfoBuy.FormCreate(Sender: TObject);
begin
  top  := 5;
  Left := 5;
	Width := 700+50;
  Height := 500;
  DTP1.Date := Date();

  //ストリンググリッドの作成
  MakeSG;
end;

procedure TfrmListInfoBuy.MakeSG;
begin
	with SG1 do begin
    DefaultDrawing     := True;
    RowCount           := 10;   // とりあえず最初は10にしておく．MakeSG2の中で動的に設定
    ColCount           := CintEnd;
    Align              := alBottom;
    FixedCols          := 0;
    FixedColor         := clYellow;
    Canvas.Font.Size   := 11;
    DefaultRowHeight   := 20;
    ColWidths[0]       := 64;
    Font.Color         := clNavy;

    // ColWidths[CintNum]:= 0; // 並び順に変更があった為，ここの順番は滅茶苦茶
    // Cells[CintNum, 0] := '分類名';

    ColWidths[CintNum]:= 70;
    Cells[CintNum, 0] := '伝票ID';

    ColWidths[CintItemCode]:= 150;
    Cells[CintItemCode, 0] := '伝票番号';

    ColWidths[CintItemInfoCode]:= 150;
    Cells[CintItemInfoCode, 0] := 'Infomart伝票No';

    ColWidths[CintItemName]:= 180;
    Cells[CintItemName, 0] := '仕入先名';

    ColWidths[CintItemCount]:= 130;
    Cells[CintItemCount, 0] := '売上金額';

  end;
end;

procedure TfrmListInfoBuy.BitBtn7Click(Sender: TObject);
var
	i,j : Integer;
begin

  // 表示をクリアする．

  with SG1 do begin
    for i := 0 to ColCount - 1 do begin
      for j := 1 to RowCount - 1 do begin
       Cells[i,j] := '';
      end;
    end;
  end;

end;

procedure TfrmListInfoBuy.cmdKennsakuClick(Sender: TObject);
var
  sNouhinbi        : String;
  sSql             : String;
  i                : Integer;
  gRowNum          : Integer;
begin
  inherited;
    DateTimeToString(sNouhinbi, 'yyyy/mm/dd', DTP1.Date);
    
  // Make SQL 
 	  sSql := 'SELECT D.ID,D.SDenpyouCode, D.InfoDenpyouNo, D.DenpyouKingaku, T.Code, T.Name FROM ' + CtblTShiire;
    sSql := sSql + ' D INNER JOIN ' + CtblMShiiresaki + ' T ON';
    sSql := sSql + ' D.ShiiresakiCode = T.Code ';
    sSql := sSql + ' WHERE D.DDate = ' + '''' + sNouhinbi + '''';
    sSql := sSql + ' AND (D.InfoDenpyouNo IS NOT NULL)';
    //sSql := sSql + ' ORDER BY CAST (D.InfoDenpyouNo as int) ASC';
    sSql := sSql + ' ORDER BY ID ASC';


    with QueryDenpyou do begin
    	Close;
      Sql.Clear;
      Sql.Add(sSql);
      Open;

      // レコードがなければメッセージを出力して抜ける
      if RecordCount = 0 then
        begin
         ShowMessage('該当する伝票はありません');
         Exit;
        end;
      // RowCountセット
      gRowNum := RecordCount + 1;  // タイトル分1行足してる
      SG1.RowCount := gRowNum;
      i := 0;
      while not EOF do begin
    	  i := i+1;
        with SG1 do begin
      	  Cells[CintNum,           i] := FieldbyName('ID').AsString;
      	  Cells[CintItemCode,      i] := FieldbyName('SDenpyouCode').AsString;
          Cells[CintItemInfoCode,  i] := FieldbyName('InfoDenpyouNo').AsString;
  	      Cells[CintItemName,      i] := FieldbyName('Code').AsString + ',' +FieldbyName('Name').AsString;
          Cells[CintItemCount,     i] := FieldbyName('DenpyouKingaku').AsString;
        end;
        next
      end;
      Close;        
    end;  
end;

procedure TfrmListInfoBuy.SG1DblClick(Sender: TObject);
var
iRow          : Integer;
begin
   iRow := SG1.Row;

   frmNyuuko := TfrmNyuuko.Create(Self);
   frmNyuuko.cmbDenpyouBanngou.Text  :=  SG1.Cells[1, iRow];
   frmNyuuko.CBShiiresakiName.Text := SG1.Cells[3, iRow];
   frmNyuuko.cmdKennsaku.OnClick(Sender);
   
end;

procedure TfrmListInfoBuy.FormActivate(Sender: TObject);
begin
  inherited;
  if GDDate <> '' then begin
    DTP1.Date := StrToDate(GDDate);
    cmdKennsaku.OnClick(Sender);
  end;
end;

procedure TfrmListInfoBuy.SG1DrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
  var
  dx: integer;
  Text: string;
begin
  if aCol in [4] then
    with SG1.Canvas do
    begin
      FillRect(Rect);
      Text := SG1.cells[aCol, aRow];
      dx := TextWidth(Text) + 2;
      TextOut(Rect.Right - dx, Rect.Top, Text);
    end;
end;

end.
