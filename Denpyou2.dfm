inherited frmDenpyou2: TfrmDenpyou2
  Left = 599
  Top = 79
  Width = 972
  Height = 566
  Caption = 'frmDenpyou2'
  FormStyle = fsNormal
  Visible = False
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 12
  inherited Panel1: TPanel
    Width = 964
    Height = 33
    inherited Label1: TLabel
      Width = 117
      Height = 13
      Caption = #22770#19978#20253#31080#12503#12524#12499#12517#12540
      Font.Height = -13
    end
    inherited Label2: TLabel
      Left = 158
      Top = 4
      Width = 190
      Height = 12
      Font.Height = -12
      Visible = False
    end
    inherited Label7: TLabel
      Left = 418
      Top = 6
    end
    inherited cmdKennsaku: TButton
      Visible = False
    end
    inherited cmdDenpyouBanngouListMake: TButton
      Visible = False
    end
    inherited cmbDenpyouBanngou: TComboBox
      Visible = False
    end
    object edbDenpyouBanngou: TEdit
      Left = 480
      Top = 3
      Width = 105
      Height = 21
      Color = clActiveBorder
      Enabled = False
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ImeMode = imDisable
      ParentFont = False
      TabOrder = 3
    end
  end
  inherited Panel2: TPanel
    Top = 482
    Width = 964
    Height = 38
    inherited lSum: TLabel
      Left = 298
      Top = 14
      Font.Color = clMaroon
      Visible = True
    end
    object lblBIkou: TLabel [1]
      Left = 10
      Top = 11
      Width = 36
      Height = 13
      Caption = #20633#32771#65306
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clMaroon
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Label15: TLabel
      Visible = False
    end
    inherited edbBikou1: TEdit [4]
      TabOrder = 5
      Visible = False
    end
    inherited BitBtn1: TBitBtn [5]
      Left = 425
      Caption = #30331#37682#12377#12427
      ModalResult = 0
    end
    inherited BitBtn2: TBitBtn [6]
      Left = 524
      Width = 103
      Caption = #21360#21047#12377#12427
      Enabled = False
      ModalResult = 0
    end
    inherited BitBtn7: TBitBtn [7]
      Left = 214
      Visible = False
    end
    object edbBikou: TEdit
      Left = 48
      Top = 8
      Width = 241
      Height = 20
      MaxLength = 100
      TabOrder = 3
    end
  end
  inherited Panel3: TPanel
    Top = 33
    Width = 964
    Height = 57
    inherited Label3: TLabel
      Left = 14
      Width = 42
      Height = 13
      Font.Height = -13
    end
    inherited Label4: TLabel
      Left = 145
      Width = 76
      Caption = #24471#24847#20808#12467#12540#12489
    end
    inherited Label5: TLabel
      Left = 290
      Visible = False
    end
    inherited Label6: TLabel
      Left = 14
      Top = 28
      Width = 42
      Height = 13
      Font.Height = -13
    end
    object lDate: TLabel [4]
      Left = 70
      Top = 8
      Width = 60
      Height = 12
      Caption = '2001/08/23'
    end
    object lTokuisakiName: TLabel [5]
      Left = 245
      Top = 8
      Width = 27
      Height = 12
      Caption = 'lDate'
    end
    object lNyuuryokusha: TLabel [6]
      Left = 70
      Top = 28
      Width = 27
      Height = 12
      Caption = 'lDate'
    end
    inherited Label8: TLabel
      Left = 145
      Top = 28
      Width = 38
      Height = 13
      Caption = #26376'  '#20998
      Font.Height = -13
    end
    object lGatsubun: TLabel [8]
      Left = 285
      Top = 28
      Width = 27
      Height = 12
      Caption = 'lDate'
      Visible = False
    end
    inherited Label12: TLabel
      Visible = False
    end
    object lblTeisei: TLabel [12]
      Left = 320
      Top = 35
      Width = 101
      Height = 24
      Caption = #12371#12428#12399#35330#27491#20253#31080#12391#12377#12290
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
      WordWrap = True
    end
    inherited Label16: TLabel
      Left = 402
    end
    inherited lbAriaCode: TLabel
      Left = 480
    end
    inherited Label17: TLabel
      Left = 498
      Top = 8
    end
    inherited Label18: TLabel
      Left = 576
      Top = 9
    end
    inherited Label19: TLabel
      Width = 5
      Caption = ''
    end
    object Label21: TLabel [19]
      Left = 588
      Top = 25
      Width = 4
      Height = 12
    end
    object lblTorikeshi: TLabel [20]
      Left = 296
      Top = 35
      Width = 123
      Height = 12
      Caption = #12371#12428#12399#21462#28040#20253#31080#12391#12377#12290
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
      WordWrap = True
    end
    object Label23: TLabel [21]
      Left = 640
      Top = 1
      Width = 4
      Height = 12
    end
    inherited oldDenpyouBangou: TLabel
      Left = 828
      Top = 32
    end
    inherited DTP1: TDateTimePicker
      Left = 436
      Top = 40
      Visible = False
    end
    inherited BitBtn3: TBitBtn
      Left = 424
    end
    inherited CBTokuisakiName: TComboBox
      Left = 350
      Top = 40
      Visible = False
    end
    inherited CBNyuuryokusha: TComboBox
      Left = 404
      Top = 40
      Visible = False
    end
    object BitBtn8: TBitBtn [29]
      Left = 426
      Top = 29
      Width = 115
      Height = 22
      Caption = #12501#12451#12540#12489#12496#12483#12463
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      OnClick = BitBtn8Click
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333444444
        33333333333F8888883F33330000324334222222443333388F3833333388F333
        000032244222222222433338F8833FFFFF338F3300003222222AAAAA22243338
        F333F88888F338F30000322222A33333A2224338F33F8333338F338F00003222
        223333333A224338F33833333338F38F00003222222333333A444338FFFF8F33
        3338888300003AAAAAAA33333333333888888833333333330000333333333333
        333333333333333333FFFFFF000033333333333344444433FFFF333333888888
        00003A444333333A22222438888F333338F3333800003A2243333333A2222438
        F38F333333833338000033A224333334422224338338FFFFF8833338000033A2
        22444442222224338F3388888333FF380000333A2222222222AA243338FF3333
        33FF88F800003333AA222222AA33A3333388FFFFFF8833830000333333AAAAAA
        3333333333338888883333330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    inherited edbYomi: TEdit
      TabOrder = 10
    end
    inherited chbTeisei: TCheckBox
      Left = 549
      Top = 36
      Enabled = False
      Font.Color = clRed
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 13
    end
    inherited EditGatsubun: TEdit
      Left = 192
      Top = 26
      Width = 73
    end
    object Panel4: TPanel [36]
      Left = 0
      Top = 43
      Width = 185
      Height = 18
      TabOrder = 12
      Visible = False
    end
    inherited GB1: TGroupBox
      TabOrder = 14
    end
  end
  inherited SB1: TStatusBar
    Top = 520
    Width = 964
  end
  inherited SG1: TStringGrid
    Top = 90
    Width = 658
    Height = 302
  end
  inherited Panel5: TPanel
    Top = 90
    Width = 964
    inherited Label9: TLabel
      Visible = False
    end
    inherited Label13: TLabel
      Visible = False
    end
    inherited MemoBikou: TMemo
      ReadOnly = True
    end
    inherited btnShowAll: TButton
      Visible = False
    end
    inherited Button2: TButton
      Visible = False
    end
  end
  inherited QueryDenpyou: TQuery
    DatabaseName = 'dabDenpyou'
    Left = 288
    Top = 9
  end
  object dabDenpyou: TDatabase [7]
    AliasName = 'taka-access'
    DatabaseName = 'dabDenpyou'
    LoginPrompt = False
    Params.Strings = (
      'user_name=sa'
      'password=netsurf'
      'USER NAME=sa')
    SessionName = 'Default'
    TransIsolation = tiDirtyRead
    Left = 336
    Top = 9
  end
  object Query1: TQuery [9]
    DatabaseName = 'taka-access'
    Left = 218
    Top = 79
  end
end
