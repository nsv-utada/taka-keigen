inherited frmUriageDenpyouIkkatuInnsatu: TfrmUriageDenpyouIkkatuInnsatu
  Left = 261
  Top = 88
  VertScrollBar.Range = 0
  BorderStyle = bsSingle
  Caption = 'frmUriageDenpyouIkkatuInnsatu'
  ClientHeight = 253
  ClientWidth = 454
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  inherited Panel1: TPanel
    Width = 454
    Height = 30
    inherited Label1: TLabel
      Top = 8
      Width = 102
      Height = 16
      Caption = #20253#31080#19968#25324#21360#21047
      Font.Height = -16
    end
    inherited Label2: TLabel
      Left = 186
      Top = 8
    end
  end
  inherited Panel2: TPanel
    Top = 200
    Width = 454
    Height = 34
    inherited BitBtn1: TBitBtn
      Left = 352
      Height = 23
      Caption = #38281#12376#12427
    end
    inherited BitBtn2: TBitBtn
      Width = 89
      Height = 23
      Caption = #19968#25324#21360#21047
      OnClick = BitBtn2Click
    end
  end
  inherited Panel3: TPanel
    Top = 30
    Width = 454
    Height = 170
    object Label3: TLabel
      Left = 320
      Top = 16
      Width = 123
      Height = 36
      Caption = #24038#35352#12398#65300#12388#12398#26465#20214#12398#20869#65292#12356#12378#12428#12363#12434#25351#23450#12375#12390#65292#19968#25324#21360#21047#12434#25276#12375#12390#19979#12373#12356#65294
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ParentFont = False
      WordWrap = True
    end
    object Label4: TLabel
      Left = 119
      Top = 99
      Width = 147
      Height = 12
      Caption = #12424#12415#12434#20837#21147#12375#12390'F1'#12461#12540#12434#25276#19979
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ParentFont = False
      WordWrap = True
    end
    object pnlNouhinDate: TPanel
      Left = 16
      Top = 12
      Width = 89
      Height = 20
      Caption = #32013'  '#21697'  '#26085
      TabOrder = 0
    end
    object pnlCourse: TPanel
      Left = 16
      Top = 42
      Width = 89
      Height = 20
      Caption = #12467'  '#65293'  '#12473
      TabOrder = 1
    end
    object pnlDenpyouBanngou: TPanel
      Left = 16
      Top = 123
      Width = 89
      Height = 20
      Caption = #20253#31080#30058#21495
      TabOrder = 2
    end
    object pnlTokuisakiName: TPanel
      Left = 16
      Top = 75
      Width = 89
      Height = 20
      Caption = #24471#24847#20808#21517
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
    end
    object pnlStart: TPanel
      Left = 112
      Top = 123
      Width = 49
      Height = 20
      Caption = 'start'
      TabOrder = 4
    end
    object pnlEnd: TPanel
      Left = 256
      Top = 123
      Width = 49
      Height = 20
      Caption = 'end'
      TabOrder = 5
    end
    object dtpNouhinDate: TDateTimePicker
      Left = 120
      Top = 12
      Width = 186
      Height = 20
      Date = 37297.655360127300000000
      Time = 37297.655360127300000000
      TabOrder = 6
    end
    object edbStart: TEdit
      Left = 167
      Top = 123
      Width = 81
      Height = 20
      TabOrder = 7
    end
    object edbEnd: TEdit
      Left = 311
      Top = 123
      Width = 81
      Height = 20
      TabOrder = 8
    end
    object cmbTokuisakiName: TComboBox
      Left = 120
      Top = 75
      Width = 185
      Height = 20
      ImeMode = imOpen
      ItemHeight = 12
      TabOrder = 9
      OnExit = cmbTokuisakiNameExit
      OnKeyDown = cmbTokuisakiNameKeyDown
    end
    object cmbCourse: TComboBox
      Left = 120
      Top = 42
      Width = 185
      Height = 20
      ItemHeight = 12
      TabOrder = 10
      OnExit = cmbCourseExit
    end
  end
  inherited SB1: TStatusBar
    Top = 234
    Width = 454
  end
  object qryUriageDenpyouIkkatuInnsatu: TQuery
    DatabaseName = 'taka-access'
    Left = 408
    Top = 166
  end
end
