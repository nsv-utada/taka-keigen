unit MItemT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, ComCtrls, StdCtrls, Buttons, ExtCtrls, Grids, Db, DBTables;

type
  TfrmMItemT = class(TfrmMaster)
    SG1: TStringGrid;
    Panel4: TPanel;
    Label3: TLabel;
    CBTokuisaki: TComboBox;
    BitBtn6: TBitBtn;
    Query1: TQuery;
    BitBtn5: TBitBtn;
    EditItemName: TEdit;
    Label4: TLabel;
    Label5: TLabel;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    EditCode1: TEdit;
    EditCode2: TEdit;
    EditTanka: TEdit;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    BitBtn7: TBitBtn;
    Label9: TLabel;
    EditYomi: TEdit;
    Label10: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure CBTokuisakiKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn5Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure SG1Enter(Sender: TObject);
    procedure EditYomiChange(Sender: TObject);
    procedure EditYomiKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SG1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private 宣言 }
    procedure MakeSG(sTokuisakiCode, sortkey:string);
    procedure MakeSG2(sTokuisakiCode, sortkey:string);
    procedure MakeCBTokuisakiName(sKey, sWord:String; iKubun:Integer);
    procedure SGClear;
    function UpdateSQL(sSql,sTanka : String): Boolean;
  public
    { Public 宣言 }
  end;

  const

	//グリッドの列の定義
  CintNum           = 0;
  CintItemCode      = 1;
  CintItemCode2     = 2;
  CintItemName      = 3;
  CintItemTani      = 4;
  CintItemIrisuu    = 5;
  CintItemKikaku    = 6;
  CintItemPrice     = 7;
  CintHindo         = 8;
  CintYN            = 9;
  CintSync          = 10;
  //2006.08.30
  CintYomi          = 11;
  CintEnd           = 12;   //ターミネーター


  //GRowCount = 3500; //行数    2006.08.30
  GRowCount = 5000; //行数    2006.12.25

var
  frmMItemT: TfrmMItemT;

implementation
uses
 Inter, DM1, DMMaster;

var
 giRow, giHit :integer;


{$R *.DFM}

procedure TfrmMItemT.FormCreate(Sender: TObject);
var
   sWhere : String;
begin
  inherited;
	Width := 650;
  Height := 550;

  //2002.08.29
	GTokuisakiCode2 := '';

{
  if (GTokuisakiCode2<>'') then begin
    //得意先名の検索
    sWhere := ' where TokuisakiCode1 = ' + GTokuisakiCode2;
    GTokuisakiName := DM1.GetFieldData(CtblMTokuisaki, 'TokuisakiName', sWhere);
    CBTokuisaki.text := GTokuisakiCode2 + ',' + GTokuisakiName;
  end;
}
  //ストリンググリッドの作成
  MakeSG(GTokuisakiCode2, 'Code');

end;

procedure TfrmMItemT.SGClear;
var
 i : Integer;
begin
 for i:=1 to GRowCount do begin
  with SG1 do begin
  Cells[CintNum, i]       := '';
  Cells[CintItemCode, i]  := '';
  Cells[CintItemCode2,i]  := '';
  Cells[CintItemName, i]  := '';
  Cells[CintItemPrice, i] := '';
  Cells[CintYN, i]        := '';
  Cells[CintSync, i]      := '';

  Cells[CintItemTani, i]     := '';
  Cells[CintItemIrisuu, i]   := '';
  Cells[CintItemKikaku, i]   := '';
  Cells[CintHindo, i]        := '';
  Cells[CintYomi, i]        := '';

  end;
 end;
end;

//
procedure TfrmMItemT.MakeSG(sTokuisakiCode, sortkey:string);
var
	i,iHindo : integer;
  sSql,sItemName, sWhere, sItemYomi : String;
  sItemTani, sItemIrisuu, sItemKikaku : String;
begin
  SGClear;
	with SG1 do begin
    DefaultDrawing := True;
    RowCount := GRowCount;
    ColCount := CintEnd;
    Align := alBottom;
    FixedCols := 0;
    FixedColor := clYellow;
    Canvas.Font.Size := 11;
    DefaultRowHeight := 20;
    ColWidths[0]:= 64;
    Font.Color := clNavy;

    ColWidths[CintNum]:= 35;
    Cells[CintNum, 0] := 'No.';

    ColWidths[CintItemCode]:= 40;
    Cells[CintItemCode, 0] := 'Code1';

    ColWidths[CintItemCode2]:= 50;
    Cells[CintItemCode2, 0] := 'Code2';

    ColWidths[CintItemName]:= 130;
    Cells[CintItemName, 0] := '商品名';

    ColWidths[CintItemPrice]:= 60;
    Cells[CintItemPrice, 0] := '売価';

{
    ColWidths[CintYN]:= 40;
    Cells[CintYN, 0] := 'Y/N';

    ColWidths[CintSync]:= 100;
    Cells[CintSync, 0] := 'チェーン同期(Y/N)';
 }
    ColWidths[CintItemTani]:= 50;
    Cells[CintItemTani, 0] := '単位';

    ColWidths[CintItemIrisuu]:= 50;
    Cells[CintItemIrisuu, 0] := '入数';

    ColWidths[CintItemKikaku]:= 50;
    Cells[CintItemKikaku, 0] := '規格';

    ColWidths[CintHindo]:= 50;
    Cells[CintHindo, 0] := '頻度';

    //2006.08.30
    ColWidths[CintYomi]:= 10;
    Cells[CintYomi, 0] := 'よみ';

    if (GTokuisakiCode2='') then begin
      exit;
    end;

    //2001.12.23
    //2005.06.16
    //MakeSg2(sTokuisakiCode, 'Code1, Code2');
    MakeSg2(sTokuisakiCode, sortkey);
    exit;

//ここから下は消す
    sSql := 'select * from ' + CtblMItem2;
    sSql := sSql + ' where ';
    sSql := sSql + ' TokuisakiCode = ' + sTokuisakiCode;

    if sortkey='Code' then begin
      sSql := sSql + ' order by Code1, Code2';
    end;
    if sortkey='Yomi' then begin
      sSql := sSql + ' order by Yomi, Code1';
    end;
    with Query1 do begin
    	close;
      sql.Clear;
      sql.Add(sSql);
      open;
      i := 1;
      while not EOF do begin
       sWhere := ' where ';
       sWhere := sWhere  + ' Code1=''' + fieldbyname('Code1').AsString + '''';
       sWhere := sWhere  + ' and ';
       sWhere := sWhere  + ' Code2=''' + fieldbyname('Code2').AsString + '''';

       sItemName := DM1.GetFieldData(CtblMItem, 'Name', sWhere);
       sItemYomi := DM1.GetFieldData(CtblMItem, 'Yomi', sWhere);

       sItemTani   := DM1.GetFieldData(CtblMItem, 'Tanni', sWhere);
       sItemIrisuu := DM1.GetFieldData(CtblMItem, 'Irisuu', sWhere);
       sItemKikaku := DM1.GetFieldData(CtblMItem, 'Kikaku', sWhere);

       //名前で絞込み
       if EditItemName.text <> '' then begin
         if pos(EditItemName.text, sItemYomi)=0 then begin
           SB1.SimpleText := IntToStr(i) + ' ** Code1->' + FieldByName('Code1').asString +
                           ' Code2->'  +FieldByName('Code2').asString +
                          ' Name->'  + sItemName;
           SB1.Update;
           next;
           continue;
         end;
       end;

       Cells[CintNum, i]       := IntToStr(i);
       Cells[CintItemCode, i]  := fieldbyname('Code1').asstring;
       Cells[CintItemCode2,i]  := fieldbyname('Code2').asstring;

       Cells[CintItemName, i]  := sItemName;
       Cells[CintItemPrice, i] := fieldbyname('Tanka').asstring;

       Cells[CintItemTani, i] := sItemTani;
       Cells[CintItemIrisuu, i] := sItemIrisuu;
       Cells[CintItemKikaku, i] := sItemKikaku;

       iHindo := fieldbyname('Hindo').AsInteger;
       Cells[CintHindo, i] := IntToStr(iHindo);

       //2006.08.30
       Cells[CintYomi, i] := sItemYomi;


       if fieldbyname('YN').AsBoolean =True then begin
          Cells[CintYN, i] := 'Y';
       end else begin
          Cells[CintYN, i] := 'N';
       end;
       if fieldbyname('CYN').AsBoolean =True then begin
          Cells[CintSync, i] := 'Y';
       end else begin
          Cells[CintSync, i] := 'N';
       end;
        SB1.SimpleText := IntToStr(i) + ' ** Code1->' + FieldByName('Code1').asString +
                          ' Code2->'  +FieldByName('Code2').asString +
                          ' Name->'  + sItemName;
        SB1.Update;
       next;
       i := i + 1;
      end;//of while
    end;//of with
  end;//of with
end;

procedure TfrmMItemT.MakeSg2(sTokuisakiCode, sortkey : String);
var
  sSql, sTmp : string;
  i,iHindo : INteger;
begin
  if sTokuisakiCode <> '' then

    // Make SQL
    sSql := 'SELECT * FROM ' + CviwMItems;
    sSql := sSql + ' WHERE TokuisakiCode =  ' + sTokuisakiCode;

    if length(EditItemName.Text) > 0 then begin
       sSql := sSql + 'and Yomi Like ''%' + EditItemName.Text + '%''';
    end;
    sSql := sSql + ' ORDER BY ' + sortkey;

    // Excecute SQL
    with Query1 do begin
    	Close;
      Sql.Clear;
      Sql.Add(sSql);
      Open;

      // Set Data to StringGrid
      i := 0;
      while not EOF do begin
    	  i := i+1;
        with SG1 do begin
      	  Cells[CintItemCode,   i] := FieldbyName('Code1').AsString;
      	  Cells[CintItemCode2,   i] := FieldbyName('Code2').AsString;
      	  Cells[CintItemName,   i] := FieldbyName('Name').AsString;
    	    Cells[CintItemIrisuu,  i] := FieldbyName('Irisuu').AsString;
          Cells[CintItemPrice,  i] := FieldbyName('Tanka').AsString;
          Cells[CintItemTani,  i] := FieldbyName('Tanni').AsString;
          Cells[CintItemKikaku,  i] := FieldbyName('Kikaku').AsString;
          //2006.08.30
          //sTmp := FieldbyName('Yomi').AsString;
          Cells[CintYomi,  i] := FieldbyName('Yomi').AsString;

          if FieldbyName('Hindo').AsString<>'' then begin
            iHindo := FieldbyName('Hindo').AsInteger;
          end else begin
            iHindo:=0;
          end;
          Cells[CintHindo,  i] := IntToStr(iHindo);

        end;
        next
      end;
      Close;
    end;
    //2002.07.05 Added by H.Kubota
    SB1.SimpleText := '総件数 ->' + IntToStr(i) + '　件です';
    SB1.Update;
end;

procedure TfrmMItemT.FormResize(Sender: TObject);
begin
  inherited;
  panel4.Align := alTop;
	SG1.Align    := alClient;
end;

procedure TfrmMItemT.FormActivate(Sender: TObject);
begin
  inherited;
	SG1.Align := alClient;
end;

//再表示ボタン（商品コード順）
procedure TfrmMItemT.BitBtn6Click(Sender: TObject);
begin
  GTokuisakiCode2 := Copy(CBTokuisaki.Text, 1, Pos(',',CBTokuisaki.Text)-1);
  MakeSG(GTokuisakiCode2,'Code1, Code2');
end;

//再表示ボタン（頻度順）
procedure TfrmMItemT.BitBtn7Click(Sender: TObject);
begin
  GTokuisakiCode2 := Copy(CBTokuisaki.Text, 1, Pos(',',CBTokuisaki.Text)-1);
  MakeSG(GTokuisakiCode2,'Hindo Desc, Code1, Code2');
end;


procedure TfrmMItemT.CBTokuisakiKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
	 	MakeCBTokuisakiName('TokuisakiNameYomi', CBTokuisaki.Text, 1);
  end;
end;
{
	得意先名のコンボボックスを作成する
  引数：sKey   -> 検索列名
  			sWord  -> 検索ワード
  			iKubun -> 0=前方一致, 1=曖昧検索, 2=数字の完全一致
}
procedure TfrmMItemT.MakeCBTokuisakiName(sKey, sWord:String; iKubun:Integer);
var
	sSql: String;
begin

  CBTokuisaki.Items.Clear;


	sSql := 'SELECT * FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + sKey;
  case iKubun of
  0: sSql := sSql + ' LIKE ''' + sWord + '%''';
  1: sSql := sSql + ' LIKE ''%' + sWord + '%''';
  2: sSql := sSql + ' = ' + sWord;
  end;

  sSql := sSql + ' ORDER BY TokuisakiCode1 ';
  CBTokuisaki.Clear;
	with frmDMMaster.QueryMTokuisaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	CBTokuisaki.Items.Add(FieldByName('TokuisakiCode1').AsString + ',' +
             FieldByName('TokuisakiNameUp').AsString + ' ' + 
						 FieldByName('TokuisakiName').AsString);
    	Next;
    end;//of while
    Close;
  end;//of with

  CBTokuisaki.DroppedDown := True;
end;

//上書登録ボタン
procedure TfrmMItemT.BitBtn5Click(Sender: TObject);
var
   iRow,i : Integer;
   Code1, Code2, Tanka, YN, CYN : string;
   myA : TInRecArry;
   sWhere : String;
   sDate,sSql : String;
   orgTanka : String;
begin
      sDate := DateToStr(Date);
  //確認
	if MessageDlg('得意先コード'+GTokuisakiCode2 + 'の商品データを上書きしますか？',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
	  Exit;
	end;
  with SG1 do begin
    iRow := 1;
    while Cells[CintItemCode,iRow]<>'' do begin
     i := 2;
     Code1 := Cells[CintItemCode, iRow];
     Code2 := Cells[CintItemCode2, iRow];
     Tanka := Cells[CintItemPrice, iRow];
     YN    := Cells[CintYN, iRow];
     CYN   := Cells[CintSync, iRow];
     GTokuisakiCode2 := Copy(CBTokuisaki.Text, 1, Pos(',',CBTokuisaki.Text)-1);
     sWhere := ' where ';
     sWhere := sWhere + ' Code1=''' + Code1 + '''';
     sWhere := sWhere + ' and Code2=''' + Code2 + '''';
     sWhere := sWhere + ' and TokuisakiCode=' + GTokuisakiCode2;


     //現在の単価を取得
      sSql := 'SELECT Tanka FROM ' +  CtblMItem2;
      sSql := sSql + sWhere;
      // Excecute SQL
      with Query1 do begin
        Close;
        Sql.Clear;
        Sql.Add(sSql);
        Open;

        FetchAll;

        orgTanka := FieldbyName('Tanka').AsString;

        next;

      end;//of Query1

     myA[i][1] := 'Tanka';
     myA[i][2] := Tanka;
     myA[i][3] := '';
     i := i + 1;
     if YN = 'Y' then begin
       YN := '1';
     end else begin
       YN := '0';
     end;
     myA[i][1] := 'YN';
     myA[i][2] := YN;
     myA[i][3] := '';
     i := i + 1;
     if CYN = 'Y' then begin
       CYN := '1';
     end else begin
       CYN := '0';
     end;
     myA[i][1] := 'CYN';
     myA[i][2] := CYN;
     myA[i][3] := '';
     i := i + 1;

     myA[i][1] := 'Hindo';
     myA[i][2] := Trim(Cells[CintHindo, iRow]);
     myA[i][3] := '';
     i := i + 1;

     //add utada 2014.09.10
     if (orgTanka <> Tanka) then begin
        myA[i][1] := 'InfoSendDate';
        myA[i][2] := 'NULL';
        myA[i][3] := '';
        i := i + 1;
     end;

     myA[i][1] := 'END';
     myA[i][2] := 'END';
     myA[i][3] := 'END';
     DM1.UpdateRecord2(myA, CtblMItem2, sWhere);

      //item2の既存単価と新単価が違う場合のみ更新を行う。
      if (orgTanka <> Tanka) then begin

          sSql := 'UPDATE tblMItem3 SET';
          sSql := sSql + ' Tanka = ' + Tanka;
          sSql := sSql + ', Bikou = ''' + sDate + '価格変更, '' + Bikou';
          sSql := sSql + sWhere;

          if UpdateSQL(sSql,Tanka) = True then begin
            //ShowMessage('売値を更新しました')
          end else begin
            ShowMessage('見積りデータの売値の更新に失敗しました。処理を中断します');
            exit;
          end;

      end;
      
     SB1.SimpleText := IntToStr(iRow) + ' ** Code1->' + Code1 +
                          ' Code2->'  + Code2;
     SB1.Update;
     iRow  := iRow + 1;

     if iRow > GRowCount then begin
        Break;
     end;
    end;
  end;
  ShowMessage('上書き登録完了しました。');
end;

procedure TfrmMItemT.Button1Click(Sender: TObject);
begin
  GTokuisakiCode2 := Copy(CBTokuisaki.Text, 1, Pos(',',CBTokuisaki.Text)-1);
  MakeSG(GTokuisakiCode2,'Yomi');
end;

//2002.03.20 Hajime Kubota
//商品削除ボタンがクリックされた
procedure TfrmMItemT.BitBtn3Click(Sender: TObject);
var
  sItemCode1, sItemCode2, sTokuisakiCode:String;
  InRecArry:TInRecArry;
  i : Integer;
begin
  //グリッド上のアイテム情報の取得
  with sg1 do begin
   if Row<1 then begin
    exit;
   end;
   sItemCode1 := Cells[CintItemCode, Row];
   sItemCode2 := Cells[CintItemCode2, Row];
  end;
  sTokuisakiCode := GTokuisakiCode2;
	if MessageDlg('得意先コード='+sTokuisakiCode +
                ' 商品コード1=' +sItemCode1 +
                ' 商品コード2=' +sItemCode2 +
                  ' の商品データを削除しますか？',
    mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
    ;
  end else begin
	  Exit;
	end;


  //  InRecArry : TInRecArry;//[フィールド名,フィールド内容, 区切り]
  i := 1;
  InRecArry[i, 1] := 'TokuisakiCode';
  InRecArry[i, 2] := sTokuisakiCode;
  InRecArry[i, 3] := '';

  i := i + 1;
  InRecArry[i, 1] := 'Code1';
  InRecArry[i, 2] := sItemCode1;
  InRecArry[i, 3] := '''';

  i := i + 1;
  InRecArry[i, 1] := 'Code2';
  InRecArry[i, 2] := sItemCode2;
  InRecArry[i, 3] := '''';

  i := i + 1;
  InRecArry[i, 1] := 'END';

	DeleteRecord(InRecArry, CtblMItem2);
  ShowMessage('削除しました');

  //再表示
  GTokuisakiCode2 := Copy(CBTokuisaki.Text, 1, Pos(',',CBTokuisaki.Text)-1);
  MakeSG(GTokuisakiCode2,'Code');

end;

procedure TfrmMItemT.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  //2002.08.14
  GTokuisakiCode2 := '';
end;

//2004.10.07 Added by Hajime Kubota
//得意先別商品の追加
procedure TfrmMItemT.BitBtn4Click(Sender: TObject);
var
  sSql : String;
  sItemCode1, sItemCode2, sTokuisakiCode:String;
  InRecArry:TInRecArry;
  i : Integer;
begin

  //2006.11.07
  //Added by hkubota
  if GTokuisakiCode2 = '' then begin
    GTokuisakiCode2 := Copy(CBTokuisaki.Text, 1, Pos(',',CBTokuisaki.Text)-1);
  end;

   sTokuisakiCode := GTokuisakiCode2;

	if MessageDlg('得意先コード='+sTokuisakiCode +
                ' 商品コード1=' +Trim(EditCode1.Text) +
                ' 商品コード2=' +Trim(EditCode2.Text) +
                ' 単価=' +Trim(EditTanka.Text) +
                  ' の商品データを追加しますか？',
    mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
    ;
  end else begin
	  Exit;
	end;

  //  InRecArry : TInRecArry;//[フィールド名,フィールド内容, 区切り]



  i := 1;
  InRecArry[i, 1] := 'TokuisakiCode';
  InRecArry[i, 2] := sTokuisakiCode;
  InRecArry[i, 3] := '';

  i := i + 1;
  InRecArry[i, 1] := 'Code1';
  InRecArry[i, 2] := Trim(EditCode1.Text);
  InRecArry[i, 3] := '''';

  i := i + 1;
  InRecArry[i, 1] := 'Code2';
  InRecArry[i, 2] := Trim(EditCode2.Text);
  InRecArry[i, 3] := '''';

  i := i + 1;
  InRecArry[i, 1] := 'Tanka';
  InRecArry[i, 2] := Trim(EditTanka.Text);
  InRecArry[i, 3] := '''';

  i := i + 1;
  InRecArry[i, 1] := 'END';

  InsertRecord(InRecArry, CtblMItem2);
  ShowMessage('追加しました');

  //2006.11.07 hkubota
  exit;

  //再表示
  //GTokuisakiCode2 := Copy(CBTokuisaki.Text, 1, Pos(',',CBTokuisaki.Text)-1);
  //MakeSG(GTokuisakiCode2,'Code');

end;


//2006.08.30
procedure TfrmMItemT.SG1Enter(Sender: TObject);
var
  sItemName, sYomi : String;
  i, iRow, iHit : Integer;
begin
{
  sItemName := EditYomi.Text;
  iRow := SG1.Row;

  if iRow > giRow then begin
    for i := (SG1.Row+1) to SG1.RowCount - 1 do begin
       sYomi := SG1.Cells[CintYomi, i];
       if AnsiPos(sItemName, sYomi) <> 0 then
         begin
           iHit := i;
           Break;
         end;
     end;//of for
  end;//of if

  // 見つからなかったら（iHitが初期値の0のままなら)，メッセージボックスを表示して抜ける．
  if iHit = 0 then begin
    ShowMessage('該当する商品はみつかりません');
  end else begin
    // 見つかったら，iHit行まで移動する．
    SG1.Col       := 1;
    SG1.Row       := iHit;
    SG1.SetFocus;
    giHit := iHit; //Added H.K.
  end;
}
end;

procedure TfrmMItemT.EditYomiChange(Sender: TObject);
begin
  //SGの行位置を初期化
  giRow := 0;
  giHit := 0;
end;

procedure TfrmMItemT.EditYomiKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
 i, iHit : integer;
 sYomi : String;
begin
if (Key=VK_RETURN) then begin

  //最初の検索
  for i := 0 to SG1.RowCount - 1 do begin
    sYomi := SG1.Cells[CintYomi, i];
    if AnsiPos(EditYomi.Text, sYomi) <> 0 then begin
     iHit := i;
     Break;
    end;
  end;//of for

  // 見つからなかったら（iHitが初期値の0のままなら)，メッセージボックスを表示して抜ける．
  if iHit = 0 then begin
    ShowMessage('該当する商品はみつかりません');
  end else begin
    // 見つかったら，iHit行まで移動する．
    SG1.Col       := CintHindo;
    SG1.Row       := iHit;
    SG1.SetFocus;
    giHit := iHit; //Added H.K.
  end;
end;

end;

procedure TfrmMItemT.SG1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
 i, iHit : integer;
 sYomi : String;
begin
  inherited;
if (Key=VK_RETURN) then begin
  //次の検索
  for i := (giHit+1) to SG1.RowCount - 1 do begin
    sYomi := SG1.Cells[CintYomi, i];
    if AnsiPos(EditYomi.Text, sYomi) <> 0 then begin
     iHit := i;
     Break;
    end;
  end;//of for

  // 見つからなかったら（iHitが初期値の0のままなら)，メッセージボックスを表示して抜ける．
  if iHit = 0 then begin
    ShowMessage('該当する商品はみつかりません');
  end else begin
    // 見つかったら，iHit行まで移動する．
    SG1.Col       := CintHindo;
    SG1.Row       := iHit;
    SG1.SetFocus;
    giHit := iHit; //Added H.K.
  end;
end;//of if

end;

function TfrmMItemT.UpdateSQL(sSql,sTanka : String): Boolean;
begin
 try
		with Query1 do begin
  		Close;
    	Sql.Clear;
	    Sql.Add(sSql);
  	  ExecSql;
	  end;
  except
	  on E: EDBEngineError do begin
    	ShowMessage(E.Message);
    	//frmDMMaster.DBShokuzai.RollBack;
      Result := False;
    end
 end;//of try

   //もう一度  実行//////////////////////////////////////　
   sSql := sSql + ' AND Tanka <> ' + sTanka;
  try
		with Query1 do begin
  		Close;
    	Sql.Clear;
	    Sql.Add(sSql);
  	  ExecSql;
	  end;
  except
	  on E: EDBEngineError do begin
    	ShowMessage(E.Message);
    	//frmDMMaster.DBShokuzai.RollBack;
      Result := False;
    end
  end;
  //of try//////////////////////////////////////////////
  Result := True;
end;


end.
