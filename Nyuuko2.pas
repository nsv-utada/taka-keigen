unit Nyuuko2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Nyuuko, DB, DBTables, Grids, ComCtrls, StdCtrls, ExtCtrls,
  Buttons;

type
  TfrmNyuuko1 = class(TfrmNyuuko)
    lNyuuryokusha: TLabel;
    lShiiresakiName: TLabel;
    lDTP1: TLabel;
    Label8: TLabel;
    CTax: TEdit;
    lSum2: TLabel;
    QueryDelete: TQuery;
    QueryInsert: TQuery;
    lSumShoukei: TLabel;
    lSumGoukei: TLabel;
    Label9: TLabel;
    Label12: TLabel;
    Label17: TLabel;
    Label14: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure CTaxChange(Sender: TObject);
  private
    { Private declarations }
    procedure MakeSG;
    //function MakeSDenpyouCode(iShiiresakiCode:integer; sNyuukabi:String): string;
    function InserttblTShiire(sDenpyouCode:String):Boolean;
    function InserttblTShiireDetail(sDenpyouCode:String):Boolean;
  public
    { Public declarations }
    function MakeSDenpyouCode(iShiiresakiCode:integer; sNyuukabi:String): string;
  end;

  const
  //グリッドの列の定義
  CintNum           = 0;
  CintItemCode1     = 1;
  CintItemCode2     = 2;
  CintItemName      = 3;
  CintItemYomi      = 4;
  CintItemMemo      = 5;
  CintItemKikaku    = 6;
  CintItemIrisuu    = 7;
  CintItemCount     = 8;
  CintItemTanni     = 9;
  CintItemTanka     = 10;
  CintItemShoukei   = 11;
  //CintItemHindo     = 12;
  CintShouhinJouken = 12; //追加　商品条件
  CintItemSaishuu   = 13;
  CintBikou         = 14;
  CintEnd           = 15;   //ターミネーター

  var
  frmNyuuko2: TfrmNyuuko1;
  curSum      : Currency;

implementation
uses
 Inter, HKLib;


{$R *.dfm}

procedure TfrmNyuuko1.FormCreate(Sender: TObject);
begin
  Width := 750;
  Height := 390;
end;

procedure TfrmNyuuko1.FormActivate(Sender: TObject);
var
  i : integer;

begin
  lShiiresakiName.Caption  := GShiiresakiName;
  lNyuuryokusha.Caption   := GNyuuryokusha2;
  lDTP1.Caption := GDate2;
  EditGatsubun.Text := GGatsubun2;

  // StringGridを作る．
  MakeSG;

  // MakeSGでセットしたStringGridをソートする．
  SG1 := HKLib.SGSort(SG1, CintNum);

  // ソート後のStringGridに連番をつける．
 	For i := 0 to SG1.RowCount - 2 do begin
  	SG1.Cells[CintNum, i+1] := IntToStr(i+1);
  end;
end;

procedure TfrmNyuuko1.MakeSG;
var
	i      : integer;

begin
	with SG1 do begin
    DefaultDrawing   := True;
    RowCount         := GRowCount2;
    ColCount         := CintEnd;
    Align            := alBottom;
    FixedCols        := 0;
    FixedColor       := clYellow;
    Canvas.Font.Size := 11;
    DefaultRowHeight := 20;
    ColWidths[0]     := 64;
    Font.Color       := clNavy;

    ColWidths[CintNum]         := 20;
    Cells[CintNum, 0]          := 'No.';

    ColWidths[CintItemCode1]:= 0;
    Cells[CintItemCode1, 0] := '商品コード1';

    ColWidths[CintItemCode2]:= 0;
    Cells[CintItemCode2, 0] := '商品コード2';

    ColWidths[CintItemName]:= 100;
    Cells[CintItemName, 0] := '商品名';

    ColWidths[CintItemYomi]:= 0;
    Cells[CintItemYomi, 0] := '商品読み';

    ColWidths[CintItemKikaku]:= 0;
    Cells[CintItemKikaku, 0] := '規格';      // このデータは下記へ統合された．

    ColWidths[CintItemIrisuu]:= 60;
    Cells[CintItemIrisuu, 0] := '規格';      // 旧入り数が規格へ変更された．

    ColWidths[CintItemSaishuu]:= 80;
    Cells[CintItemSaishuu, 0] := '最終入荷日';

    ColWidths[CintItemCount]:= 120;
    Cells[CintItemCount, 0] := '数量';

    ColWidths[CintItemCount]:= 30;
    Cells[CintItemTanni, 0] := '単位';

    ColWidths[CintItemTanka]:= 60;
    Cells[CintItemTanka, 0] := '単価';

    ColWidths[CintItemShoukei]:= 60;
    Cells[CintItemShoukei, 0] := '小計';

    ColWidths[CintBikou]:= 120;
    Cells[CintBikou, 0] := '備考';


    curSum := 0.0;

    // もともと Rowcount - 1 が行数．タイトル分を考えて RowCount - 2迄となる
    	For i := 0 to RowCount - 2 do begin
    		with SG1 do begin
      		Cells[CintNum,        i+1] := GArray2[i, CintNum];
    			Cells[CintItemCode1,  i+1] := GArray2[i, CintItemCode1];
  		  	Cells[CintItemCode2,  i+1] := GArray2[i, CintItemCode2];
  		  	Cells[CintItemName,   i+1] := GArray2[i, CintItemName];
  		   	Cells[CintItemYomi,   i+1] := GArray2[i, CintItemYomi];
  			  Cells[CintItemKikaku, i+1] := GArray2[i, CintItemKikaku];
  			  Cells[CintItemIrisuu, i+1] := GArray2[i, CintItemIrisuu];
  			  Cells[CintItemSaishuu,i+1] := GArray2[i, CintItemSaishuu];
  			  Cells[CintItemCount,  i+1] := GArray2[i, CintItemCount];
  			  Cells[CintItemTanni,  i+1] := GArray2[i,CintItemTanni];
  			  Cells[CintItemTanka,  i+1] := GArray2[i,CintItemTanka];
  			  Cells[CintItemShoukei,i+1] := GArray2[i,CintItemShoukei];
          //2006.04.23
          Cells[CintBikou,i+1] := GArray2[i,CintBikou];
          curSum := curSum + StrToCurr(Cells[CintItemShoukei, i+1]);
      	end;
      end;

      lSumShoukei.Caption := CurrToStr(curSum);

      //2014.04.01 税対応
      if lDtp1.caption < CsChangeTaxDate then begin
          lSumGoukei.Caption  := CurrToStr(curSum+curSum * CsOldTaxRate);
          CTax.Text := CurrToStr(curSum * CsOldTaxRate);
      end else begin
          lSumGoukei.Caption  := CurrToStr(curSum+curSum * CsTaxRate);
          CTax.Text := CurrToStr(curSum * CsTaxRate);
      end;
  end;

end;

//登録ボタンが押された
procedure TfrmNyuuko1.BitBtn2Click(Sender: TObject);
var
 sNyuukabi, sDenpyouCode : String;
 iShiiresakiCode : Integer;
begin
  //確認メッセージ
  if MessageDlg('登録しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;

  sNyuukabi := lDtp1.caption;
  sDenpyouCode := MakeSDenpyouCode(StrToInt(GShiiresakiCode), sNyuukabi);
  Showmessage(sDenpyouCode);

  // tblTDenpyouDetail へデータを登録
  if InserttblTShiire(sDenpyouCode) = false then begin
    Showmessage('入庫入力の登録に失敗しました->' + sDenpyouCode);
    exit;
  end;
  if InserttblTShiireDetail(sDenpyouCode) = false then begin
    Showmessage('入庫入力（明細）の登録に失敗しました->' + sDenpyouCode);
    exit;
  end;

  Beep();
  Showmessage('登録しました->' + sDenpyouCode);
  //2006.04.23
  BitBtn2.Font.Color := clRed;

end;

//登録
//tblTShiire
function TfrmNyuuko1.InserttblTShiire(sDenpyouCode:String):Boolean;
var
  sSql : String;
begin
try
  // 更新の場合も，新規の場合もまず伝票番号のレコードをtblTDenpyouDetailから削除する．
  // その後，データを挿入する．
  sSql := 'DELETE FROM ' + CtblTShiire;
  sSql := sSql + ' WHERE SDenpyouCode = ' + '''' + sDenpyouCode + '''';
  with QueryDelete do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    ExecSql;
    Close;
  end;

  //add utada 2013.10.15
  if Label18.Caption <> '' then begin

    sSql := 'DELETE FROM ' + CtblTShiire;
    sSql := sSql + ' WHERE InfoDenpyouNo = ' + '''' + Label18.Caption + '''';
    with QueryDelete do begin
      Close;
      Sql.Clear;
      Sql.Add(sSql);
      ExecSql;
      Close;
    end;

  end;


  //登録
  sSql := 'Insert into ' + CtblTShiire;
  sSql := sSql + '(';
  sSql := sSql + 'ShiiresakiCode,';
  sSql := sSql + 'SDenpyouCode,';
  sSql := sSql + 'Gatsubun,';
  sSql := sSql + 'InputDate,';
  sSql := sSql + 'DDate,';
  sSql := sSql + 'DenpyouKingaku,';
  sSql := sSql + 'TAX,';
  sSql := sSql + 'Memo,';
  sSql := sSql + 'Flag,';
  sSql := sSql + 'NyukinStatus,';
  sSql := sSql + 'Nyuuryokusha,';
  if Label18.Caption <> '' then begin
	sSql := sSql + ' InfoDenpyouNo, ';
  end;
  sSql := sSql + 'Bikou ';
  sSql := sSql + ') Values (';
  sSql := sSql + GShiiresakiCode  + ',';
  sSql := sSql + '''' + sDenpyouCode + '''' + ',';
  sSql := sSql + '''' + lDTP1.Caption + '''' + ','; //月分
  sSql := sSql + '''' + DateToStr(Date()) + ''''  + ','; //入力日
  sSql := sSql + '''' + lDTP1.Caption + '''' + ','; //納品日
  sSql := sSql + lSumGoukei.Caption + ',';
  sSql := sSql + CTAX.Text + ',';
  sSql := sSql + '''' + MemoBikou.Text + '''' + ',';
  //フラグ　　仕入れ=0 支払い=1
  sSql := sSql + '0' + ',';
  sSql := sSql + '0' + ',';
  sSql := sSql + ''''  + lNyuuryokusha.Caption + ''',';  //入力者
  if Label18.Caption  <> '' then begin
	sSql := sSql + '''' + Label18.Caption    + ''', ' ;
  end;  
  sSql := sSql + '''''';  //備考
  sSql := sSql + ')';

  with QueryInsert do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    ExecSql;
    Close;
  end;
  Result := true;

except
  on E: EDBEngineError do begin
    //dabDenpyou.Rollback;
    ShowMessage(E.Message);
    Result := False;
  end;
  end;
end;


//登録
//tblTShiireDetail
//消費税の行も挿入する。
function TfrmNyuuko1.InserttblTShiireDetail(sDenpyouCode:String):Boolean;
var
  sSql, strNum, strCode1, strCode2 : String;
  strSuuryou, strTannka, strShoukei,strUpdateDate : String;
  strBikou : String;
  i : Integer;
begin
try
  // 更新の場合も，新規の場合もまず伝票番号のレコードをtblTDenpyouDetailから削除する．
  // その後，データを挿入する．
  sSql := 'DELETE FROM ' + CtblTShiireDetail;
  sSql := sSql + ' WHERE SDenpyouCode = ' + '''' + sDenpyouCode + '''';
  with QueryDelete do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    ExecSql;
    Close;
  end;


  //add utada 2013.10.15
  if Label18.Caption <> '' then begin

    sSql := 'DELETE FROM ' + CtblTShiireDetail;
    sSql := sSql + ' WHERE InfoDenpyouNo = ' + '''' + Label18.Caption + '''';
    with QueryDelete do begin
      Close;
      Sql.Clear;
      Sql.Add(sSql);
      ExecSql;
      Close;
    end;

  end;

  //登録
  with SG1 do begin
    i := 0;
  	while Cells[CintItemCode1,i+1]<>'' do begin
      // データ作成（個別部分)
      strNum        := Cells[CintNum,        i+1];
      strCode1      := Cells[CintItemCode1,  i+1];
      strCode2      := Cells[CintItemCode2,  i+1];
      strSuuryou    := Cells[CintItemCount,  i+1];
      strTannka     := Cells[CintItemTanka,  i+1];
      strShoukei    := Cells[CintItemShoukei,i+1];
      strUpdateDate := DateTimeToStr(Now);
      //2006.04.23
      strBikou      := Cells[CintBikou,i+1];

      // SQL文作成
	    sSql := 'INSERT INTO ' + CtblTShiireDetail;
      sSql := sSql + ' (';
      sSql := sSql + 'SDenpyouCode,';
      sSql := sSql + 'ShiiresakiCode,';
      sSql := sSql + 'NouhinDate,';
      sSql := sSql + 'NyuuryokuDate,';
      sSql := sSql + 'Hyoujijun,';
      sSql := sSql + 'Code1,';
      sSql := sSql + 'Code2,';
      sSql := sSql + 'Suuryou,';
      sSql := sSql + 'ShiireKingaku,';
      sSql := sSql + 'Shoukei,';
      sSql := sSql + 'UpdateDate,';
      sSql := sSql + 'Memo,';
	  if Label18.Caption <> '' then begin
		sSql := sSql + ' InfoDenpyouNo, ';
	  end;	  
      sSql := sSql + 'ShiireShiharaiFlg';
      sSql := sSql + ') VALUES ( ';
      sSql := sSql + '''' + sDenpyouCode + ''', ' ;
      sSql := sSql +        GShiiresakiCode  +   ', ' ;
      sSql := sSql + '''' + lDTP1.Caption + '''' + ','; //納品日
      sSql := sSql + '''' + DateToStr(Date()) + ''''  + ','; //入力日
      sSql := sSql +        strNum             + ', ' ; //表示順
      sSql := sSql + '''' + strCode1           + ''', ' ;
      sSql := sSql + '''' + strCode2           + ''', ' ;
      sSql := sSql +        strSuuryou         + ', ' ;
      sSql := sSql +        strTannka          + ', ' ;
      sSql := sSql +        strShoukei         + ', ' ;
      sSql := sSql + '''' + strUpdateDate      + ''', ' ;
      sSql := sSql + '''' + strBikou           + ''', ' ;
	  if Label18.Caption  <> '' then begin
		sSql := sSql + '''' + Label18.Caption    + ''', ' ;
	  end;	  
      sSql := sSql +        '0'                + ' )' ;

      // SQL文実行
      with QueryInsert do begin
  		 	Close;
	      Sql.Clear;
  	  	Sql.Add(sSql);
  		  ExecSql;
	      Close;
      end;

      i := i + 1;

    end;//of while
  end;//of with

  //消費税の行を追加
  //
{ 2006.05.04 廃止
  sSql := 'INSERT INTO ' + CtblTShiireDetail;
  sSql := sSql + ' (';
  sSql := sSql + 'SDenpyouCode,';
  sSql := sSql + 'ShiiresakiCode,';
  sSql := sSql + 'NouhinDate,';
  sSql := sSql + 'NyuuryokuDate,';
  sSql := sSql + 'Hyoujijun,';
  sSql := sSql + 'Code1,';
  sSql := sSql + 'Code2,';
  sSql := sSql + 'Suuryou,';
  sSql := sSql + 'ShiireKingaku,';
  sSql := sSql + 'Shoukei,';
  sSql := sSql + 'UpdateDate,';
  sSql := sSql + 'ShiireShiharaiFlg';
  sSql := sSql + ') VALUES ( ';
  sSql := sSql + '''' + sDenpyouCode + ''', ' ;
  sSql := sSql +        GShiiresakiCode  +   ', ' ;
  sSql := sSql + '''' + lDTP1.Caption + '''' + ','; //納品日
  sSql := sSql + '''' + DateToStr(Date()) + ''''  + ','; //入力日
  sSql := sSql +        '0'             + ', ' ; //表示順
  sSql := sSql + '''' + '0'           + ''', ' ;   //Code1
  sSql := sSql + '''' + '0'           + ''', ' ;   //Code2
  sSql := sSql + '''' + '0'         + ''', ' ;
  sSql := sSql + '''' + '0'         + ''', ' ;
  sSql := sSql +        CTax.Text   + ', ' ;
  sSql := sSql + '''' + strUpdateDate      + ''', ' ;
  sSql := sSql +        '2'                + ' )' ;   //消費税は2

  // SQL文実行
      with QueryInsert do begin
  		 	Close;
	      Sql.Clear;
  	  	Sql.Add(sSql);
  		  ExecSql;
	      Close;
      end;
}

  Result := true;

except
  on E: EDBEngineError do begin
    //dabDenpyou.Rollback;
    ShowMessage(E.Message);
    Result := False;
  end;
end; //of try

end;

//仕入れ伝票番号の作成
//2006.03.19
//仕入先コード＋入荷日＋000
//001         + 060301+001 = 001060301001

function TfrmNyuuko1.MakeSDenpyouCode(iShiiresakiCode:integer; sNyuukabi:String):String;
var
  sYyyy, sMm, sDd, sDenpyouCode : String;
  sNum, sYyMmDd, sYyMmDd2, sShiiresakiCode, sSql, sTmp:String;
  i, inum : Integer;
begin
  sYyyy := Copy(sNyuukabi,0,4);
  sMm := Copy(sNyuukabi,6,2);
  sDd := Copy(sNyuukabi,9,2);
  sNyuukabi := sYyyy + sMm + sDd;

  sShiiresakiCode := IntToStr(iShiiresakiCode);
  if length(sShiiresakiCode)=1 then begin
    sShiiresakiCode := '00' + sShiiresakiCode;
  end else if length(sShiiresakiCode)=2 then begin
    sShiiresakiCode := '0' + sShiiresakiCode;
  end;

  sSql := 'Select m=MAX(SDenpyouCode) from ' + CtblTShiire;
  sSql := sSql + ' where ShiiresakiCode=' +  sShiiresakiCode;
  sSql := sSql + ' and DDate=''' +  lDtp1.Caption + '''';
  with Query1 do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    open;
    sTmp := FieldByName('m').asString;
    Close;
  end;
  if sTmp='' then begin
    sDenpyouCode := sShiiresakiCode + sNyuukabi + '001';
  end else begin
    sYyMmDd := Copy(sTmp, 4, 6);
    sNum    := Copy(sTmp, 12, 3);
    iNum := StrToInt(sNum) + 1;
    if iNum > 999 then begin
      ShowMessage('伝票番号がオーバーしました。管理者に連絡してください');
      exit;
    end;
    sNum := IntToStr(iNum);
    if length(sNum) = 1 then begin
      sNum := '00' + sNum;
    end else if length(sNum) = 2 then begin
      sNum := '0' + sNum;
    end;
    sDenpyouCode := sShiiresakiCode + sNyuukabi + sNum;
  end;//of if
    Result := sDenpyouCode;
end;


procedure TfrmNyuuko1.CTaxChange(Sender: TObject);
var
  curTax      : Currency;
begin
  curTax := StrToCurr(CTax.Text);
  lSumGoukei.Caption := CurrToStr(curSum+curTax);
end;

end.
