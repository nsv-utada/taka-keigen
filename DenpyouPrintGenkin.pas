unit DenpyouPrintGenkin;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Qrctrls, QuickRpt, ExtCtrls, Db, DBTables, jpeg, StdCtrls, QRPrntr, Mask;

type
  TfrmDenpyouPrintGenkin = class(TForm)
    qrpDenpyouPrint: TQuickRep;
    PageHeaderBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRSysData1: TQRSysData;
    qryDenpyouPrint: TQuery;
    qryForDetailBand: TQuery;
    QRLabel7: TQRLabel;
    QRShape7: TQRShape;
    lblTokuisakiCode1: TQRLabel;
    lblTokuisakiCode2: TQRLabel;
    lblTokuisakiName: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    lblDenpyouBanngou: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRImage1: TQRImage;
    lblYear: TQRLabel;
    lblMonth: TQRLabel;
    QRLabel20: TQRLabel;
    lblDate: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRShape13: TQRShape;
    QRLabel13: TQRLabel;
    DetailBand1: TQRBand;
    SummaryBand1: TQRBand;
    QRLabel6: TQRLabel;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape6: TQRShape;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    qdtDetail0: TQRDBText;
    qdtDetail1: TQRDBText;
    qdtDetail2: TQRDBText;
    qdtDetail3: TQRDBText;
    qdtDetail4: TQRDBText;
    qdtDetail4_1: TQRDBText;
    qdtDetail6: TQRDBText;
    qdtDetail13: TQRDBText;
    qdtGoukei: TQRDBText;
    QRShape18: TQRShape;
    QRLabel18: TQRLabel;
    QRShape19: TQRShape;
    QRShape20: TQRShape;
    QRLabel19: TQRLabel;
    QRLabel21: TQRLabel;
    qdtShouhizei: TQRDBText;
    qdtGoukei1: TQRDBText;
    qdtGoukei2: TQRDBText;
    QRShape21: TQRShape;
    QRLabel25: TQRLabel;
    lblBikou: TQRLabel;
    lblTokuisakiNameUp: TQRLabel;
    qryForBikou: TQuery;
    QRExprMemo2: TQRExprMemo;
    QRLabel4: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    qdtDetail4_Tax: TQRDBText;
    QRShape5: TQRShape;
    QRShape12: TQRShape;
    QRLabel5: TQRLabel;
    QRLabel10: TQRLabel;
    qdtShouhizei10: TQRDBText;
    qdtGoukei10: TQRDBText;
    qryDenpyou10Print: TQuery;
    qryDenpyouSumPrint: TQuery;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure MakeDenpyouGenkin(strDenpyouBanngou: String);
  private
    { Private 宣言 }
  public
    { Public 宣言 }
  end;

var
  frmDenpyouPrintGenkin   : TfrmDenpyouPrintGenkin;
  strDenpyouBanngou       : String;

implementation

uses Inter, Denpyou2;

{$R *.DFM}

procedure TfrmDenpyouPrintGenkin.FormCreate(Sender: TObject);
var
 LArrayOfDenpyouBanngou : array of String;  // 伝票番号のローカル保存用のリスト
 i,j                    : Integer;

begin

 // 伝票番号グローバル配列からローカル配列へコピー
 //　　他の印刷作業とダブって，作業中にグローバル変数が上書きされるといけないので，
 //    まずグローバルからローカルへ値を保存しておく．
 SetLength(LArrayOfDenpyouBanngou, Length(GArrayOfDenpyouBanngou));
 For i:=0 to Length(GArrayOfDenpyouBanngou)-1 do
   begin
     LArrayOfDenpyouBanngou[i] := GArrayOfDenpyouBanngou[i];
   end;

//
// 伝票番号のリストが尽きる迄,印刷を行う．
//

 For i:=0 to Length(LArrayOfDenpyouBanngou)-1 do begin

   // 伝票番号セット
   strDenpyouBanngou := LArrayOfDenpyouBanngou[i];

   // 伝票作成
   MakeDenpyouGenkin(strDenpyouBanngou);


   // １つの掛伝票につき，2枚印刷する．
   For j:=1 to 2 do begin

    // 1枚目：受領書　2枚目：納品書

      // 使用するプリンタをデフォルトのプリンタに設定（実はこれなくてもいいかも)
      qrpDenpyouPrint.PrinterSettings.PrinterIndex := -1;

      // 1枚目印刷　納品証
      if j=1 then
        begin
          QRLabel7.Caption := '納　　　品      書';
          qrpDenpyouPrint.PrinterSettings.OutputBin := Envelope;
          QRLabel25.Caption := '上記の通り納品致しました．';
          qrpDenpyouPrint.Print;
        end
      // 2枚目印刷　領収証
      else if j=2 then
        begin
          QRLabel7.Caption := '領　　　収　　　書';
          qrpDenpyouPrint.PrinterSettings.OutputBin := Lower;
          QRLabel25.Caption := '上記の通り領収致しました．';
          qrpDenpyouPrint.Print;
        end
      else
        begin
          ShowMessage('エラー発生');
        end;

   end;

 end;

end;


procedure TfrmDenpyouPrintGenkin.FormDestroy(Sender: TObject);
begin

 // リソースの開放
 qryDenpyouPrint.Close;
 qryForDetailBand.Close;

end;


procedure TfrmDenpyouPrintGenkin.MakeDenpyouGenkin(strDenpyouBanngou: String);
var
 sSql                   : String;
 strTokuisakiCode1      : String;
 strDenpyouDate         : String;
 strTokuisakiCode2      : String;
 strTokuisakiName       : String;
 strTokuisakiNameUp     : String;
 sYear,sMonth,sDay      : String;
 wY,wM,wD               : Word;
 curUriageKingaku       : Currency;
 curShouhizei           : Currency;
 curGoukei              : Currency;
 strBikou               : String;
 S                      : String;
 douTax                 : Double;   //2014.04.01 消費税対応
 newTax                 : Double;

begin

 // 伝票番号セット
 lblDenpyouBanngou.Caption := strDenpyouBanngou;

 // 得意先Code1, 伝票日付, 得意先Code2，得意先名 取得

// #thuyptt 20190313
// sSql := 'SELECT D.TokuisakiCode1 TokuisakiCode1, D.DenpyouDate DenpyouDate';
// sSql := sSql + ', T.TokuisakiCode2 TokuisakiCode2';
// sSql := sSql + ', T.TokuisakiNameUp TokuisakiNameUp, T.TokuisakiName TokuisakiName FROM ';
// sSql := sSql + CtblTDenpyouDetail + ' D, ' + CtblMTokuisaki + ' T';
// sSql := sSql + ' WHERE D.TokuisakiCode1 = T.TokuisakiCode1';
// sSql := sSql + ' AND D.DenpyouCode = ' + '''' + strDenpyouBanngou + '''';

 sSql := 'SELECT D.TokuisakiCode1 AS TokuisakiCode1, D.DenpyouDate AS DenpyouDate';
 sSql := sSql + ', T.TokuisakiCode2 AS TokuisakiCode2';
 sSql := sSql + ', T.TokuisakiNameUp AS TokuisakiNameUp, T.TokuisakiName AS TokuisakiName FROM ';
 sSql := sSql + CtblTDenpyouDetail + ' AS D, ' + CtblMTokuisaki + ' AS T';
 sSql := sSql + ' WHERE D.TokuisakiCode1 = T.TokuisakiCode1';
 sSql := sSql + ' AND D.DenpyouCode = ' + '''' + strDenpyouBanngou + '''';
  with qryDenpyouPrint do begin
	 	Close;
    Sql.Clear;
  	Sql.Add(sSql);
    Open;
    strTokuisakiCode1 := FieldbyName('TokuisakiCode1').AsString;
    strDenpyouDate    := FieldbyName('DenpyouDate').AsString;
    strTokuisakiCode2 := FieldbyName('TokuisakiCode2').AsString;
    strTokuisakiNameUp:= FieldbyName('TokuisakiNameUp').AsString;
    strTokuisakiName  := FieldbyName('TokuisakiName').AsString;
    Close;
  end;

 // 得意先Code1, 得意先Code2，得意先名 セット （得意先コードは4桁にしている)
 lblTokuisakiCode1.Caption := FormatFloat('0000', StrToInt(strTokuisakiCode1));
 lblTokuisakiCode2.Caption := strTokuisakiCode2;
 lblTokuisakiName.Caption  := strTokuisakiName;
 lblTokuisakiNameUp.Caption:= strTokuisakiNameUp;

 // 伝票日付セット
 DecodeDate(StrToDate(strDenpyouDate), wY, wM, wD);
 //sYear  := IntToStr(wY-1988); // 平成へ変換
 sYear  := IntToStr(wY); // 平成へ変換
 sMonth := IntToStr(wM);
 sDay   := IntToStr(wD);

 lblYear.Caption  := sYear;
 lblMonth.Caption := sMonth;
 lblDate.Caption  := sDay;

  //2014.04.01 消費税対応 begin
 if strDenpyouDate < CsChangeTaxDate then begin
    douTax :=  CsOldTaxRate * 100;
 end else begin
    douTax :=  CsTaxRate * 100;
 end;

  newTax := CsNewTaxRate * 100;

 //QRLabel21.Caption   :=  '消費税' + IntToStr(Round(douTax)) + '%';
 //2014.04.01 消費税対応 end

 
 // 明細行セット
// #thuyptt 20190313
// sSql := 'SELECT DD.No No, MI.Name Name, MI.Kikaku Kikaku, MI.Irisuu Irisuu, ';
// sSql := sSql + 'DD.Suuryou Suuryou, MI.Tanni Tanni, DD.Tannka Tannka, DD.Shoukei Shoukei FROM ';
// sSql := sSql + CtblTDenpyouDetail + ' DD INNER JOIN ' + CtblMItem;
// sSql := sSql + '  MI ON DD.Code1 = MI.Code1 AND DD.Code2 = MI.Code2';
// sSql := sSql + ' WHERE DD.DenpyouCode =' + '''' + strDenpyouBanngou + '''';
// sSql := sSql + ' ORDER BY No';

 sSql := 'SELECT DD.No AS No, MI.Name AS Name, MI.Kikaku AS Kikaku, MI.Irisuu AS Irisuu, ';
 sSql := sSql + 'DD.Suuryou AS Suuryou, MI.Tanni AS Tanni, DD.Tannka AS Tannka, DD.Shoukei AS Shoukei, DD.tax AS tax FROM ';
 sSql := sSql + CtblTDenpyouDetail + ' AS DD INNER JOIN ' + CtblMItem;
 sSql := sSql + '  AS MI ON DD.Code1 = MI.Code1 AND DD.Code2 = MI.Code2';
 sSql := sSql + ' WHERE DD.DenpyouCode =' + '''' + strDenpyouBanngou + '''';
 sSql := sSql + ' ORDER BY [No] ASC';
  with qryForDetailBand do begin
	 	Close;
    Sql.Clear;
  	Sql.Add(sSql);
    Open;
  end;

  qdtDetail0.DataField  := 'No';
  qdtDetail1.DataField  := 'Name';
  qdtDetail2.DataField  := 'Kikaku';
  qdtDetail3.DataField  := 'Irisuu';
  qdtDetail4.DataField  := 'Suuryou';
  qdtDetail4_1.DataField:= 'Tanni';
  qdtDetail6.DataField  := 'Tannka';
  qdtDetail13.DataField := 'Shoukei';
  qdtDetail4_Tax.DataField := 'tax';


 // 合計セット
   // labelではMaskプロパティを使えないので \#,##と指定して値を整形できない．
   // しょうがなく，DBTextを使用している．
 //sSql := 'SELECT Shoukei, Shouhizei, Goukei FROM ' + CtblTGennkinnKaishuu;
 //sSql := sSql + ' WHERE DenpyouCode =' + '''' + strDenpyouBanngou + '''';
 
 sSql := 'SELECT SUM(Shoukei) AS Shoukei8, (SUM(Shoukei)*' + floattostr(CsTaxRate) + ') AS Shouhizei FROM ' + CtblTDenpyouDetail;
 sSql := sSql + ' WHERE DenpyouCode =' + '''' + strDenpyouBanngou + '''';
 sSql := sSql + ' AND tax =' + IntToStr(Round(douTax));
 sSql := sSql + ' GROUP BY DenpyouCode, tax';

  with qryDenpyouPrint do begin
	 	Close;
    Sql.Clear;
  	Sql.Add(sSql);
    Open;
  end;

//  qdtGoukei.DataField    := 'Shoukei';
//  qdtShouhizei.DataField := 'Shouhizei';
//  qdtGoukei1.DataField   := 'Goukei';
//  qdtGoukei2.DataField   := 'Goukei';

  qdtGoukei.DataField    := 'Shoukei8';
  qdtShouhizei.DataField := 'Shouhizei';

 sSql := 'SELECT SUM(Shoukei) AS Shoukei10, (SUM(Shoukei)*' + floattostr(CsNewTaxRate) + ') AS Shouhizei10 FROM ' + CtblTDenpyouDetail;
 sSql := sSql + ' WHERE DenpyouCode =' + '''' + strDenpyouBanngou + '''';
 sSql := sSql + ' AND tax =' + IntToStr(Round(newTax));
 sSql := sSql + ' GROUP BY DenpyouCode, tax';

  with qryDenpyou10Print do begin
	 	Close;
    Sql.Clear;
  	Sql.Add(sSql);
    Open;
  end;

  qdtGoukei10.DataField    := 'Shoukei10';
  qdtShouhizei10.DataField := 'Shouhizei10';

 sSql := 'SELECT SUM(tab.Goukei8 + tab.Goukei10) AS Goukei  FROM';
 sSql := sSql + ' (';
 sSql := sSql + ' SELECT ROUND((SUM(Shoukei)+(SUM(Shoukei)*' + floattostr(CsTaxRate) + ')), 0) AS Goukei8, 0 AS Goukei10 FROM ' + CtblTDenpyouDetail;
 sSql := sSql + ' WHERE DenpyouCode =' + '''' + strDenpyouBanngou + '''';
 sSql := sSql + ' AND tax =' + IntToStr(Round(douTax));
 sSql := sSql + ' GROUP BY DenpyouCode, tax';
 sSql := sSql + ' UNION ';
 sSql := sSql + ' SELECT 0 AS Goukei8, ROUND((SUM(Shoukei)+(SUM(Shoukei)*' + floattostr(CsNewTaxRate) + ')), 0) AS Goukei10 FROM ' + CtblTDenpyouDetail;
 sSql := sSql + ' WHERE DenpyouCode =' + '''' + strDenpyouBanngou + '''';
 sSql := sSql + ' AND tax =' + IntToStr(Round(newTax));
 sSql := sSql + ' GROUP BY DenpyouCode, tax';
 sSql := sSql + ' ) AS tab';

  with qryDenpyouSumPrint do begin
	 	Close;
    Sql.Clear;
  	Sql.Add(sSql);
    Open;
  end;
  
  qdtGoukei1.DataField   := 'Goukei';
  qdtGoukei2.DataField   := 'Goukei';

 // 備考セット
 sSql := 'SELECT Bikou FROM ' + CtblTDenpyou;
 sSql := sSql + ' WHERE DenpyouCode =' + '''' + strDenpyouBanngou + '''';
  with qryForBikou do begin
	 	Close;
    Sql.Clear;
  	Sql.Add(sSql);
    Open;
    strBikou  := FieldbyName('Bikou').AsString;
	 	Close;
  end;

  lblBikou.Caption     := '備考：' + strBikou;


 // 鷹の絵のセット
 begin
   S := '.\logo.bmp';
   try
   QRImage1.Picture.LoadFromFile(S);
   except
     on EInvalidGraphic do
       QRImage1.Picture.Graphic := nil;
   end;
 end;

end;

end.
