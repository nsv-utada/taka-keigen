inherited frmShukko: TfrmShukko
  Left = 576
  Top = 75
  VertScrollBar.Range = 0
  BorderStyle = bsSingle
  Caption = #20986#24235#21360#21047
  ClientHeight = 529
  ClientWidth = 586
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  inherited Panel1: TPanel
    Width = 586
    inherited Label1: TLabel
      Width = 142
      Height = 16
      Caption = #20986#24235#34920#21360#21047'(Tokyo)'
      Font.Height = -16
    end
    inherited Label2: TLabel
      Left = 196
      Top = 20
    end
  end
  inherited Panel2: TPanel
    Top = 476
    Width = 586
    Height = 35
    inherited BitBtn1: TBitBtn
      Left = 317
      Height = 23
      Cancel = True
      Kind = bkCustom
    end
    inherited BitBtn2: TBitBtn
      Left = 220
      Width = 90
      Height = 23
      Caption = #20986#24235#34920#21360#21047
      OnClick = BitBtn2Click
      Glyph.Data = {00000000}
      Kind = bkCustom
    end
  end
  inherited Panel3: TPanel
    Width = 586
    Height = 435
    object Label3: TLabel
      Left = 22
      Top = 42
      Width = 50
      Height = 13
      Caption = #12467#12540#12473' '#65306
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 334
      Top = 44
      Width = 44
      Height = 13
      Caption = #12501#12525#12450#65306
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 266
      Top = 88
      Width = 16
      Height = 15
      Caption = #65286
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -15
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 22
      Top = 18
      Width = 50
      Height = 13
      Caption = #32013#21697#26085#65306
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object chkB1F: TCheckBox
      Left = 400
      Top = 42
      Width = 45
      Height = 17
      Alignment = taLeftJustify
      Caption = 'B1'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object chk1F: TCheckBox
      Left = 400
      Top = 61
      Width = 45
      Height = 17
      Alignment = taLeftJustify
      Caption = #65297'F'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
    object chk2F: TCheckBox
      Left = 400
      Top = 79
      Width = 45
      Height = 17
      Alignment = taLeftJustify
      Caption = '2F'
      Checked = True
      State = cbChecked
      TabOrder = 2
    end
    object chk3F: TCheckBox
      Left = 400
      Top = 98
      Width = 45
      Height = 17
      Alignment = taLeftJustify
      Caption = '3F'
      Checked = True
      State = cbChecked
      TabOrder = 3
    end
    object chkAll: TCheckBox
      Left = 90
      Top = 44
      Width = 130
      Height = 13
      Caption = #20840#12467#12540#12473#21512#35336
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = chkAllClick
      OnMouseDown = chkAllMouseDown
    end
    object dtpNouhinDate: TDateTimePicker
      Left = 76
      Top = 12
      Width = 110
      Height = 20
      Date = 37297.655360127300000000
      Time = 37297.655360127300000000
      TabOrder = 5
    end
  end
  inherited SB1: TStatusBar
    Top = 511
    Width = 586
    Height = 18
  end
  object qryShukko: TQuery
    DatabaseName = 'taka'
    Left = 360
    Top = 201
  end
  object Query1: TQuery
    DatabaseName = 'taka'
    Left = 432
    Top = 193
  end
end
