unit PBill;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, StdCtrls, Buttons, ExtCtrls, ComCtrls, DBTables, ShellAPI, Db,DBXpress,Grids, SqlExpr;

type
 TfrmPBill = class(TfrmMaster)
//  TfrmPBill = class(TForm) 
    DTP1: TDateTimePicker;
    Shape1: TShape;
    Shape2: TShape;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EditTokuisakiCode1: TEdit;
    Label6: TLabel;
    LabelTokuisakiName: TLabel;
    Label8: TLabel;
    CBYyyy: TComboBox;
    BitBtn3: TBitBtn;
    Label9: TLabel;
    CBMm: TComboBox;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    CBDd: TComboBox;
    Label13: TLabel;
    Panel4: TPanel;
    Label7: TLabel;
    DTPFrom: TDateTimePicker;
    DTPTo: TDateTimePicker;
    CheckBox1: TCheckBox;
    Label15: TLabel;
    Label14: TLabel;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    qrySeikyuu: TQuery;
    EditTantoushaCode: TEdit;
    Label24: TLabel;
    EditShuukeiCode: TEdit;
    Label16: TLabel;
    Label17: TLabel;
    EditTokuisakiCode2: TEdit;
    dabBill: TDatabase;
    qryNyuukin: TQuery;
    qryClaim: TQuery;
    QueryZandaka: TQuery;

    procedure FormCreate(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure EditTokuisakiCode1Exit(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private 宣言 }
		//Function GetBillTarm(sTokuisakiCode1:String; Var sDateFrom, sDateTo : String) : Boolean;
	 	function MakeTokuisakiBill(sTokuisakiCode1:String ; iZanOnOff:Integer) : Boolean;
	 	function MakeTokuisakiBill2(sTokuisakiCode1:String ; iZanOnOff:Integer) : Boolean;
		function GetTokuisakiCodeArray() : Boolean;
		function GetNyuukinKigenAndShimebi(sTokuisakiCode:String; sGatsubunText:String) : Boolean;

      

    function GetZenkaiSeikyuu(sTokuisakiCode1, sGatsubun: String):Integer;
		function GetNyuu(sTokuisakiCode1, sFrom, sTo: String):Integer;
		function GetKonkaiUrige(sTokuisakiCode1, sFrom, sTo: String):Integer;
		function GetShouhizeiGaku(sTokuisakiCode1, sGatsubun:String):Integer;

  public
    { Public 宣言 }
  end;


Function GetShimebi(sTokuisakiCode1:String) : Integer;
Function GetRuikeiZandaka(sTokuisakiCode1, sDateFrom, sDateTo:String):Integer;
Function GetDeliveryFee(sTokuisakiCode1, sDateFrom, sDateTo:String):Integer;           //thuyptt 2019/05/14
//procedure InsertCTax2(MyQuery:TQuery; sYyyyMmDd : String);
//procedure InsertCTax(sYyyy, sMm, sDd:String ; iTokuisakiCode1 : Integer);
Function InsertCTax(sYyyy, sMm, sDd:String ; iTokuisakiCode1 : Integer; SB1:TStatusBar) : Boolean;
Function InsertCTax2(sTokuisakiCode1, sYyyyMmDd : String) : Boolean;

//procedure InsertCTax(sYyyy, sMm, sDd:String ; iTokuisakiCode1 : Integer; SB1:TStatusBar) ;
//procedure InsertCTax2(sTokuisakiCode1, sYyyyMmDd : String) ;

function MakeSqlUriageSum(sTokuisakiCode1, sDateFrom, sDateTo : String) : String;
function GetTokuisakiTax(sTokuisakiCode1, sGatsubun,sTargetTax : String) : String;   // 2014.04.01 消費税対応

var
  frmPBill: TfrmPBill;
  DTShimebi, DTShiharaibi : TDateTime;
  strPreviousClaim, strDeposit, strAdjustment, strCarryOver, strCurrentSales, strConsumption, strTotal, strTotalClain: String;
  strDTShimebi, strDTShiharaibi : String;
  GiKungakuSum : Integer;
//  gZenkaiSeikyuu,gNyuukinKingaku :Integer ;

  LArrayOfTokuisakiCodeExport : array[0..9999] of String;

implementation

uses DMMaster, Inter, HKLib, MTokuisaki, DenpyouPrintSeikyuu, TKaishuu;

{$R *.DFM}

procedure TfrmPBill.FormCreate(Sender: TObject);
var
  wYyyy, wMm, wDd      : Word;
  sYear, sMonth, sDay  : String;
begin
  inherited;
  width := 680;
  Height := 480;
  DTP1.Date := Date;
  DTPFrom.Date := Date;
  DTPTo.Date := Date;

   // 年・月を当日の年・月に設定
   DecodeDate(Date(), wYyyy, wMm, wDd);
    sYear  := IntToStr(wYyyy);
   sMonth := IntToStr(wMm);
   sDay   := IntToStr(wDd);

   CBYyyy.Text := sYear;
   //2006.06.22
   if length(sMonth) = 1 then begin
     sMonth := '0' + sMonth;
   end;
   CBMm.Text   := sMonth;

end;


//印刷開始ボタンがクリックされた
//1999/05/04
//
//1999/05/14
//締め日より１ヶ月前までの売上に対して消費税をけいさんして
//１レコードとしてテーブルに挿入する。
//もし既にある場合でも再計算するようにする。
procedure TfrmPBill.BitBtn3Click(Sender: TObject);
var
	sLine, sSql, sTokuisakiCode1, sDateFrom, sDateTo, sTokuisakiCode1Tmp: string;
 	F : TextFile;
  iSum, iSum2, iZanOnOff : integer;
  wYyyy, wMm, wDd : Word;
  sDate,sDd : String;
begin

try
	//確認メッセージ
  if MessageDlg('印刷しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;

  //
  //メッセージを出し、過去のバージョンで作業してもらうように促す。
  //
  if CBDd.Text = '0' then begin
    sDd := '28';
  end else begin
    sDd := CBDd.Text;
  end;
  sDate := CbYyyy.Text + '/' + CBMm.Text + '/' + sDd;
  if StrToDate(sDate) < StrToDate('2004/03/28') then begin
    Showmessage('2004/3/31 以前の請求書は、Ver48にて作業してください');
    Exit;
  end;

	//スタートトランザクション

  //2011.0.20 add utada
 // dabBill.Open;
 // dabBill.StartTransaction;


	//得意先ごとに請求書の計算をする
  if Length(EditTokuisakiCode1.Text) > 0 then
	  if Copy(EditTokuisakiCode1.Text, Length(EditTokuisakiCode1.Text) , 1) <> ',' then
  		EditTokuisakiCode1.Text := EditTokuisakiCode1.Text + ',';

  sTokuisakiCode1 := EditTokuisakiCode1.Text;

  //請求期間の消費税を入力する
  ////すでに消費税の伝票がある場合には上書きする
	SB1.SimpleText := '消費税の計算中 ';

  if (Length(sTokuisakiCode1) > 0) then begin
  	//2000/01/22 得意先コードの複数指定に対応
    while sTokuisakiCode1 <> '' do begin
	    sTokuisakiCode1Tmp := Copy(sTokuisakiCode1, 1, Pos(',', sTokuisakiCode1)-1);

		  CBDd.Text := IntToStr(GetShimebi(sTokuisakiCode1Tmp));
      //2002.09.29
      if CBDd.Text = '99' then begin //閉め日が入力されていない
      	CBDd.Text := '';
      end;

  	  if InsertCTax(CBYyyy.Text, CBMm.Text, CBDd.Text, StrToInt(sTokuisakiCode1Tmp), SB1) = False then
        begin
    	  Showmessage('請求処理に失敗しました。管理者に連絡して下さい。Step1');
      //  dabBill.Rollback;
      //  dabBill.Close;
        exit;
        end
      else
      begin
        sTokuisakiCode1 := Copy(sTokuisakiCode1, (Pos(',', sTokuisakiCode1)+1), Length(sTokuisakiCode1));
	  		SB1.SimpleText := '消費税の計算中 ' + sTokuisakiCode1;   //koko
        SB1.Update;
      end;
    end;//of while
	end else begin
    if InsertCTax(CBYyyy.Text, CBMm.Text, CBDd.Text, 0, SB1) = False then
      begin
    	  Showmessage('請求処理に失敗しました。管理者に連絡して下さい。Step2');
     //   dabBill.Rollback;
     //   dabBill.Close;
        exit;
      end

  end;
	//出力するファイルを作成する、すでにあれば削除する
  //請求明細用
 	AssignFile(F, CFileName_Seikyuu);
  Rewrite(F);
	CloseFile(F);

	SB1.SimpleText := '請求金額計算中 ';
  Update;

  //2000/01/23
  sTokuisakiCode1 := EditTokuisakiCode1.Text;

  if (Length(sTokuisakiCode1) > 0) then begin
  	//2000/01/22 得意先コードの複数指定に対応
    while sTokuisakiCode1 <> '' do begin
	    sTokuisakiCode1Tmp := Copy(sTokuisakiCode1, 1, Pos(',', sTokuisakiCode1)-1);
		  CBDd.Text := IntToStr(GetShimebi(sTokuisakiCode1Tmp));
      //2002.09.29
      if CBDd.Text = '99' then begin //閉め日が入力されていない
      	CBDd.Text := '';
        ShowMessage('閉め日のデータが不正なので処理を中止します');
        exit;
      end;
    	iZanOnOff := MTokuisaki.GetZanOnOff(sTokuisakiCode1Tmp);
			if MakeTokuisakiBill2(sTokuisakiCode1Tmp, iZanOnOff) = False then
      begin
    	  Showmessage('請求処理に失敗しました。管理者に連絡して下さい。Step3');
      //  dabBill.Rollback;
      //  dabBill.Close;
        exit;
      end;
      sTokuisakiCode1 := Copy(sTokuisakiCode1, (Pos(',', sTokuisakiCode1)+1), Length(sTokuisakiCode1));
    end;//of while
  end else begin
  	sSql := 'SELECT TokuisakiCode1, ZanOnOff FROM ' + CtblMTokuisaki;
    sSql := sSql + ' WHERE SeikyuuSimebi = ' + Trim(CBDd.Text);

    //2000/01/22 請求書フラグを見る　１は出さない　０又はヌルは出す。
    sSql := sSql + ' AND ';
    sSql := sSql + ' SeikyuushoFlag <> 1 ';

    //テスト用　2000/09/27
    //sSql := sSql + ' AND ';
    //sSql := sSql + ' TokuisakiCode1 BETWEEN 2630 AND 2691';
    //ここまで

    sSql := sSql + ' AND ';
    sSql := sSql + ' TokuisakiCode1 < 50000 ';

    sSql := sSql + ' ORDER BY InfomartUse,TokuisakiCode1';
    with frmDMMaster.QueryMTokuisaki do begin
	  	Close;
  	  Sql.Clear;
    	Sql.Add(sSql);
	    open;
      while not EOF do begin
				SB1.SimpleText := '請求金額計算中 -> '+FieldByName('TokuisakiCode1').AsString;
        iZanOnOff := FieldByName('ZanOnOff').AsInteger;
        sTokuisakiCode1 := FieldByName('TokuisakiCode1').AsString;
				if MakeTokuisakiBill2(sTokuisakiCode1, iZanOnOff) = False then
        begin
    	    Showmessage('請求処理に失敗しました。管理者に連絡して下さい。Step4');
         // dabBill.Rollback;
        //  dabBill.Close;
          exit;
        end;

      	Next;
      end;//of while
      Close;
    end;//of with
  end;//of if

	//エンドトランザクション
 // dabBill.Commit;
  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', '請求書new.xls', '', SW_SHOW);

except
  on E: EDBEngineError do begin
    ShowMessage('請求処理に失敗しました。管理者に連絡して下さい。' + E.Message);
   // dabBill.Rollback;
  //  dabBill.Close;
  end;
end;


end;


//得意先ごとに請求書の計算をする
//iZanOnOff -> 0 のせない 1 のせる
function TfrmPBill.MakeTokuisakiBill(sTokuisakiCode1:String ; iZanOnOff:Integer):Boolean;
var
	sYuusousaki, sWhere, sTokuisakiCode2, sLine, sSql, sDateFrom, sDateTo, sSum2 : string;
 	F : TextFile;
  iSum, iSum2 : integer;
  wYyyy, wMm, wDd : Word;
  wYyyy2, wMm2, wDd2 : Word;
  //sTokusakiTax,sGatsubun : String; //2014.04.01 消費税対応
  sTokusakiTaxOrg,sTokusakiTaxnew,sGatsubun : String; //2014.04.01 消費税対応
begin
try

  sSql := 'SELECT UriageSum = SUM(UriageKingaku) ';
  sSql := sSql + ' FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';

 	sSql := sSql + 'TokuisakiCode1 = ' + sTokuisakiCode1;
 	sSql := sSql + ' AND ';

  //請求期間の取得
  //請求締め日の１ヶ月前を取得
  if CBDd.Text = '0' then begin //締め日が月末
  	wDd := GetGetsumatsu(StrToInt(CBYyyy.Text), StrToInt(CBMm.Text));
  end else begin
  	wDd := StrToint(CBDd.Text);
  end;
  sDateTo := CBYyyy.Text + '/' + CBMm.Text + '/' + IntToStr(wDd);
	DecodeDate(StrToDate(sDateTo), wYyyy, wMm, wDd);

  //
  if CBDd.Text = '0' then begin //締め日が月末
	  wDd := 1;
  end else begin
		wMm := wMm - 1;
	  if wMm = 0 then begin
  		wMm := 12;
    	wYyyy := wYyyy - 1;
	  end;
	  wDd := wDd + 1;
	end;
  sDateFrom := DateToStr(EncodeDate(wYyyy, wMm, wDd));


	sSql := sSql + 'DDate Between ';

  //1999/08/08追加
  //請求期間を強制的に変更する
  {回収のみ
  if CheckBox1.Checked = True then begin
    sDateFrom := DateToStr(DTPFrom.Date);
    sDateTo   := DateToStr(DTPTo.Date);
  end;
  }
	sSql := sSql + '''' + sDateFrom + '''' + ' AND ';
	sSql := sSql + '''' + sDateTo + '''';
	sSql := sSql + ' AND ';
	//sSql := sSql + ' UriageKingaku > 0 ';
//  sSql := sSql + ' ((UriageKingaku > 0) OR (SUBSTRING(Memo, 1 ,2) IN (''返品'', ''値引'')))' ;
  sSql := sSql + ' ((UriageKingaku > 0) OR (SUBSTRING(Memo, 1 ,2) IN (''返品'', ''値引'',''取消'')))' ;
	sSql := sSql + ' AND ';
	sSql := sSql + ' Memo <> ''' + CsCTAX + '''';//消費税ではない

	//1999/08/09
	sSql := sSql + ' AND ';
  sSql := sSql + ' PATINDEX(''%返金%'', Memo) = 0 '; //返金でない


  //当月請求金額の算出
  //with frmDMMaster.QueryUriage do begin
  with frmDMMaster.Query1Master do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql); //税込みのsql文になっているしかも返品の考慮なし
    open;
    iSum := FieldByName('UriageSum').AsInteger;
    Close;
  end;

  //前月請求残高の取得
  //前月分までの売上金額（税抜き）と本日までの回収（税込み）の差額
	if iZanOnOff = 1 then begin

	  //1999/08/08追加
  	//請求期間を強制的に変更する
  	if CheckBox1.Checked = True then begin
    	//sDateFrom := DateToStr(DTPFrom.Date); //1999/09/25
    	sDateTo   := DateToStr(DTPTo.Date);
  	end;
	  iSum2 := GetRuikeiZandaka(sTokuisakiCode1, sDateFrom, sDateTo);
	  sSum2 := IntToStr(iSum2);

  end else begin
	  iSum2 := GetRuikeiZandaka(sTokuisakiCode1, sDateFrom, sDateTo);
	  sSum2 := IntToStr(iSum2);
    if iSum2 < 0 then begin
    	;//前月末残高がマイナスのときは出力する
    end else begin
	  	iSum2 := 0;
  	  sSum2 := '　';
    end;
  end;

  if (iSum = 0) AND (iSum2 = 0) then begin
  	;
  end else begin
	  //得意先名の取得
    sTokuisakiCode2 := MTokuisaki.GetTokuisakiCode2(sTokuisakiCode1);
  	sLine := MTokuisaki.GetTokuisakiName(sTokuisakiCode1);
	  sLine := sLine + ',"' + sTokuisakiCode1+ '−'+ sTokuisakiCode2 + '",' + IntToStr(iSum) + ',' + sSum2;

    //1999/07/15 請求締め日から発行日に変更
	  //1999/07/25 発行日から請求締め日に戻す
    //sLine := sLine + ',' + DateTostr(DTP1.Date);
	  sLine := sLine + ',' + sDateTo;

    sLine := sLine + ',' + CBMm.Text; //月分

    //Ver2.0 で追加
    sLine := sLine + ',' + sTokuisakiCode2; //得意先コード２
    sWhere := 'TokuisakiCode1 = ' + sTokuisakiCode1;
    sLine := sLine + ',' + DMMaster.GetFieldData(CtblMTokuisaki, 'Ka', sWhere);//課
    sLine := sLine + ',' + DMMaster.GetFieldData(CtblMTokuisaki, 'TantoushaCode', sWhere);//担当者コード
    sLine := sLine + ',' + DMMaster.GetFieldData(CtblMTokuisaki, 'ChainCode', sWhere);//チェーンコード
    sLine := sLine + ',' + DMMaster.GetFieldData(CtblMTokuisaki, 'TokuisakiNameYomi', sWhere);//よみ
    sYuusousaki :=DMMaster.GetFieldData(CtblMTokuisaki, 'SeikyuushoHassouSaki', sWhere);//請求書発行先 郵送、持参
    if sYuusousaki = '0' then sYuusousaki := '持参' else sYuusousaki := '郵送';
    sLine := sLine + ',' + sYuusousaki;

    //2014.04.01 消費税対応 beggn
    sGatsubun :=  CBYyyy.Text + '/' +  CBMm.Text + '01';
    //sTokusakiTax := GetTokuisakiTax(sTokuisakiCode1,sGatsubun);

    sTokusakiTaxOrg := GetTokuisakiTax(sTokuisakiCode1,sGatsubun,'8%');
    sTokusakiTaxNew := GetTokuisakiTax(sTokuisakiCode1,sGatsubun,'10%');

    //sLine := sLine + ',' + sTokusakiTax;
    sLine := sLine + ',' + sTokusakiTaxOrg + ',' + sTokusakiTaxNew;

    // end

    HMakeFile(CFileName_Seikyuu, sLine);
  end;//of if

  Result := true;

except
  on E: EDBEngineError do begin
   	ShowMessage(E.Message);

    Result := False;

  end;
end;

end;


//得意先ごとに請求書の計算をする
//iZanOnOff -> 0 のせない 1 のせる
function TfrmPBill.MakeTokuisakiBill2(sTokuisakiCode1:String ; iZanOnOff:Integer) :Boolean;
var
	sTokuisakiCode2, sLine, sSql, sDateFrom, sDateTo, sSum2, sSumFee : string;
 	F : TextFile;
  iSum, iSum2, iSumFee : integer;
  wYyyy, wMm, wDd : Word;
  wYyyy2, wMm2, wDd2 : Word;
  sWhere, sYuusousaki, sShuukei : String;
  sTokusakiTax,sGatsubun,sSqlIn,sSqlDel : String; //2014.04.01 消費税対応
  sTokusakiTaxOrg, sTokusakiTaxNew : String; //2014.04.01 消費税対応


begin
try
//  sSql := 'SELECT UriageSum = SUM(UriageKingaku) '; #thuyptt 20190313
  sSql := 'SELECT SUM(UriageKingaku) AS UriageSum ';
  sSql := sSql + ' FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';

 	sSql := sSql + 'TokuisakiCode1 = ' + sTokuisakiCode1;
 	sSql := sSql + ' AND ';

  //請求期間の取得
  //請求締め日の１ヶ月前を取得
  if CBDd.Text = '0' then begin //締め日が月末
  	wDd := GetGetsumatsu(StrToInt(CBYyyy.Text), StrToInt(CBMm.Text));
  end else begin
  	wDd := StrToint(CBDd.Text);
  end;
  sDateTo := CBYyyy.Text + '/' + CBMm.Text + '/' + IntToStr(wDd);
	DecodeDate(StrToDate(sDateTo), wYyyy, wMm, wDd);

  if CBDd.Text = '0' then begin //締め日が月末
	  wDd := 1;
  end else begin
		wMm := wMm - 1;
	  if wMm = 0 then begin
  		wMm := 12;
    	wYyyy := wYyyy - 1;
	  end;
	  wDd := wDd + 1;
	end;
  sDateFrom := DateToStr(EncodeDate(wYyyy, wMm, wDd));
	sSql := sSql + 'DDate Between ';

  //1999/08/08追加
  //請求期間を強制的に変更する
  {回収のみ
  if CheckBox1.Checked = True then begin
    sDateFrom := DateToStr(DTPFrom.Date);
    sDateTo   := DateToStr(DTPTo.Date);
  end;
  }
//	sSql := sSql + '''' + sDateFrom + '''' + ' AND '; #thuyptt 20190313
//	sSql := sSql + '''' + sDateTo + '''';  #thuyptt 20190313
	sSql := sSql + '#' + sDateFrom + '#' + ' AND ';
	sSql := sSql + '#' + sDateTo + '#';
	sSql := sSql + ' AND ';
	//sSql := sSql + ' UriageKingaku > 0 ';
  //sSql := sSql + ' ((UriageKingaku > 0) OR (SUBSTRING(Memo, 1 ,2) IN (''返品'', ''値引'')))' ;          #thuyptt 20190313
  sSql := sSql + ' ((UriageKingaku > 0) OR (Mid(Memo, 1 ,2) IN (''返品'', ''値引'',''取消'')))' ;
	sSql := sSql + ' AND ';
	sSql := sSql + ' Memo <> ''' + CsCTAX + '''';//消費税ではない

  
	sSql := sSql + ' AND ';
	sSql := sSql + ' Memo <> ''送料''';//add new column 送料 thuyptt 20190515

	//1999/08/09
	sSql := sSql + ' AND ';
//  sSql := sSql + ' PATINDEX(''%返金%'', Memo) = 0 '; //返金でない
  sSql := sSql + ' InStr(1, Memo, ''返金'') = 0 '; //返金でない

  //当月請求金額の算出
  //with frmDMMaster.QueryUriage do begin
  with frmDMMaster.Query1Master do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql); //税込みのsql文になっているしかも返品の考慮なし
    open;
    iSum := FieldByName('UriageSum').AsInteger;
    Close;
  end;

  //請求期間を強制的に変更する
  //2000/03/23
  if CheckBox1.Checked = True then begin
    	sDateTo   := DateToStr(DTPTo.Date);
  end;

  //前月請求残高の取得
  //前月分までの売上金額（税抜き）と本日までの回収（税込み）の差額
	if iZanOnOff = 1 then begin
	  iSum2 := GetRuikeiZandaka(sTokuisakiCode1, sDateFrom, sDateTo);
	  sSum2 := IntToStr(iSum2);  
	  	  
	  iSumFee := GetDeliveryFee(sTokuisakiCode1, sDateFrom, sDateTo);
	  sSumFee := IntToStr(iSumFee);
  end else begin
	  iSum2 := GetRuikeiZandaka(sTokuisakiCode1, sDateFrom, sDateTo);
	  sSum2 := IntToStr(iSum2); 
	  	  
	  iSumFee := GetDeliveryFee(sTokuisakiCode1, sDateFrom, sDateTo);
	  sSumFee := IntToStr(iSumFee);
    if iSum2 < 0 then begin
    	;//前月末残高がマイナスのときは出力する
    end else begin
	  	iSum2 := 0;
  	  sSum2 := '　';
	  
	  	iSumFee := 0;
  	  //sSumFee := ' ';
    end;
  end;

  if (iSum = 0) AND (iSum2 = 0) AND (iSumFee = 0)then begin      //thuyptt 2019/05/14
  	;
  end else begin
	  //得意先名の取得
    sTokuisakiCode2 := MTokuisaki.GetTokuisakiCode2(sTokuisakiCode1);
  	sLine := MTokuisaki.GetTokuisakiName(sTokuisakiCode1);
    //Ver2.0
	  //sLine := sLine + ',"' + sTokuisakiCode1+ '−'+ sTokuisakiCode2 + '",' + IntToStr(iSum) + ',' + sSum2;

    //2001/07/18
    //月の請求金額がマイナスの場合？
    //iSumは税込みになっている
    //sSum2(前月までの請求残)はどうなってるの？
    //
    if (iSum<0) then begin
    	sSum2 := '　';
      //iSum := StrToInt(Real2Str(iSum/1.05,0,0));
      //iSum := StrToInt(Real2Str(iSum*1.05,0,0));
	  	sLine := sLine + ',' + sTokuisakiCode1+ ',' + IntToStr(iSum) + ',' + sSum2;
    end else begin
	  	sLine := sLine + ',' + sTokuisakiCode1+ ',' + IntToStr(iSum) + ',' + sSum2;
    end;//of if

    //1999/07/15 請求締め日から発行日に変更
	  //1999/07/25 発行日から請求締め日に戻す
    //sLine := sLine + ',' + DateTostr(DTP1.Date);
	  sLine := sLine + ',' + sDateTo;

    sLine := sLine + ',' + CBMm.Text; //月分

    //Ver2.0 で追加
    sWhere := 'TokuisakiCode1 = ' + sTokuisakiCode1;
    sLine := sLine + ',' + sTokuisakiCode2; //得意先コード２
    sLine := sLine + ',' + DMMaster.GetFieldData(CtblMTokuisaki, 'Ka', sWhere);//課
    sLine := sLine + ',' + DMMaster.GetFieldData(CtblMTokuisaki, 'TantoushaCode', sWhere);//担当者コード
    sLine := sLine + ',' + DMMaster.GetFieldData(CtblMTokuisaki, 'ChainCode', sWhere);//チェーンコード
    sLine := sLine + ',' + DMMaster.GetFieldData(CtblMTokuisaki, 'TokuisakiNameYomi', sWhere);//よみ

    sYuusousaki :=DMMaster.GetFieldData(CtblMTokuisaki, 'SeikyuushoHassouSaki', sWhere);//請求書発行先 郵送、持参
    if sYuusousaki = '0' then begin
    	sYuusousaki := '持参';
    end else if sYuusousaki = '1' then begin
    	sYuusousaki := '郵送(会社)';
    end else if sYuusousaki = '2' then begin
    	sYuusousaki := '郵送(店)';
    end else if sYuusousaki = '3' then begin
    	sYuusousaki := '郵送(自宅)';
    end else begin
    	sYuusousaki := 'データ不正';
    end;

    sLine := sLine + ',' + sYuusousaki;

    //2002.10.31 集計コードを追加
    sShuukei := DMMaster.GetFieldData(CtblMTokuisaki, 'ShuukeiCode', sWhere);
    sLine := sLine + ',' + sShuukei;

    //2014.04.01 消費税対応 beggn
    sGatsubun :=  CBYyyy.Text + '/' +  CBMm.Text + '/01';

    sTokusakiTaxOrg := GetTokuisakiTax(sTokuisakiCode1,sGatsubun,'8%');
    sTokusakiTaxNew := GetTokuisakiTax(sTokuisakiCode1,sGatsubun,'10%');

    //sLine := sLine + ',' + sTokusakiTax + ' ';
    sLine := sLine + ',' + sTokusakiTaxOrg + ',' + sTokusakiTaxNew;

    // end

     sLine := sLine + ',' + sSumFee;


    HMakeFile(CFileName_Seikyuu, sLine);

    //addd utada 2019.05.21
    sSqlDel := 'DELETE FROM ' + CtblTBill;
    sSqlDel := sSqlDel + ' WHERE ';
    sSqlDel := sSqlDel + ' Gatsubun = #' + sGatsubun + '# ';
    sSqlDel := sSqlDel + ' AND ';
    sSqlDel := sSqlDel + ' TokuisakiCode1 = ' + sTokuisakiCode1;

   with frmDMMaster.Query1Master do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSqlDel);
    ExecSql;
    Close;
  end;

    sSqlIn := 'INSERT INTO ' + CtblTBill;
    sSqlIn := sSqlIn + '(';
    sSqlIn := sSqlIn + 'TokuisakiCode1, ';
    sSqlIn := sSqlIn + 'TokuisakiCode2, ';
    sSqlIn := sSqlIn + 'TougetsuSeikyuuKingaku, ';
    sSqlIn := sSqlIn + 'ZengetsuSeikyuuKingaku, ';
    sSqlIn := sSqlIn + 'Seikyuubi, ';
    sSqlIn := sSqlIn + 'Gatsubun, ';
    sSqlIn := sSqlIn + 'Tax1, ';
    sSqlIn := sSqlIn + 'Tax2, ';
    sSqlIn := sSqlIn + 'DeliveryFee, ';
    sSqlIn := sSqlIn + 'CreatedDate ';
    sSqlIn := sSqlIn + ') Values (';
    sSqlIn := sSqlIn + sTokuisakiCode1 + ',';
    sSqlIn := sSqlIn + sTokuisakiCode2 + ',';
    sSqlIn := sSqlIn + IntToStr(iSum) + ', ';
    sSqlIn := sSqlIn + IntToStr(iSum2) + ',';
    sSqlIn := sSqlIn + '#' + sDateTo + '#, ';
    sSqlIn := sSqlIn + '#' + sGatsubun + '#, ';
    sSqlIn := sSqlIn + Real2Str(Str2Real(sTokusakiTaxOrg), 0,0) + ', ';
    sSqlIn := sSqlIn + Real2Str(Str2Real(sTokusakiTaxNew), 0,0) + ', ';    
    sSqlIn := sSqlIn + Real2Str(Str2Real(sSumFee), 0,0) + ', ';
    sSqlIn := sSqlIn + 'Now() ' ;
    sSqlIn := sSqlIn + ')';

    with frmDMMaster.Query1Master do begin
         Sql.Clear;
         Sql.Add(sSqlIn);
         ExecSql;
    end;//of with



  end;//of if

  Result := true;

except
  on E: EDBEngineError do begin
   	ShowMessage(E.Message);

    Result := False;

  end;
end;

end;

function TfrmPBill.GetNyuukinKigenAndShimebi(sTokuisakiCode:String; sGatsubunText:String) :  Boolean;
var
  sSql, sSql2, sLine, sShiharaiKubun, sGatsubun : String;
  sShimebi, sShiharaibi, sShiharaituki : String;
  wYyyy, wMm, wDd : Word;
  sYyyy, sMm : String;
  iUriageSum, iKaishuuSum : Integer;
  sGatsubunMin, sGatsubunMax : String ;
begin
	sSql := 'SELECT TokuisakiCode1, TokuisakiCode2, TokuisakiName, SeikyuuSimebi, ShiharaibiD, ShiharaibiM, ShiharaiKubun';
	sSql := sSql + ' FROM ' + CtblMTokuisaki;
	sSql := sSql + ' WHERE TokuisakiCode1 = ' + sTokuisakiCode;
	//sSql := sSql + ' AND SeikyuushoFlag <> 1 ';   2017.01.26 コメントアウト

	strDTShimebi := '';
	strDTShiharaibi := '';
	with qryNyuukin do begin
		Close;
		Sql.Clear;
		Sql.Add(sSql);
		Open;
	 //	while not EOF do begin
		sShimebi := FieldByName('SeikyuuSimebi').AsString;
		if sShimebi = '' then begin
			Next;
			//Continue;
		end;

		sShiharaibi := FieldByName('ShiharaibiD').AsString;
		if sShiharaibi = '' then begin
			Next;
		 //	Continue;
		end;
			//sGatsubunMin := '2009/08/01';
			//sGatsubunMax := DMMaster.CheckGatsubun(sTokuisakiCode, 'Max');
			//if StrToDate(sGatsubunMax) > StrToDate(sGatsubunText + '/01') then begin
		 //		sGatsubunMax := sGatsubunText + '/01';
		 //	end;
      sGatsubun := sGatsubunText + '/01';
//			DecodeDate(StrToDate(sGatsubunMin), wYyyy, wMm, wDd);
			sYyyy := IntToStr(wYyyy);
			sMm := IntToStr(wMm);
			//sGatsubun := sYyyy + '/' + sMm + '/' + '01';
		//	while StrToDate(sGatsubun) <= StrToDate(sGatsubunMax) do begin
		//		iUriageSum := GetGatsubunSum(sTokuisakiCode, sGatsubun);
	 //			if iUriageSum <> 0 then begin
					DecodeDate(StrToDate(sGatsubun), wYyyy, wMm, wDd);
					sShimebi := FieldByName('SeikyuuSimebi').AsString;
					if sShimebi = '0' then begin
						sShimebi := IntToStr(GetGetsumatsu(wYyyy, wMm));
					end;
					wDd := StrToInt(sShimebi);
					DTShimebi := EncodeDate(wYyyy, wMm, wDd);
					strDTShimebi := DateToStr(DTShimebi);

					sShiharaituki := FieldByName('ShiharaibiM').AsString;
					DecodeDate(StrToDate(sGatsubun), wYyyy, wMm, wDd);
					wMm := wMm + StrToInt(sShiharaituki);
					if wMm >= 13 then begin
						wMm := wMm-12;
						wYyyy := wYyyy + 1;
					end;
					if sShiharaibi = '0' then begin
						sShiharaibi := IntToStr(GetGetsumatsu(wYyyy, wMm));
					end;
					if wDd >= StrToInt(sShiharaibi) then begin
						wDd := StrToInt(sShiharaibi);
						DTShiharaibi := EncodeDate(wYyyy, wMm, wDd);
					end else begin
						wDd := StrToInt(sShiharaibi);
						if wMm = 0 then begin
							wMm := 12;
							wYyyy := wYyyy - 1;
						end;
					end;
					if FieldByName('ShiharaibiD').AsString = '0' then begin
						sShiharaibi := IntToStr(GetGetsumatsu(wYyyy, wMm));
						wDd := StrToInt(sShiharaibi);
					end;
					DTShiharaibi := EncodeDate(wYyyy, wMm, wDd);
					strDTShiharaibi := DateToStr(DTShiharaibi);
   

				sMm := IntToStr(StrToInt(sMm) + 1);
				if sMm = '13' then begin
				  sYyyy := IntToStr(StrToInt(sYyyy) + 1);
				  sMm := '01';
				end;
				//sGatsubun := sYyyy + '/' + sMm + '/' + '01';
	 //		end;
	//		Next;
	 //	end;
		Result := True;
		Close;

	end;

  
end;



function TfrmPBill.GetZenkaiSeikyuu(sTokuisakiCode1, sGatsubun:String):Integer;
var
	sSql : String;
  sLastSeikyuuBi : String;
begin

//	sSql := 'SELECT Gatsubun = MAX(Gatsubun) ';
	sSql := 'SELECT MAX(Gatsubun) AS SumGatsubun';
	sSql := sSql + ' FROM ' + CtblTDenpyou;
	sSql := sSql + ' WHERE ';
	sSql := sSql + ' TokuisakiCode1 = ' + sTokuisakiCode1;
	sSql := sSql + ' AND ';
  sSql := sSql + ' FORMAT([Gatsubun], ''yyyy/mm/dd'') < ';
//  sSql := sSql + ''''  + sGatsubun + ''''; #thuyptt 20190313
  sSql := sSql + '#'  + sGatsubun + '#';
	sSql := sSql + ' AND ';
	sSql := sSql + ' Memo = ''' + CsCTAX + '''';
	with QueryZandaka do begin
		Close;
		Sql.Clear;
		Sql.Add(sSql);
		Open;
//		sLastSeikyuuBi := FieldByName('Gatsubun').AsString; #thuyptt 20190313
		sLastSeikyuuBi := FieldByName('SumGatsubun').AsString;
		Close;
	end;

  if sLastSeikyuuBi <> '' then begin
//  sSql := 'SELECT USum=Sum(tblTDenpyou.UriageKingaku)'; #thuyptt 20190313
  sSql := 'SELECT Sum(tblTDenpyou.UriageKingaku) AS USum';
	sSql := sSql + ' FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';
//  sSql := sSql + ' Gatsubun = ''' + sLastSeikyuuBi + ''''; #thuyptt 20190313
  sSql := sSql + ' Gatsubun = #' + sLastSeikyuuBi + '#';
  sSql := sSql + ' AND ';
	sSql := sSql + ' TokuisakiCode1 = ' + sTokuisakiCode1;
  sSql := sSql + ' AND ';
 //2000/06/26 微妙だが売り上げがマイナスの」ときもある
//  sSql := sSql + ' ((UriageKingaku > 0) OR (SUBSTRING(tblTDenpyou.Memo, 1, 2) IN (''返品'', ''値引'', ''取消'')) OR tblTDenpyou.Memo=''' + CsCTAX + ''')' ; #thuyptt 20190312
  sSql := sSql + ' ((UriageKingaku > 0) OR (Mid(tblTDenpyou.Memo, 1, 2) IN (''返品'', ''値引'', ''取消'')) OR tblTDenpyou.Memo=''' + CsCTAX + ''')' ;

	with QueryZandaka do begin
		Close;
		Sql.Clear;
		Sql.Add(sSql);
		Open;
		Result := FieldByName('USum').AsInteger;
		Close;
	end;

  end else begin
  
  end;

end;


function TfrmPBill.GetNyuu(sTokuisakiCode1, sFrom, sTo: String):Integer;
var
	sSql : String;
begin
	//表示情報の取得
//	sSql := 'SELECT KSum = Sum(UriageKingaku) * -1 '; #thuyptt 20190313
	sSql := 'SELECT (Sum(UriageKingaku) * -1) AS KSum';
	sSql := sSql + ' FROM ';
	sSql := sSql + CtblTDenpyou;
	sSql := sSql + ' WHERE ';
	sSql := sSql + 'TokuisakiCode1 = ' +  sTokuisakiCode1;
	sSql := sSql + ' AND ';
	sSql := sSql + 'DDate Between ';
//	sSql := sSql + '''' + sFrom + ''''; #thuyptt 20190313
	sSql := sSql + '#' + sFrom + '#';
	sSql := sSql + ' AND ';
//	sSql := sSql + '''' + sTo + ''''; #thuyptt 20190313
	sSql := sSql + '#' + sTo + '#';
	sSql := sSql + ' AND ';
	sSql := sSql + 'KaishuuHouhou IN (1, 2, 3)';


  with QueryZandaka do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    Result := FieldByName('KSum').AsInteger;
    Close;
  end;
end;

function TfrmPBill.GetKonkaiUrige(sTokuisakiCode1,sFrom, sTo: String):Integer;
var
	sSql : String;
begin

{
	sSql := 'SELECT Zan = SUM(UriageKingaku) ';
	sSql := sSql + ' FROM ' + CtblTDenpyou;
	sSql := sSql + ' WHERE ';
	sSql := sSql + ' TokuisakiCode1 = ' + sTokuisakiCode1;
	sSql := sSql + ' AND ';
  sSql := sSql + ' Gatsubun = ';
  sSql := sSql + ''''  + sGatsubun + '''';
	sSql := sSql + ' AND ';
  sSql := sSql + ' KaishuuHouhou is NULL';
	sSql := sSql + ' AND ';
	sSql := sSql + ' Memo <> ''' + CsCTAX + '''';
  }
//  sSql := 'SELECT UriageSum = SUM(UriageKingaku) '; #thuyptt 20190313
  sSql := 'SELECT SUM(UriageKingaku) AS UriageSum ';
  sSql := sSql + ' FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';

 	sSql := sSql + 'TokuisakiCode1 = ' + sTokuisakiCode1;
 	sSql := sSql + ' AND ';


 {
  //請求期間の取得
  //請求締め日の１ヶ月前を取得
  if CBDd.Text = '0' then begin //締め日が月末
  	wDd := GetGetsumatsu(StrToInt(CBYyyy.Text), StrToInt(CBMm.Text));
  end else begin
  	wDd := StrToint(CBDd.Text);
  end;
  sDateTo := CBYyyy.Text + '/' + CBMm.Text + '/' + IntToStr(wDd);
	DecodeDate(StrToDate(sDateTo), wYyyy, wMm, wDd);

  if CBDd.Text = '0' then begin //締め日が月末
	  wDd := 1;
  end else begin
		wMm := wMm - 1;
	  if wMm = 0 then begin
  		wMm := 12;
    	wYyyy := wYyyy - 1;
	  end;
	  wDd := wDd + 1;
	end;
  sDateFrom := DateToStr(EncodeDate(wYyyy, wMm, wDd));
	sSql := sSql + 'DDate Between ';
  }
  //1999/08/08追加
  //請求期間を強制的に変更する
  {回収のみ
  if CheckBox1.Checked = True then begin
    sDateFrom := DateToStr(DTPFrom.Date);
    sDateTo   := DateToStr(DTPTo.Date);
  end;
  }
{#thuyptt 20190313
  sSql := sSql + 'DDate Between ';
	sSql := sSql + '''' + sFrom + '''' + ' AND ';
	sSql := sSql + '''' + sTo + '''';
	sSql := sSql + ' AND ';
	//sSql := sSql + ' UriageKingaku > 0 ';
  //sSql := sSql + ' ((UriageKingaku > 0) OR (SUBSTRING(Memo, 1 ,2) IN (''返品'', ''値引'')))' ;
  sSql := sSql + ' ((UriageKingaku > 0) OR (SUBSTRING(Memo, 1, 2) IN (''返品'', ''値引'',''取消'')))' ;   // mod 2015.04.19
	sSql := sSql + ' AND ';
	sSql := sSql + ' Memo <> ''' + CsCTAX + '''';//消費税ではない

	//1999/08/09
	sSql := sSql + ' AND ';
  sSql := sSql + ' PATINDEX(''%返金%'', Memo) = 0 '; //返金でない
}
  sSql := sSql + 'DDate Between ';
	sSql := sSql + '#' + sFrom + '#' + ' AND ';
	sSql := sSql + '#' + sTo + '#';
	sSql := sSql + ' AND ';
	//sSql := sSql + ' UriageKingaku > 0 ';
  //sSql := sSql + ' ((UriageKingaku > 0) OR (SUBSTRING(Memo, 1 ,2) IN (''返品'', ''値引'')))' ;
  sSql := sSql + ' ((UriageKingaku > 0) OR (Mid(Memo, 1, 2) IN (''返品'', ''値引'',''取消'')))' ;   // mod 2015.04.19
	sSql := sSql + ' AND ';
	sSql := sSql + ' Memo <> ''' + CsCTAX + '''';//消費税ではない

	//1999/08/09
	sSql := sSql + ' AND ';
  sSql := sSql + ' InStr(1, Memo, ''返金'') = 0 '; //返金でない


	with QueryZandaka do begin
		Close;
		Sql.Clear;
		Sql.Add(sSql);
		Open;
		Result := FieldByName('UriageSum').AsInteger;
		Close;
	end;
end;

function TfrmPBill.GetShouhizeiGaku(sTokuisakiCode1, sGatsubun:String):Integer;
var
	sSql : String;
begin
//	sSql := 'SELECT Zan = SUM(UriageKingaku) '; #thuyptt 20190313
	sSql := 'SELECT SUM(UriageKingaku) AS Zan';
	sSql := sSql + ' FROM ' + CtblTDenpyou;
	sSql := sSql + ' WHERE ';
	sSql := sSql + ' TokuisakiCode1 = ' + sTokuisakiCode1;
	sSql := sSql + ' AND ';
  sSql := sSql + ' Gatsubun = ';
//  sSql := sSql + ''''  + sGatsubun + ''''; #thuyptt 20190313
  sSql := sSql + '#'  + sGatsubun + '#';
	sSql := sSql + ' AND ';
	sSql := sSql + ' Memo = ''' + CsCTAX + '''';
	with QueryZandaka do begin
		Close;
		Sql.Clear;
		Sql.Add(sSql);
		Open;
		Result := FieldByName('Zan').AsInteger;
		Close;
	end;
end;

function TfrmPBill.GetTokuisakiCodeArray() : Boolean;
var
	strTokuisakiCode1Tmp, strShimebi, sSql : String;
	i: Integer;
begin

	if EditTokuisakiCode1.Text <> '' then begin
		strTokuisakiCode1Tmp := EditTokuisakiCode1.Text + ',';
		i := 0;
		while strTokuisakiCode1Tmp <> '' do begin
			LArrayOfTokuisakiCodeExport[i] := Copy(strTokuisakiCode1Tmp, 1, Pos(',', strTokuisakiCode1Tmp)-1);
			strTokuisakiCode1Tmp := Copy(strTokuisakiCode1Tmp, (Pos(',', strTokuisakiCode1Tmp)+1), Length(strTokuisakiCode1Tmp));
			i := i + 1;
		end;//of while
	end else begin
		strShimebi := CBDd.Text;
		sSql := 'SELECT TokuisakiCode1 FROM ' + CtblMTokuisaki;
//		sSql := sSql + ' WHERE SeikyuuSimebi = ' + '''' + strShimebi + ''''; #thuyptt 20190313
		sSql := sSql + ' WHERE SeikyuuSimebi = ' + strShimebi;
		sSql := sSql + ' AND TokuisakiCode1<50000 ';
    sSql := sSql + ' AND SeikyuushoFlag <> 1';
		sSql := sSql + ' ORDER BY TantoushaCode DESC, ShuukeiCode, TokuisakiCode2, TokuisakiNameYomi';

		with qrySeikyuu do begin
		Close;
		Sql.Clear;
		Sql.Add(sSql);
		Open;

		// 得意先コードのリスト設定
		i := 0;
		while not EOF do begin
			LArrayOfTokuisakiCodeExport[i] := FieldbyName('TokuisakiCode1').AsString;
			i := i+1;
			next
		end;
		Close;
		end;
   end;

   Result := True;

end;

//請求期間の消費税を入力する
//請求締め日を同じくする得意先のsYyyy, sMmの消費税の伝票を
//すべて再計算する
//すでに消費税の伝票がある場合には上書きする
//引数 sYyyy->
//     sMm  ->
//     sDd  -> 請求締め日
//     iTokuisakiCode1 -> 0ならば締め日の同じものすべて
//
function InsertCTax(sYyyy, sMm, sDd:String ; iTokuisakiCode1 : Integer; SB1:TStatusBar):Boolean;
var
	sSql, sSqlDel, sSqlIn, sDd2 : String;
begin
try


	//20020929
  if sDd='' then begin //得意先の閉め日がない又は得意先がない場合
  	Exit;
  end;
	//請求締め日で得意先コードを抽出する
  if iTokuisakiCode1 = 0 then begin
		sSql := 'SELECT TokuisakiCode1 FROM ' + CtblMTokuisaki;
  	sSql := sSql + ' WHERE ';
	  sSql := sSql + 'SeikyuuSimebi = ' + sDd;
  	sSql := sSql + ' AND ';
	  sSql := sSql + 'TokuisakiCode1 < 50000';
    //add utadada 2019.0607
    sSql := sSql + ' ORDER BY TantoushaCode DESC,InfomartUse DESC';

  end else begin
  	sSql := IntToStr(iTokuisakiCode1);
  end;

  //sDdをその月の月末日に変換する
  if StrToInt(sDd) = CSeikyuuSimebi_End then begin
  	sDd2 := IntToStr(HKLib.GetGetsumatsu(StrToInt(syyyy), strToInt(sMm)));
  end else begin
  	sDd2 := sDd;
  end;

  //すでにその月の消費税がある場合には削除してから
  //消費税額を再計算する。
  //削除のSql文
  //消費税の伝票の規則
  //Memo = '##消費税##'
  sSqlDel := 'DELETE FROM ' + CtblTDenpyou;
  sSqlDel := sSqlDel + ' WHERE ';
  sSqlDel := sSqlDel + '(Memo = ''' + CsCTAX + '''';
  sSqlDel := sSqlDel + ' OR ';
  sSqlDel := sSqlDel + 'Memo = ''' + '送料' + ''')';
  sSqlDel := sSqlDel + ' AND ';
//  sSqlDel := sSqlDel + ' DDate = ''' + sYyyy + '/' + sMm + '/' + sDd2 + '''';    #thuyptt 20190313
  sSqlDel := sSqlDel + ' DDate = #' + sYyyy + '/' + sMm + '/' + sDd2 + '#';
  sSqlDel := sSqlDel + ' AND ';
  sSqlDel := sSqlDel + ' TokuisakiCode1 IN (' + sSql + ')';

  SB1.SimpleText := '消費税の計算中 （前消費税データの再計算）';
  SB1.Update;

// with frmDMMaster.QueryDelete do begin
 with frmDMMaster.Query1Master do begin
 //with TfrmPBill.qrySeikyuu do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSqlDel);
    ExecSql;
    Close;
  end;

  //消費税伝票の入力
	sSql := 'SELECT TokuisakiCode1 FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + 'SeikyuuSimebi = ' + sDd;
  if iTokuisakiCode1 = 0 then begin
  	;
  end else begin
	  sSql := sSql + ' AND ';
  	sSql := sSql + 'TokuisakiCode1 = ' + IntToStr(iTokuisakiCode1);
  end;

  sSql := sSql + ' AND ';
  sSql := sSql + 'TokuisakiCode1<50000';
  sSql := sSql + ' ORDER BY TokuisakiCode1';

 with frmDMMaster.QueryMTokuisaki do begin
 //with frmDMMaster.Query1Master do begin
//  with qrySeikyuu do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
		  //消費税の伝票の入力
		  SB1.SimpleText := '消費税の計算中　' + FieldByName('TokuisakiCode1').AsString;
		  SB1.Update;
    	if InsertCTax2(FieldByName('TokuisakiCode1').AsString, sYyyy + '/' + sMm + '/' + sDd2) = False then
      begin
          Result := False;
          exit;
      end;
      Next;
    end;//of while

    Close;
  end;//of with

  Result := true;

except
  on E: EDBEngineError do begin
   	ShowMessage(E.Message);

    Result := False;

  end;
end;

end;


//消費税の伝票の入力
//sYyyyMmDd -> 伝票日付
//
//1999/06/05
//  消費税の額が０の場合は伝票を挿入しない
//
//2005/10/11
//kaishuuHouhou が4以上のものは消費税の計算対象外にする
//1005/10/12
// 過去のデータの絡みがあるので上記の方法は危険
// kaishuuHouhou
function InsertCTax2(sTokuisakiCode1, sYyyyMmDd : String) : Boolean;
var
	sSqlIn, sSql, sYear, sMonth, sDay : String;
  sCTax, sSeikyuuShimebi, sGatsubun : String;
  dCTax : Double;
  dCTaxOrg,dCTaxNew : Double; //add utada 2019.08.16 for keigen-tax
  sCTaxOrg,sCTaxNew : String; //add utada 2019.08.16 for keigen-tax  
  wYyyy, wMm, wDd : Word;
  curUriageKingaku         : Currency; //2014.04.01 消費税対応
  douTax  :Double;    //2014.04.01 消費税対応
  dDeliveryTax : Currency;
  sDeliveryFeeFlag : String;
  sWhere : String;
  sDeliveryFee : String;
begin
try

  //請求締め日の１ヶ月前を取得
	DecodeDate(StrToDate(sYyyyMmDd), wYyyy, wMm, wDd);
  wDd := wDd +1;
  if wDd > 28 then begin //月末締めの場合
	  wDd := 1;
  end else begin
	  wMm := wMm - 1;
  	if wMm = 0 then begin
  		wMm := 12;
	    wYyyy := wYyyy - 1;
  	end;
  end;

  dCTax := 0;
	//消費税額の計算
  {
 // sSql := 'SELECT TAXSUM = SUM(UriageKingaku) '; //2014.04.01 消費税対応  before
  sSql := 'SELECT UriageKingaku,DDate '; //2014.04.01 消費税対応  after
  sSql := sSql + ' FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  //sSql := sSql + ' TokuisakiCode1 =  ' + MyQuery.FieldByName('TokuisakiCode1').AsString;
  sSql := sSql + ' TokuisakiCode1 =  ' + sTokuisakiCode1;
  sSql := sSql + ' AND ';
  sSql := sSql + ' DDate Between ';
  sSql := sSql + '''' + DateToStr(EncodeDate(wYyyy, wMm, wDd)) + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + '''' + sYyyyMmDd + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + ' ((UriageKingaku > 0) OR (SUBSTRING(Memo, 1, 2) IN (''返品'', ''値引'')))' ;
  sSql := sSql + ' AND ';
  sSql := sSql + ' Memo <> ''' + CsCTAX + '''';

  sSql := sSql + ' AND ';
  sSql := sSql + ' PATINDEX(''%返金%'', Memo) = 0 ';

  //2005.10.11
  //2004.10.12
  //sSql := sSql + ' AND ';
  //sSql := sSql + ' KaishuuHouhou <> 8';//雑収入=8
  //2006.01.10
  sSql := sSql + ' AND ';
  sSql := sSql + ' (KaishuuHouhou <> 8 or KaishuuHouhou is Null)';
}
//  with frmDMMaster.QueryCTaxSum do begin
  //2014.04.01 消費税対応  before
   {with frmDMMaster.Query1Master do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    dCTax := FieldByName('TAXSUM').AsFloat;


    //2004.3.31 前と以後で処理をかえる
    if StrToDate(sYyyyMmDd) >= StrToDate('2004/3/31') then begin
      //dCTax := Trunc(dCTax);
      //2004.04.02  切り捨てから四捨五入へ
      dCTax := (dCTax+0.01) * CsTaxRate;
      dCTax := Round(dCTax);
    end else begin
      dCTax := Trunc(dCTax * CsTaxRate);
    end;


    sCTax := FieldByName('TAXSUM').AsString;
    sCTax := Real2Str(dCTax, 0, 0);
    Close;
  end;
}

{  //2014.04.01 消費税対応  after
  with frmDMMaster.QueryCTaxSum  do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
        // レコードがなければメッセージを出力して抜ける
      if RecordCount = 0 then
        begin
         //ShowMessage('該当する伝票はありません');
         Exit;
      end;

      while not EOF do begin
        curUriageKingaku := FieldByName('UriageKingaku').AsFloat;
        if FieldbyName('DDate').AsString  < CsChangeTaxDate then begin
          douTax :=  CsOldTaxRate;
        end else begin
          douTax :=  CsTaxRate;
        end;
        dCTax      := dCTax + Round(curUriageKingaku * douTax); //2014.04.01 消費税対応

        next;
      end
  end;
}
    //2014.04.01 消費税対応
{#thuyptt 20190313
   sSql := 'SELECT';
   sSql := sSql + ' SUM(Shoukei) as Soukei,';
   sSql := sSql + ' SUM(CASE WHEN NouhinDate < ''' + CsChangeTaxDate + ''' then Shoukei * ' + FloatToStr(CsOldTaxRate) + ' else Shoukei * ' +  FloatToStr(CsTaxRate) + ' end)  as Tax';
   sSql := sSql + ' FROM ' + CtblTDenpyouDetail;
   sSql := sSql + ' WHERE TokuisakiCode1 =' + '''' + sTokuisakiCode1 + '''';
   sSql := sSql + ' AND NouhinDate >= ' + '''' + DateToStr(EncodeDate(wYyyy, wMm, wDd)) + '''';
   sSql := sSql + ' AND NouhinDate <= ' + '''' + sYyyyMmDd + '''';
   sSql := sSql + ' GROUP BY TokuisakiCode1';
}

  //2019.08.16軽減税率対応 mod  before utada  for keigen-tax
   {
   sSql := 'SELECT';
   sSql := sSql + ' SUM(Shoukei) as Soukei,';
//   sSql := sSql + ' SUM(CASE WHEN NouhinDate < ''' + CsChangeTaxDate + ''' then Shoukei * ' + FloatToStr(CsOldTaxRate) + ' else Shoukei * ' +  FloatToStr(CsTaxRate) + ' end)  as Tax'; #thuyptt 20190313
   sSql := sSql + ' SUM(IIf( NouhinDate < #' + CsChangeTaxDate + '# , Shoukei * ' + FloatToStr(CsOldTaxRate) + ', Shoukei * ' +  FloatToStr(CsTaxRate) + '))  as Tax';
   sSql := sSql + ' FROM ' + CtblTDenpyouDetail;
   sSql := sSql + ' WHERE TokuisakiCode1 =' + sTokuisakiCode1;
   sSql := sSql + ' AND NouhinDate >= ' + '#' + DateToStr(EncodeDate(wYyyy, wMm, wDd)) + '#';
   sSql := sSql + ' AND NouhinDate <= ' + '#' + sYyyyMmDd + '#';
   sSql := sSql + ' GROUP BY TokuisakiCode1';
   }

   //2019.08.16軽減税率対応 add utada  for keigen-tax
   sSql := 'SELECT';
   sSql := sSql + ' SUM(IIF(tax=8,Shoukei , 0 )) * 0.08 AS Shouhizei8,' ;
   sSql := sSql + ' SUM(IIF(tax=10,Shoukei , 0 )) * 0.10 AS Shouhizei10';
   sSql := sSql + ' FROM ' + CtblTDenpyouDetail;
   sSql := sSql + ' WHERE TokuisakiCode1 = ' + sTokuisakiCode1;
   sSql := sSql + ' AND NouhinDate >= ' + '#' + DateToStr(EncodeDate(wYyyy, wMm, wDd)) + '#';
   sSql := sSql + ' AND NouhinDate <= ' + '#' + sYyyyMmDd + '#';

  with frmDMMaster.QueryCTaxSum do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;

        // レコードがなければメッセージを出力して抜ける
      if RecordCount = 0 then
        begin
         //ShowMessage('該当する伝票はありません');
         Exit;
      end;

     //2019.08.16軽減税率対応 add utada  for keigen-tax
     dCTaxOrg := FieldByName('Shouhizei8').AsFloat;
     dCTaxNew := FieldByName('Shouhizei10').AsFloat;

     //dCTax := FieldByName('Tax').AsFloat;

  end;

  //sCTax := Real2Str(dCTax, 0, 0);


  sCTax := Real2Str(dCTax, 0, 0);
  sCTaxOrg := Real2Str(dCTaxOrg, 0, 0);
  sCTaxNew := Real2Str(dCTaxNew, 0, 0);

  //2014.04.01 消費税対応  after end


  //月分の計算
  //1999/09/14
	// TDenpyouのGatsubun に何月分の売上かを入力する
  //伝票日付が締め日を越えていたら月を１進める
  sSeikyuuShimebi := MTokuisaki.GetShimebi(sTokuisakiCode1);
  sDeliveryFeeFlag := MTokuisaki.GetDeliveryFeeFlag(sTokuisakiCode1);
  sGatsubun := sYyyyMmDd;
 	sYear := Copy(sGatsubun, 1, 4);
 	sMonth := Copy(sGatsubun, 6, 2);
 	sDay := Copy(sGatsubun, 9, 2);
  if sSeikyuuShimebi = '0' then begin //月末ならそのまま
    sGatsubun := sYear + '/' + sMonth + '/01';
  end else begin
    if StrToInt(sDay) > StrToInt(sSeikyuuShimebi) then begin
    	if StrToInt(sMonth) = 12 then begin
        sMonth := '01';
      end else begin
      	sMonth := IntToStr(StrToInt(sMonth) + 1);
      end;
    end;
    sGatsubun := sYear + '/' + sMonth + '/01';
  end;

  //thuyptt 2019/05/14
  dDeliveryTax := 0;

  if (dCTax <> 0) then begin
  
    if StrToInt(sDeliveryFeeFlag) = 1 then begin

        //get tax for delivery fee
        sWhere := 'name = ' + ''''+'配達料金'+'''';
        sDeliveryFee := DMMaster.GetFieldData(CtblMConfig, 'value', sWhere);
        dDeliveryTax := CsTaxRate * StrToCurr(sDeliveryFee);


        sSqlIn := 'INSERT INTO ' + CtblTDenpyou;
        sSqlIn := sSqlIn + '(';
        sSqlIn := sSqlIn + 'TokuisakiCode1, ';
        sSqlIn := sSqlIn + 'TokuisakiCode2, ';
        sSqlIn := sSqlIn + 'InputDate, ';
        sSqlIn := sSqlIn + 'DDate, ';
        sSqlIn := sSqlIn + 'UriageKingaku, ';
        sSqlIn := sSqlIn + 'Gatsubun, ';
        sSqlIn := sSqlIn + 'CreatedDate, ';        
        sSqlIn := sSqlIn + 'Memo';
        sSqlIn := sSqlIn + ') Values (';
        sSqlIn := sSqlIn + sTokuisakiCode1 + ',';
        sSqlIn := sSqlIn + MTokuisaki.GetTokuisakiCode2(sTokuisakiCode1) + ',';
        sSqlIn := sSqlIn + '#' + DateToStr(Date()) + '#, ';
        sSqlIn := sSqlIn + '#' + sYyyyMmDd + '#, ';
        sSqlIn := sSqlIn + Real2Str(Str2Real(sDeliveryFee), 0,0) + ', ';
        sSqlIn := sSqlIn + '''' + sGatsubun + ''', ';
        sSqlIn := sSqlIn + 'Now(), ' ;
        sSqlIn := sSqlIn + '''' + '送料' + '''';
        sSqlIn := sSqlIn + ')';

        with frmDMMaster.Query1Master do begin
            Sql.Clear;
            Sql.Add(sSqlIn);
            ExecSql;
        end;//of with
    end;

   sCTax := Real2Str(dCTax+dDeliveryTax, 0, 0);

  end;
  //end thuyptt 2019/05/14

  if (sCTaxOrg = '') then begin
		 sCTaxOrg := '0';
  end;

		sSqlIn := 'INSERT INTO ' + CtblTDenpyou;
	  sSqlIn := sSqlIn + '(';
		sSqlIn := sSqlIn + 'TokuisakiCode1, ';
	  sSqlIn := sSqlIn + 'TokuisakiCode2, ';
		//sSqlIn := sSqlIn + 'DNumber, ';
		sSqlIn := sSqlIn + 'InputDate, ';
		sSqlIn := sSqlIn + 'DDate, ';
	  sSqlIn := sSqlIn + 'UriageKingaku, ';
	  sSqlIn := sSqlIn + 'Gatsubun, '; //1999/09/14
    sSqlIn := sSqlIn + 'CreatedDate, ';
		sSqlIn := sSqlIn + 'Memo, ';    
	  sSqlIn := sSqlIn + 'Bikou';
	  //sSqlIn := sSqlIn + 'KaishuuHouhou ';
		sSqlIn := sSqlIn + ') Values (';
		sSqlIn := sSqlIn + sTokuisakiCode1 + ',';
		sSqlIn := sSqlIn + MTokuisaki.GetTokuisakiCode2(sTokuisakiCode1) + ',';
		//sSqlIn := sSqlIn + 'DNumber, ';
//		sSqlIn := sSqlIn + '''' + DateToStr(Date()) + ''', '; #thuyptt 20190313
		sSqlIn := sSqlIn + '#' + DateToStr(Date()) + '#, ';
//		sSqlIn := sSqlIn + '''' + sYyyyMmDd + ''', '; #thuyptt 20190313
		sSqlIn := sSqlIn + '#' + sYyyyMmDd + '#, ';
	  sSqlIn := sSqlIn + Real2Str(Str2Real(sCTaxOrg), 0,0) + ', ';
		sSqlIn := sSqlIn + '''' + sGatsubun + ''', ';
    sSqlIn := sSqlIn + 'Now(), ' ;
		sSqlIn := sSqlIn + '''' + CsCTAX + ''', ';
	  sSqlIn := sSqlIn + '''8%''';
		sSqlIn := sSqlIn + ')';
// with frmDMMaster.QueryInsert do begin
    with frmDMMaster.Query1Master do begin
      //2007.05.15
  		//Close;
  	  Sql.Clear;
	    Sql.Add(sSqlIn);
    	ExecSql;
      //2007.05.15
  	  //Close;
	  end;//of with


  if (sCTaxNew = '') then begin
		 sCTaxNew := '0';
  end;

		sSqlIn := 'INSERT INTO ' + CtblTDenpyou;
	  sSqlIn := sSqlIn + '(';
		sSqlIn := sSqlIn + 'TokuisakiCode1, ';
	  sSqlIn := sSqlIn + 'TokuisakiCode2, ';
		//sSqlIn := sSqlIn + 'DNumber, ';
		sSqlIn := sSqlIn + 'InputDate, ';
		sSqlIn := sSqlIn + 'DDate, ';
	  sSqlIn := sSqlIn + 'UriageKingaku, ';
	  sSqlIn := sSqlIn + 'Gatsubun, '; //1999/09/14
    sSqlIn := sSqlIn + 'CreatedDate, ';
		sSqlIn := sSqlIn + 'Memo, ';    
	  sSqlIn := sSqlIn + 'Bikou';
	  //sSqlIn := sSqlIn + 'KaishuuHouhou ';
		sSqlIn := sSqlIn + ') Values (';
		sSqlIn := sSqlIn + sTokuisakiCode1 + ',';
		sSqlIn := sSqlIn + MTokuisaki.GetTokuisakiCode2(sTokuisakiCode1) + ',';
		sSqlIn := sSqlIn + '#' + DateToStr(Date()) + '#, ';
		sSqlIn := sSqlIn + '#' + sYyyyMmDd + '#, ';
	  sSqlIn := sSqlIn + Real2Str(Str2Real(sCTaxNew), 0,0) + ', ';
		sSqlIn := sSqlIn + '''' + sGatsubun + ''', ';
    sSqlIn := sSqlIn + 'Now(), ' ;
		sSqlIn := sSqlIn + '''' + CsCTAX + ''', ';
	  sSqlIn := sSqlIn + '''10%''';
		sSqlIn := sSqlIn + ')';
// with frmDMMaster.QueryInsert do begin
    with frmDMMaster.Query1Master do begin
      //2007.05.15
  		//Close;
  	  Sql.Clear;
	    Sql.Add(sSqlIn);
    	ExecSql;
      //2007.05.15
  	  //Close;
	  end;//of with


  Result := true;

except
  on E: EDBEngineError do begin
   	ShowMessage(E.Message);
    Result := False;
  end;
end;

end;


//請求期間の取得
//1999/05/04
{
Function TfrmPBill.GetBillTarm(sTokuisakiCode1:String; Var sDateFrom, sDateTo : String) : Boolean;
var
	sSql, sShiharaitsuki, sShimebi, sYyyy, sMm : String;
  iMm, iYyyy : Integer;
  iShimebi : Integer;
begin
	//締め日関連情報の取得
  iYyyy := StrToInt(CBYyyy.Text);
  iMm := StrToInt(CbMm.Text);

	//得意先コードから締め日を取得する
	iShimebi := GetShimebi(sTokuisakiCode1);
  if iShimebi = 99 then begin
  	ShowMessage('得意先コード' + sTokuisakiCode1 + 'が見つかりません');
    Exit;
  end;
  case iShimebi of
  0 :begin //月末
       sDateFrom := sYyyy + '/' + sMm + '/' + '1';
       sDateTo   := sYyyy + '/' + sMm + '/' + IntToStr(HKLib.GetGetsumatsu(iYyyy, iMm));
     end;
  10:begin
       sDateFrom := sYyyy + '/' + sMm + '/' + '11';
       //翌月の
       iMm := iMm + 1;
       if iMm = 13 then begin
       	iMm := 1;
        iYyyy := iYyyy + 1;
       end;
       sDateTo   := sYyyy + '/' + sMm + '/10';
     end;
  20:begin
       sDateFrom := sYyyy + '/' + sMm + '/' + '21';
       //翌月の
       iMm := iMm + 1;
       if iMm = 13 then begin
       	iMm := 1;
        iYyyy := iYyyy + 1;
       end;
       sDateTo   := sYyyy + '/' + sMm + '/20';
     end;

  end;//of case

end;
}


//得意先コードから支払い月を取得する
//0 当月
//1 翌月
//2 翌々月
//3 翌翌々月
function GetShiharaitsuki(sTokuisakiCode1:String) : String;
var
	sSql: String;
begin
	sSql := 'SELECT ShiharaibiM ';
  sSql := sSql + ' FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
	sSql := sSql + ' TokuisakiCode1 = ' + sTokuisakiCode1;
  with frmDMMaster.QueryShiharaibiM do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    if not EOF then begin
    	Result := FieldByName('ShiharaibiM').AsString;
    end else begin
    	Result := '';
    end;//of if
  	Close;
  end;
end;


//得意先コードから締め日を取得する
//5  日
//10 日
//15 日
//20 日
//25 日
//0  月末
function GetShimebi(sTokuisakiCode1:String) : Integer;
var
	sSql: String;
begin
	sSql := 'SELECT SeikyuuSimebi ';
  sSql := sSql + ' FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
	sSql := sSql + ' TokuisakiCode1 = ' + sTokuisakiCode1;
  with frmDMMaster.QueryShiharaibiM do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    if not EOF then begin
    	Result := FieldByName('SeikyuuSimebi').AsInteger;
    end else begin
    	Result := 99;
    end;//of if
  	Close;
  end;
end;


//前月請求残高の取得
//前月分までの売上金額（税抜き）と本日までの回収（税込み）の差額
Function GetRuikeiZandaka(sTokuisakiCode1, sDateFrom, sDateTo:String):Integer;
var
	sSql : String;
  iSum, iSum2, iSum3 : Integer;
  wYyyy,wMm,wDd:word;
begin
try

	//前月までの累計金額
  //1999/07/25 返品に対応
//  sSql := 'SELECT UriageSum = Sum(UriageKingaku) ';     #thuyptt 20190313
  sSql := 'SELECT Sum(UriageKingaku) AS UriageSum ';
  sSql := sSql + ' FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';

	sSql := sSql + 'TokuisakiCode1 = ' + sTokuisakiCode1;
	sSql := sSql + ' AND ';
	//sSql := sSql + 'DDate <= ''' + DateToStr(StrToDate(sDateFrom)-1) + '''';
// 	sSql := sSql + 'DDate < ''' + DateToStr(StrToDate(sDateFrom)) + '''';  #thuyptt 20190313  // mod utada .2016.12.06 ssDateFromが10/31の場合10/30になってしまい、締めの消費税データが入らなくなってしまうため修正
 	sSql := sSql + 'DDate < #' + DateToStr(StrToDate(sDateFrom)) + '#';

	sSql := sSql + ' AND ';
	//sSql := sSql + ' UriageKingaku > 0';
//  sSql := sSql + ' ((UriageKingaku > 0) OR (SUBSTRING(Memo, 1, 2) IN (''返品'', ''値引'')))' ;
//  sSql := sSql + ' ((UriageKingaku > 0) OR (SUBSTRING(Memo, 1 ,2) IN (''返品'', ''値引'',''取消'')))' ; #thuyptt 20190313
  sSql := sSql + ' ((UriageKingaku > 0) OR (Mid(Memo, 1 ,2) IN (''返品'', ''値引'',''取消'')))' ;

	//1999/09/25
		sSql := sSql + ' AND ';
//  sSql := sSql + ' PATINDEX(''%返金%'', Memo) = 0 '; //返金でない  #thuyptt 20190313
  sSql := sSql + ' InStr(1, Memo, ''返金'') = 0 ';

//  with frmDMMAster.QueryZandaka do begin
  with frmDMMaster.Query1Master do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    iSum := FieldByName('UriageSum').AsInteger;
   // gZenkaiSeikyuu := iSum;
    Close;
  end;

  //請求締め日までの回収金額
//  sSql := 'SELECT UriageSum = Sum(UriageKingaku) '; #thuyptt 20190313
  sSql := 'SELECT Sum(UriageKingaku) AS UriageSum ';
  sSql := sSql + ' FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';

	sSql := sSql + 'TokuisakiCode1 = ' + sTokuisakiCode1;
	sSql := sSql + ' AND ';

	sSql := sSql + ' ( ';

//	sSql := sSql + 'DDate <= ''' + sDateTo + ''''; #thuyptt 20190313
	sSql := sSql + 'DDate <= #' + sDateTo + '#';
	sSql := sSql + ' AND ';
	//sSql := sSql + ' UriageKingaku < 0';
//  sSql := sSql + ' ((UriageKingaku < 0) AND (SUBSTRING(Memo, 1, 2) NOT IN(''返品'', ''値引'')))' ;
//  sSql := sSql + ' ((UriageKingaku < 0) AND (SUBSTRING(Memo, 1, 2) NOT IN(''返品'', ''値引'', ''取消'')))' ; #thuyptt 20190313
  sSql := sSql + ' ((UriageKingaku < 0) AND (Mid(Memo, 1, 2) NOT IN(''返品'', ''値引'', ''取消'')))' ;

  //1999/08/09
 	sSql := sSql + ' OR ';
//  sSql := sSql + ' PATINDEX(''%返金%'', Memo) > 0 '; #thuyptt 20190313
  sSql := sSql + ' InStr(1, Memo, ''返金'') > 0 ';
	sSql := sSql + ' ) ';

//  with frmDMMAster.QueryZandaka do begin
  with frmDMMaster.Query1Master do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    iSum2 := FieldByName('UriageSum').AsInteger;
//    gNyuukinKingaku := iSum2;
    Close;
  end;

  //added 2001/10/12
  //消費税のマイナス分を引く
  //ラフアンドレディー対策
//  sSql := 'SELECT TaxSum = Sum(UriageKingaku) '; #thuyptt 20190313
  sSql := 'SELECT Sum(UriageKingaku) AS TaxSum';
  sSql := sSql + ' FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';

	sSql := sSql + 'TokuisakiCode1 = ' + sTokuisakiCode1;
	sSql := sSql + ' AND ';

	sSql := sSql + ' ( ';

  sDateTo := DateToStr(IncMonth(strtodate(sDateTo),-1));
//	sSql := sSql + 'DDate <= ''' + sDateTo + ''''; #thuyptt 20190313
	sSql := sSql + 'DDate <= #' + sDateTo + '#';
	sSql := sSql + ' AND ';
	//sSql := sSql + ' UriageKingaku < 0';
//  sSql := sSql + ' ((UriageKingaku < 0) AND (PATINDEX(''%#消費税#%'', Memo) > 0))' ; #thuyptt 20190313
  sSql := sSql + ' ((UriageKingaku < 0) AND (InStr(1, Memo, ''#消費税#'') > 0))' ;

	sSql := sSql + ' ) ';

 // with frmDMMAster.QueryZandaka do begin
  with frmDMMaster.Query1Master do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    iSum3 := FieldByName('TaxSum').AsInteger;
    Close;
  end;

  iSum := iSum + iSum2;
  //2005.10.10
  //hkubota 消費税のマイナスは今後考慮する必要なし？？
  //Result := iSum - iSum3;
  Result := iSum;

except
  on E: EDBEngineError do begin
    ShowMessage('請求処理に失敗しました。管理者に連絡して下さい。' + E.Message);

  end;
end;

end;

//delevery_fee    //thuyptt 2019/05/14
Function GetDeliveryFee(sTokuisakiCode1, sDateFrom, sDateTo:String):Integer;
var
  sSql : String;
  iSum : Integer;
  wYyyy,wMm,wDd:word;
begin
try

	sSql := 'SELECT Sum(UriageKingaku) AS UriageSum ';
	sSql := sSql + ' FROM ' + CtblTDenpyou;
	sSql := sSql + ' WHERE ';
	sSql := sSql + 'TokuisakiCode1 = ' + sTokuisakiCode1;
  sSql := sSql + ' AND ';
  sSql := sSql + ' DDate Between ';
  sSql := sSql + '#' + DateToStr(StrToDate(sDateFrom)) + '#';
  sSql := sSql + ' AND ';
  sSql := sSql + '#' + DateToStr(StrToDate(sDateTo)) + '#';
	sSql := sSql + ' AND ';
	sSql := sSql + ' Memo = ''送料''' ;

//  with frmDMMAster.QueryZandaka do begin
  with frmDMMaster.Query1Master do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    iSum := FieldByName('UriageSum').AsInteger;
    Close;
  end;
  Result := iSum;

except
  on E: EDBEngineError do begin
    ShowMessage('請求処理に失敗しました。管理者に連絡して下さい。' + E.Message);
  end;
end;

end;


procedure TfrmPBill.EditTokuisakiCode1Exit(Sender: TObject);
begin
	{
  if Length(EditTokuisakiCode1.Text) > 0 then begin
  	ShowMessage(EditTokuisakiCode1.Text + 'の請求書を発行します');
  end else begin
  	Exit;
  end;
  LabelTokuisakiName.Caption := GetTokuisakiName(EditTokuisakiCode1.Text);
	}
  //得意先コードの締め日を取得する
  //CBDd.Text := IntToStr(GetShimebi(EditTokuisakiCode1.Text));
  if CBDd.Text = '99' then begin
  	ShowMessage('得意先コードが見つかりません');
    CBDd.Text := '0';
    Exit;
  end;
end;


//期間限定売上金額の合計を求めるSQl文を作る
function MakeSqlUriageSum(sTokuisakiCode1, sDateFrom, sDateTo : String) : String;
var
	sSql : String;
begin
	//消費税額の計算
//  sSql := 'SELECT TAXSUM = SUM(UriageKingaku) '; #thuyptt 20190313
  sSql := 'SELECT SUM(UriageKingaku) AS TAXSUM';
  sSql := sSql + ' FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' TokuisakiCode1 =  ' + sTokuisakiCode1;
  sSql := sSql + ' AND ';
  sSql := sSql + ' DDate Between ';
//  sSql := sSql + '''' + sDateFrom + ''''; #thuyptt 20190313
  sSql := sSql + '#' + sDateFrom + '#';
  sSql := sSql + ' AND ';
//  sSql := sSql + '''' + sDateTo + ''''; #thuyptt 20190313
  sSql := sSql + '#' + sDateTo + '#';
  sSql := sSql + ' AND ';
  //sSql := sSql + ' ((UriageKingaku > 0) OR (SUBSTRING(Memo, 1, 2) IN (''返品'', ''値引'')))' ;
//  sSql := sSql + ' ((UriageKingaku > 0) OR (SUBSTRING(Memo, 1 ,2) IN (''返品'', ''値引'',''取消'')))' ; #thuyptt 20190313
  sSql := sSql + ' ((UriageKingaku > 0) OR (Mid(Memo, 1 ,2) IN (''返品'', ''値引'',''取消'')))' ;
  sSql := sSql + ' AND ';
  sSql := sSql + ' Memo <> ''' + CsCTAX + '''';

	Result := sSql;
end;

//2014.04.01 消費税対応 beggn
function GetTokuisakiTax(sTokuisakiCode1, sGatsubun, sTargetTax : String) : String;
var
	sSql : String;
  sTax : String;
begin
	//消費税額の計算
  sSql := 'SELECT UriageKingaku ';
  sSql := sSql + ' FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' TokuisakiCode1 =  ' + sTokuisakiCode1;
  sSql := sSql + ' AND ';
//  sSql := sSql + ' Gatsubun = ''' + sGatsubun + ''''; #thuyptt 20190313
  sSql := sSql + ' Gatsubun = #' + sGatsubun + '#';
  sSql := sSql + ' AND ';
  sSql := sSql + ' Memo = ''' + CsCTAX + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + ' Bikou = ''' + sTargetTax + '''';

  with frmDMMAster.QueryZandaka do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    sTax := FieldByName('UriageKingaku').AsString;
    Close;
  end;

  Result := sTax;

end;
//2014.04.01 消費税対応 end

procedure TfrmPBill.CheckBox1Click(Sender: TObject);
begin
	//請求対象期間を指定する
	if CheckBox1.Checked = True then begin
  	if EditTokuisakiCode1.Text = '' then begin
	  	ShowMessage('得意先コードを指定してください');
      EditTokuisakiCode1.Setfocus;
      Exit;
    end else begin
	  	ShowMessage(EditTokuisakiCode1.Text + 'の回収期間を指定します');
    end;
  end;

end;


procedure TfrmPBill.BitBtn4Click(Sender: TObject);
var
	i, iNengajou : Integer;
  sLine, sSql, sYuubin, sAdd1, sAdd2, sAdd3, sName : String;
  sAddPre, sTokuisakiCode1, sTokuisakiCode2, sYomi,sHeitenFlag : String;
 	F : TextFile;
begin
	//確認メッセージ
  if MessageDlg('請求書用タックシールを出力しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;
  sSql := 'SELECT * FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' SeikyuushoHassouSaki>0 ';

  //テスト用
  //sSql := sSql + ' AND TokuisakiCode1 < 100 ';

  //2000/11/20 Added
  sSql := sSql + ' AND HeitenFlag <> 1 ';

  //得意先コード指定
  if Length(EditTokuisakiCode1.Text) > 0 then begin
	  sSql := sSql + ' AND TokuisakiCode1 in (' + EditTokuisakiCode1.Text + ')';
  end;


//  sSql := sSql + ' ORDER BY TokuisakiCode1';
  sSql := sSql + ' ORDER BY ChainCode';

	//出力するファイルを作成する、すでにあれば削除する
  AssignFile(F, CFileName_Nengajou);
	Rewrite(F);
	CloseFile(F);

  //ファイルへ書き込み
  with frmDMMaster.QueryMTokuisaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    i := 0;
    while not EOF do begin
    	iNengajou := FieldByName('SeikyuushoHassouSaki').AsInteger;
      //  sYuubin, sAdd1, sAdd2, sName : String;
      sName := '';
    	sYuubin := '';
			sAdd1   := '';
      sAdd2   := '';
      sAdd3   := '';
      sName   := '';
      if iNengajou = 1 then begin  //会社
      	sYuubin := FieldByName('ZipOffice').AsString;
	    	sAdd1   := FieldByName('Add1Office').AsString;
	    	sAdd2   := FieldByName('Add2Office').AsString;
	    	sAdd3   := FieldByName('Add3Office').AsString;
        sName   := FieldByName('AtenaOffice').AsString;
      end else if iNengajou = 2 then begin //店
      	sYuubin := FieldByName('Zip').AsString;
	    	sAdd1   := FieldByName('Add1').AsString;
	    	sAdd2   := FieldByName('Add2').AsString;
	    	sAdd3   := FieldByName('Add3').AsString;
        sName   := FieldByName('TokuisakiName').AsString;
      end else if iNengajou = 3 then begin //自宅
      	sYuubin := FieldByName('ZipHome').AsString;
	    	sAdd1   := FieldByName('Add1Home').AsString;
	    	sAdd2   := FieldByName('Add2Home').AsString;
	    	sAdd3   := FieldByName('Add3Home').AsString;
        sName   := FieldByName('AtenaHome').AsString;
      end else begin
	    	ShowMessage(FieldByName('TokuisakiCode1').AsString+'のデータが不完全です');

      end;
      //2000/11/20 Added
      sTokuisakiCode1 := FieldByName('TokuisakiCode1').AsString;
      sTokuisakiCode2 := FieldByName('TokuisakiCode2').AsString;
      sYomi := FieldByName('TokuisakiNameYomi').AsString;
      sHeitenFlag := FieldByName('HeitenFlag').AsString;

      //発送先によりかえる
      if iNengajou = 1 then begin  //会社
	      sName := sName + '　御中'; //2006.06.20 御中を追加
      end else if iNengajou = 2 then begin //店
	      //sNameの整形
  	    //半角スペース以降を表示
			  if Pos(' ', sName) > 0 then begin
      		sName := Copy(sName, Pos(' ', sName)+1, Length(sName));
     	 	end else if Pos('　', sName) > 0 then begin
      		sName := Copy(sName, Pos('　', sName)+2, Length(sName));
      	end;
	      sName := sName + '　御中';
      end else if iNengajou = 3 then begin //自宅
	      sName := sName + '　様';
      end else begin
         ;
      end;

      //ファイルに書き込み
    	sLine := sYuubin;
    	sLine := sLine + ',' + sAdd2 + ',' + sAdd3;
    	sLine := sLine + ',' + sName;

      //2000/11/20 Added
    	sLine := sLine + ',' + sTokuisakiCode1;
    	sLine := sLine + ',' + sTokuisakiCode2;
    	sLine := sLine + ',' + sYomi;

      //2001/01/28 Added
    	sLine := sLine + ',' + FieldByName('Ka').asString;
    	sLine := sLine + ',' + FieldByName('TantoushaCode').asString;
    	sLine := sLine + ',' + FieldByName('ChainCode').asString;
    	sLine := sLine + ',' + FieldByName('SeikyuuSimebi').asString;

      i := i + 1;
      SB1.SimpleText := IntToStr(i) + '件目処理中 ' + sLine;
      //If Address is same then do not write.
      if (sAddPre = (sAdd2 + sAdd3)) and (Length(sAdd2 + sAdd3)>0) then begin
      	;
      end else begin
		  	HMakeFile(CFileName_Nengajou, sLine);
      end;
      sAddPre := sAdd2+sAdd3;
      Next;
    end;//of while
  end;//of with
  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', '鷹松屋宛名ラベル.xls', '', SW_SHOW);

end;

//請求明細印刷ボタンがクリックされた
procedure TfrmPBill.BitBtn5Click(Sender: TObject);
var
strTokuisakiCode1      : String;
strTokuisakiCode1Tmp   : String;
LArrayOfTokuisakiCode1 : array[0..9999] of String;
i                      : Integer;
strShimebi             : String;
sSql, sWhere           : String;
wYyyy,wMm,wDd          : word;
strShimeDate           : String;
strStartDate           : String;
strEndDate             : String;

dlg: TForm;
res: Word;
begin
//確認メッセージを出す
//2002/07/02 Added by H.Kubota
dlg := CreateMessageDialog('請求明細書を印刷しますか', mtInformation,
                             [mbYes, mbNo]);
dlg.ActiveControl := TWinControl(dlg.FindComponent('No'));
res := dlg.ShowModal;
if res <> mrYes then begin
  exit;
end;
dlg.Free;



// 取引先コード１，開始日，終了日をDenpyouPrintSeikyuuに渡す．
// この際，4通りの指定方法がある．
//　　�@ 得意先コードをカンマ区切りで複数指定
//　　�A 得意先コードを複数指定＆回収対象帰還指定
//　　�B 締日指定
//    �C 締日指定＆回収対象帰還指定

 //
 // 取引先コード１のリストを取得
 //

 // 得意先コードを指定している場合
 if EditTokuisakiCode1.Text <> '' then begin

   strTokuisakiCode1Tmp := EditTokuisakiCode1.Text + ',';

   i := 0;
   while strTokuisakiCode1Tmp <> '' do begin
	   LArrayOfTokuisakiCode1[i] := Copy(strTokuisakiCode1Tmp, 1, Pos(',', strTokuisakiCode1Tmp)-1);
     strTokuisakiCode1Tmp := Copy(strTokuisakiCode1Tmp, (Pos(',', strTokuisakiCode1Tmp)+1), Length(strTokuisakiCode1Tmp));
     i := i + 1;
   end;//of while

 end

 // 得意先コードでなく，締日を指定している場合
 else begin

   strShimebi := CBDd.Text;

//   sSql := 'SELECT ic=Count(TokuisakiCode1) FROM ' + CtblMTokuisaki; #thuyptt 20190313
   sSql := 'SELECT Count(TokuisakiCode1) AS ic FROM ' + CtblMTokuisaki;
//   sWhere := ' WHERE SeikyuuSimebi = ' + '''' + strShimebi + ''''; #thuyptt 20190313
   sWhere := ' WHERE SeikyuuSimebi = ' + strShimebi;
   sWhere := sWhere + ' and TokuisakiCode1<50000 ';

   sWhere := sWhere + ' and SeikyuushoFlag <> 1 '; // add 2015.04.19


   //Added by Hajime Kubota 2003.01.06
   //
   if Length(EditTantoushaCode.Text)>0 then begin
	   sWhere := sWhere + ' and TantoushaCode=' + EditTantoushaCode.Text;
   end;
   if Length(EditShuukeiCode.Text)>0 then begin
	   sWhere := sWhere + ' and ShuukeiCode=' + EditShuukeiCode.Text;
   end;
   if Length(EditTokuisakiCode2.Text)>0 then begin
	   sWhere := sWhere + ' and TokuisakiCode2=' + EditTokuisakiCode2.Text;
   end;

   sSql := sSql + sWhere;
   with qrySeikyuu do begin
   	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    SB1.SimpleText := FieldbyName('ic').AsString;
    if SB1.SimpleText = '0' then begin
    	ShowMessage('該当する得意先はありません'+sSql);
      exit;
    end;
    Close;
   end; //

   sSql := 'SELECT TokuisakiCode1 FROM ' + CtblMTokuisaki;
   sSql := sSql + sWhere;
   //2002.07.31
   //   sSql := sSql + ' ORDER BY TokuisakiCode1';

   //sSql := sSql + ' and TokuisakiCode1<50000 ';
   //sSql := sSql + ' and TokuisakiCode2 = 23 ';

   {
   sSql := sSql + ' ORDER BY TokuisakiCode2';
   sSql := sSql + ', TantoushaCode, Ka, TokuisakiNameYomi';
   }
   //2002.10.31 並び替えを変更
   //郵送持参(TantoushaCode)、集計コード(ShuukeiCode)、得意先コード２、よみ
   sSql := sSql + ' ORDER BY TantoushaCode desc, ShuukeiCode, TokuisakiCode2';
   sSql := sSql + ', TokuisakiNameYomi';

    SB1.SimpleText := '対象得意先リスト作成中';
    SB1.Update;
   with qrySeikyuu do begin
   	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;

    // もしレコードが無かったら，メッセージボックスを表示して処理を抜ける．
    if RecordCount =0 then
      begin
        ShowMessage('該当する得意先がありません');
        Close;
        Exit;
      end;

    // 得意先コードのリスト設定
    i := 0;
    while not EOF do begin
      LArrayOfTokuisakiCode1[i] := FieldbyName('TokuisakiCode1').AsString;
      SB1.SimpleText := intToStr(i);
      SB1.Update;
      i := i+1;
      next
    end;

    Close;
   end;

 end;//of if

 //
 // 得意先コードでループして，DenpyouPrintSeikyuuをコール
 //

 i := 0;
 while LArrayOfTokuisakiCode1[i] <> '' do begin
  SB1.SimpleText := IntTostr(i) + '件目 得意先コード=' + LArrayOfTokuisakiCode1[i];
  SB1.Update;
  // �@ 得意先コード設定
  strTokuisakiCode1 := LArrayOfTokuisakiCode1[i];

  // �A 締日取得
  strShimebi        := IntToStr(GetShimebi(strTokuisakiCode1));
  if strShimebi = '0' then begin //締め日が月末
    wDd := GetGetsumatsu(StrToInt(CBYyyy.Text), StrToInt(CBMm.Text));
  end
  else begin
    wDd := StrToint(strShimebi);
  end;
  strShimeDate := CBYyyy.Text + '/' + CBMm.Text + '/' + IntToStr(wDd);

  // �B 終了日設定
   // 回収対象期間を指定している場合は，それを終了日とする．
   if CheckBox1.Checked = True then begin
     DateTimeToString(strEndDate, 'yyyy/mm/dd', DTPTo.Date);
   end
   // 回収対象帰還を指定しない場合は，締日＝最終日とする．
   else begin
     strEndDate := strShimeDate;
   end;

  // �C 開始日設定
  DecodeDate(StrToDate(strShimeDate), wYyyy, wMm, wDd);
  if strShimebi = '0' then begin //締め日が月末
	  wDd := 1;
  end else begin
		wMm := wMm - 1;
	  if wMm = 0 then begin
  		wMm := 12;
    	wYyyy := wYyyy - 1;
	  end;
	  wDd := wDd + 1;
	end;
  strStartDate := DateToStr(EncodeDate(wYyyy, wMm, wDd));

  // グローバル変数へ値をセット
	GTokuisakiCode1 := strTokuisakiCode1;
  GStartDate      := strStartDate;
	GEndDate        := strEndDate;

  // DenpyouPrintSeikyuu呼出
  TfrmDenpyouPrintSeikyuu.Create(Self);

  i := i + 1;
 end;

 ShowMessage('印刷が完了しました');

end;


procedure TfrmPBill.BitBtn2Click(Sender: TObject);
var
	sSql, sLine, strDate, strStartDate, strEndDate, sWhere, sDd, strKenmei, strNyuukinKigen, CFileName_Bill, sCurrentTokuisakiCode1 : String;
	strTokuisakiCode1, strShimebi, strShimeDate, sTokuisakiCode1 : String;
 	F : TextFile;
	EndDate, StartDate : TDateTime;
	i, wDd, iZenkaiSeikyuu, iNyuu, iKonkaiUrige, iShouhizeiGaku,iKurikoshi  : Integer;
  sYyyyMmDd:String;
begin
	// Get Loop TokuisakiCode1
	//GetTokuisakiCodeArray();

	//出力するファイルを作成する、すでにあれば削除する
	CFileName_Bill := CbYyyy.Text + '' + CBMm.Text + '_' + CBDd.Text + '_bill.csv';
	AssignFile(F, CFileName_Bill);
	Rewrite(F);
	CloseFile(F);
  
	//Export Title
	sLine := sLine + '請求書番号';
	sLine := sLine + ',件名';
	sLine := sLine + ',発行先コード';
	sLine := sLine + ',入金期限';
	sLine := sLine + ',おもての自由項目１(文字)';
	sLine := sLine + ',おもての自由項目２(文字)';
	sLine := sLine + ',おもての自由項目３(文字)';
	sLine := sLine + ',前回請求金額';
	sLine := sLine + ',入金額';//Not yet
	sLine := sLine + ',調整金額';
	sLine := sLine + ',繰越金額';
	sLine := sLine + ',今回売上金額';
	sLine := sLine + ',消費税額';
	sLine := sLine + ',今回合計金額';
	sLine := sLine + ',おもての請求金額';
	sLine := sLine + ',おもての自由項目1(数値)';
	sLine := sLine + ',おもての自由項目2(数値)';
	sLine := sLine + ',おもての自由項目3(数値)';
	sLine := sLine + ',おもての自由項目4(数値)';
	sLine := sLine + ',おもての自由項目5(数値)';
	sLine := sLine + ',おもての自由項目6(数値)';
	sLine := sLine + ',おもての自由項目7(数値)';
	sLine := sLine + ',締日';
	sLine := sLine + ',備考';
	sLine := sLine + ',顧客コード1';
	sLine := sLine + ',顧客コード2';
	sLine := sLine + ',EDI情報';
	sLine := sLine + ',担当';
	sLine := sLine + ',明細日付';
	sLine := sLine + ',明細番号';
	sLine := sLine + ',明細項目';
	sLine := sLine + ',数量';
	sLine := sLine + ',単価';
	sLine := sLine + ',金額';
	sLine := sLine + ',消費税額';
	sLine := sLine + ',請求金額';
	sLine := sLine + ',単位';
	sLine := sLine + ',明細の自由項目2(文字)';
	sLine := sLine + ',明細の自由項目3(文字)';
	sLine := sLine + ',明細の自由項目4(文字)';
	sLine := sLine + ',明細の自由項目5(文字)';
	sLine := sLine + ',明細の自由項目6(文字)';
	sLine := sLine + ',明細の自由項目7(文字)';
	sLine := sLine + ',明細の自由項目8(文字)';
	sLine := sLine + ',明細の自由項目9(文字)';
	sLine := sLine + ',明細の自由項目10(文字)';
	sLine := sLine + ',明細の自由項目11(文字)';
	sLine := sLine + ',備考';
	sLine := sLine + ',振込先コード';
	sLine := sLine + ',金融機関コード';
	sLine := sLine + ',金融機関名';
	sLine := sLine + ',金融機関名カナ';
	sLine := sLine + ',支店コード';
	sLine := sLine + ',支店名';
	sLine := sLine + ',支店名カナ';
	sLine := sLine + ',預金種別';
	sLine := sLine + ',口座番号';
	sLine := sLine + ',預金者名';
	sLine := sLine + ',預金者名カナ';

	HMakeFile(CFileName_Bill, sLine);



		strShimebi := CBDd.Text;

		sSql := 'SELECT TokuisakiCode1 FROM ' + CtblMTokuisaki;
		sSql := sSql + ' WHERE ';
{  #thuyptt 20190313
    if EditTokuisakiCode1.Text = '' then begin
          sSql := sSql + ' SeikyuuSimebi = ' + '''' + strShimebi + '''';
    end else begin
          sSql := sSql + ' TokuisakiCode1 = ' + '''' + EditTokuisakiCode1.Text  + '''';
    end;
}
    if EditTokuisakiCode1.Text = '' then begin
          sSql := sSql + ' SeikyuuSimebi = ' +  strShimebi;
    end else begin
          sSql := sSql + ' TokuisakiCode1 = ' + EditTokuisakiCode1.Text;
    end;
		sSql := sSql + ' AND TokuisakiCode1<50000 ';
    //sSql := sSql + ' AND SeikyuushoFlag <> 1';  2017.01.27コメントアウト
		//sSql := sSql + ' ORDER BY TantoushaCode DESC, ShuukeiCode, TokuisakiCode2, TokuisakiNameYomi';
    sSql := sSql + ' ORDER BY TokuisakiCode1';
		with qrySeikyuu do begin
		  Close;
		  Sql.Clear;
		  Sql.Add(sSql);
      Open;
 		  SB1.SimpleText := '請求データの出力中 ';   //koko
      SB1.Update;


      while not EOF do begin
	      strTokuisakiCode1 := FieldbyName('TokuisakiCode1').AsString;
        SB1.SimpleText := '請求データの出力中 ' + strTokuisakiCode1;
        SB1.Update;


        // �A 締日取得
        strShimebi        := IntToStr(GetShimebi(strTokuisakiCode1));

        if strShimebi = '0' then begin //締め日が月末
          wDd := GetGetsumatsu(StrToInt(CBYyyy.Text), StrToInt(CBMm.Text));
          strKenmei := '末日請求書';
        end
	  	  else begin
		     wDd := StrToint(strShimebi);
         strKenmei := strShimebi + '日請求書';
        end;


        strShimeDate := CBYyyy.Text + '/' + CBMm.Text + '/' + IntToStr(wDd);

       // �B 終了日設定
      // 回収対象期間を指定している場合は，それを終了日とする．
        if CheckBox1.Checked = True then begin
           DateTimeToString(strEndDate, 'yyyy/mm/dd', DTPTo.Date);
        end
    	   // 回収対象帰還を指定しない場合は，締日＝最終日とする．
        else begin
  		    strEndDate := strShimeDate;

   	    end;
        // �C 開始日設定


        if strShimebi = '0' then begin //締め日が月末
          wDd := GetGetsumatsu(StrToInt(CBYyyy.Text), StrToInt(CBMm.Text));
          strKenmei := '末日請求書';
          strStartDate :=  CbYyyy.text + '/' + CBMm.text + '/01';
        end
	  	  else begin
		      wDd := StrToint(strShimebi);
          strKenmei := strShimebi + '日請求書';
          strStartDate :=  DateToStr(IncMonth(StrToDate(strShimeDate), -1) + 1);     // mod utada

        end;


        i := i + 1;

        sYyyyMmDd   := CbYyyy.text + '/' + CBMm.text + '/01';

				if (sCurrentTokuisakiCode1 <> FieldByName('TokuisakiCode1').AsString) then begin
			    GetNyuukinKigenAndShimebi(FieldByName('TokuisakiCode1').AsString, CbYyyy.Text + '/' + CBMm.Text);
          sTokuisakiCode1 := FieldByName('TokuisakiCode1').AsString;
         // gZenkaiSeikyuu :=0;
         // gNyuukinKingaku :=0;

          //前回請求金額 = 前回売上額 + 前回までの繰越
          iZenkaiSeikyuu := GetZenkaiSeikyuu(sTokuisakiCode1, sYyyyMmDd);
          //入金額
			    //iNyuu := GetNyuu(sTokuisakiCode1, strStartDate, strEndDate);

          //繰越金額     /前回請求金額,入金額も一緒に取得
          iKurikoshi :=  GetRuikeiZandaka(sTokuisakiCode1, strStartDate, strEndDate); //GetKurikoshi(sTokuisakiCode1, strStartDate);

			    iKonkaiUrige := GetKonkaiUrige(sTokuisakiCode1, strStartDate, strEndDate);
				  iShouhizeiGaku := GetShouhizeiGaku(sTokuisakiCode1, sYyyyMmDd);
        end;


      //Begin Loop TokuisakiCode1
{   #thuyptt 20190313
        sSql := 'SELECT DD.TokuisakiCode1, DD.Nyuuryokusha, DD.NouhinDate, DD.DenpyouCode, DD.No, DD.Suuryou, DD.Tannka, DD.Shoukei, MI.Name, MI.Tanni';
        sSql := sSql + ' FROM ' + CtblTDenpyouDetail + ' DD ';
        sSql := sSql + ' INNER JOIN ' + CtblTDenpyou + ' D ';
        sSql := sSql + ' ON DD.DenpyouCode = D.DenpyouCode';
        sSql := sSql + ' INNER JOIN ' + CtblMItem + ' MI ';
        sSql := sSql + ' ON DD.Code1 = MI.Code1 AND DD.Code2 = MI.Code2';
        sSql := sSql + ' WHERE ';
        sSql := sSql + ' DD.NouhinDate >= ' +  '''' + strStartDate + '''';        // mod utada .2017.01.11 >=に変更した 
        sSql := sSql + ' AND DD.NouhinDate <= ' +  '''' + strEndDate + '''';
        sSql := sSql + ' AND DD.TokuisakiCode1 in (' + strTokuisakiCode1 + ')';
        sSql := sSql + ' AND D.Teiseiflg != 2';
        sSql := sSql + ' ORDER BY DD.TokuisakiCode1, DD.NouhinDate, DD.DenpyouCode, DD.No';
}
        sSql := 'SELECT DD.TokuisakiCode1, DD.Nyuuryokusha, DD.NouhinDate, DD.DenpyouCode, DD.No, DD.Suuryou, DD.Tannka, DD.Shoukei, MI.Name, MI.Tanni';
        sSql := sSql + ' FROM ((' + CtblTDenpyouDetail + ' AS DD ';
        sSql := sSql + ' INNER JOIN ' + CtblTDenpyou + ' AS D ';
        sSql := sSql + ' ON DD.DenpyouCode = D.DenpyouCode)';
        sSql := sSql + ' INNER JOIN ' + CtblMItem + ' MI ';
        sSql := sSql + ' ON DD.Code1 = MI.Code1 AND DD.Code2 = MI.Code2)';
        sSql := sSql + ' WHERE ';
        sSql := sSql + ' DD.NouhinDate >= ' +  '#' + strStartDate + '#';        // mod utada .2017.01.11 >=に変更した
        sSql := sSql + ' AND DD.NouhinDate <= ' +  '#' + strEndDate + '#';
        sSql := sSql + ' AND DD.TokuisakiCode1 in (' + strTokuisakiCode1 + ')';
        sSql := sSql + ' AND D.Teiseiflg <> 2';
        sSql := sSql + ' ORDER BY DD.TokuisakiCode1, DD.NouhinDate, DD.DenpyouCode, DD.No';
        sCurrentTokuisakiCode1 := '';

        with frmDMMaster.QueryMTokuisaki do begin
				Close;
				Sql.Clear;
				Sql.Add(sSql);
				Open;


        //当月の売上がなく、繰越金額、入金が発生している場合、ヘッダーだけ出す
        if (RecordCount = 0) AND (iKurikoshi <> 0) then begin

          sLine := '';//FieldByName('DenpyouCode').AsString + '-' + FieldByName('No').AsString;
          sLine := sLine + ',' + strKenmei;
          sLine := sLine + ',' + sTokuisakiCode1;
          sLine := sLine + ',' + strDTShiharaibi;
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + ''; //IntToStr(iZenkaiSeikyuu);// IntToStr(gZenkaiSeikyuu);
          sLine := sLine + ',' + ''; //IntToStr(gNyuukinKingaku);
          sLine := sLine + ',' + ''; //Not yet
          sLine := sLine + ',' + IntToStr(iKurikoshi); // IntToStr(iZenkaiSeikyuu-iNyuu);
          sLine := sLine + ',' + IntToStr(iKonkaiUrige);
          sLine := sLine + ',' + IntToStr(iShouhizeiGaku);
          sLine := sLine + ',' + IntToStr(iKonkaiUrige+iShouhizeiGaku);
          sLine := sLine + ',' + IntToStr(iKonkaiUrige+iShouhizeiGaku+iKurikoshi);
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + strDTShimebi;
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '1'; //dumy
          sLine := sLine + ',' + strDTShimebi;
          sLine := sLine + ',' + '1'; //dumy
          sLine := sLine + ',' + '1'; //dumy
          sLine := sLine + ',' + '0'; //dumy
          sLine := sLine + ',' + '0'; //dumy
          sLine := sLine + ',' + '0'; //dumy
          HMakeFile(CFileName_Bill, sLine);

        end else begin


				while not EOF do begin

          sLine := FieldByName('DenpyouCode').AsString + '-' + FieldByName('No').AsString;
          sLine := sLine + ',' + strKenmei;
          sLine := sLine + ',' + FieldByName('TokuisakiCode1').AsString;
          sLine := sLine + ',' + strDTShiharaibi;
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';//IntToStr(gZenkaiSeikyuu);
          sLine := sLine + ',' + '';//IntToStr(gNyuukinKingaku);
          sLine := sLine + ',' + '';//Not yet
          sLine := sLine + ',' + IntToStr(iKurikoshi); // IntToStr(iZenkaiSeikyuu-iNyuu);
          sLine := sLine + ',' + IntToStr(iKonkaiUrige);
          sLine := sLine + ',' + IntToStr(iShouhizeiGaku);
          sLine := sLine + ',' + IntToStr(iKonkaiUrige+iShouhizeiGaku);
          sLine := sLine + ',' + IntToStr(iKonkaiUrige+iShouhizeiGaku+iKurikoshi);
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + strDTShimebi;
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + FieldByName('Nyuuryokusha').AsString;
          sLine := sLine + ',' + FieldByName('NouhinDate').AsString;
          sLine := sLine + ',' + FieldByName('DenpyouCode').AsString + '-' + FieldByName('No').AsString;
          sLine := sLine + ',' + FieldByName('Name').AsString;
          sLine := sLine + ',' + FieldByName('Suuryou').AsString;
          sLine := sLine + ',' + FieldByName('Tannka').AsString;
          sLine := sLine + ',' + FieldByName('Shoukei').AsString;
          sLine := sLine + ',' + '0';
          sLine := sLine + ',' + '0';
          sLine := sLine + ',' + FieldByName('Tanni').AsString;
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';
          sLine := sLine + ',' + '';

          HMakeFile(CFileName_Bill, sLine);

          //sCurrentTokuisakiCode1 := FieldByName('TokuisakiCode1').AsString;
          Next;
        end;//end while

      end;
      
		end;//end with
		//End Loop TokuisakiCode1

 	 sCurrentTokuisakiCode1 := strTokuisakiCode1;// FieldByName('TokuisakiCode1').AsString;
	 Next;
   end;  //end while tokusaki
   Close;
	 end;  //qrySeikyuu

	
	//エクセルの起動
	ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', PChar(CFileName_Bill), '', SW_SHOW);
end;

end.
