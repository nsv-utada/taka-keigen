unit MitsumoriArari;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, ComCtrls, StdCtrls, Buttons, ExtCtrls, Grids, Db, DBTables, ShellAPI,
  DBXpress, SqlExpr;

type
  TfrmMitsumoriArari = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    DTPFrom: TDateTimePicker;
    DTPTo: TDateTimePicker;
    Label3: TLabel;
    Label4: TLabel;
    SB1: TStatusBar;
    EditArariFrom: TEdit;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Panel2: TPanel;
    BtnClose: TBitBtn;
    BitBtn3: TBitBtn;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Shape1: TShape;
    EditArariTo: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure EditArariFromKeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn3Click(Sender: TObject);
    procedure BtnCloseClick(Sender: TObject);
  private
    { Private declarations }
function Replace(Str, Before, After: string): string;

  public
    { Public declarations }
  end;

var
  frmMitsumoriArari: TfrmMitsumoriArari;

implementation
uses
  HKLib, DMMaster,inter, Types, PasswordDlg;

{$R *.dfm}

procedure TfrmMitsumoriArari.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TfrmMitsumoriArari.FormCreate(Sender: TObject);
begin

	Width := 510;
  Height := 310;
  DTPTo.Date := Date();
  DTPFrom.Date := Date();

end;



procedure TfrmMitsumoriArari.EditArariFromKeyPress(Sender: TObject;
  var Key: Char);
var
AcceptStr: String;

begin
    AcceptStr := '0123456789.';

    if (Pos(Key,AcceptStr)=0) and (Ord(Key) <> VK_BACK) and (Ord(Key) <> VK_RETURN) then

      Key := #00

    else

      Key := Key;

    end;


procedure TfrmMitsumoriArari.BitBtn3Click(Sender: TObject);

var
  sLine, sSql: string;
  sCreateDate,sUser,sTokuisakiCode,sTokuisakiName,sMitsumoriNo,sHArari,
  sCourseCode,sChainCode,sCode,sBikou,sStatus,sKakakuKubun,sMemo,sGCode:String;
  i   :Integer;
 	F : TextFile;

begin

  if EditArariFrom.Text <> '' then begin
     if (StrToInt(EditArariFrom.Text) < 0) or (StrToInt(EditArariFrom.Text) > 100) then
     begin
       showMessage('粗利率は0〜100の範囲で入力して下さい。');
       EditArariFrom.SetFocus ;
       exit;
     end;
  end;

  if EditArariTo.Text <> '' then begin
     if (StrToInt(EditArariTo.Text) < 0) or (StrToInt(EditArariTo.Text) > 100) then
     begin
       showMessage('粗利率は0〜100の範囲で入力して下さい。');
       EditArariTo.SetFocus ;
       exit;
     end;
  end;

  Screen.Cursor := crHourGlass;

	//スタートトランザクション
  sSql := 'SELECT';
  sSql := sSql + ' a.CreateDate,a.UserID,UserName,b.TokuisakiCode1,b.TokuisakiCode2,b.ChainCode,';
  sSql := sSql + ' a.TokuisakiName,a.MitsumoriNo,a.HArari,a.status,a.Tanka,b.Memo,b.Bikou,b.GCode';
  sSql := sSql + ' FROM ' + CtblTMitsumori + ' AS a';
  sSql := sSql + ' LEFT JOIN ' + CtblMTokuisaki + ' AS b';
  sSql := sSql + ' ON a.tokuisakicode = b.tokuisakicode1';
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' a.CreateDate >=  ''' + DateToStr(DTPFrom.Date) + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + ' a.CreateDate <=  ''' + DateToStr(DTPTo.Date) + '''';
  if EditArariFrom.Text <> '' then begin
    sSql := sSql + ' AND ';
    sSql := sSql + ' a.Harari >= ''' + EditArariFrom.Text + '''';
  end;
  if EditArariTo.Text <> '' then begin
    sSql := sSql + ' AND ';
    sSql := sSql + ' a.Harari <= ''' + EditArariTo.Text + '''';
  end;
  sSql := sSql + ' ORDER BY ';
  sSql := sSql + ' a.CreateDate';

  i := 1;
  with frmDMMaster.QueryMTokuisaki do begin
		Close;
    Sql.Clear;
   	Sql.Add(sSql);
	  open;

    if EOF then begin
    	ShowMessage('該当するデータは見つかりませんでした。');
      Screen.Cursor := crDefault;
      Exit;
    end;

	//出力するファイルを作成する、すでにあれば削除する
 	AssignFile(F, CFileName_M_ArariKensaku);
  Rewrite(F);
	CloseFile(F);

  //検索条件の出力
  sLine := DateToStr(DTPFrom.Date) + '〜' +  DateToStr(DTPTo.Date);
  sLine := sLine + ',' +  EditArariFrom.Text + '〜' +  EditArariTo.Text;

 	HMakeFile(CFileName_M_ArariKensaku, sLine);

    while not EOF do begin
			SB1.SimpleText := '出力中 -> '+ IntToStr(i) + ':' + FieldByName('CreateDate').AsString;
      Update;

      sMemo := '';
      sBikou:='';
      sCreateDate    := FieldByName('CreateDate').AsString;
      sUser          := FieldByName('UserId').AsString + '.' + FieldByName('UserName').AsString;
      sTokuisakiCode := FieldByName('TokuisakiCode1').AsString;
      sCourseCode    := FieldByName('TokuisakiCode2').AsString;
      sChainCode     := FieldByName('ChainCode').AsString;
      sTokuisakiName := FieldByName('TokuisakiName').AsString;
      sMitsumoriNo   := FieldByName('MitsumoriNo').AsString;
      sHArari        := FieldByName('HArari').AsString;
      sStatus        := FieldByName('status').AsString;
      sKakakuKubun   := FieldByName('Tanka').AsString;
      sMemo          := Replace(FieldByName('Memo').AsString,#13#10,' ');
      sBikou         := Replace(FieldByName('Bikou').AsString,#13#10,' ');
      sGCode         := FieldByName('GCode').AsString;

      case StrToInt(sStatus)  of
      0: sStatus := '仮保存' ;
      1: sStatus := '見積中';
      2: sStatus := '本登録済';
      end;


      sLine := sCreateDate + ', ' + sUser + ',' + sTokuisakiCode + ',' + sCourseCode;
      sLine := sLine + ',' + sChainCode + ',' + sTokuisakiName + ',' + sMitsumoriNo;
      sLine := sLine + ',' + sHArari + ',' + sStatus + ',' + sKakakuKubun;
      sLine := sLine + ',' + sMemo + ',' + sBikou + ',' + sGCode;
 		  HMakeFile(CFileName_M_ArariKensaku, sLine);
      i := i + 1;
     	Next;
    end;//of while
    Close;
  end;//of with

  Screen.Cursor := crDefault;


	//エンドトランザクション
  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '見積一覧.xls', '', SW_SHOW);


end;

procedure TfrmMitsumoriArari.BtnCloseClick(Sender: TObject);
begin
 Close;

end;

function TfrmMitsumoriArari.Replace(Str, Before, After: string): string;
var
  Len, N: Integer;
begin
  Len := Length(Before) - 1;
  N := AnsiPos(Before, Str);
  while N > 0 do begin
    Result := Result + Copy(Str, 1, N - 1) + After;
    Delete(Str, 1, N + Len);
    N := AnsiPos(Before, Str)
  end;
  Result := Result + Str
end;

end.
