unit Denpyou;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, ComCtrls, StdCtrls, Buttons, ExtCtrls, Grids, Db, DBTables, ShellAPI;

type
  TfrmDenpyou = class(TfrmMaster)
    Label3: TLabel;
    DTP1: TDateTimePicker;
    Label4: TLabel;
    BitBtn3: TBitBtn;
    Label5: TLabel;
    CBTokuisakiName: TComboBox;
    SG1: TStringGrid;
    lSum: TLabel;
    Label6: TLabel;
    CBNyuuryokusha: TComboBox;
    Label7: TLabel;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    QueryDenpyou: TQuery;
    Label8: TLabel;
    BitBtn7: TBitBtn;
    cmdKennsaku: TButton;
    edbYomi: TEdit;
    cmdGo: TButton;
    Label10: TLabel;
    Label11: TLabel;
    RadioGroup1: TRadioGroup;
    cmdDenpyouBanngouListMake: TButton;
    cmbDenpyouBanngou: TComboBox;
    Label12: TLabel;
    chbTeisei: TCheckBox;
    Panel5: TPanel;
    Label9: TLabel;
    lbTokuisakiCode2: TLabel;
    Label13: TLabel;
    lbChainCode: TLabel;
    Label14: TLabel;
    MemoBikou: TMemo;
    Timer1: TTimer;
    Label15: TLabel;
    edbBikou1: TEdit;
    BitBtnExcel: TBitBtn;
    lbiInputNo: TLabel;
    Label16: TLabel;
    lbAriaCode: TLabel;
    Query2: TQuery;
    EditGatsubun: TEdit;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    GB1: TGroupBox;
    rbHenpin: TRadioButton;
    rbTorikeshi: TRadioButton;
    oldDenpyouBangou: TLabel;
    btnShowAll: TButton;
    Label23Status: TLabel;
    Button2: TButton;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure CBTokuisakiNameKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBTokuisakiNameExit(Sender: TObject);
    procedure SG1DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure cmdKennsakuClick(Sender: TObject);
    procedure cmdGoClick(Sender: TObject);
    procedure edbYomiChange(Sender: TObject);
    procedure SG1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBNyuuryokushaExit(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure cmdDenpyouBanngouListMakeClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure DTP1Exit(Sender: TObject);
    procedure BitBtnExcelClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnShowAllClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private 宣言 }
    procedure GetGatsubun(D: TDate);
		procedure MakeSG;
		procedure MakeSG2;
		procedure MakeCBTokuisakiName(sKey, sWord:String; iKubun:Integer);
    procedure DenpyouBanngouListMake; //2003.03.10
    function  GetAriaCode(sTokuisakiCode1:String) : String;
  public
    { Public 宣言 }
  end;
  const
	//グリッドの列の定義

  CintNum           = 0;
  CintItemCode1     = 1;
  CintItemCode2     = 2;
  CintItemName      = 3;
  CintItemTax       = 4;
  CintItemYomi      = 5;
  CintItemMemo      = 6;
  CintItemKikaku    = 7;
  CintItemIrisuu    = 8;
  CintItemCount     = 9;
  CintItemTanni     = 10;
  CintItemTanka     = 11;
  CintItemShoukei   = 12;
  CintItemHindo     = 13;
  CintItemSaishuu   = 14;
  CintEnd           = 15;   //ターミネーター

var
  frmDenpyou : TfrmDenpyou;
  iInputNo   : Integer;        // 入力順番用変数
  iBtnAll    : Integer; 

implementation
uses
	Denpyou2,TokuisakiKensaku,ItemKensaku,
  HKLib, DMMaster,inter,MTokuisaki;

var
 giSum :integer;
 giHit : integer;
{$R *.DFM}

procedure TfrmDenpyou.FormCreate(Sender: TObject);
begin

  //inherited;
  top  := 5;
  Left := 5;
	Width := 700+50;
  Height := 700;
  DTP1.Date := Date();

  iBtnAll := 0;

  //2002.08.29
  //GTokuisakiCode := '';

  //ストリンググリッドの作成
  MakeSG;

  // よみ使用可/不使用の初期値を不使用にする
  RadioGroup1.ItemIndex := 0;
  memobikou.Text := '';

end;

procedure TfrmDenpyou.MakeSG2;
var
	i               : Integer;
  sTokuisakiCode1 : String;
  sSql,sWhere     : String;
  gRowNum         : Integer;
  sFoodParkFlg    : String;
  sTax            : String;
  strDDate        : String;  
begin
   Label23Status.Caption := '';

  //2002.06.29 Added by H.Kubota
  edbBikou1.Text := '';

  //2004.03.30 Added by H.Kubota
  // 入力順番用変数クリア
  iInputNo := 0;

  // Get TokuisakiCode1
  sTokuisakiCode1 := Copy(CBTokuisakiName.Text, 1, (pos(',', CBTokuisakiName.Text)-1));

  //得意先の情報を取得する 2002.04.26 H.K.
  // TokuisakiCode2,EstimateMemo,ChainCode
  sWhere := ' TokuisakiCode1 = ' + sTokuisakiCode1;
  lbTokuisakiCode2.Caption := GetFieldData(CtblMTokuisaki, 'TokuisakiCode2', sWhere);
  lbChainCode.Caption      := GetFieldData(CtblMTokuisaki, 'ChainCode', sWhere);
  MemoBikou.Text    := GetFieldData(CtblMTokuisaki, 'Bikou', sWhere);



  // add start 20150616 utada
  sFoodParkFlg := GetFieldData(CtblMTokuisaki, 'FoodParkFlg', sWhere);
  if sFoodParkFlg = '0' then begin
      rbTorikeshi.Enabled := False;
  end else if sFoodParkFlg = '1' then begin
    rbTorikeshi.Enabled := true;
  end else begin
    rbTorikeshi.Enabled := False;
  end;
  // add end  20150616 utada

  // Make StringGrid
  if sTokuisakiCode1 <> '' then

    // Make SQL
 	  sSql := 'SELECT * FROM ' + CviwMItems;
    sSql := sSql + ' WHERE TokuisakiCode =  ' + sTokuisakiCode1;
    sSql := sSql + ' AND DispFlag = 0';
    sSql := sSql + ' ORDER BY Hindo desc ,Code1 ASC,Code2 ASC';

    if iBtnAll = 1 then begin
      sSql := 'SELECT * FROM ' + CviwMItems;
      sSql := sSql + ' WHERE TokuisakiCode =  ' + sTokuisakiCode1;
      sSql := sSql + ' AND DispFlag = 0 AND Hindo >  0';  //thuyptt 2019/05/07';
      sSql := sSql + ' ORDER BY Hindo desc ,Code1 ASC,Code2 ASC';
    end;

    iBtnAll := 0;

    // Excecute SQL
    with QueryDenpyou do begin
    	Close;
      Sql.Clear;
      Sql.Add(sSql);
      Open;

      // もしレコードが無かったら，メッセージボックスを表示して処理を抜ける．
      if RecordCount =0 then
        begin
          ShowMessage('この得意先に登録されている商品がありません');
          Close;
          Exit;
        end;

      // Set RowCount
      gRowNum := RecordCount + 1;  // タイトル分1行足してる
      SG1.RowCount := gRowNum;

      //Get Tax
      strDDate := DateToStr(DTP1.Date);

      // Set Data to StringGrid
      i := 0;
      while not EOF do begin
    	  i := i+1;
        sTax      :=  FieldbyName('tax').AsString  + '%';

        //thuyptt comment out
        //if strDDate < CsChangeTaxtDate10 then begin
        //  sTax      :=  FloatToStr(CsTaxRate * 100) + '%';
        //end;

        with SG1 do begin
      	  Cells[CintItemCode1,   i] := FieldbyName('Code1').AsString;
      	  Cells[CintItemCode2,   i] := FieldbyName('Code2').AsString;
      	  Cells[CintItemName,    i] := FieldbyName('Name').AsString;
      	  Cells[CintItemTax,     i] := sTax;
      	  Cells[CintItemYomi,    i] := FieldbyName('Yomi').AsString;
      	  Cells[CintItemMemo,    i] := FieldbyName('Memo').AsString;  //thuyptt 2019.09.22
      	  Cells[CintItemKikaku,  i] := FieldbyName('Kikaku').AsString;
      	  Cells[CintItemIrisuu,  i] := FieldbyName('Irisuu').AsString;
      	  Cells[CintItemHindo,   i] := FieldbyName('Hindo').AsString;
      	  //Cells[CintItemSaishuu, i] := FieldbyName('SaishuuShukkabi').AsString;
      	  Cells[CintItemSaishuu, i] := FieldbyName('SaishuuShukkabi').AsString;
      	  Cells[CintItemCount,   i] := '';
      	  Cells[CintItemTanni,   i] := FieldbyName('Tanni').AsString;
      	  Cells[CintItemTanka,   i] := FieldbyName('Tanka').AsString;
      	  Cells[CintItemShoukei, i] := '';
          //Added h.Kubota 2004.03.30
      	  Cells[CintNum, i] := '';
        end;
        next
      end;
      Close;
    end;
end;

procedure TfrmDenpyou.MakeSG;
begin
	with SG1 do begin
    DefaultDrawing     := True;
    RowCount           := 10;   // とりあえず最初は10にしておく．MakeSG2の中で動的に設定 ふな copyText
    ColCount           := CintEnd;
    Align              := alBottom;
    FixedCols          := 0;
    FixedColor         := clYellow;
    Canvas.Font.Size   := 11;
    DefaultRowHeight   := 20;
    ColWidths[0]       := 64;
    Font.Color         := clNavy;


    //ColWidths[CintNum]:= 0; // 並び順に変更があった為，ここの順番は滅茶苦茶
    // Cells[CintNum, 0] := '分類名';

    ColWidths[CintNum]:= 10;
    Cells[CintNum, 0] := '';

    ColWidths[CintItemCode1]:= 0;
    Cells[CintItemCode1, 0] := '商品コード1';

    ColWidths[CintItemCode2]:= 0;
    Cells[CintItemCode2, 0] := '商品コード2';

    ColWidths[CintItemName]:= 100+50;
    Cells[CintItemName, 0] := '商品名';  

    ColWidths[CintItemTax]:= 60;
    Cells[CintItemTax, 0] := '消費税';

    ColWidths[CintItemYomi]:= 0;
    Cells[CintItemYomi, 0] := '商品読み';

    ColWidths[CintItemMemo]:= 100;
    Cells[CintItemMemo, 0] := 'メモ';

    ColWidths[CintItemKikaku]:= 0;
    Cells[CintItemKikaku, 0] := '規格';        // このデータは下記へ統合された．

    ColWidths[CintItemIrisuu]:= 60;
    Cells[CintItemIrisuu, 0] := '規格';        // 旧入り数が規格へ変更された．

    ColWidths[CintItemHindo]:= 60;
    Cells[CintItemHindo, 0] := '頻度';

    ColWidths[CintItemSaishuu]:= 80;
    Cells[CintItemSaishuu, 0] := '最終出荷日';

    ColWidths[CintItemTanni]:= 30;
    Cells[CintItemTanni, 0] := '単位';

    ColWidths[CintItemCount]:= 60;
    Cells[CintItemCount, 0] := '数量';

    ColWidths[CintItemTanka]:= 60;
    Cells[CintItemTanka, 0] := '単価';

    ColWidths[CintItemShoukei]:= 60;
    Cells[CintItemShoukei, 0] := '小計';

  end;
end;

procedure TfrmDenpyou.BitBtn3Click(Sender: TObject);
begin
  TfrmTokuisakiKensaku.Create(Self);
end;

procedure TfrmDenpyou.BitBtn4Click(Sender: TObject);
begin
  TfrmItemKensaku.Create(Self);
end;

procedure TfrmDenpyou.FormActivate(Sender: TObject);
begin

  // 入力順番用変数クリア
  iInputNo := 0;

	//EditSuuryou.SetFocus;
{
	if (GTokuisakiCode<>'') then begin
   	CBTokuisakiName.Text := GTokuisakiCode + ',' + GTokuisakiName2;
	  CBNyuuryokusha.DroppedDown := True;
  	MakeSg2;
    GTokuisakiCode := '';
    GTokuisakiName2 := '';
  end;
}
  //2001.12.26 Added by H.K.
{
  if CBNyuuryokusha.Text ='' then begin
	  CBNyuuryokusha.SetFocus;
  end;
}
  //2002.07.05 Added by H.K.
  //検索後の伝票番号をクリアする。
  cmbDenpyouBanngou.Text := '';

  refresh;

  //2002/08/29
  if CBNyuuryokusha.Text ='' then begin
	  CBNyuuryokusha.SetFocus;
  end else begin
	  //CBTokuisakiName.SetFocus;
    edbYomi.SetFocus; //2002.08.30 高橋さんの指示で変更
  end;

//2003.02.09
//入力者をテキストファイルから読む
//CBNyuuryokusha.Items.LoadFromFile('\\ytaka01\bin\nyuuryokusha.txt');
CBNyuuryokusha.Items.LoadFromFile('nyuuryokusha.txt');

//2003.3.10
//伝票が正常に登録されたかどうか確認するために追加した
if GDenpyouFlag = 1 then begin
  DenpyouBanngouListMake;
  //検索ボタンのクリック
  //cmdKennsaku.OnClick(sender);
end;

//if GDenpyouCode <> '' then begin
  //cmbDenpyouBanngou.Text := GDenpyouCode;
  //cmdKennsaku.OnClick(Sender);
//end;





//if GInfoDenpyouno <> '' then begin
 // cmbDenpyouBanngou.Visible := False;
 // Label7.Visible := False;
 // cmdDenpyouBanngouListMake.Visible := False;
  //cmdKennsaku.Visible := False;
  //Label18.Caption := GInfoDenpyouno;
//end;

end;

procedure TfrmDenpyou.FormResize(Sender: TObject);
begin
	SG1.Align := alClient;
end;

procedure TfrmDenpyou.CBTokuisakiNameKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin

	if (Key=VK_F1) then begin

    // Make TokuisakiName
  	MakeCBTokuisakiName('TokuisakiNameYomi', CBTokuisakiName.Text, 1);

  end;

end;

{
	得意先名のコンボボックスを作成する
  引数：sKey   -> 検索列名
  			sWord  -> 検索ワード
  			iKubun -> 0=前方一致, 1=曖昧検索, 2=数字の完全一致
}
procedure TfrmDenpyou.MakeCBTokuisakiName(sKey, sWord:String; iKubun:Integer);
var
	sSql : String;
begin
	//2002.09.29
  if sWord='' then begin
  	Exit;
  end;

  CBTokuisakiName.Items.Clear;


	sSql := 'SELECT * FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + sKey;
  case iKubun of
  0: sSql := sSql + ' LIKE ''' + sWord + '%''';
  1: sSql := sSql + ' LIKE ''%' + sWord + '%''';
  2: sSql := sSql + ' = ' + sWord;
  end;

  sSql := sSql + ' AND HeitenFlag = 0 ORDER BY TokuisakiCode1 ASC';  //thuyptt  2019/05/07
  CBTokuisakiName.Clear;
	with frmDMMaster.QueryMTokuisaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	CBTokuisakiName.Items.Add(FieldByName('TokuisakiCode1').AsString + ',' +
       FieldByName('TokuisakiNameUp').AsString + ' ' + 
       FieldByName('TokuisakiName').AsString);
    	Next;
    end;//of while
    Close;
  end;//of with

  CBTokuisakiName.DroppedDown := True;
end;


procedure TfrmDenpyou.CBTokuisakiNameExit(Sender: TObject);
var
	dToday : TDateTime;
	dToday2 : TDateTime;
  sTokuisakiCode1 : String;
begin
  //2002.01.09
  //Added by H.Kubota
  if CBTokuisakiName.Text = '' then begin
    chbTeisei.Checked := False;
    rbHenpin.Checked := False;
    rbTorikeshi.Checked := False;

    Exit;
  end;

  // Get Gatsubun
  Getgatsubun(Date());

  // Make StringGrid Data
	MakeSg2;

  //2002.06.11 Hajime Kubota
  chbTeisei.Checked := False;
  rbHenpin.Checked := False;
  rbTorikeshi.Checked := False;


//2002.08.07
//現在の日と比較
	dToday := Date;
  dToday2 :=DTP1.Date;
  if dToday > dToday2 then begin
  	ShowMessage('過去の日付の伝票を作成・修正しようとしています');
  end;

  //2003.03.10
  cmbDenpyouBanngou.Text := '';
  GDenpyouFlag := 0;

  //
  lbiInputNo.Caption := '0';

  //2004.09.18
  //Get AriaCode
  // lbAriaCode
  sTokuisakiCode1 := Copy(CBTokuisakiName.Text, 0, pos(',', CBTokuisakiName.Text)-1);
  lbAriaCode.Caption := GetAriaCode(sTokuisakiCode1);


end;

function  TfrmDenpyou.GetAriaCode(sTokuisakiCode1:String) : String;
var
  sSql,sTokuisakiCode2 : String;
begin
  sSql := 'Select TokuisakiCode2 from ' + CtblMTokuisaki;
  sSql := sSql + ' where ';
  sSql := sSql + ' TokuisakiCode1 = ' + sTokuisakiCode1;
  with Query2 do begin
   	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    // もしレコードが無かったら，メッセージボックスを表示して処理を抜ける．
    if RecordCount =0 then begin
      ShowMessage('この得意先に登録されているエリアコードがありません');
      Close;
      Exit;
    end else begin
      sTokuisakiCode2 := FieldbyName('TokuisakiCode2').AsString;
      Close;
    end;
    Result := sTokuisakiCode2;
  end;//of with
end;

procedure TfrmDenpyou.GetGatsubun(D: TDate);
var
	sTokuisakiCode1, sSeikyuuShimebi, sGatsubun, sDay, sMonth, sYear : String;
  wYyyy, wMm, wDd : Word;
begin

  // Get TokuisakiCode1
  sTokuisakiCode1 := Copy(CBTokuisakiName.Text, 1, (pos(',', CBTokuisakiName.Text)-1));

  // Get SeikyuuShimebi
  sSeikyuuShimebi := MTokuisaki.GetShimebi(sTokuisakiCode1);


  // Get Year, Month, Day
  sGatsubun := DateTimeToStr(TDate(D));
  //2002.08.29
  //月分をカレントデートでなく納品日にあわせる
  sGatsubun := DateToStr(DTP1.Date);

  DecodeDate(StrToDate(sGatsubun), wYyyy, wMm, wDd);
	sYear  := IntToStr(wYyyy);
  sMonth := IntToStr(wMm);
  sDay   := IntToStr(wDd);

  // Get Gatsubun
  if sSeikyuuShimebi = '0' then begin // 月末ならそのまま
    sGatsubun := sYear + '/' + sMonth;
  end else begin
    if StrToInt(sDay) > StrToInt(sSeikyuuShimebi) then begin
    	if StrToInt(sMonth) = 12 then begin
        sMonth := '01';
        sYear := INtToStr(StrToInt(sYear) + 1);
      end else begin
      	sMonth := IntToStr(StrToInt(sMonth) + 1);
      end;
    end;
    sGatsubun := sYear + '/' + sMonth;
  end;

  // Set Gatsubun to Panel
  EditGatsubun.Text := sGatsubun;

end;


procedure TfrmDenpyou.SG1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
	curCount,curTanka,curSum : Currency;
begin

  // 現在のカラムが数量もしくは単価で，数値を入力し終わり，Enterを押した時に小計を計算する．
  if (Key=VK_RETURN) and ( (SG1.Col=CintItemCount) or (SG1.Col=CintItemTanka) ) then begin

    try

	    with SG1 do begin
        // 小計を計算する．この際，数量に小数点を許す．
        // 　入力を取り消す場合，CountにNULLが入れられる事になる．
        //   この場合は，CountとSumにNullをセットする．
        if Cells[CintItemCount, Row] = '' then
          begin
            Cells[CintItemCount,   Row] := '';
            Cells[CintItemShoukei, Row] := '';
            lSum.Caption;
          end
        else
          begin
            // マイナスの数量を入力していたならメッセージを表示して Exit
            {
            if StrToInt(Cells[CintItemCount, Row]) < 0 then begin
              ShowMessage('マイナスは入力できません');
              Cells[CintItemCount, Row] := '';
              Exit;
            end;
  		      }
            //上記では小数点に対応しないので以下のように修正した
            // 2002.06.06 Hajime Kubota
            if HKLib.Str2Real(Cells[CintItemCount, Row]) < 0 then begin
              ShowMessage('マイナスは入力できません');
              Cells[CintItemCount, Row] := '';
              Exit;
            end;

            curCount  := StrToCurr(Cells[CintItemCount, Row]);
  		      curTanka  := StrToCurr(Cells[CintItemTanka, Row]);
  		      curSum    := Trunc(curCount * curTanka);
    	      Cells[CintItemShoukei, Row] := CurrToStr(curSum);
            lSum.Caption;
          end;

        // 入力順をセットする．

        //Added by H.K. 2004.02.01
{
        if Cells[CintNum, SG1.Row]='' then begin
          iInputNo := iInputNo + 1;
          Cells[CintNum, SG1.Row] := IntToStr(iInputNo);
        end else
}
        //Added by H.K. 2004.03.30
        if (Cells[CintNum, SG1.Row]='') and
           (Cells[CintItemCount, SG1.Row]<>'') then begin
          iInputNo :=  StrToInt(lbiInputNo.Caption);
          iInputNo := iInputNo + 1;
          Cells[CintNum, SG1.Row] := IntToStr(iInputNo);
          lbiInputNo.Caption :=  IntToStr(iInputNo);
        end; //of if


        // KeyDownで小計を計算後，よみ使用可が選択されている場合のみ，
        // よみエディットボックスにフォーカスを移す

        if RadioGroup1.ItemIndex = 0 then
          begin
            edbYomi.SetFocus;
          end;

	    end;

    except
    	if SG1.Col=CintItemCount then begin
      	//if not Key=VK_BS then begin
    	  //	Showmessage('正しい数値を入力してください');
        //end;
      end;
    end;

  end;

end;


procedure TfrmDenpyou.SG1DrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);

type
  TVowels = set of char;

var
	DRect  : TRect;
  Mode   : Integer;
  Vowels : TVowels;
	C      : LongInt;
  myRect : TRect;
  iCol   : Integer;

begin

  if not (gdFixed in state)  then begin

    Vowels := [char(CintItemName), char(CintItemKikaku),
     char(CintItemIrisuu), char(CintItemHindo), char(CintItemSaishuu)];
		SG1.Canvas.FillRect(Rect);
	  DRect.Top := Rect.Top + 2;
  	DRect.Left := Rect.Left + 2;
	  DRect.Right := Rect.Right - 2;
  	DRect.Bottom := Rect.Bottom - 2;

	  if  char(ACol) IN Vowels then begin       //右寄せ
    	Mode := DT_LEFT;
	  end else begin
  		Mode := DT_RIGHT;
    end;

  	//else if Col = 2 then   //左寄せ
    //	Mode := DT_LEFT
	  //else    Mode := DT_CENTER;   //中央

    //分類の行は色をつける
    {
  	if SG1.Cells[CintNum, ARow] <> '' then begin
			//IdentToColor('clBlue', C);
			IdentToColor('$0000FF22', C);

  		SG1.Canvas.Brush.Color := C;
    	SG1.Canvas.Font.Color := $FFFFFF - C;
    	SG1.Canvas.Font.Size := 10;

		  for iCol := 0 to CintEnd - 1 do begin
    		myRect := SG1.CellRect(iCol, ARow);
	      SG1.Canvas.FillRect(myRect);
		    DrawText(SG1.Canvas.Handle,
  	    					PChar(SG1.Cells[iCol, ARow]),
	  		          Length(SG1.Cells[iCol, ARow]),
        		      myRect,
            	    DT_CENTER or DT_VCENTER or DT_SINGLELINE);
      end;//of for
      Exit;
    end;                                                            
    }
  	DrawText(SG1.Canvas.Handle, PChar(SG1.Cells[ACol,ARow]),
	           Length(SG1.Cells[ACol,ARow]), DRect, Mode);

    //数量は大きく
  	if SG1.Cells[CintItemCount, ARow] <> '' then begin
			IdentToColor('$0000FF22', C);

  		SG1.Canvas.Brush.Color := C;

    	SG1.Canvas.Font.Color := $FFFFFF - C;
    	SG1.Canvas.Font.Size := 12;

		  for iCol := CintItemCount to CintItemCount do begin
    		myRect := SG1.CellRect(iCol, ARow);
	      SG1.Canvas.FillRect(myRect);
		    DrawText(SG1.Canvas.Handle,
  	    					PChar(SG1.Cells[iCol, ARow]),
	  		          Length(SG1.Cells[iCol, ARow]),
        		      myRect,
            	    DT_CENTER or DT_VCENTER or DT_SINGLELINE);
      end;//of for
      Exit;

    end;

  end;//of if

end;

procedure TfrmDenpyou.BitBtn2Click(Sender: TObject);
var
	i,j   : Integer;
  sSql  : String;

begin

  // 前回分の入力の影響を消す為，まずGlobal変数を初期化する．
  GDenpyouBanngou       := '';
	GDate                 := '';
  GNyuuryokusha         := '';
  GTokuisakiName2       := '';
  GGatsubun             := '';
  GMemo                 := '';


  //2004.09.18
  GAriaCode             := '';


  GRowCount             := 0;
  SetLength(GArray,0,0);

  // frmDenpyou2へのデータを保存する（StringGrid以外)
  GDenpyouBanngou := cmbDenpyouBanngou.Text;
  GDate           := DateToStr(DTP1.Date);
  GNyuuryokusha   := CBNyuuryokusha.Text;
  GTokuisakiName2  := CBTokuisakiName.text;
  GGatsubun       := EditGatsubun.Text;
  GMemo           := MemoBikou.Text;

  //2004.09.18
  GAriaCode       := lbAriaCode.Caption;
  


  // mod bdfore 2015.03.26
  //if chbTeisei.Checked = True then GTeisei := 1 else GTeisei := 0;
  // mod after 2015.03.26
  if rbHenpin.Checked = True then GTeisei := 1
  else if rbTorikeshi.Checked = True then GTeisei := 2
  else GTeisei :=0;

  GBikou          := edbBikou1.Text;



  // Key Null Check!
  if (GDate          ='') or (GNyuuryokusha  ='') or (GTokuisakiName2 ='') then
    begin
      ShowMessage('納品日，得意先コード，入力者は省略できません');
      Exit;
    end;

  // 伝票番号の存在をチェックする．(伝票番号が nullでない場合，チェックをかける)
   if GDenpyouBanngou <> '' then begin

    sSql := 'SELECT * FROM ' + CtblTDenpyouDetail;
    sSql := sSql + ' WHERE DenpyouCode = ' + '''' + GDenpyouBanngou + '''';

      with QueryDenpyou do begin
   	    Close;
        Sql.Clear;
        Sql.Add(sSql);
        Open;
        if RecordCount = 0 then
          begin
            ShowMessage('この伝票番号の伝票は存在しません');
            Exit;
          end;
        Close;
      end;
   end;

  // GArrayの要素数をセット
  SetLength(GArray, SG1.RowCount-1, SG1.ColCount);

  // frmDenpyou2へのデータを保存する（StringGrid分)
  with SG1 do begin
    // i=0はタイトル行なのでi=1から始める
    j := 0;
  	for i:=1 to RowCount - 1 do begin
    	if Cells[CintItemCount, i]<>'' then begin
        // ｊは入力のあったレコード数となる．
  			GArray[j, 0] := Cells[CintNum,          i];
  			GArray[j, 1] := Cells[CintItemCode1,    i];
  			GArray[j, 2] := Cells[CintItemCode2,    i];
  			GArray[j, 3] := Cells[CintItemName,     i];
  			GArray[j, 4] := Cells[CintItemYomi,     i];
  			GArray[j, 5] := Cells[CintItemKikaku,   i];
  			GArray[j, 6] := Cells[CintItemIrisuu,   i];
  			GArray[j, 7] := Cells[CintItemHindo,    i];
  			GArray[j, 8] := Cells[CintItemSaishuu,  i];
  			GArray[j, 13] := Cells[CintItemTax,  i];
        // 訂正伝票の場合，数量，小計をマイナスに変換する．

        //if chbTeisei.Checked = True then begin
        //mod 2015.03
        if (rbHenpin.Checked = True) OR (rbTorikeshi.Checked = True) then begin
          //GArray[j, 9] := Real2Str(-1 * Str2Real(Cells[CintItemCount, i]),2,2);
          //GArray[j,12] := Real2str(-1 * Str2Real(Cells[CintItemShoukei,i]),2,0);
          //2007.04.08
          //マイナスでも小数点以下3桁まで有効にする
          GArray[j, 9] := Real2Str(-1 * Str2Real(Cells[CintItemCount, i]),2,3);//
          GArray[j,12] := Real2str(-1 * Str2Real(Cells[CintItemShoukei,i]),2,0);
        end else begin
          GArray[j, 9] := Cells[CintItemCount,  i];
   			  GArray[j,12] := Cells[CintItemShoukei,i];
        end;
  			GArray[j,10] := Cells[CintItemTanni,    i];
  			GArray[j,11] := Cells[CintItemTanka,    i];
        j := j + 1;
      end;
    end;
  end;

  // GRowCountのセット
  GRowCount := j + 1;


  //GInfoDenpyouno := Label18.Caption; // Add utada 2012.11.27


  // Denpyou2のModal表示
  //frmDenpyou2 := TfrmDenpyou2.Create(Self);
//  MyForm2:= TfrmDenpyou2.Create(Self);

    frmDenpyou2 := TfrmDenpyou2.Create(Application); // Add utada 2012.11.28
    frmDenpyou2.Label18.Caption := Label18.Caption; // Add utada 2012.11.28


    //取消し伝票の場合だけ、Infomart取引ＩＤをセットする 2015.05.29 
    if GTeisei = 2 then begin
      frmDenpyou2.Label21.Caption := Label20.Caption; // Add utada 20150116
    end else begin
      frmDenpyou2.Label21.Caption := '';
    end;

    frmDenpyou2.oldDenpyouBangou.Caption := oldDenpyouBangou.Caption;
    if  (frmDenpyou2.ShowModal = mrOk)  then  begin

    end;
    frmDenpyou2.Release;

  iInputNo := 0;

  cmbDenpyouBanngou.Text := '';

  refresh;

  //2002/08/29
  if CBNyuuryokusha.Text ='' then begin
	  CBNyuuryokusha.SetFocus;
  end else begin
	  //CBTokuisakiName.SetFocus;
    edbYomi.SetFocus; //2002.08.30 高橋さんの指示で変更
  end;

//2003.02.09
//入力者をテキストファイルから読む
//CBNyuuryokusha.Items.LoadFromFile('\\ytaka01\bin\nyuuryokusha.txt');
CBNyuuryokusha.Items.LoadFromFile('nyuuryokusha.txt');

//2003.3.10
//伝票が正常に登録されたかどうか確認するために追加した
if GDenpyouFlag = 1 then begin
  DenpyouBanngouListMake;
  //検索ボタンのクリック
  //cmdKennsaku.OnClick(sender);
end;



{ 何故かModal表示の定石の下記コマンドがエラーになる．
  try
    frmDenpyou2.ShowModal;
  finally
    frmDenpyou2.Release;
  end;
}
end;


procedure TfrmDenpyou.BitBtn7Click(Sender: TObject);
var
	i,j : Integer;
begin

  // 表示をクリアする．

  cmbDenpyouBanngou.Clear;
  CBTokuisakiName.Clear;
  CBNyuuryokusha.Clear;
  EditGatsubun.Text := '';
  chbTeisei.Checked := False;
  rbHenpin.Checked := False;
  rbTorikeshi.Enabled := true;
  rbTorikeshi.Checked := False;

  edbBikou1.Text := '';

  with SG1 do begin
    for i := 0 to ColCount - 1 do begin
      for j := 1 to RowCount - 1 do begin
       Cells[i,j] := '';
      end;
    end;
  end;

end;

procedure TfrmDenpyou.cmdKennsakuClick(Sender: TObject);
var
  strDenpyouBanngou: String;
  strTokuisakiCode1: String;
  strTeiseiflg     : String;
  strBikou         : String;
  sSql             : String;
  i                : Integer;
  gRowNum          : Integer;
  iStatus          : String;

begin

  // 伝票番号セット
  strDenpyouBanngou := cmbDenpyouBanngou.Text;

  // セットする前に一旦表示をクリアする．
  BitBtn7.OnClick(Sender);

  // クリア後，伝票番号を再セット
  cmbDenpyouBanngou.Text := strDenpyouBanngou;

  // データセット開始

  if strDenpyouBanngou = '' then

    begin
      ShowMessage('伝票番号が入力されていません．');
    end

  else begin

    // Make SQL
 	  sSql := 'SELECT * FROM ' + CtblTDenpyouDetail;
    sSql := sSql + ' WHERE DenpyouCode = ' + '''' + strDenpyouBanngou + '''';
    sSql := sSql + ' ORDER BY [No] ASC';

    // tblTDenpyouDetailより取得できるデータを取得
    with QueryDenpyou do begin
    	Close;
      Sql.Clear;
      Sql.Add(sSql);
      Open;

      // レコードがなければメッセージを出力して抜ける
      if RecordCount = 0 then
        begin
         ShowMessage('該当する伝票はありません');
         Exit;
        end;

      // 納品日＆入力者セット，得意先コード１取得
      DTP1.Date             := FieldbyName('NouhinDate').AsDateTime;
      strTokuisakiCode1     := FieldbyName('TokuisakiCode1').AsString;


      CBNyuuryokusha.Text   := FieldbyName('Nyuuryokusha').AsString;

      if CBNyuuryokusha.Text = '' then begin
         CBNyuuryokusha.Items.LoadFromFile('nyuuryokusha.txt');
      end;

      //2010/2/12  追加 By utada
      lbAriaCode.Caption := GetAriaCode(FieldByName('TokuisakiCode1').AsString);

       //2011/11/27 追加 By utada
      Label18.Caption       := FieldbyName('InfoDenpyouNo').AsString;
      Label20.Caption       := FieldbyName('InfoTorihikiId').AsString; // add 20150116
      oldDenpyouBangou.Caption := strDenpyouBanngou;
      // RowCountセット
      gRowNum := RecordCount + 1;  // タイトル分1行足してる
      SG1.RowCount := gRowNum;



      i := 0;
      while not EOF do begin
    	  i := i+1;
        with SG1 do begin
      	  Cells[CintItemCode1,  i] := FieldbyName('Code1').AsString;
      	  Cells[CintItemCode2,  i] := FieldbyName('Code2').AsString;
  	      Cells[CintItemCount,  i] := floattostrf(FieldbyName('Suuryou').AsCurrency, ffGeneral, 8, 4);    //thuyptt fix float value
          Cells[CintItemTanka,  i] := FieldbyName('Tannka').AsString;
          Cells[CintItemShoukei,i] := FieldbyName('Shoukei').AsString;
          Cells[CintItemTax,    i] := FieldbyName('tax').AsString;
        end;
        next
      end;
      Close;
    end;

    // 得意先コード生成

    CBTokuisakiName.Items.Clear;

  	sSql := 'SELECT * FROM ' + CtblMTokuisaki;
    sSql := sSql + ' WHERE TokuisakiCode1 = ' + strTokuisakiCode1;
  	with QueryDenpyou do begin
  	  Close;
      Sql.Clear;
      Sql.Add(sSql);
      Open;
    	CBTokuisakiName.Text := FieldByName('TokuisakiCode1').AsString + ',' + FieldByName('TokuisakiName').AsString;
      //2002.09.29 H.K.
      //コースコードとチェーンコードの表示
		  lbTokuisakiCode2.Caption := FieldByName('TokuisakiCode2').AsString;
		  lbChainCode.Caption      := FieldByName('ChainCode').AsString;
			MemoBikou.Text           := FieldByName('Bikou').AsString;

      Close;
    end;

    
    // 訂正フラグ取得＆訂正チェックボックスセット, 備考セット

  	sSql := 'SELECT * FROM ' + CtblTDenpyou;
    sSql := sSql + ' WHERE DenpyouCode = ' + '''' + strDenpyouBanngou + '''';
  	with QueryDenpyou do begin
  	  Close;
      Sql.Clear;
      Sql.Add(sSql);
      Open;
      strTeiseiflg := FieldByName('Teiseiflg').AsString;
      strBikou     := FieldByName('Bikou').AsString;
      iStatus      := FieldByName('Status').AsString;
      Close;
    end;

    If StrToInt(strTeiseiflg) = 1 then begin
      chbTeisei.Checked := True;
      rbHenpin.Checked := True;
    end else if StrToInt(strTeiseiflg) = 2 then begin
      rbTorikeshi.Checked := True;
    end else
      chbTeisei.Checked := False;

    edbBikou1.Text := strBikou;

    if StrToInt(iStatus) = 1 then begin
      Label23Status.Caption := '状況 : 発送' ;
    end else
      Label23Status.Caption := '状況 : 発注' ;

    // 月分取得
    // 　TDateTimeの小数点以下(時間)が付く形で，Getgatsubunの中のDecodeDateに
    //　 引数(DTP1.Date)を渡すと何故かエラーになる．しょうがないので，
    // 　Intにキャストして小数点以下(時間)を無くした形で引数を渡している．

    Getgatsubun(Int(DTP1.Date));

    // StringGridの残り部分を viwMItemsから取得してセットする．

    With SG1 do begin

      for i := 1 to RowCount -1 do begin

        // SQL分作成
        sSql := 'SELECT * FROM ' + CviwMItems;
        sSql := sSql + ' WHERE Code1 = ' + '''' + Cells[CintItemCode1,i] + '''';
        sSql := sSql + ' AND   Code2 = ' + '''' + Cells[CintItemCode2,i] + '''';

        //2006.10.05 バグ修正　hkubota
        sSql := sSql + ' AND TokuisakiCode = ' + strTokuisakiCode1;

        // tblTDenpyouDetailより取得できるデータを取得
        with QueryDenpyou do begin
    	    Close;
          Sql.Clear;
          Sql.Add(sSql);
          Open;

      	  Cells[CintItemName,   i] := FieldbyName('Name').AsString;
      	  Cells[CintItemTax,    i] := Cells[CintItemTax,i] + '%';          
      	  Cells[CintItemYomi,   i] := FieldbyName('Yomi').AsString;
      	  Cells[CintItemKikaku, i] := FieldbyName('Kikaku').AsString;
      	  Cells[CintItemIrisuu, i] := FieldbyName('Irisuu').AsString;
      	  Cells[CintItemHindo,  i] := FieldbyName('Hindo').AsString;
      	  Cells[CintItemSaishuu,i] := FieldbyName('SaishuuShukkabi').AsString; //#thuyptt 20190313
      	  Cells[CintItemTanni,  i] := FieldbyName('Tanni').AsString;
          Close;
        end;

      end;

    end;

  end;

end;

procedure TfrmDenpyou.cmdGoClick(Sender: TObject);
var
 i     : Integer;
 iHit  : Integer;
 sKey  : String;
 sYomi : String;

begin
 //added Hajime Kubota 2001/12/26
 //edbYomi.ImeMode := imClose;


 sKey := edbYomi.Text;
 iHit := 0;

 // 現在のフォーカス位置より下で，最初によみが商品よみに含まれる行 iHitを探す．

 // 1行目対策
 if (SG1.Row =1) AND (giHit=0) then begin
    sYomi := SG1.Cells[CintItemYomi, 1];
    if AnsiPos(sKey, sYomi) <> 0 then
      begin
        iHit := 1;
      end;
 end;
{ // 1行目以外
 else
   begin
}
 if iHit=0 then begin
     for i := (SG1.Row+1) to SG1.RowCount - 1 do begin
       sYomi := SG1.Cells[CintItemYomi, i];
       if AnsiPos(sKey, sYomi) <> 0 then
         begin
           iHit := i;
           Break;
         end;
     end;//of for
  end;//of if

//   end;

 // 見つからなかったら（iHitが初期値の0のままなら)，メッセージボックスを表示して抜ける．
 if iHit = 0 then
   begin
     ShowMessage('該当する商品はみつかりません');
   end
 else
   begin
    // 見つかったら，iHit行まで移動する．
    SG1.Col       := CintItemCount;
    SG1.Row       := iHit;
    SG1.SetFocus;
    giHit := iHit; //Added H.K.
   end;

end;

procedure TfrmDenpyou.edbYomiChange(Sender: TObject);
begin
  // よみが新しくなる度に検索初期値を1行にする．
  SG1.Row := 1;
  giHit:=0;
end;


procedure TfrmDenpyou.CBNyuuryokushaExit(Sender: TObject);
begin
  if CBNyuuryokusha.Text = '' then begin
     CBNyuuryokusha.DroppedDown := True;
  end;

end;

procedure TfrmDenpyou.RadioGroup1Click(Sender: TObject);
begin

 // ラジオボタンに応じて，よみeditboxとGOボタンを使用可/不可能に設定する．
 if RadioGroup1.ItemIndex = 0 then  // 使用可
   begin
     edbYomi.Enabled    := True;
     edbYomi.Color      := clWindow;
     cmdGo.Enabled    := True;
   end
 else if RadioGroup1.ItemIndex = 1 then  // 使用不可能
   begin
     edbYomi.Enabled := False;
     edbYomi.Color   := clSilver;
     cmdGo.Enabled := False;
   end
 else
   begin
     ShowMessage('エラー発生');
   end;

end;

procedure TfrmDenpyou.cmdDenpyouBanngouListMakeClick(Sender: TObject);
begin
  DenpyouBanngouListMake;
end;

procedure TfrmDenpyou.DenpyouBanngouListMake;
var
 sNouhinbi       : String;
 sTokuisakiCode1 : String;
 sSql            : String;

begin

 // 納品日 or 得意先コードがNullの場合，Messegeを出力して抜ける．
 if (DateTimeToStr(DTP1.Date) = '') or (CBTokuisakiName.Text = '') then
  begin
    ShowMessage('納品日と得意先コードを指定して下さい．該当する伝票番号リストを作成します．');
    Exit;
  end;

 // パラメータ作成
 DateTimeToString(sNouhinbi, 'yyyy/mm/dd', DTP1.Date);
 sTokuisakiCode1 := Copy(CBTokuisakiName.Text, 1, (pos(',', CBTokuisakiName.Text)-1));

 // コンボボックスクリア
 cmbDenpyouBanngou.Items.Clear;

 // SQL作成
// sSql := 'SELECT DISTINCT DenpyouCode FROM ' + CtblTDenpyouDetail;
 sSql := 'SELECT DISTINCT DenpyouCode FROM ' + CtblTDenpyouDetail;
 sSql := sSql + ' WHERE NouhinDate = ' + '#' + sNouhinbi + '#';
 sSql := sSql + ' AND TokuisakiCode1 = ' + sTokuisakiCode1;
// sSql := sSql + ' ORDER BY DenpyouCode'; thuyptt 2019/080/3
 sSql := sSql + ' ORDER BY DenpyouCode ASC';

 // tblTDenpyouDetailより取得できるデータを取得
   with QueryDenpyou do begin
     Close;
     Sql.Clear;
     Sql.Add(sSql);
     Open;

   // レコードがなければメッセージを出力して抜ける
     if RecordCount = 0 then
       begin
         ShowMessage('該当する伝票番号はありません');
         Exit;
       end;

   // レコードが尽きる迄，ComboBoxのリストにItemを追加
      while not EOF do begin
      	cmbDenpyouBanngou.Items.Add(FieldByName('DenpyouCode').AsString);
    	  Next;
      end;
     Close;
   end;

 // コンボボックスをドロップダウンさせる
 cmbDenpyouBanngou.DroppedDown := True;

 //フラッグの初期化
 GDenpyouFlag := 0;
end;

procedure TfrmDenpyou.Timer1Timer(Sender: TObject);
begin
  if Length(MemoBikou.Text) > 1 then begin
  	if Label14.Font.Color = clRed then begin
	  	Label14.Font.Color := clNavy;
      MemoBikou.Font.Color := clRed;
    end else begin
		  Label14.Font.Color := clRed;
      MemoBikou.Font.Color := clNavy;
    end;
  end;
end;

procedure TfrmDenpyou.DTP1Exit(Sender: TObject);
var
	dToday : TDateTime;
	dToday2 : TDateTime;
begin
  inherited;
//2002.08.07
//現在の日と比較
	dToday := Date;
  dToday2 :=DTP1.Date;
  if dToday > dToday2 then begin
  	ShowMessage('過去の日付の伝票を作成・修正しようとしています');
  end;
  

end;

procedure TfrmDenpyou.BitBtnExcelClick(Sender: TObject);
//エクセルにコンバート
var
  sLine :String;
  iRow : Integer;
 	F : TextFile;
  sCNum, sCItemCode1, sCItemCode2, sCItemName :String;
  sCItemYomi, sCItemMemo, sCItemKikaku, sCItemIrisuu :String;
  sCItemCount, sCItemTanni, sCItemTanka, sCItemShoukei :String;
  sCItemHindo, sCItemSaishuu :String;
begin
	//確認メッセージ
  if MessageDlg('エクセルへしますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;

	//スタートトランザクション
  Screen.Cursor := crHourGlass;

	//出力するファイルを作成する、すでにあれば削除する
 	AssignFile(F, CFileName_Item);
  Rewrite(F);
	CloseFile(F);

  //2003.09.24 追加
  sLine := '得意先名:,' + Trim(CBTokuisakiName.Text);
  HMakeFile(CFileName_Item, sLine);
  sLine := '出力日:,' + DatetoStr(DTP1.Date);
  HMakeFile(CFileName_Item, sLine);
  //
  sLine := '商品コード1' + ',' + '商品コード2' + ',' + '商品名';
  sLine := sLine + ',' + '商品読み' + ',' + 'メモ' + ',' + '規格';
  sLine := sLine + ',' + '入り数' + ',' + '数量' + ',' + '単位';
  sLine := sLine + ',' + '単価' + ',' + '小計' + ',' + '頻度';
  sLine := sLine + ',' + '最終出荷日';
  HMakeFile(CFileName_Item, sLine);

  //基本情報の取得
  with SG1 do begin
  	for iRow := 1 to RowCount do begin
		  //sCNum          := Cells[CintNum, iRow];
		  sCItemCode1    := Cells[CintItemCode1, iRow];
		  sCItemCode2    := Cells[CintItemCode2, iRow];
		  sCItemName     := Cells[CintItemName, iRow];
		  sCItemYomi     := Cells[CintItemYomi, iRow];
		  sCItemMemo     := Cells[CintItemMemo, iRow];
		  sCItemKikaku   := Cells[CintItemKikaku, iRow];
		  sCItemIrisuu   := Cells[CintItemIrisuu, iRow];
		  sCItemCount    := Cells[CintItemCount, iRow];
		  sCItemTanni    := Cells[CintItemTanni, iRow];
		  sCItemTanka    := Cells[CintItemTanka, iRow];
		  sCItemShoukei  := Cells[CintItemShoukei, iRow];
		  sCItemHindo    := Cells[CintItemHindo, iRow];
		  sCItemSaishuu  := Cells[CintItemSaishuu, iRow];


		  sLine := sCItemCode1 + ',' + sCItemCode2 + ',' + sCItemName;
      sLine := sLine + ',' + sCItemYomi + ',' + sCItemMemo + ',' + sCItemKikaku;
      sLine := sLine + ',' + sCItemIrisuu + ',' + sCItemCount + ',' + sCItemTanni;
      sLine := sLine + ',' + sCItemTanka + ',' + sCItemShoukei + ',' + sCItemHindo;
      sLine := sLine + ',' + sCItemSaishuu;

 		  HMakeFile(CFileName_Item, sLine);
    end;//of for
  end;//of with

	//エンドトランザクション
  Screen.Cursor := crDefault;

  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '商品一覧.xls', '', SW_SHOW);
end;

procedure TfrmDenpyou.FormDestroy(Sender: TObject);
begin
  inherited;
  GDenpyouBanngou := '';
  //GInfoDenpyouno :='';

end;

procedure TfrmDenpyou.btnShowAllClick(Sender: TObject);
begin
  inherited;

  if CBTokuisakiName.Text = '' then begin
    chbTeisei.Checked := False;
    rbHenpin.Checked := False;
    rbTorikeshi.Checked := False;

    Exit;
  end;

  // Get Gatsubun
  Getgatsubun(Date());

  iBtnAll := 1;

  // Make StringGrid Data
	MakeSg2;

end;

procedure TfrmDenpyou.Button2Click(Sender: TObject);
begin
  inherited;
  if CBTokuisakiName.Text = '' then begin
    chbTeisei.Checked := False;
    rbHenpin.Checked := False;
    rbTorikeshi.Checked := False;

    Exit;
  end;

  // Get Gatsubun
  Getgatsubun(Date());

  iBtnAll := 0;

  // Make StringGrid Data
	MakeSg2;

  
end;

end.
