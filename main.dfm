object frmMain: TfrmMain
  Left = 536
  Top = 175
  Width = 681
  Height = 489
  Caption = 'Ver2019 '#26481#20140'20190814-01'
  Color = clCream
  Font.Charset = SHIFTJIS_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
  Font.Style = []
  FormStyle = fsMDIForm
  Menu = MainMenu1
  OldCreateOrder = True
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  object MainMenu1: TMainMenu
    Left = 132
    Top = 98
    object File1: TMenuItem
      Caption = #12501#12449#12452#12523
      object Exit1: TMenuItem
        Caption = #32066#20102
        OnClick = Exit1Click
      end
    end
    object N1: TMenuItem
      Caption = #12510#12473#12479#12540#31649#29702
      Visible = False
      object N21: TMenuItem
        Caption = #39015#23458#12487#12540#12479
        Visible = False
        OnClick = N21Click
      end
      object N4: TMenuItem
        Caption = #24471#24847#20808#31649#29702#30011#38754
        Enabled = False
        Visible = False
        OnClick = N4Click
      end
      object gyoushu1: TMenuItem
        Caption = #26989#31278#12487#12540#12479
        Visible = False
        OnClick = gyoushu1Click
      end
      object N17: TMenuItem
        Caption = '-------------------'
        Visible = False
      end
      object N15: TMenuItem
        Caption = #21830#21697#30331#37682
        object N18: TMenuItem
          Caption = #21830#21697#32207#12510#12473#12479#12540
          Visible = False
          OnClick = N18Click
        end
        object N28: TMenuItem
          Caption = #21830#21697#12487#12540#12479#12467#12500#12540
          Visible = False
          OnClick = N28Click
        end
        object N19: TMenuItem
          Caption = #24471#24847#20808#21029#21830#21697#12510#12473#12479#12540
          OnClick = N19Click
        end
        object N20: TMenuItem
          Caption = #12481#12455#12540#12531#21029#21830#21697#12510#12473#12479#12540
          Visible = False
          OnClick = N20Click
        end
        object N35: TMenuItem
          Caption = #20385#26684#19968#25324#22793#26356
          Visible = False
          OnClick = N35Click
        end
      end
      object N16: TMenuItem
        Caption = #20181#20837#20808#12487#12540#12479
        Visible = False
        OnClick = N16Click
      end
    end
    object N2: TMenuItem
      Caption = #26085#24120#26989#21209
      Visible = False
      object N5: TMenuItem
        Caption = #20253#31080#12481#12455#12483#12463
        OnClick = N5Click
      end
      object N7: TMenuItem
        Caption = #22770#25499#22238#21454#12481#12455#12483#12463
        OnClick = N7Click
      end
      object N10: TMenuItem
        Caption = #12467#12540#12473#21029#22770#19978#38598#35336
        OnClick = N10Click
      end
      object N11: TMenuItem
        Caption = #22770#25499#26410#21454#12522#12473#12488
        OnClick = N11Click
      end
      object N30: TMenuItem
        Caption = #29694#37329#22238#21454#12481#12455#12483#12463
        OnClick = N30Click
      end
    end
    object N3: TMenuItem
      Caption = #21360#21047
      object N6: TMenuItem
        Caption = #35531#27714#26360#30330#34892
        OnClick = N6Click
      end
      object N22: TMenuItem
        Caption = #22770#25499#21488#24115
        Visible = False
        OnClick = N22Click
      end
      object N9: TMenuItem
        Caption = #38936#21454#26360#30330#34892
        Visible = False
        OnClick = N9Click
      end
      object N8: TMenuItem
        Caption = #22770#25499#21488#24115#34920#31034#30011#38754
        Visible = False
        OnClick = N8Click
      end
    end
    object Tool1: TMenuItem
      Caption = #12484#12540#12523
      Visible = False
      object N12: TMenuItem
        Caption = #22770#25499#37329#27531#39640
        Visible = False
        OnClick = N12Click
      end
      object N13: TMenuItem
        Caption = #22770#19978#38598#35336
        Visible = False
        OnClick = N13Click
      end
      object N31: TMenuItem
        Caption = #29694#37329#26410#21454#12522#12473#12488
        Visible = False
        OnClick = N31Click
      end
      object N34: TMenuItem
        Caption = '------------------'
      end
      object N32: TMenuItem
        Caption = #20385#26684#26908#32034
        OnClick = N32Click
      end
      object N33: TMenuItem
        Caption = #38971#24230#26908#32034
        Visible = False
        OnClick = N33Click
      end
      object N36: TMenuItem
        Caption = #38971#24230#21516#26399
        Visible = False
        OnClick = N36Click
      end
      object MP1: TMenuItem
        Caption = #31649#29702#32773'TMP'
        Visible = False
        OnClick = MP1Click
      end
    end
    object N14: TMenuItem
      Caption = #22770#19978#20253#31080
      object Info1: TMenuItem
        Caption = 'InfoMart'#20253#31080#19968#35239#30011#38754'(V)'
        Visible = False
        OnClick = Info1Click
      end
      object InfoMartW1: TMenuItem
        Caption = 'InfoMart'#20253#31080#19968#25324#30331#37682'(W)'
        Visible = False
        OnClick = InfoMartW1Click
      end
      object N23: TMenuItem
        Caption = #20253#31080#20316#25104
        OnClick = N23Click
      end
      object N25: TMenuItem
        Caption = #20986#24235#34920
        Visible = False
        OnClick = N25Click
      end
      object N29: TMenuItem
        Caption = #20253#31080#19968#25324#21360#21047
        OnClick = N29Click
      end
    end
    object N24: TMenuItem
      Caption = #20181#20837#31649#29702
      Visible = False
      object N241: TMenuItem
        Caption = 'InfoMart'#20253#31080#19968#35239#30011#38754'(V)'
        Visible = False
        OnClick = N241Click
      end
      object N242: TMenuItem
        Caption = 'InfoMart'#20253#31080#19968#25324#30331#37682'(W)'
        Visible = False
        OnClick = N242Click
      end
      object N44: TMenuItem
        Caption = #20181#20837#20808#19968#25324#22793#26356
        Visible = False
        OnClick = N44Click
      end
      object N26: TMenuItem
        Caption = #20837#24235#20837#21147
        OnClick = N26Click
      end
      object N27: TMenuItem
        Caption = #20181#20837#12428#21488#24115
        Visible = False
        OnClick = N27Click
      end
      object N37: TMenuItem
        Caption = #25903#25173#20837#21147
        Visible = False
        OnClick = N37Click
      end
      object N38: TMenuItem
        Caption = #12294#12398#20966#29702
        Visible = False
        OnClick = N38Click
      end
      object N39: TMenuItem
        Caption = #24115#31080#38306#36899
        Visible = False
        OnClick = N39Click
      end
    end
    object Tool2: TMenuItem
      Caption = #12484#12540#12523
      Visible = False
      OnClick = Tool2Click
    end
    object N40: TMenuItem
      Caption = #35211#31309#12426
      object N41: TMenuItem
        Caption = #26032#35215#20316#25104
        OnClick = N41Click
      end
      object N42: TMenuItem
        Caption = #26082#23384#20316#25104
        OnClick = N42Click
      end
      object N43: TMenuItem
        Caption = #35211#31309#31895#21033#26908#32034
        Visible = False
        OnClick = N43Click
      end
    end
  end
  object QuerySelect: TQuery
    DatabaseName = 'taka-mysql-local'
    ParamCheck = False
    Left = 220
    Top = 74
  end
end
