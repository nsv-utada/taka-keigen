unit Sync_Hindo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, ComCtrls, StdCtrls, Buttons, ExtCtrls, DB, DBTables;

type
  TfrmSync_Hindo = class(TfrmMaster)
    Button1: TButton;
    lbHindoCount: TLabel;
    Button2: TButton;
    Query1: TQuery;
    Query2: TQuery;
    Query3: TQuery;
    lbI: TLabel;
    lbJ: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
    Function GetHindoFromDenpyouDetail(sCode1,sCode2,sTCode1:String):Integer;
    Function UpdateHindoItem2(sCode1,sCode2,sTCode1,sHindo:String):String;
  public
    { Public declarations }
  end;

var
  frmSync_Hindo: TfrmSync_Hindo;

implementation

{$R *.dfm}

procedure TfrmSync_Hindo.FormActivate(Sender: TObject);
begin
  inherited;
  Width  := 550;
  Height := 250;
end;

procedure TfrmSync_Hindo.Button1Click(Sender: TObject);
var
  sSql : String;
begin
  sSql := 'Select hsum=Count(*) from tblMItem2';
  sSql := sSql + ' where ';
  sSql := sSql + ' Hindo > 0';
  with Query1 do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    lbHindoCount.Caption := lbHindoCount.Caption + ' -> ' +
    FieldbyName('hsum').AsString;
    Close;
  end;
end;

procedure TfrmSync_Hindo.Button2Click(Sender: TObject);
var
  sSql : String;
  sCode1,sCode2,sTCode1,sHindoIT2 : String;
  iHIndo : Integer;
  i, j : Integer;
begin
  i := 0;
  j := 0;
  sSql := 'Select * from tblMItem2';
  sSql := sSql + ' where ';
  sSql := sSql + ' Hindo > 0';
  sSql := sSql + ' Order by TokuisakiCode,Code1,Code2';
  with Query1 do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
      sCode1  := FieldByName('Code1').AsString;
      sCode2  := FieldByName('Code2').AsString;
      sTCode1 := FieldByName('TokuisakiCode').AsString;

      sHindoIT2 := FieldByName('Hindo').AsString;
      //tblTDenpyouDetail の本当の頻度を計算する
      iHIndo := GetHindoFromDenpyouDetail(sCode1,sCode2,sTCode1);
      if sHindoIT2 <> IntToStr(iHIndo) then begin
        //本当の頻度をtblMItem2に書き込む
        SB1.SimpleText :=
        UpdateHindoItem2(sCode1,sCode2,sTCode1,IntToStr(iHindo));
        j := j + 1;
      end;
      i := i + 1;
      lbI.Caption := '処理した頻度の数 = ' + IntToStr(i);
      lbJ.Caption := '実際に変更した頻度の数 = ' + IntToStr(j);
      Next;
    end;
    Close;
  end;

end;

Function TfrmSync_Hindo.GetHindoFromDenpyouDetail(sCode1,sCode2,sTCode1:String):Integer;
var
  sSql : String;
  sHindoR : String;
begin
  sSql := 'select sHIndo=count(*) from tblTDenpyouDetail';
  sSql := sSql + ' where ';
  sSql := sSql + ' Code1 = ''' + sCode1 + ''' and ';
  sSql := sSql + ' Code2 = ''' + sCode2 + ''' and ';
  sSql := sSql + ' TokuisakiCode1 = ' + sTCode1;
  with Query2 do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    sHindoR := FieldByName('sHindo').AsString;
    Close;
  end;
  result := StrToINt(sHindoR);
end;

Function TfrmSync_Hindo.UpdateHindoItem2(sCode1,sCode2,sTCode1,sHindo:String):String;
var
  sSql : String;
begin
  sSql := 'Update tblMItem2 set Hindo = ' + sHindo;
  sSql := sSql + ' where ';
  sSql := sSql + ' Code1 = ''' + sCode1 + ''' and ';
  sSql := sSql + ' Code2 = ''' + sCode2 + ''' and ';
  sSql := sSql + ' TokuisakiCode = ' + sTCode1;

  with Query3 do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    ExecSql;
  end;
  Result := sSql;
end;

end.
