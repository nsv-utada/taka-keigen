inherited frmNyuuko1: TfrmNyuuko1
  Left = 494
  Top = 161
  Width = 797
  Height = 478
  Caption = 'frmNyuuko2'
  PixelsPerInch = 96
  TextHeight = 12
  inherited Panel1: TPanel
    Width = 781
    inherited Label1: TLabel
      Width = 160
      Caption = #20837#24235#20837#21147#30906#35469#30011#38754
    end
    inherited Label7: TLabel
      Enabled = False
      Visible = False
    end
    inherited cmbDenpyouBanngou: TComboBox
      Enabled = False
      Visible = False
    end
    inherited cmdDenpyouBanngouListMake: TButton
      Enabled = False
      Visible = False
    end
    inherited cmdKennsaku: TButton
      Enabled = False
      Visible = False
    end
  end
  inherited Panel2: TPanel
    Top = 373
    Width = 781
    Height = 48
    inherited lSum: TLabel
      Left = 24
      Top = 24
      Width = 64
      Height = 15
      Font.Color = clBlack
      Font.Height = -15
      Visible = True
    end
    object lSum2: TLabel [2]
      Left = 24
      Top = 7
      Width = 64
      Height = 15
      Caption = #23567#35336#37329#38989
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlack
      Font.Height = -15
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lSumShoukei: TLabel [3]
      Left = 104
      Top = 7
      Width = 36
      Height = 15
      Caption = '1000'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlack
      Font.Height = -15
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lSumGoukei: TLabel [4]
      Left = 104
      Top = 24
      Width = 36
      Height = 15
      Caption = '1050'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlack
      Font.Height = -15
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel [5]
      Left = 168
      Top = 7
      Width = 16
      Height = 15
      Caption = #20870
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlack
      Font.Height = -15
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label12: TLabel [6]
      Left = 168
      Top = 24
      Width = 16
      Height = 15
      Caption = #20870
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlack
      Font.Height = -15
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited BitBtn1: TBitBtn
      Top = 16
    end
    inherited BitBtn2: TBitBtn
      Top = 16
      Caption = #30331#37682
    end
  end
  inherited Panel3: TPanel
    Width = 781
    Height = 88
    inherited Label10: TLabel
      Enabled = False
      Visible = False
    end
    inherited Label4: TLabel
      Width = 70
      Caption = #20181#20837#20808#20808#21517
    end
    inherited Label11: TLabel
      Enabled = False
      Visible = False
    end
    inherited Label5: TLabel
      Top = 34
    end
    object lNyuuryokusha: TLabel [6]
      Left = 72
      Top = 36
      Width = 75
      Height = 12
      Caption = 'lNyuuryokusha'
    end
    object lShiiresakiName: TLabel [7]
      Left = 304
      Top = 8
      Width = 82
      Height = 12
      Caption = 'lShiiresakiName'
    end
    object lDTP1: TLabel [8]
      Left = 80
      Top = 10
      Width = 31
      Height = 12
      Caption = 'lDTP1'
    end
    object Label8: TLabel [9]
      Left = 264
      Top = 60
      Width = 48
      Height = 15
      Caption = #28040#36027#31246
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Label13: TLabel
      Top = 34
    end
    object Label17: TLabel [12]
      Left = 538
      Top = 24
      Width = 5
      Height = 13
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Label19: TLabel
      Width = 5
      Caption = ''
    end
    inherited Label18: TLabel
      Left = 480
    end
    object Label14: TLabel [15]
      Left = 394
      Top = 8
      Width = 70
      Height = 13
      Caption = 'Info'#20253#31080'No'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited DTP1: TDateTimePicker
      Left = 510
      Top = 54
      Enabled = False
      Visible = False
    end
    inherited CBNyuuryokusha: TComboBox
      Left = 502
      Top = 71
      Enabled = False
      Visible = False
    end
    inherited edbYomi: TEdit
      Enabled = False
      Visible = False
    end
    inherited CBShiiresakiName: TComboBox
      Left = 496
      Top = 60
      Enabled = False
      Visible = False
    end
    object CTax: TEdit [20]
      Left = 320
      Top = 56
      Width = 65
      Height = 20
      TabOrder = 7
      OnChange = CTaxChange
    end
    inherited cmdGo: TButton
      Enabled = False
      Visible = False
    end
    inherited MemoBikou: TMemo
      Lines.Strings = ()
    end
    inherited RadioGroup1: TRadioGroup
      Left = 528
      Top = -9
      Enabled = False
      Visible = False
    end
    inherited EditGatsubun: TEdit
      Width = 59
      TabOrder = 8
    end
  end
  inherited SB1: TStatusBar
    Top = 421
    Width = 781
  end
  inherited SG1: TStringGrid
    Top = 153
    Width = 781
    Height = 220
  end
  object QueryDelete: TQuery
    DatabaseName = 'taka'
    Left = 200
    Top = 9
  end
  object QueryInsert: TQuery
    DatabaseName = 'taka'
    Left = 168
    Top = 9
  end
end
