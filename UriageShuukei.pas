unit UriageShuukei;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, ComCtrls, StdCtrls, Buttons, ExtCtrls, Db, DBTables, ShellAPI;

type
  TfrmUriageShuukei = class(TfrmMaster)
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    QueryTokuisaki: TQuery;
    QueryCalc: TQuery;
    CBYyyyTo: TComboBox;
    CBMmTo: TComboBox;
    Label7: TLabel;
    Label8: TLabel;
    CBYyyyFrom: TComboBox;
    CBMmFrom: TComboBox;
    Label9: TLabel;
    Label10: TLabel;
    procedure BitBtn2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private 宣言 }
  public
    { Public 宣言 }
  end;

var
  frmUriageShuukei: TfrmUriageShuukei;

implementation
uses
	Inter, HKLib;

{$R *.DFM}

//エクセルへ出力ボタンがクリックされた
procedure TfrmUriageShuukei.BitBtn2Click(Sender: TObject);
var
	sSql, sTokuisakiCode1, sLine, sSum, sDateFrom, sDateTo : String;
 	F : TextFile;
begin
	//確認メッセージ
  if MessageDlg('集計しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;

  //集計期間の作成
  sDateFrom := CBYyyyFrom.Text + '/' + CBMmFrom.Text + '/1';
  sDateTo   := CBYyyyTo.Text   + '/' + CBMmTo.Text   + '/1';

	//出力するファイルを作成する、すでにあれば削除する
  //請求明細用
 	AssignFile(F, CFileName_Uriage);
  Rewrite(F);
	CloseFile(F);

  //タイトル部分の出力
  sLine := CBMmFrom.Text + '月分から' + CBMmTo.Text + '月分までの売上金額';
  HMakeFile(CFileName_Uriage, sLine);
  //2000/03/15　変更
  //sLine := '集計コード'+ ',' + '課' + ',' + '売上高';
  sLine := '集計コード';
  sLine := sLine + ',' + '課';
  sLine := sLine + ',' + '売上高';
  sLine := sLine + ',' + '得意先コード1';
  sLine := sLine + ',' + '得意先コード2';
  sLine := sLine + ',' + 'チェーンコード';
  sLine := sLine + ',' + 'よみ';

  //2000/05/04
  sLine := sLine + ',' + '得意先名';

  sLine := sLine + ',' + '';

  HMakeFile(CFileName_Uriage, sLine);

	SB1.SimpleText := '計算中 ';

	sSql := 'SELECT USum=Sum(tblTDenpyou.UriageKingaku)';
  sSql := sSql + ', tblMTokuisaki.TokuisakiCode1 ';
  sSql := sSql + ', tblMTokuisaki.TokuisakiCode2 ';
  sSql := sSql + ', tblMTokuisaki.ChainCode ';
  sSql := sSql + ', tblMTokuisaki.ShuukeiCode ';
  sSql := sSql + ', tblMTokuisaki.TokuisakiNameYomi ';
  sSql := sSql + ', tblMTokuisaki.TokuisakiName ';
  sSql := sSql + ', tblMTokuisaki.Ka ';
  sSql := sSql + ' FROM tblTDenpyou INNER JOIN tblMTokuisaki ON tblTDenpyou.TokuisakiCode1 = tblMTokuisaki.TokuisakiCode1';

  sSql := sSql + ' WHERE ';
  sSql := sSql + ' Gatsubun BETWEEN ''' + sDateFrom + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + '''' + sDateTo + '''';
  sSql := sSql + ' AND ';

  { 高橋さんの指示により月分に変更
  sSql := sSql + ' DDate BETWEEN ''' + DateToStr(DTP1.Date) + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + '''' + DateToStr(DTP2.Date) + '''';
  sSql := sSql + ' AND ';
  }

  //2000/05/04 返品も考慮
  //sSql := sSql + ' UriageKingaku > 0';
  //sSql := sSql + ' ((UriageKingaku > 0) OR (SUBSTRING(tblTDenpyou.Memo, 1, 2) IN (''返品'', ''値引'')))' ;

  //2000/06/26 微妙だが売り上げがマイナスの」ときもある
  sSql := sSql + ' ((UriageKingaku > 0) OR (SUBSTRING(tblTDenpyou.Memo, 1, 2) IN (''返品'', ''値引'', ''取消'')) OR tblTDenpyou.Memo=''' + CsCTAX + ''')' ;


  //2000/05/04 月分表示にしたため税込み金額で集計する
  //sSql := sSql + ' AND ';
  //sSql := sSql + ' tblTDenpyou.Memo <> ''' + CsCTAX + '''';
  //得意先コード指定
  {
  if EditTokuisakiCode.Text <> '' then begin
	  sSql := sSql + ' WHERE TokuisakiCode1 = ' + EditTokuisakiCode.Text;
  end;
  }
  //テスト用
  //sSql := sSql + ' AND ';
  //sSql := sSql + ' tblTDenpyou.TokuisakiCode1 = 2227';
  //ここまでテスト

	sSql := sSql + ' GROUP BY ShuukeiCode, Ka, tblMTokuisaki.TokuisakiCode1';
  sSql := sSql + ', tblMTokuisaki.TokuisakiCode2, ChainCode, TokuisakiNameYomi, TokuisakiName';
  //Add utada 2009.10
  sSql := sSql + ' ORDER BY tblMTokuisaki.TokuisakiCode1,tblMTokuisaki.TokuisakiCode2';



  with QueryTokuisaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    While not EOF do begin
      sLine := FieldByName('ShuukeiCode').AsString;
      sLine := sLine + ',' + FieldByName('Ka').AsString;
      sLine := sLine + ',' + FieldByName('USum').AsString;
      sLine := sLine + ',' + FieldByName('TokuisakiCode1').AsString;
      sLine := sLine + ',' + FieldByName('TokuisakiCode2').AsString;
      sLine := sLine + ',' + FieldByName('ChainCode').AsString;
      sLine := sLine + ',' + FieldByName('TokuisakiNameYomi').AsString;

      //2000/05/04
      sLine := sLine + ',' + FieldByName('TokuisakiName').AsString;

      SB1.SimpleText := sLine;
      SB1.Update;
		  HMakeFile(CFileName_Uriage, sLine);
    	Next;
    end;
    Close;
  end;
  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '売上集計.xls', '', SW_SHOW);
end;


procedure TfrmUriageShuukei.FormCreate(Sender: TObject);
var
  wYyyy, wMm, wDd      : Word;
  sYear, sMonth, sDay  : String;
begin
	width := 554;
  height := 220;

  // 年・月を当日の年・月に設定
   DecodeDate(Date(), wYyyy, wMm, wDd);
	 sYear  := IntToStr(wYyyy);
   sMonth := IntToStr(wMm);
   sDay   := IntToStr(wDd);

   CBYyyyFrom.Text := sYear;
   CBMmFrom.Text   := sMonth;
   CBYyyyTo.Text := sYear;
   CBMmTo.Text   := sMonth;

end;

end.
