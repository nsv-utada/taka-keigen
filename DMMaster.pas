unit DMMaster;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables;

type
  TfrmDMMaster = class(TDataModule)
    QueryMTokuisaki: TQuery;
    QueryTokuisakiName: TQuery;
    QueryUpDate: TQuery;
    QueryInsert: TQuery;
    QueryDKingaku: TQuery;
    QueryDelete: TQuery;
    QueryDenpyou: TQuery;
    QueryUDaichou: TQuery;
    DSUDaichou: TDataSource;
    QueryZandaka: TQuery;
    QueryShiharaibiM: TQuery;
    QueryUriage: TQuery;
    QueryCTaxSum: TQuery;
    QueryEstimate: TQuery;
    QueryEstimateSum: TQuery;
    QPMTokuisaki: TQuery;
    QPMUriage: TQuery;
    QKaishuu: TQuery;
    QueryKaishuu: TQuery;
    QueryTTokuisaki: TQuery;
    QueryTokuisakiEdit: TQuery;
    QueryGatsubun: TQuery;
    QuerySelect: TQuery;
    Query1Master: TQuery;
  private
    { Private 宣言 }
  public
    { Public 宣言 }
  end;

var
  frmDMMaster: TfrmDMMaster;
	function CheckGatsubun(sTokuisakiCode, sMaxMin : String):String;
	function GetGatsubunSum(sTokuisakiCode, sGatsubun:String):Integer;

  //汎用的な関数////////////////////////////////////////////////////////////
  //特定のテーブルから特定のフィールドデータを文字型で返す
  function GetFieldData(sTableName, sReturnFieldName, sWhere : String):String;

implementation
uses
Inter;

{$R *.DFM}

//Gatsubunデータがすべて入力されているかチェック
//1999/09/13
function CheckGatsubun(sTokuisakiCode, sMaxMin : String):String;
var
	sSql : String;
begin

	//sSql := 'SELECT MNGatsubun';
  //sSql := sSql + sMaxMin;
  //sSql := sSql + '(ISNULL(Gatsubun,''' + CsMinDate + ''')) FROM ' + CtblTDenpyou; //1999/09/25 ISNULL関数参照

	sSql := 'SELECT ';
  sSql := sSql + sMaxMin;
  //sSql := sSql + '(ISNULL(Gatsubun,''' + CsMinDate + ''')) as MNGatsubun FROM ' + CtblTDenpyou; //1999/09/25 ISNULL関数参照 thuyptt 20190730
  sSql := sSql + '(Gatsubun) as MNGatsubun FROM ' + CtblTDenpyou; //1999/09/25 ISNULL関数参照



  sSql := sSql + ' WHERE ';
  sSql := sSql + ' TokuisakiCode1 = ' + sTokuisakiCode;
  //sSql := sSql + ' AND ';
  //sSql := sSql + ' UriageKingaku < 0';
  with frmDMMaster.QueryGatsubun do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    if not EOF then begin
    	Result := FieldByName('MNGatsubun').AsString;
      if Result = '' then begin
	    	Result := CsMinDate;
      end;
    end else begin
    	Result := CsMinDate;
    end;
    Close;
  end;
end;


function GetGatsubunSum(sTokuisakiCode, sGatsubun:String):Integer;
var
	sSql : String;
begin
	//sSql := 'SELECT SumGatsubun = Sum(UriageKingaku) FROM ' + CtblTDenpyou; thuyptt 20190730
	sSql := 'SELECT Sum(UriageKingaku) AS SumGatsubun FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' TokuisakiCode1 = ' + sTokuisakiCode;
  sSql := sSql + ' AND ';
  sSql := sSql + ' Gatsubun = #' + sGatsubun + '#';
  with frmDMMaster.QueryGatsubun do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    if not EOF then begin
    	Result := FieldByName('SumGatsubun').AsInteger;
    end else begin
    	Result := 0;
    end;
    Close;
  end;
end;

//特定のテーブル(sTableName)からWHERE句(sWhere)の
//特定のフィールド(sReturnFieldName)のデータを
//文字型で返す
//平均などにも対応
function GetFieldData(sTableName, sReturnFieldName, sWhere : String):String;
var
	sSql : String;
  sReturnFieldName2 : String;
begin
	if Pos('=', sReturnFieldName) > 0 then begin
  	sReturnFieldName2 := Trim(Copy(sReturnFieldName, 1, Pos('=', sReturnFieldName)-1));
  end else begin
  	sReturnFieldName2 := sReturnFieldName;
  end;

	sSql := 'SELECT ' + sReturnFieldName + ' FROM ' + sTableName;
  sSql := sSql + ' WHERE ' + sWhere;
  if sWhere = 'Code=小田食品' then begin
   ShowMessage('AAA');
   result := '';
   exit;
  end;
	with frmDMMaster.QuerySelect do begin
 		Close;
   	Sql.Clear;
    Sql.Add(sSql);
 	  Open;
    if not EOF then begin
	    result := FieldByName(sReturnFieldName2).AsString;
    end else begin
      result := '';
    end;
    Close;
  end;
end;

end.
