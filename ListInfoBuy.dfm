inherited frmListInfoBuy: TfrmListInfoBuy
  Left = 515
  Top = 366
  Width = 720
  Height = 499
  Caption = 'frmListInfoBuy'
  OldCreateOrder = False
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  inherited Panel1: TPanel
    Width = 713
    Height = 49
    inherited Label1: TLabel
      Top = 4
      Width = 136
      Height = 16
      Caption = #20181#20837#20253#31080#19968#35239#30011#38754
      Font.Height = -16
    end
    inherited Label2: TLabel
      Left = 28
      Top = 28
    end
  end
  inherited Panel2: TPanel
    Top = 393
    Width = 713
    Height = 48
    TabOrder = 3
    inherited BitBtn1: TBitBtn
      Left = 581
      TabOrder = 1
    end
    inherited BitBtn2: TBitBtn
      Width = 0
      TabOrder = 0
      Visible = False
    end
    object BitBtn7: TBitBtn
      Left = 470
      Top = 8
      Width = 108
      Height = 25
      BiDiMode = bdLeftToRight
      Caption = #12463#12522#12450
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentBiDiMode = False
      ParentFont = False
      TabOrder = 2
      OnClick = BitBtn7Click
      Kind = bkRetry
    end
  end
  inherited Panel3: TPanel
    Top = 49
    Width = 713
    Height = 48
    Align = alTop
    TabOrder = 1
    object Label3: TLabel
      Left = 16
      Top = 8
      Width = 48
      Height = 15
      Caption = #32013#21697#26085
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DTP1: TDateTimePicker
      Left = 70
      Top = 6
      Width = 117
      Height = 23
      Date = 37111.466265312500000000
      Time = 37111.466265312500000000
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      TabStop = False
    end
    object cmdKennsaku: TButton
      Left = 196
      Top = 6
      Width = 77
      Height = 25
      Caption = #34920#31034
      TabOrder = 1
      OnClick = cmdKennsakuClick
    end
  end
  inherited SB1: TStatusBar
    Top = 441
    Width = 713
    Height = 23
  end
  object SG1: TStringGrid
    Left = -2
    Top = 98
    Width = 715
    Height = 295
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
    TabOrder = 2
    OnDblClick = SG1DblClick
    OnDrawCell = SG1DrawCell
    RowHeights = (
      24
      24
      24
      24
      24)
  end
  object QueryDenpyou: TQuery
    DatabaseName = 'taka'
    Left = 280
    Top = 49
  end
end
