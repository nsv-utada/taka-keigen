unit DenpyouPrint;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Qrctrls, QuickRpt, ExtCtrls, Db, DBTables, jpeg, StdCtrls, QRPrntr;

type
  TfrmDenpyouPrint = class(TForm)
    qrpDenpyouPrint: TQuickRep;
    PageHeaderBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRSysData1: TQRSysData;
    qryDenpyouPrint: TQuery;
    qryForDetailBand: TQuery;
    QRLabel7: TQRLabel;
    QRShape7: TQRShape;
    lblTokuisakiCode1: TQRLabel;
    lblTokuisakiCode2: TQRLabel;
    lblTokuisakiName: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    lblDenpyouBanngou: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRExprMemo1: TQRExprMemo;
    QRImage1: TQRImage;
    lblYear: TQRLabel;
    lblMonth: TQRLabel;
    QRLabel20: TQRLabel;
    lblDate: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRShape13: TQRShape;
    QRLabel13: TQRLabel;
    DetailBand1: TQRBand;
    QRShape6: TQRShape;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    qdtDetail0: TQRDBText;
    qdtDetail1: TQRDBText;
    qdtDetail2: TQRDBText;
    qdtDetail3: TQRDBText;
    qdtDetail4: TQRDBText;
    qdtDetail4_1: TQRDBText;
    qdtDetail6: TQRDBText;
    qdtDetail13: TQRDBText;
    QRLabel4: TQRLabel;
    SummaryBand1: TQRBand;
    QRLabel6: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    qexGoukei: TQRExpr;
    lblBikou: TQRLabel;
    lblTokuisakiNameUp: TQRLabel;
    QRShape4: TQRShape;
    QRShape3: TQRShape;
    QRLabel5: TQRLabel;
    qdtDetail4_Tax: TQRDBText;
    QRShape18: TQRShape;
    QRShape5: TQRShape;
    QRLabel10: TQRLabel;
    QRLabel17: TQRLabel;
    qdtShoukei8: TQRExpr;
    qdtShoukei10: TQRExpr;
    qryDenpyouTaxPrint: TQuery;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure MakeDenpyou(strDenpyouBanngou: String);
    function ChkGenkin(strDenpyouBanngou: String):Integer;
    function ChkTeisei(strDenpyouBanngou: String):Integer;
  private
    { Private 宣言 }
  public
    { Public 宣言 }
  end;

var
  frmDenpyouPrint   : TfrmDenpyouPrint;
  strDenpyouBanngou : String;

implementation

uses Inter, Denpyou2, DenpyouPrintGenkin;

{$R *.DFM}

procedure TfrmDenpyouPrint.FormCreate(Sender: TObject);
var
 LArrayOfDenpyouBanngou : array of String;  // 伝票番号のローカル保存用のリスト
 i,j                    : Integer;

begin

 // 伝票番号グローバル配列からローカル配列へコピー
 //　　他の印刷作業とダブって，作業中にグローバル変数が上書きされるといけないので，
 //    まずグローバルからローカルへ値を保存しておく．
 SetLength(LArrayOfDenpyouBanngou, Length(GArrayOfDenpyouBanngou));
 For i:=0 to Length(GArrayOfDenpyouBanngou)-1 do
   begin
     LArrayOfDenpyouBanngou[i] := GArrayOfDenpyouBanngou[i];
   end;

//
// 伝票番号のリストが尽きる迄,印刷を行う．
//

  For i:=0 to Length(LArrayOfDenpyouBanngou)-1 do begin

    // 伝票番号セット
    strDenpyouBanngou := LArrayOfDenpyouBanngou[i];

    // 伝票作成
    if ChkGenkin(strDenpyouBanngou) = 1 then
      begin
        // 伝票番号をグローバル変数にセットして
        SetLength(GArrayOfDenpyouBanngou, 1);
	      GArrayOfDenpyouBanngou[0] := strDenpyouBanngou;
        // 現金伝票印刷フォームを生成する．
        TfrmDenpyouPrintGenkin.Create(Self);
        // 次の伝票番号の処理へ
        continue;
      end

    else begin

     // 掛伝票か訂正伝票なのかを判断する．

     // �@ 掛伝票の場合
     if ChkTeisei(strDenpyouBanngou) = 0 then begin

      // 伝票データ作成
      MakeDenpyou(strDenpyouBanngou);

      // １つの掛伝票につき，2枚印刷する．
      For j:=1 to 2 do begin

        // 1枚目：納品書　2枚目：領収書

        // 使用するプリンタをデフォルトのプリンタに設定（実はこれなくてもいいかも)
        qrpDenpyouPrint.PrinterSettings.PrinterIndex := -1;

        // 1枚目印刷　納品証
        if j=1 then
          begin
            QRLabel7.Caption := '納　　　品      書';
            QRLabel11.Caption := '様';
            QRLabel4.Enabled  := False;
            QRLabel24.Caption := '下記の通り納品申し上げます';
            qrpDenpyouPrint.PrinterSettings.OutputBin := Lower;
            qrpDenpyouPrint.Print;
          end
        // 2枚目印刷　領収証
        else if j=2 then
          begin
            QRLabel7.Caption := '受　　　領      書';
            QRLabel11.Caption := '様より';
            QRLabel4.Enabled  := True;
            QRLabel24.Caption := '下記の通り受取りました';
            //qrpDenpyouPrint.PrinterSettings.OutputBin := Middle;
            qrpDenpyouPrint.PrinterSettings.OutputBin := Lower; //20190705伝票印刷が遅い対応
            qrpDenpyouPrint.Print;
          end
        else
          begin
              ShowMessage('エラー発生');
          end;
      end;

     // リソースの開放
     qryDenpyouPrint.Close;
     qryForDetailBand.Close;

     end

     // �A 訂正伝票の場合
     else begin

      // 伝票データ作成
      MakeDenpyou(strDenpyouBanngou);

       // １つの訂正伝票につき，2枚印刷する．
       For j:=1 to 2 do begin

         // 1枚目：訂正伝票　2枚目：訂正伝票(控)

         // 使用するプリンタをデフォルトのプリンタに設定（実はこれなくてもいいかも)
         qrpDenpyouPrint.PrinterSettings.PrinterIndex := -1;

         // 1枚目印刷　訂正伝票
         if j=1 then
           begin
             QRLabel7.Caption := '訂正・返品伝票';
             QRLabel11.Caption := '様';
             QRLabel4.Enabled  := False;
             QRLabel24.Caption := '下記の通り訂正・返品致しました';
             qrpDenpyouPrint.PrinterSettings.OutputBin := Middle;
             qrpDenpyouPrint.Print;
           end
         // 2枚目印刷　訂正伝票(控)
         else if j=2 then
           begin
             QRLabel7.Caption := '訂正・返品伝票 (控)';
             QRLabel11.Caption := '様より';
             QRLabel4.Enabled  := True;
             QRLabel24.Caption := '下記の通り訂正・返品致しました';
             qrpDenpyouPrint.PrinterSettings.OutputBin := Middle;
             qrpDenpyouPrint.Print;
           end
         else
           begin
             ShowMessage('エラー発生');
           end;
       end;

     // リソースの開放
     qryDenpyouPrint.Close;
     qryForDetailBand.Close;

     end;

    end;

  end;

end;


procedure TfrmDenpyouPrint.FormDestroy(Sender: TObject);
begin

{ // リソースの開放
 qryDenpyouPrint.Close;
 qryForDetailBand.Close;}

end;


function TfrmDenpyouPrint.ChkGenkin(strDenpyouBanngou: String):Integer;
// Result値は以下の通り．
//　0 : 掛伝票
//　1 : 現金伝票
var
 sSql              : String;
 strTokuisakiCode1 : String;

begin

 // 得意先Code1の取得
 sSql := 'SELECT TokuisakiCode1 FROM ' + CtblTDenpyouDetail;
 sSql := sSql + ' WHERE DenpyouCode = ' + '''' + strDenpyouBanngou + '''';
  with qryDenpyouPrint do begin
	 	Close;
    Sql.Clear;
  	Sql.Add(sSql);
    Open;
    strTokuisakiCode1 := FieldbyName('TokuisakiCode1').AsString;
    Close;
  end;

 // 伝票番号が50000番代ならば現金伝票
 if (StrToInt(strTokuisakiCode1) >= 50000) and (StrToInt(strTokuisakiCode1) < 60000) then
   begin
     Result := 1;
   end
 else
   begin
     Result := 0;
   end;

end;


function TfrmDenpyouPrint.ChkTeisei(strDenpyouBanngou: String):Integer;
// Result値は以下の通り．
//　0 : 掛伝票
//　1 : 訂正伝票
var
 sSql           : String;
 iTeiseiflg     : Integer;

begin

 // Teiseiflgの取得
 sSql := 'SELECT Teiseiflg FROM ' + CtblTDenpyou;
 sSql := sSql + ' WHERE DenpyouCode = ' + '''' + strDenpyouBanngou + '''';
  with qryDenpyouPrint do begin
	 	Close;
    Sql.Clear;
  	Sql.Add(sSql);
    Open;
    iTeiseiflg := FieldbyName('Teiseiflg').AsInteger;
    Close;
  end;

  Result := iTeiseiflg;

end;


procedure TfrmDenpyouPrint.MakeDenpyou(strDenpyouBanngou: String);
var
 sSql                   : String;
 strTokuisakiCode1      : String;
 strDenpyouDate         : String;
 strTokuisakiCode2      : String;
 strTokuisakiNameUp     : String;
 strTokuisakiName       : String;
 sYear,sMonth,sDay      : String;
 wY,wM,wD               : Word;
 strGoukei              : String;
 strBikou               : String;
 S                      : String;
 douTax                 : Double;
 newTax                 : Double;
 strShoukei8            : String;
 strShoukei10           : String;  

begin

 // 伝票番号セット
 lblDenpyouBanngou.Caption := strDenpyouBanngou;

 //koko
 // 得意先Code1, 伝票日付, 得意先Code2，得意先名 取得
 sSql := 'SELECT D.TokuisakiCode1 AS TokuisakiCode1, D.DenpyouDate AS DenpyouDate';
 sSql := sSql + ', T.TokuisakiCode2 AS TokuisakiCode2';
 sSql := sSql + ', T.TokuisakiNameUp AS TokuisakiNameUp, T.TokuisakiName AS TokuisakiName FROM ';
 sSql := sSql + CtblTDenpyouDetail + ' D, ' + CtblMTokuisaki + ' T';
 sSql := sSql + ' WHERE D.TokuisakiCode1 = T.TokuisakiCode1';
 sSql := sSql + ' AND D.DenpyouCode = ' + '''' + strDenpyouBanngou + '''';
  with qryDenpyouPrint do begin
	 	Close;
    Sql.Clear;
  	Sql.Add(sSql);
    Open;
    strTokuisakiCode1 := FieldbyName('TokuisakiCode1').AsString;
    strDenpyouDate    := FieldbyName('DenpyouDate').AsString;
    strTokuisakiCode2 := FieldbyName('TokuisakiCode2').AsString;
    strTokuisakiNameUp:= FieldbyName('TokuisakiNameUp').AsString;
    strTokuisakiName  := FieldbyName('TokuisakiName').AsString;
    Close;
  end;

 // 得意先Code1, 得意先Code2，得意先名 セット （得意先コードは4桁にしている)
 lblTokuisakiCode1.Caption := FormatFloat('0000', StrToInt(strTokuisakiCode1));
 lblTokuisakiCode2.Caption := strTokuisakiCode2;
 lblTokuisakiName.Caption  := strTokuisakiName;
 lblTokuisakiNameUp.Caption:= strTokuisakiNameUp;

 // 伝票日付セット
 DecodeDate(StrToDate(strDenpyouDate), wY, wM, wD);
 //sYear  := IntToStr(wY-1988); // 平成へ変換
 sYear  := IntToStr(wY);
 sMonth := IntToStr(wM);
 sDay   := IntToStr(wD);

 lblYear.Caption  := sYear;
 lblMonth.Caption := sMonth;
 lblDate.Caption  := sDay;

 // 明細行セット
 sSql := 'SELECT DD.No AS No, MI.Name AS Name, MI.Kikaku AS Kikaku, MI.Irisuu AS Irisuu, ';
 sSql := sSql + 'DD.Suuryou AS Suuryou, MI.Tanni AS Tanni, DD.Tannka AS Tannka, DD.Shoukei AS Shoukei, DD.tax AS tax FROM ';
 sSql := sSql + CtblTDenpyouDetail + ' DD INNER JOIN ' + CtblMItem;
 sSql := sSql + '  MI ON DD.Code1 = MI.Code1 AND DD.Code2 = MI.Code2';
 sSql := sSql + ' WHERE DD.DenpyouCode =' + '''' + strDenpyouBanngou + '''';
 sSql := sSql + ' ORDER BY [No]';
  with qryForDetailBand do begin
	 	Close;
    Sql.Clear;
  	Sql.Add(sSql);
    Open;
  end;

  qdtDetail0.DataField  := 'No';
  qdtDetail1.DataField  := 'Name';
  qdtDetail2.DataField  := 'Kikaku';
  qdtDetail3.DataField  := 'Irisuu';
  qdtDetail4.DataField  := 'Suuryou';
  qdtDetail4_1.DataField:= 'Tanni';
  qdtDetail6.DataField  := 'Tannka';
  qdtDetail13.DataField := 'Shoukei';
  qdtDetail4_Tax.DataField  := 'tax';

 douTax := CsTaxRate * 100;
 newTax := CsNewTaxRate * 100;

 sSql := 'SELECT SUM(tab.Shouhizei8) AS Shouhizei8, SUM(tab.Shouhizei10) AS Shouhizei10 FROM';
 sSql := sSql + ' (';
 sSql := sSql + ' SELECT';
 sSql := sSql + ' SUM(IIF(tax = ' + floattostr(douTax) + ', Shoukei, 0)) AS Shouhizei8, 0 AS Shouhizei10' ;
 sSql := sSql + ' FROM ' + CtblTDenpyouDetail;
 sSql := sSql + ' WHERE DenpyouCode =' + '''' + strDenpyouBanngou + '''';
 sSql := sSql + ' GROUP BY TokuisakiCode1, tax';
 sSql := sSql + ' UNION ';
 sSql := sSql + ' SELECT';
 sSql := sSql + ' 0 AS Shouhizei8, SUM(IIF (tax = ' + floattostr(newTax) + ' , Shoukei , 0 )) AS Shouhizei10';
 sSql := sSql + ' FROM ' + CtblTDenpyouDetail;
 sSql := sSql + ' WHERE DenpyouCode =' + '''' + strDenpyouBanngou + '''';
 sSql := sSql + ' GROUP BY TokuisakiCode1, tax';
 sSql := sSql + ' ) AS tab';

  with qryDenpyouTaxPrint do begin
	 	Close;
    Sql.Clear;
  	Sql.Add(sSql);
    Open;  
    strShoukei8   := FieldbyName('Shouhizei8').AsString;
    strShoukei10  := FieldbyName('Shouhizei10').AsString;
	 	Close;
  end;

  qdtShoukei8.Expression   := strShoukei8;
  qdtShoukei10.Expression  := strShoukei10;

 // 合計, 備考セット
   // labelではMaskプロパティを使えないので \#,##と指定して値を整形できない．
   // しょうがなく，DBTextを使用している．
 sSql := 'SELECT UriageKingaku, Bikou FROM ' + CtblTDenpyou;
 sSql := sSql + ' WHERE DenpyouCode =' + '''' + strDenpyouBanngou + '''';
  with qryDenpyouPrint do begin
	 	Close;
    Sql.Clear;
  	Sql.Add(sSql);
    Open;
    strGoukei := FieldbyName('UriageKingaku').AsString;
    strBikou  := FieldbyName('Bikou').AsString;
	 	Close;
  end;

  qexGoukei.Expression := strGoukei;
  lblBikou.Caption     := '備考：' + strBikou;

 // 鷹の絵のセット
 begin
   S := '.\logo.bmp';
   try
   QRImage1.Picture.LoadFromFile(S);
   except
     on EInvalidGraphic do
       QRImage1.Picture.Graphic := nil;
   end;
 end;


end;

end.
