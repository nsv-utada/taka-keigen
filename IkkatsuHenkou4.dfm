inherited frmIkkatsuHenkou4: TfrmIkkatsuHenkou4
  Left = 431
  Top = 151
  Width = 550
  Height = 380
  Caption = 'frmIkkatsuHenkou4'
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  inherited Panel1: TPanel
    Width = 542
    inherited Label1: TLabel
      Width = 160
      Caption = #20385#26684#19968#25324#22793#26356#30011#38754
    end
    inherited Label2: TLabel
      Left = 278
      Width = 243
    end
  end
  inherited Panel2: TPanel
    Top = 293
    Width = 542
    inherited BitBtn2: TBitBtn
      Left = 296
      Width = 115
      Caption = #19968#25324#22793#26356
      OnClick = BitBtn2Click
    end
  end
  inherited Panel3: TPanel
    Width = 542
    Height = 252
    object Label3: TLabel
      Left = 18
      Top = 12
      Width = 76
      Height = 12
      Caption = #21830#21697#12467#12540#12489'(1)'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 18
      Top = 36
      Width = 39
      Height = 12
      Caption = #21830#21697#21517
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 20
      Top = 63
      Width = 24
      Height = 12
      Caption = #12424#12415
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 372
      Top = 40
      Width = 47
      Height = 12
      Caption = #21336#20385'420'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label17: TLabel
      Left = 372
      Top = 62
      Width = 47
      Height = 12
      Caption = #21336#20385'450'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 220
      Top = 108
      Width = 14
      Height = 13
      Caption = #20870
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 224
      Top = 132
      Width = 8
      Height = 13
      Caption = '%'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 276
      Top = 116
      Width = 72
      Height = 12
      Caption = #22793#26356#24460#12398#20385#26684
    end
    object RadioGroup1: TRadioGroup
      Left = 24
      Top = 92
      Width = 117
      Height = 65
      Caption = #20385#26684#22793#26356
      TabOrder = 0
    end
    object RadioButton1: TRadioButton
      Left = 36
      Top = 108
      Width = 97
      Height = 17
      Caption = #20870#12288#12391#22793#26356
      TabOrder = 1
    end
    object RadioButton2: TRadioButton
      Left = 36
      Top = 124
      Width = 93
      Height = 17
      Caption = '%'#12288#12391#22793#26356
      TabOrder = 2
    end
    object Edit1: TEdit
      Left = 148
      Top = 104
      Width = 65
      Height = 20
      TabOrder = 3
    end
    object Edit2: TEdit
      Left = 148
      Top = 128
      Width = 65
      Height = 20
      TabOrder = 4
    end
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 540
      Height = 250
      Align = alClient
      TabOrder = 5
      object Label9: TLabel
        Left = 18
        Top = 12
        Width = 76
        Height = 12
        Caption = #21830#21697#12467#12540#12489'(1)'
        Font.Charset = SHIFTJIS_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label10: TLabel
        Left = 18
        Top = 36
        Width = 39
        Height = 12
        Caption = #21830#21697#21517
        Font.Charset = SHIFTJIS_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label12: TLabel
        Left = 20
        Top = 63
        Width = 24
        Height = 12
        Caption = #12424#12415
        Font.Charset = SHIFTJIS_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label13: TLabel
        Left = 372
        Top = 40
        Width = 47
        Height = 12
        Caption = #21336#20385'420'
        Font.Charset = SHIFTJIS_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label14: TLabel
        Left = 372
        Top = 62
        Width = 47
        Height = 12
        Caption = #21336#20385'450'
        Font.Charset = SHIFTJIS_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label15: TLabel
        Left = 220
        Top = 108
        Width = 14
        Height = 13
        Caption = #20870
        Font.Charset = SHIFTJIS_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label16: TLabel
        Left = 224
        Top = 132
        Width = 8
        Height = 13
        Caption = '%'
        Font.Charset = SHIFTJIS_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label18: TLabel
        Left = 276
        Top = 116
        Width = 72
        Height = 12
        Caption = #22793#26356#24460#12398#20385#26684
      end
      object RadioGroup2: TRadioGroup
        Left = 24
        Top = 92
        Width = 117
        Height = 65
        Caption = #20385#26684#22793#26356
        TabOrder = 5
      end
      object ComboBox1: TComboBox
        Left = 102
        Top = 6
        Width = 163
        Height = 21
        Font.Charset = SHIFTJIS_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
        Font.Style = []
        ImeMode = imClose
        ItemHeight = 13
        ParentFont = False
        TabOrder = 0
        Items.Strings = (
          '01,'#26989#21209#29992#39135#26448
          '02,'#35519#21619#26009#35519#21619#39135#21697
          '03,'#39321#36763#26009
          '04,'#12499#12531#12289#32566#35440#39006
          '05,'#12467#12540#12498#12540#12289#21931#33590#26448#26009
          '06,'#20083#35069#21697#65288#12481#12540#12474#39006#65289
          '07,'#39135#29992#27833
          '08,'#31881#35069#21697
          '09,'#12418#12385
          '10,'#12473#12497#40634
          '11,'#40634
          '12,'#31859
          '13,'#20013#33775#26448#26009
          '14,'#21508#31278#12472#12517#12540#12473
          '15,'#12362#33590
          '16,'#27703#12471#12525#12483#12503
          '17,'#28460#29289#24803#33756
          '18,'#12362#36890#12375#29289
          '19,'#39135#32905#21152#24037#21697
          '20,'#28023#29987#29645#21619
          '21,'#20919#20941#39135#21697#39770#20171#39006
          '22,'#20919#20941#39135#21697#36786#29987#29289
          '23,'#20919#20941#39135#21697#19968#27425#21152#24037#21697
          '24,'#20919#20941#39135#21697#28857#24515#39006
          '25,'#12486#12522#12540#12492#39006
          '26,'#12524#12488#12523#12488#39006#28271#29006
          '27,'#20919#20941#35201#20919#12289#39321#36763#26009
          '28,'#12362#12388#12414#12415#35910#39006
          '29,'#12362#12388#12414#12415#12362#12363#12365#39006
          '30,'#12362#12388#12414#12415#12481#12483#12503#39006
          '31,'#12481#12519#12467#12524#12540#12488#12289#12476#12522#12540#39006
          '32,'#28023#29987#29289
          '33,'#24178#12375#32905
          ''
          '')
      end
      object ComboBox2: TComboBox
        Left = 102
        Top = 31
        Width = 250
        Height = 21
        Font.Charset = SHIFTJIS_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
        Font.Style = []
        ImeMode = imOpen
        ItemHeight = 13
        ParentFont = False
        TabOrder = 1
      end
      object ComboBox3: TComboBox
        Left = 102
        Top = 58
        Width = 250
        Height = 21
        Font.Charset = SHIFTJIS_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
        Font.Style = []
        ImeMode = imHira
        ItemHeight = 13
        ParentFont = False
        TabOrder = 2
      end
      object Edit3: TEdit
        Left = 448
        Top = 36
        Width = 73
        Height = 20
        ImeMode = imClose
        TabOrder = 3
      end
      object Edit4: TEdit
        Left = 448
        Top = 54
        Width = 73
        Height = 20
        ImeMode = imClose
        TabOrder = 4
      end
      object RadioButton3: TRadioButton
        Left = 36
        Top = 108
        Width = 97
        Height = 17
        Caption = #20870#12288#12391#22793#26356
        TabOrder = 6
      end
      object RadioButton4: TRadioButton
        Left = 36
        Top = 124
        Width = 93
        Height = 17
        Caption = '%'#12288#12391#22793#26356
        TabOrder = 7
      end
      object Edit5: TEdit
        Left = 148
        Top = 104
        Width = 65
        Height = 20
        TabOrder = 8
      end
      object Edit6: TEdit
        Left = 148
        Top = 128
        Width = 65
        Height = 20
        TabOrder = 9
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 538
        Height = 248
        Align = alClient
        TabOrder = 10
        object Label19: TLabel
          Left = 18
          Top = 12
          Width = 76
          Height = 12
          Caption = #21830#21697#12467#12540#12489'(1)'
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clRed
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label20: TLabel
          Left = 18
          Top = 36
          Width = 39
          Height = 12
          Caption = #21830#21697#21517
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clRed
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label21: TLabel
          Left = 20
          Top = 63
          Width = 24
          Height = 12
          Caption = #12424#12415
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clRed
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label22: TLabel
          Left = 384
          Top = 40
          Width = 39
          Height = 12
          Caption = #21336#20385'_S'
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label23: TLabel
          Left = 384
          Top = 62
          Width = 38
          Height = 12
          Caption = #21336#20385'_L'
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label24: TLabel
          Left = 196
          Top = 108
          Width = 14
          Height = 13
          Caption = #20870
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label25: TLabel
          Left = 200
          Top = 132
          Width = 8
          Height = 13
          Caption = '%'
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label26: TLabel
          Left = 300
          Top = 116
          Width = 78
          Height = 12
          Caption = #22793#26356#24460#12398#20385#26684
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clMaroon
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label27: TLabel
          Left = 384
          Top = 104
          Width = 70
          Height = 12
          Caption = #21336#20385'_S(420)'
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clMaroon
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label28: TLabel
          Left = 384
          Top = 126
          Width = 69
          Height = 12
          Caption = #21336#20385'_L(450)'
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clMaroon
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label29: TLabel
          Left = 22
          Top = 168
          Width = 78
          Height = 12
          Caption = #24471#24847#20808#12398#25351#23450
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clRed
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label30: TLabel
          Left = 132
          Top = 168
          Width = 72
          Height = 12
          Caption = #12481#12455#12540#12531#12467#12540#12489
        end
        object LabelSID: TLabel
          Left = 370
          Top = 40
          Width = 4
          Height = 12
          Visible = False
        end
        object Label32: TLabel
          Left = 280
          Top = 12
          Width = 68
          Height = 12
          Caption = #21830#21697#12467#12540#12489'(2)'
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = []
          ParentFont = False
        end
        object Label33: TLabel
          Left = 252
          Top = 168
          Width = 111
          Height = 12
          Caption = #12300'0'#12301#12398#22580#21512#12399#12377#12409#12390
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clRed
          Font.Height = -12
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label31: TLabel
          Left = 132
          Top = 192
          Width = 61
          Height = 12
          Caption = #12456#12522#12450#12467#12540#12489
        end
        object RadioGroup3: TRadioGroup
          Left = 24
          Top = 92
          Width = 117
          Height = 65
          Caption = #20385#26684#22793#26356
          TabOrder = 5
        end
        object CBCode1: TComboBox
          Left = 102
          Top = 6
          Width = 163
          Height = 21
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = []
          ImeMode = imClose
          ItemHeight = 13
          ParentFont = False
          TabOrder = 0
          Items.Strings = (
            '01,'#26989#21209#29992#39135#26448
            '02,'#35519#21619#26009#35519#21619#39135#21697
            '03,'#39321#36763#26009
            '04,'#12499#12531#12289#32566#35440#39006
            '05,'#12467#12540#12498#12540#12289#21931#33590#26448#26009
            '06,'#20083#35069#21697#65288#12481#12540#12474#39006#65289
            '07,'#39135#29992#27833
            '08,'#31881#35069#21697
            '09,'#12418#12385
            '10,'#12473#12497#40634
            '11,'#40634
            '12,'#31859
            '13,'#20013#33775#26448#26009
            '14,'#21508#31278#12472#12517#12540#12473
            '15,'#12362#33590
            '16,'#27703#12471#12525#12483#12503
            '17,'#28460#29289#24803#33756
            '18,'#12362#36890#12375#29289
            '19,'#39135#32905#21152#24037#21697
            '20,'#28023#29987#29645#21619
            '21,'#20919#20941#39135#21697#39770#20171#39006
            '22,'#20919#20941#39135#21697#36786#29987#29289
            '23,'#20919#20941#39135#21697#19968#27425#21152#24037#21697
            '24,'#20919#20941#39135#21697#28857#24515#39006
            '25,'#12486#12522#12540#12492#39006
            '26,'#12524#12488#12523#12488#39006#28271#29006
            '27,'#20919#20941#35201#20919#12289#39321#36763#26009
            '28,'#12362#12388#12414#12415#35910#39006
            '29,'#12362#12388#12414#12415#12362#12363#12365#39006
            '30,'#12362#12388#12414#12415#12481#12483#12503#39006
            '31,'#12481#12519#12467#12524#12540#12488#12289#12476#12522#12540#39006
            '32,'#28023#29987#29289
            '33,'#24178#12375#32905
            ''
            '')
        end
        object CBName: TComboBox
          Left = 102
          Top = 31
          Width = 250
          Height = 21
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = []
          ImeMode = imOpen
          ItemHeight = 13
          ParentFont = False
          TabOrder = 1
          OnExit = CBNameExit
        end
        object CBYomi: TComboBox
          Left = 102
          Top = 58
          Width = 250
          Height = 21
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = []
          ImeMode = imHira
          ItemHeight = 13
          ParentFont = False
          TabOrder = 2
          OnKeyDown = CBYomiKeyDown
        end
        object EditTankaS: TEdit
          Left = 440
          Top = 36
          Width = 73
          Height = 20
          ImeMode = imClose
          TabOrder = 3
        end
        object EditTankaL: TEdit
          Left = 440
          Top = 54
          Width = 73
          Height = 20
          ImeMode = imClose
          TabOrder = 4
        end
        object RadioButton5: TRadioButton
          Left = 36
          Top = 108
          Width = 97
          Height = 17
          Caption = #20870#12288#12391#22793#26356
          TabOrder = 6
          OnClick = RadioButton5Click
        end
        object RadioButton6: TRadioButton
          Left = 36
          Top = 124
          Width = 93
          Height = 17
          Caption = '%'#12288#12391#22793#26356
          TabOrder = 7
          OnClick = RadioButton6Click
        end
        object EditYen: TEdit
          Left = 148
          Top = 104
          Width = 30
          Height = 20
          ImeMode = imDisable
          TabOrder = 8
          Text = '0'
          OnExit = EditYenExit
        end
        object EditPercent: TEdit
          Left = 148
          Top = 128
          Width = 30
          Height = 20
          ImeMode = imDisable
          TabOrder = 9
          Text = '0'
          OnExit = EditPercentExit
        end
        object EditTankaS_New: TEdit
          Left = 456
          Top = 100
          Width = 57
          Height = 20
          ImeMode = imDisable
          TabOrder = 10
          Text = '0'
        end
        object EditTankaL_New: TEdit
          Left = 456
          Top = 122
          Width = 57
          Height = 20
          ImeMode = imDisable
          TabOrder = 11
          Text = '0'
        end
        object EditChainCode: TEdit
          Left = 216
          Top = 164
          Width = 30
          Height = 20
          ImeMode = imDisable
          TabOrder = 12
          Text = '0'
          OnEnter = EditChainCodeEnter
        end
        object CBCode2: TComboBox
          Left = 354
          Top = 6
          Width = 61
          Height = 21
          Font.Charset = SHIFTJIS_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
          Font.Style = []
          ImeMode = imClose
          ItemHeight = 13
          ParentFont = False
          TabOrder = 13
        end
        object RG4: TRadioGroup
          Left = 128
          Top = 208
          Width = 129
          Height = 33
          TabOrder = 14
        end
        object RB_S: TRadioButton
          Left = 136
          Top = 216
          Width = 73
          Height = 17
          Caption = 'S(420)'
          TabOrder = 15
          OnClick = RB_SClick
        end
        object RB_L: TRadioButton
          Left = 192
          Top = 216
          Width = 57
          Height = 17
          Caption = 'L(450)'
          TabOrder = 16
          OnClick = RB_LClick
        end
        object EditAria: TEdit
          Left = 216
          Top = 188
          Width = 30
          Height = 20
          ImeMode = imDisable
          TabOrder = 17
          Text = '0'
          OnEnter = EditAriaEnter
        end
      end
    end
  end
  inherited SB1: TStatusBar
    Top = 334
    Width = 542
  end
  object Query1: TQuery
    DatabaseName = 'taka'
    Left = 236
    Top = 19
  end
  object Query2: TQuery
    DatabaseName = 'taka'
    Left = 270
    Top = 19
  end
  object Query3: TQuery
    DatabaseName = 'taka'
    Left = 310
    Top = 15
  end
end
