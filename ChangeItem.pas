unit ChangeItem;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBTables, StdCtrls, Buttons, ExtCtrls;

type
  TfrmChangeItem = class(TForm)
    Label3: TLabel;
    Label4: TLabel;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Panel2: TPanel;
    BitBtn1: TBitBtn;
    btnUpdate: TBitBtn;
    cbxShiiresakiNameOld: TComboBox;
    cbxShiiresakiNameNew: TComboBox;
    QueryMShiiresaki: TQuery;
    QueryShiiresakiUpdate: TQuery;
    procedure btnUpdateClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbxShiiresakiNameOldKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cbxShiiresakiNameNewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Split(const Delimiter: Char;Input: string;const Strings: TStrings) ;
    procedure MakeCBShiiresakiName(sKey, sWord:String; iKubun:Integer; flg: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { P                                    ublic declarations }
  end;

var
  frmChangeItem: TfrmChangeItem;

implementation
uses DMMaster, Inter, HKLib,PasswordDlg;

{$R *.dfm}

procedure TfrmChangeItem.btnUpdateClick(Sender: TObject);
  var sSql: String;
  var sKey: String;
  var sNewCode: String;
  var sOldCode: String;
  var slstNewCode: TStringList;
  var slstOldCode: TStringList;
  var sMsg: String;
  var iCode: Integer;
  //var s:String;
  begin
    if cbxShiiresakiNameOld.Text='' then begin
      Application.MessageBox('元仕入先が選択されていません。', 'Information', MB_OK);
    end else if cbxShiiresakiNameNew.Text='' then begin
      Application.MessageBox('新仕入先が選択されていません。', 'Information', MB_OK);
    end else begin
     // update
     sMsg:='元仕入先「'+cbxShiiresakiNameOld.Text+'」の商品を'+sLineBreak
     +'新仕入先「'+cbxShiiresakiNameNew.Text+'」へ一括変更します。'+sLineBreak+'よろしいですか？';
     if(Application.MessageBox(PChar(sMsg)
        , 'Information', MB_OKCANCEL)=IDOK) then begin
        
        sKey:='ShiiresakiCode';
        slstOldCode:=TStringList.Create;
        Split(',', cbxShiiresakiNameOld.Text, slstOldCode);
        sOldCode:= slstOldCode[0];

        slstNewCode:=TStringList.Create;
        Split(',', cbxShiiresakiNameNew.Text, slstNewCode);
        sNewCode:=slstNewCode[0];

        if TryStrToInt(sOldCode, iCode) and TryStrToInt(sNewCode, iCode) then
        begin
          sSql:='Update '+CtblMItem+' set '+sKey+'='+sNewCode+' where '+sKey
              +'='+sOldCode;
          //s:=sSql;
          //Application.MessageBox(Pchar(s), 0, 0);

          With QueryShiiresakiUpdate do
          begin
            try
              SQL.Clear;
              SQL.Add(sSql);
              ExecSQL;
              Application.MessageBox('一括更新処理が完了しました。', 'Information', MB_OK);
            except
            end;           
          end;
        end; // end if
        
     end;
  end;
end;

procedure TfrmChangeItem.BitBtn1Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmChangeItem.FormCreate(Sender: TObject);
begin
  //2006.05.17
  dlgPasswordDlg := TdlgPasswordDlg.Create(Self);
  if dlgPasswordDlg.ShowModal = mrOK then begin
    ;
  end else begin
    Exit;
  end;

  if GPassWord = CPassword1 then begin
    Beep;

  //add2009.04.08 utada
	end else if GPassWord = CPassword2 then begin
    Beep;

	end else if GPassWord = CPassword3 then begin
    Beep;

  //add end utada
  end else begin
    //ShowMessage('PassWordが違います');
    close;
  end;
  GPassWord := '';
end;

procedure TfrmChangeItem.cbxShiiresakiNameOldKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key=VK_F1) then begin
    // Make TokuisakiName
    MakeCBShiiresakiName('Yomi', cbxShiiresakiNameOld.Text, 1, 0);
  end; //of if
end;

procedure TfrmChangeItem.cbxShiiresakiNameNewKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key=VK_F1) then begin
    // Make TokuisakiName
    MakeCBShiiresakiName('Yomi', cbxShiiresakiNameNew.Text, 1, 1);
  end; //of if
end;

procedure TfrmChangeItem.Split(const Delimiter: Char;Input: string;const Strings: TStrings) ;
begin
  Assert(Assigned(Strings)) ;
  Strings.Clear;
  Strings.Delimiter := Delimiter;
  Strings.DelimitedText := Input;
end;

procedure TfrmChangeItem.MakeCBShiiresakiName(sKey, sWord:String; iKubun:Integer; flg: Integer);
var
	sSql : String;
begin
	//2002.09.29
  if sWord='' then begin
  	Exit;
  end;

  
  if flg=0 then
  begin
    cbxShiiresakiNameOld.Items.Clear;
  end else begin
    cbxShiiresakiNameNew.Items.Clear;
  end;


	sSql := 'SELECT * FROM ' + CtblMShiiresaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + sKey;
  case iKubun of
  0: sSql := sSql + ' LIKE ''' + sWord + '%''';
  1: sSql := sSql + ' LIKE ''%' + sWord + '%''';
  2: sSql := sSql + ' = ' + sWord;
  end;

  sSql := sSql + ' ORDER BY Code ';

  if flg=0 then begin
    cbxShiiresakiNameOld.Clear;
  end else begin
    cbxShiiresakiNameNew.Clear;
  end;
  
	with QueryMShiiresaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin

      if flg=0 then begin
    	cbxShiiresakiNameOld.Items.Add(FieldByName('Code').AsString + ',' +
       FieldByName('Name').AsString);
      end else begin
        cbxShiiresakiNameNew.Items.Add(FieldByName('Code').AsString + ',' +
        FieldByName('Name').AsString);
      
      end;
    	Next;
    end;//of while
    Close;
  end;//of with

  if flg=0 then begin
    cbxShiiresakiNameOld.DroppedDown:=True;
  end else begin
    cbxShiiresakiNameNew.DroppedDown:=True;
  end;
end;


procedure TfrmChangeItem.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action:=caFree;
end;

end.
