inherited frmShiireKakakuRireki: TfrmShiireKakakuRireki
  Left = 415
  Top = 160
  Width = 468
  Caption = 'frmShiireKakakuRireki'
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  inherited Panel1: TPanel
    Width = 460
    inherited Label1: TLabel
      Width = 160
      Caption = #20181#20837#20385#26684#23653#27508#30011#38754
    end
    inherited Label2: TLabel
      Left = 186
      Visible = False
    end
    object DBNavigator1: TDBNavigator
      Left = 192
      Top = 16
      Width = 150
      Height = 18
      DataSource = DS1
      VisibleButtons = [nbDelete, nbEdit, nbPost]
      TabOrder = 0
    end
  end
  inherited Panel2: TPanel
    Width = 460
    inherited BitBtn1: TBitBtn
      Left = 208
    end
    inherited BitBtn2: TBitBtn
      Left = 8
      Enabled = False
      Visible = False
    end
  end
  inherited Panel3: TPanel
    Width = 460
    object DBGrid1: TDBGrid
      Left = 1
      Top = 1
      Width = 458
      Height = 243
      Align = alClient
      DataSource = DS1
      ImeMode = imDisable
      TabOrder = 0
      TitleFont.Charset = SHIFTJIS_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #65325#65331' '#65328#12468#12471#12483#12463
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'ID'
          Width = 55
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Code1'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Code2'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Setteibi'
          Title.Caption = #35373#23450#26085
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'kakaku'
          Title.Caption = #20181#20837#20385#26684
          Visible = True
        end>
    end
  end
  inherited SB1: TStatusBar
    Width = 460
  end
  object Query1: TQuery
    Active = True
    DatabaseName = 'taka'
    RequestLive = True
    SQL.Strings = (
      'Select * from tblMShiireKakakuRireki')
    Left = 132
    Top = 11
    object Query1ID: TIntegerField
      FieldName = 'ID'
      Origin = 'TAKA.tblMShiireKakakuRireki.ID'
      ReadOnly = True
    end
    object Query1Code1: TStringField
      FieldName = 'Code1'
      Origin = 'TAKA.tblMShiireKakakuRireki.Code1'
      ReadOnly = True
      FixedChar = True
      Size = 2
    end
    object Query1Code2: TStringField
      FieldName = 'Code2'
      Origin = 'TAKA.tblMShiireKakakuRireki.Code2'
      ReadOnly = True
      FixedChar = True
      Size = 4
    end
    object Query1Setteibi: TDateTimeField
      FieldName = 'Setteibi'
      Origin = 'TAKA.tblMShiireKakakuRireki.Setteibi'
    end
    object Query1kakaku: TFloatField
      FieldName = 'kakaku'
      Origin = 'TAKA.tblMShiireKakakuRireki.kakaku'
    end
  end
  object DS1: TDataSource
    DataSet = Query1
    Left = 160
    Top = 8
  end
end
