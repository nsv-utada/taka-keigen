unit PUrikakeDaichou2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, Grids, StdCtrls, Buttons, ExtCtrls, DBGrids, ComCtrls, DBTables, ShellAPI,
  PUrikakeDaichou;

type
  TfrmPUrikakeDaichou2 = class(TfrmPUrikakeDaichou)
    BitBtn7: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure SGUDaichouDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure CBTokuisakiNameKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBTokuisakiNameExit(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
  private
    { Private 宣言 }
		procedure MakeSG;
		procedure PrintSG(iFlag:integer);
		procedure ClearSG;
		procedure MakeSG2(myQuery : TQuery; iFlag:integer);
		function GetZandaka(sTokuisakiCode1, sFrom, sTo: String):Integer;
		//procedure MakeCBTokuisakiName(sKey, sWord:String; iKubun:Integer);
  public
    { Public 宣言 }
		procedure MakeCBTokuisakiName(sKey, sWord:String; iKubun:Integer);
  end;

  const
	//グリッドの列の定義
  CintDate          = 0; //日付
  CintMemo          = 1; //適用
  CintUriKingaku    = 2; //売上金額（税抜き）
  CintCTax          = 3; //消費税
  CintUriKingaku2   = 4; //売上金額（税込み）
  CintTougetsu        = 5; //当月請求
  CintUkeKingaku    = 6; //受け入れ金額
  CintZandakan      = 7; //残高
  CintGatsubun      = 8; //月分 199/09/10追加
  CintEnd           = 9;   //ターミネーター


	GRowCount = 700; //行数
var
  frmPUrikakeDaichou2: TfrmPUrikakeDaichou2;

implementation
uses
	DMMAster, MTokuisaki, Inter, HKLib, PBill, TKaishuu;

{$R *.DFM}
var
	GiRow : Integer;
  GiKungakuSum, GiTax: Integer;
  GsShimebi : String;
  GbFlag : Boolean; //締め日の計算をしたかどうか　する前->False したあと- > True

procedure TfrmPUrikakeDaichou2.MakeSG;
begin
  with SGUDaichou do begin
    DefaultDrawing := True;
    RowCount := GRowCount;
    ColCount := CintEnd;
    Align := alBottom;
    FixedCols := 0;
    FixedColor := clYellow;
    Canvas.Font.Size := 11;
    DefaultRowHeight := 20;
    ColWidths[0]:= 64;
    Font.Color := clNavy;

    ColWidths[CintDate]:= 70;
    Cells[CintDate, 0] := '日付';

    ColWidths[CintMemo]:= 165;
    Cells[CintMemo, 0] := '適 用';

    ColWidths[CintUriKingaku]:= 80;
    Cells[CintUriKingaku, 0] := '売上(税抜)';

    ColWidths[CintCTax]:= 60;
    Cells[CintCTax, 0] := '消費税';

    ColWidths[CintUriKingaku2]:= 80;
    Cells[CintUriKingaku2, 0] := '売上(税込)';

    ColWidths[CintTougetsu]:= 80;
    Cells[CintTougetsu, 0] := '当月請求';

    ColWidths[CintUkeKingaku]:= 80;
    Cells[CintUkeKingaku, 0] := '受入金額';

    ColWidths[CintGatsubun]:= 80;
    Cells[CintGatsubun, 0] := '月分';

    ColWidths[CintZandakan]:= 80;
    Cells[CintZandakan, 0] := '残';

  end;//of with

end;

procedure TfrmPUrikakeDaichou2.FormCreate(Sender: TObject);
begin
	Width := 890;
  Height := 540;


  //ストリンググリッドの作成
  MakeSG;

  //でーたの引継
  EditTokuisakiCode.Text := GTokuisakiCode;
  GTokuisakiName := CBTokuisakiName.Text;

  //1999/08/10変更
  //DTPFrom.Date := Date() - 60;
  //DTPFrom.Date := StrToDate('1998/12/30');
  //2000/05/04変更
  //DTPFrom.Date := StrToDate('2000/04/01');

  //2001/06/20
  DTPFrom.Date := Date() - 180;

  DTPTo.Date := Date();


end;

procedure TfrmPUrikakeDaichou2.BitBtn5Click(Sender: TObject);
begin
	PrintSG(0);
end;

//グリッドのクリア
procedure TfrmPUrikakeDaichou2.ClearSG;
var
  iCol, iRow : Integer;
begin
  with SGUDaichou do begin
  	for iCol := 0 to ColCount do begin
    	for iRow := 1 to RowCount do begin
	      Cells[iCol, iRow] := '';
      end;
    end;

  end;
end;

//
//iFlag = 0  -> 通常表示
//iFlag = 1  -> 折りたたみ表示
//                Ver2.0 2000/03/03 返金も出るように変更
//
//              適用が消費税か金額がマイナスのもののみ表示する
//
// 2005/09/15
// 7 貸し倒れ　8　雑収入に対応
//
procedure TfrmPUrikakeDaichou2.PrintSG(iFlag:integer);
var
	sSql, sTokuisakiCode, sDateTO:String;
  sDateFrom : String;
  iZandaka : Integer;
  wYyyy, wMm, wDd:Word;
const
	CsDateFrom = '1998/12/01'; //残高計算対象期間
begin
  ClearSG;

	//締め日の取得
  GsShimebi := MTokuisaki.GetShimebi(EditTokuisakiCode.Text);
	//20020929
  if GsShimebi ='' then begin //得意先の閉め日がない又は得意先がない場合
    ShowMessage('閉め日が入力されていません');
  	Exit;
  end;

	//表示情報の取得       check utada
	sSql := 'SELECT * ';
  sSql := sSql + ' FROM ';
  sSql := sSql + CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  sSql := sSql + 'TokuisakiCode1 = ' +  EditTokuisakiCode.Text;
  sSql := sSql + ' AND ';
  sSql := sSql + 'DDate Between ';

  //2000/05/16
  sSql := sSql + '''' + DateToStr(DTPFrom.Date) + '''';

  sSql := sSql + ' AND ';
  sSql := sSql + '''' + DateToStr(DTPTo.Date) + '''';
  sSql := sSql + ' ORDER BY DDate, ID';

  GiRow := 1;
  GiKungakuSum := 0;
  GiTax := 0;
  GbFlag := False;

  //表示期間の前日までの残高を求める
  //この間数の中でグリッドに表示もする
  //iZandaka := GetZandaka(EditTokuisakiCode.Text, '1999/01/01', DateToStr(DTPFrom.Date - 1));
  //締め日の翌月から
  DecodeDate(DTPFrom.Date, wYyyy, wMm, wDd);
  wDd := StrToInt(MTokuisaki.GetShimebi(EditTokuisakiCode.Text));
  //sDdをその月の月末日の翌日に変換する
  if wDd = CSeikyuuSimebi_End then begin
  	//wDd := HKLib.GetGetsumatsu(wYyyy, wMm);
    wDd := 1; //月末の翌日は必ず１日
    wMm := wMm + 1;
    if wMm = 13 then begin
    	wMm := 1;
      wYyyy := wYyyy + 1;
    end;
  end else begin
	  wDd := wDd + 1;
  end;

  if EnCodeDate(wYyyy,wMm,wDd) > DTPFrom.Date then begin
  	//１ヶ月戻す
    wMm := wMm - 1;
    if wMm = 0 then begin
    	wMm := 12;
      wYyyy := wyyyy-1;
    end;//of if
  end;

  //2000/05/16
  //iZandaka := GetZandaka(EditTokuisakiCode.Text, DateToStr(EnCodeDate(wYyyy,wMm,wDd)), DateToStr(DTPFrom.Date - 1));
  iZandaka := GetZandaka(EditTokuisakiCode.Text, CsDateFrom, DateToStr(DTPFrom.Date - 1));

  GiRow := GiRow + 1;
  GiKungakuSum := iZandaka;

  with frmDMMaster.QueryUDaichou do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    While not EOF do begin
	    MakeSG2(frmDMMaster.QueryUDaichou, iFlag);
	   	Next;
	    GiRow := GiRow + 1;
    end;
    Close;
  end;//of with

end;
//得意先の期間の売上高の合計を求めて、グリッドに表示する
function TfrmPUrikakeDaichou2.GetZandaka(sTokuisakiCode1, sFrom, sTo: String):Integer;
var
	sSql : String;
begin
	//表示情報の取得
	sSql := 'SELECT KSum = Sum(UriageKingaku) ';
  sSql := sSql + ' FROM ';
  sSql := sSql + CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  sSql := sSql + 'TokuisakiCode1 = ' +  sTokuisakiCode1;
  sSql := sSql + ' AND ';
  sSql := sSql + 'DDate Between ';
  sSql := sSql + '''' + sFrom + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + '''' + sTo + '''';

  //2000/06/19
  {
  sSql := sSql + ' AND ';
  sSql := sSql + '(';
  sSql := sSql + ' UriageKingaku > 0 ';
  sSql := sSql + ' OR ';
  sSql := sSql + ' ((UriageKingaku < 0) AND (SUBSTRING(Memo, 1, 2) NOT IN (''返品'', ''値引'')))' ;
  sSql := sSql + ')';
  }


  with frmDMMaster.QueryZandaka do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    Result := FieldByName('KSum').AsInteger;
		SGUDaichou.Cells[CintDate, GiRow] := '';
		SGUDaichou.Cells[CintMemo, GiRow] := '繰越残高';
	  SGUDaichou.Cells[CintUriKingaku2, GiRow] := FormatFloat('0,', Result);
	  SGUDaichou.Cells[CintTougetsu, GiRow] := FormatFloat('0,', Result);
    Close;
  end;
end;

//グリッドのコンテンツ作成
// 2005/09/15
// 7 貸し倒れ　8　雑収入に対応
//
procedure TfrmPUrikakeDaichou2.MakeSG2(myQuery : TQuery; iFlag:integer);
var
	iKingaku, iSum, iRuikei : Integer;
  wYyyy, wMm, wDd : Word;
  sMemo, sYyyyMmDdTo, sYyyyMmDdFrom, sDate, sGatsbun:String;
  iKaisuuHouhou : Integer;
begin
	//締め日の比較
  DecodeDate(StrToDate(myQuery.FieldByName('DDate').AsString), wYyyy, wMm, wDd);

  //売上金額の取得
  iKingaku := myQuery.FieldByName('UriageKingaku').AsInteger;

  //2005.10.11
  //回収方法の取得
  // 5 = 返金
  iKaisuuHouhou := myQuery.FieldByName('KaishuuHouhou').AsInteger;

	with SGUDaichou do begin
  	sDate := myQuery.FieldByName('DDate').AsString;
  	sMemo := myQuery.FieldByName('Memo').AsString;
    sMemo := Trim(sMemo);

  	Cells[CintMemo, GiRow] := sMemo;
  	Cells[CintDate, GiRow] := Copy(sDate, 3, 10);

    GiKungakuSum := GiKungakuSum + iKingaku;

    //1999/09/14追加
    sGatsbun := myQuery.FieldByName('Gatsubun').AsString;
	 	Cells[CintGatsubun, GiRow] := Copy(sGatsbun, 1, 7);

    //通常の売上伝票
    if (iKingaku > 0) AND (iFlag = 0) then begin
      //2005.10.11
      if iKaisuuHouhou > 3 then begin //返金伝票など
        Cells[CintUkeKingaku, GiRow] := FormatFloat('0,', iKingaku);//３桁ごとに区切って表示
        Cells[CintZandakan, GiRow] := FormatFloat('0,', GiKungakuSum);//３桁ごとに区切って表示
      end else begin //通常の伝票
	  	  Cells[CintUriKingaku, GiRow] := FormatFloat('0,', iKingaku);//３桁ごとに区切って表示
      end;
      //GiKungakuSum := GiKungakuSum + iKingaku;

      //1999/06/01　復活
      if ((Cells[CintUkeKingaku, GiRow] <> '') or
         (Cells[CintMemo, GiRow] = CsCTAX)) and
         (iKaisuuHouhou < 4) then begin
		  	Cells[CintTougetsu, GiRow] := FormatFloat('0,', GiKungakuSum);//３桁ごとに区切って表示
      end;

    end else if (iKingaku > 0) AND (iFlag = 1) then begin
	    if (Cells[CintMemo, GiRow] <> CsCTAX) then begin
      	if Copy(Cells[CintMemo, GiRow], 1, 4) = '返金' then begin //2000/03/03　Added
		 			Cells[CintUkeKingaku, GiRow] := FormatFloat('0,', iKingaku);
	  			Cells[CintTougetsu, GiRow] := FormatFloat('0,', GiKungakuSum);

        //2005.10.11
        end else if iKaisuuHouhou > 3 then begin //返金伝票など
          Cells[CintUkeKingaku, GiRow] := FormatFloat('0,', iKingaku);
          Cells[CintZandakan, GiRow] := FormatFloat('0,', GiKungakuSum);//３桁ごとに区切って表示

  	    end else begin
		    	GiRow := GiRow - 1;
  	      Exit;
        end;
      end;
    end else if (iKingaku <= 0)then begin
      //返品・値引の場合
      if (Copy(Cells[CintMemo, GiRow], 1, 4) = '返品') or
         (Copy(Cells[CintMemo, GiRow], 1, 4) = '値引') or
         (Copy(Cells[CintMemo, GiRow], 1, 4) = '取消') then begin    // 2015.03 add 

        Cells[CintUriKingaku, GiRow] := FormatFloat('0,', iKingaku);
	      //GiKungakuSum := GiKungakuSum + iKingaku;
      //2005.10.11
      end else if iKaisuuHouhou > 3 then begin //返金伝票など
        Cells[CintUriKingaku, GiRow] := FormatFloat('0,', iKingaku);
      end else begin
      	//2001/03/15
        if sMemo = '##消費税##' then begin
		  		Cells[CintUkeKingaku, GiRow] := '';
          GiKungakuSum := GiKungakuSum - iKingaku;
	  			Cells[CintTougetsu, GiRow] := FormatFloat('0,', GiKungakuSum);
        end else begin
		      //受け入れ金額の場合
	  			Cells[CintUkeKingaku, GiRow] := FormatFloat('0,', iKingaku);
		      //1999/06/01　復活
  		    //GiKungakuSum := GiKungakuSum + iKingaku;
	  			//Cells[CintTougetsu, GiRow] := FormatFloat('0,', GiKungakuSum);

          //2001/07/19
     	  	Cells[CintZandakan, GiRow] := FormatFloat('0,', GiKungakuSum);//３桁ごとに区切って表示

        end;
      end;//of if
    end;

    //消費税の行だったら当月の税抜き合計の残を計算して表示する
    //1999/06/01　累計を表示する
    if Cells[CintMemo, GiRow] = CsCTAX then begin
	    sYyyyMmDdTo := Cells[CintDate, GiRow];
      DecodeDate(StrToDate(sYyyyMmDdTo), wyyyy, Wmm, wDd);

      if wDd >= 28 then begin //月末ならば
      	wDd := 1;
      end else begin
	      wDd := wDd + 1;
	      wMm := wMm - 1;
  	    if wMm = 0 then begin
    	  	wMm := 12;
      	  wYyyy := wYyyy-1;
	      end;
      end;

      sYyyyMmDdFrom := DateToStr(EncodeDate(wYyyy, wMm, wDd));
	    iRuikei := TKaishuu.GetZandaka(EditTokuisakiCode.Text, sYyyyMmDdFrom, sYyyyMmDdTo);

      //2001/10/12 added
      if (iKingaku<0) then begin
  	  	Cells[CintUriKingaku, GiRow] := FormatFloat('0,', iRuikei);//３桁ごとに区切って表示
        GiKungakuSum := GiKungakuSum + iKingaku;
        iRuikei := iRuikei + iKingaku
      end else begin
  	  	Cells[CintUriKingaku, GiRow] := FormatFloat('0,', iRuikei-iKingaku);//３桁ごとに区切って表示
      end;
	  	Cells[CintSasihiki, GiRow] := FormatFloat('0,', iRuikei);
	  	Cells[CintTougetsu, GiRow] := FormatFloat('0,', GiKungakuSum);//３桁ごとに区切って表示

	  	Cells[CintUriKingaku2, GiRow] := FormatFloat('0,', iRuikei);//３桁ごとに区切って表示
	  	Cells[CintCTax, GiRow] := FormatFloat('0,', iKingaku);//３桁ごとに区切って表示

      //残高表示　2001/07/26追加
	  	//Cells[CintZandakan, GiRow] := FormatFloat('0,', iRuikei);//３桁ごとに区切って表示
	  	Cells[CintZandakan, GiRow] := FormatFloat('0,', GiKungakuSum);//３桁ごとに区切って表示
    end;
  end;

end;

procedure TfrmPUrikakeDaichou2.BitBtn6Click(Sender: TObject);
begin
	PrintSG(1);
end;

//右寄せなど
procedure TfrmPUrikakeDaichou2.SGUDaichouDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);

type
  TVowels = set of char;
var
	DRect:  TRect;  Mode: Integer;
  Vowels: TVowels;
	C : LongInt;
  myRect : TRect;
  iCol : Integer;
begin
  if not (gdFixed in state)  then begin

    Vowels := [char(CintUriKingaku), char(CintUkeKingaku), char(CintSasihiki), char(CintTougetsu)];
		SGUDaichou.Canvas.FillRect(Rect);
	  DRect.Top := Rect.Top + 2;
  	DRect.Left := Rect.Left + 2;
	  DRect.Right := Rect.Right - 2;
  	DRect.Bottom := Rect.Bottom - 2;

	  if  char(ACol) IN Vowels then begin       //右寄せ
  		Mode := DT_RIGHT;
	  end else begin
    	Mode := DT_LEFT;
    end;

  	//else if Col = 2 then   //左寄せ
    //	Mode := DT_LEFT
	  //else    Mode := DT_CENTER;   //中央


{
  	DrawText(SGUDaichou.Canvas.Handle, PChar(SGUDaichou.Cells[Col,Row]),
	           Length(SGUDaichou.Cells[Col,Row]), DRect, Mode);
}

    //消費税の行は色をつける
  	if SGUDaichou.Cells[1, ARow] = CsCTAX then begin
			IdentToColor('clBlue', C);
  		SGUDaichou.Canvas.Brush.Color := C;
    	SGUDaichou.Canvas.Font.Color := $FFFFFF - C;

		  for iCol := 0 to CintEnd - 1 do begin
    		myRect := SGUDaichou.CellRect(iCol, ARow);
	      SGUDaichou.Canvas.FillRect(myRect);
		    DrawText(SGUDaichou.Canvas.Handle,
  	    					PChar(SGUDaichou.Cells[iCol, ARow]),
	  		          Length(SGUDaichou.Cells[iCol, ARow]),
        		      myRect,
            	    DT_CENTER or DT_VCENTER or DT_SINGLELINE);
      end;//of for
      Exit;
    end;
    //回収の行も色をつける
  	if SGUDaichou.Cells[CintUkeKingaku, ARow] <> '' then begin
			IdentToColor('clTeal', C);
  		SGUDaichou.Canvas.Brush.Color := C;
    	SGUDaichou.Canvas.Font.Color := $FFFFFF - C;

		  for iCol := 0 to CintEnd - 1 do begin
    		myRect := SGUDaichou.CellRect(iCol, ARow);
	      SGUDaichou.Canvas.FillRect(myRect);
		    DrawText(SGUDaichou.Canvas.Handle,
  	    					PChar(SGUDaichou.Cells[iCol, ARow]),
	  		          Length(SGUDaichou.Cells[iCol, ARow]),
        		      myRect,
            	    DT_CENTER or DT_VCENTER or DT_SINGLELINE);
      end;//of for
      Exit;
    end;
  	DrawText(SGUDaichou.Canvas.Handle, PChar(SGUDaichou.Cells[ACol,ARow]),
	           Length(SGUDaichou.Cells[ACol,ARow]), DRect, Mode);

  end;//of if
end;

procedure TfrmPUrikakeDaichou2.BitBtn3Click(Sender: TObject);
var
  sLine, sCTax,sDate, sMemo, sUriKingaku2, sZan,sUriKingaku, sUkeKingaku, sSasihiki, sRuikei, sGatsubun:String;
  iRow : Integer;
 	F : TextFile;
begin
	if CBTokuisakiName.Text = '' then begin
  	ShowMessage('得意先名が特定されていません');
    exit;
  end;
	//確認メッセージ
  if MessageDlg('印刷しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;

	//スタートトランザクション

	//出力するファイルを作成する、すでにあれば削除する
  //請求明細用
 	AssignFile(F, CFileName_UriDaichou);
  Rewrite(F);
	CloseFile(F);

  //基本情報の取得
  Screen.Cursor := crHourGlass;

  sLine := EditTokuisakiCode.Text  + ',' + CBTokuisakiName.Text;
  sLine := sLine + ',(' + EditShiharaijouken.Text + ')';
  HMakeFile(CFileName_UriDaichou, sLine);
  sLine := '日付,適用,売上(税抜き),消費税,売上（税込）,当月請求,受入金額,残,月分';
  HMakeFile(CFileName_UriDaichou, sLine);
  with SGUDaichou do begin
  	for iRow := 1 to RowCount do begin
		  sDate        := Cells[CintDate, iRow];
      
      // 日付不正表示対応 ADD 2006.12.06 matsukaze
      if (sDate <> '') then begin
        sDate        := '20' + sDate;
      end;

		  sMemo        := Cells[CintMemo, iRow];
      if (sDate = '') AND (sMemo = '') then begin
      	Break;
      end;

      sCTax        := Cells[CintCTax, iRow];//消費税
		  sUriKingaku  := Cells[CintUriKingaku, iRow];//売上金額（税抜き）
		  sUriKingaku2  := Cells[CintUriKingaku2, iRow];//売上金額（税込み）
		  sUkeKingaku  := Cells[CintUkeKingaku, iRow];//受け入れ金額
		  sSasihiki    := Cells[CintSasihiki, iRow];

		  sRuikei      := Cells[CintTougetsu, iRow]; //当月請求
		  sGatsubun    := Cells[CintGatsubun, iRow];

      sZan         := Cells[CintZandakan, iRow];

		  sLine := '"' + sDate + '","' + sMemo + '","' + sUriKingaku + '","';
      sLine := sLine + sCTax + '","' + sUriKingaku2 + '","' +
               sRuikei + '","' +  sUkeKingaku + '","' + sZan + '","' +
               sGatsubun + '"';
 		  HMakeFile(CFileName_UriDaichou, sLine);
    end;//of for
  end;//of with
  Screen.Cursor := crDefault;

	//エンドトランザクション
  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '売掛台帳.xls', '', SW_SHOW);
end;

procedure TfrmPUrikakeDaichou2.BitBtn7Click(Sender: TObject);
begin
	//確認メッセージ
  if MessageDlg('売掛回収画面を表示しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;
  if EditTokuisakiCode.Text = '' then begin
  	exit;
  end;

	GTokuisakiCode := EditTokuisakiCode.Text;
  GTokuisakiName := CBTokuisakiName.Text;

  TfrmTKaishuu.Create(Self);

end;

procedure TfrmPUrikakeDaichou2.CBTokuisakiNameKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
  	MakeCBTokuisakiName('TokuisakiName', CBTokuisakiName.Text, 1);
  end;
end;


{
	得意先名のコンボボックスを作成する
  引数：sKey   -> 検索列名
  			sWord  -> 検索ワード
  			iKubun -> 0=前方一致, 1=曖昧検索, 2=数字の完全一致
}
procedure TfrmPUrikakeDaichou2.MakeCBTokuisakiName(sKey, sWord:String; iKubun:Integer);
var
	sSql : String;
begin

  CBTokuisakiName.Items.Clear;


	sSql := 'SELECT * FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + sKey;
  case iKubun of
  0: sSql := sSql + ' LIKE ''' + sWord + '%''';
  1: begin
  	sSql := sSql + ' LIKE ''%' + sWord + '%''';
		sSql := sSql + ' OR ';
		sSql := sSql + 'TokuisakiNameYomi Like ''%' + sWord + '%''';
 end;
  2: sSql := sSql + ' = ' + sWord;
  end;

  sSql := sSql + ' ORDER BY TokuisakiCode1 ';
  CBTokuisakiName.Clear;
	with frmDMMaster.QueryMTokuisaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	CBTokuisakiName.Items.Add(FieldByName('TokuisakiCode1').AsString + ',' +
       FieldByName('TokuisakiNameUp').AsString + ' ' +
       FieldByName('TokuisakiName').AsString);
    	Next;
    end;//of while
    Close;
  end;//of with

  CBTokuisakiName.DroppedDown := True;
end;


procedure TfrmPUrikakeDaichou2.CBTokuisakiNameExit(Sender: TObject);
var
	sTokuisakiCode : String;
begin
	//得意先コードの取得
  if Pos(',',CBTokuisakiName.Text)>1 then begin
  	sTokuisakiCode := Copy(CBTokuisakiName.Text, 1, Pos(',',CBTokuisakiName.Text)-1);
  	EditTokuisakiCode.Text := sTokuisakiCode;
  	EditTokuisakiCode.SetFocus;
  	CBTokuisakiName.SetFocus;
  end;


end;

procedure TfrmPUrikakeDaichou2.BitBtn4Click(Sender: TObject);
var
	sMm, syyyy, sDd : String;
begin
  if CBTokuisakiName.Text = '' then begin
    Showmessage('得意先名が入力されていません');
    exit
  end;
	sMm := InputBox('月の取得', '消費税を計算する年月度を半角数字で入力してくださいEx.200506', '');
  if sMm = '' then begin
    exit
  end;
  if Length(sMm) <> 6 then begin
    Showmessage('年月日の入力が不正です');
    exit
  end;

  sYyyy := Copy(sMm, 1, 4);
  sMm := Copy(sMm, 5, 2);
  sDd := MTokuisaki.GetShimebi(EditTokuisakiCode.Text);
  if StrToInt(sYyyy)<1998 then begin
    showmessage('1998年以前の計算はできません');
    exit;
  end;
  if (StrToInt(sMm)<1) or (StrToInt(sMm)>12) then begin
    showmessage('月が不正です');
    exit;
  end;

	PBill.InsertCTax(sYyyy, sMm, sDd, StrToInt(EditTokuisakiCode.Text), SB1);

  //再表示
  PrintSG(0);

end;

end.
