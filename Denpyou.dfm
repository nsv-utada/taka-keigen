inherited frmDenpyou: TfrmDenpyou
  Left = 884
  Top = 157
  Width = 1036
  Height = 678
  Caption = 'frmDenpyou'
  ParentBiDiMode = False
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 12
  inherited Panel1: TPanel
    Width = 1028
    Height = 49
    inherited Label1: TLabel
      Top = 4
      Width = 68
      Height = 16
      Caption = #20253#31080#20316#25104
      Font.Height = -16
    end
    inherited Label2: TLabel
      Left = 28
      Top = 28
    end
    object Label7: TLabel
      Left = 316
      Top = 4
      Width = 56
      Height = 13
      Caption = #20253#31080#30058#21495
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cmdKennsaku: TButton
      Left = 564
      Top = 24
      Width = 49
      Height = 15
      Caption = #26908#32034
      TabOrder = 0
      OnClick = cmdKennsakuClick
    end
    object cmdDenpyouBanngouListMake: TButton
      Left = 434
      Top = 22
      Width = 121
      Height = 17
      Caption = #20253#31080#30058#21495#12522#12473#12488#20316#25104
      TabOrder = 1
      OnClick = cmdDenpyouBanngouListMakeClick
    end
    object cmbDenpyouBanngou: TComboBox
      Left = 312
      Top = 20
      Width = 113
      Height = 20
      ItemHeight = 12
      TabOrder = 2
    end
  end
  inherited Panel2: TPanel
    Top = 566
    Width = 1028
    Height = 66
    object lSum: TLabel [0]
      Left = 48
      Top = 16
      Width = 56
      Height = 13
      Caption = #21512#35336#37329#38989
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label15: TLabel [1]
      Left = 10
      Top = 14
      Width = 36
      Height = 13
      Caption = #20633#32771#65306
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clMaroon
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbiInputNo: TLabel [2]
      Left = 666
      Top = 50
      Width = 4
      Height = 12
    end
    inherited BitBtn1: TBitBtn
      Left = 581
      ModalResult = 1
      Kind = bkCustom
    end
    inherited BitBtn2: TBitBtn
      Left = 358
      Width = 108
      Caption = #12503#12524#12499#12517#12540
      ModalResult = 5
      OnClick = BitBtn2Click
      Kind = bkCustom
    end
    object BitBtn7: TBitBtn
      Left = 470
      Top = 8
      Width = 108
      Height = 25
      Caption = #12463#12522#12450
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = BitBtn7Click
      Kind = bkRetry
    end
    object edbBikou1: TEdit
      Left = 48
      Top = 10
      Width = 303
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imOpen
      MaxLength = 100
      ParentFont = False
      TabOrder = 3
    end
    object BitBtnExcel: TBitBtn
      Left = 358
      Top = 34
      Width = 217
      Height = 25
      Caption = #12456#12463#12475#12523#12408#12467#12531#12496#12540#12488
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = BitBtnExcelClick
    end
  end
  inherited Panel3: TPanel
    Top = 49
    Width = 1028
    Height = 112
    Align = alTop
    object Label3: TLabel
      Left = 16
      Top = 8
      Width = 48
      Height = 15
      Caption = #32013#21697#26085
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 210
      Top = 8
      Width = 70
      Height = 13
      Caption = #24471#24847#20808#26908#32034
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 320
      Top = 40
      Width = 216
      Height = 12
      Caption = 'F1'#12461#12540#12391#24471#24847#20808#21517#12424#12415#12398#12354#12356#12414#12356#26908#32034
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label6: TLabel
      Left = 16
      Top = 36
      Width = 48
      Height = 15
      Caption = #20837#21147#32773
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 284
      Top = 35
      Width = 32
      Height = 15
      Caption = #26376#20998
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 32
      Top = 68
      Width = 26
      Height = 13
      Caption = #12424#12415
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 72
      Top = 92
      Width = 285
      Height = 13
      Caption = #21830#21697#26908#32034#12399'"'#12424#12415'"'#12398#19968#37096#12434#20837#21147#12375#12390'Go'#12508#12479#12531#12434#25276#19979
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ParentFont = False
    end
    object Label12: TLabel
      Left = 530
      Top = 63
      Width = 78
      Height = 12
      Caption = #36820#21697#20253#31080#21306#20998
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label16: TLabel
      Left = 538
      Top = 8
      Width = 71
      Height = 13
      Caption = #12456#12522#12450#12467#12540#12489
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbAriaCode: TLabel
      Left = 616
      Top = 9
      Width = 56
      Height = 12
      Caption = 'lbAriaCode'
    end
    object Label17: TLabel
      Left = 538
      Top = 24
      Width = 70
      Height = 13
      Caption = 'Info'#20253#31080'No'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label18: TLabel
      Left = 620
      Top = 25
      Width = 4
      Height = 12
    end
    object Label19: TLabel
      Left = 538
      Top = 40
      Width = 66
      Height = 13
      Caption = 'Info'#21462#24341'ID'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label20: TLabel
      Left = 620
      Top = 41
      Width = 4
      Height = 12
    end
    object oldDenpyouBangou: TLabel
      Left = 772
      Top = 40
      Width = 4
      Height = 12
    end
    object RadioGroup1: TRadioGroup
      Left = 376
      Top = 64
      Width = 145
      Height = 41
      Caption = #12424#12415#20351#29992'/'#19981#20351#29992
      Columns = 2
      Items.Strings = (
        #20351#29992
        #19981#20351#29992)
      TabOrder = 9
      OnClick = RadioGroup1Click
    end
    object DTP1: TDateTimePicker
      Left = 70
      Top = 6
      Width = 117
      Height = 23
      Date = 37111.466265312500000000
      Time = 37111.466265312500000000
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      TabStop = False
      OnExit = DTP1Exit
    end
    object BitBtn3: TBitBtn
      Left = 606
      Top = 92
      Width = 83
      Height = 20
      Caption = #31777#26131#26908#32034
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      Visible = False
      OnClick = BitBtn3Click
    end
    object CBTokuisakiName: TComboBox
      Left = 288
      Top = 4
      Width = 233
      Height = 20
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ImeMode = imOpen
      ItemHeight = 12
      ParentFont = False
      TabOrder = 1
      OnExit = CBTokuisakiNameExit
      OnKeyDown = CBTokuisakiNameKeyDown
    end
    object CBNyuuryokusha: TComboBox
      Left = 70
      Top = 34
      Width = 131
      Height = 20
      ImeMode = imClose
      ItemHeight = 12
      TabOrder = 0
      OnExit = CBNyuuryokushaExit
      Items.Strings = (
        '001  '#39640#27211'(K)'
        '002  '#39640#27211'(T)'
        '003  '#26032#20117
        '004  '#37428#26408
        '005  '#22810#36032#35895
        '006  '#23567#29577)
    end
    object BitBtn4: TBitBtn
      Left = 186
      Top = 60
      Width = 125
      Height = 25
      Caption = #12472#12515#12531#12523#12391#20006#12409#26367#12360
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      Visible = False
    end
    object BitBtn5: TBitBtn
      Left = 142
      Top = 60
      Width = 125
      Height = 25
      Caption = #38971#24230#12391#20006#12409#26367#12360
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      Visible = False
    end
    object BitBtn6: TBitBtn
      Left = 266
      Top = 60
      Width = 125
      Height = 25
      Caption = #12424#12415#12391#20006#12409#26367#12360
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      Visible = False
    end
    object edbYomi: TEdit
      Left = 72
      Top = 64
      Width = 121
      Height = 20
      ImeMode = imOpen
      TabOrder = 7
      OnChange = edbYomiChange
    end
    object cmdGo: TButton
      Left = 204
      Top = 66
      Width = 41
      Height = 17
      Caption = 'Go'
      Default = True
      TabOrder = 8
      OnClick = cmdGoClick
    end
    object chbTeisei: TCheckBox
      Left = 760
      Top = 8
      Width = 65
      Height = 17
      Caption = #36820#21697#20253#31080
      TabOrder = 10
      Visible = False
    end
    object EditGatsubun: TEdit
      Left = 224
      Top = 34
      Width = 49
      Height = 20
      ImeMode = imClose
      TabOrder = 11
    end
    object GB1: TGroupBox
      Left = 528
      Top = 75
      Width = 65
      Height = 29
      TabOrder = 12
      object rbHenpin: TRadioButton
        Left = 8
        Top = 8
        Width = 49
        Height = 17
        Caption = #36820#21697
        Font.Charset = SHIFTJIS_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object rbTorikeshi: TRadioButton
        Left = 76
        Top = 8
        Width = 53
        Height = 17
        Caption = #21462#28040
        Font.Charset = SHIFTJIS_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        Visible = False
      end
    end
  end
  inherited SB1: TStatusBar
    Top = 632
    Width = 1028
  end
  object SG1: TStringGrid
    Left = 48
    Top = 194
    Width = 689
    Height = 263
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
    TabOrder = 4
    OnDrawCell = SG1DrawCell
    OnKeyDown = SG1KeyDown
    RowHeights = (
      24
      24
      24
      24
      24)
  end
  object Panel5: TPanel
    Left = 0
    Top = 161
    Width = 1028
    Height = 50
    Align = alTop
    TabOrder = 5
    object Label9: TLabel
      Left = 30
      Top = 8
      Width = 69
      Height = 12
      Caption = #12467#12540#12473#12467#12540#12489
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clPurple
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbTokuisakiCode2: TLabel
      Left = 116
      Top = 8
      Width = 4
      Height = 12
    end
    object Label13: TLabel
      Left = 30
      Top = 24
      Width = 79
      Height = 12
      Caption = #12481#12455#12540#12531#12467#12540#12489
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clPurple
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbChainCode: TLabel
      Left = 128
      Top = 24
      Width = 4
      Height = 12
    end
    object Label14: TLabel
      Left = 408
      Top = 8
      Width = 104
      Height = 12
      Caption = #27880#24847#65281#20253#31080#20316#25104#26178
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clPurple
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label23Status: TLabel
      Left = 608
      Top = 8
      Width = 5
      Height = 13
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object MemoBikou: TMemo
      Left = 514
      Top = 6
      Width = 267
      Height = 37
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      Lines.Strings = (
        'MemoEstimateMemo')
      ParentFont = False
      TabOrder = 0
    end
    object btnShowAll: TButton
      Left = 164
      Top = 6
      Width = 117
      Height = 43
      Caption = #38971#24230#65297#20197#19978#12434#34920#31034
      TabOrder = 1
      OnClick = btnShowAllClick
    end
    object Button2: TButton
      Left = 280
      Top = 6
      Width = 117
      Height = 43
      Caption = #12377#12409#12390#12398#21830#21697#12434#34920#31034
      TabOrder = 2
      OnClick = Button2Click
    end
  end
  object QueryDenpyou: TQuery
    DatabaseName = 'taka-access'
    Left = 696
    Top = 49
  end
  object Timer1: TTimer
    Interval = 500
    OnTimer = Timer1Timer
    Left = 176
    Top = 18
  end
  object Query2: TQuery
    DatabaseName = 'taka-access'
    Left = 688
    Top = 97
  end
end
